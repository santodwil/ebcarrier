<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Description of Controller Result Type
 *
 * @author SUSANTO DWI LAKSONO
 */

class Widget extends Widgets {

	public function __construct() {
		parent::__construct();
		
		if (!$this->ion_auth->logged_in()) {
			die;
		}
		$this->load->model('result_type'); 
	}

	public function index() {
		$get = $this->input->get();
		$data = array(
			'title' => 'Result Type',
		);
		$this->render_widget($data);
	}

	public function get_tests(){
		$params = $this->input->get();
		$response['result'] = $this->result_type->get_tests('get', $params['search'], $params['offset']);
		$response['total'] = $this->result_type->get_tests('count', $params['search'], $params['offset']);
		$response['success'] = TRUE; 
		$this->json_result($response);
	}

	public function calculation_result(){
		$params = $this->input->get();
		$this->load->library('test_result'); 
		$response['result'] = $this->test_result->calculation_result($params['test_type_id'], $params['sdate'], $params['edate']);
		$this->json_result($response);
	}
	public function show_result(){
		$get = $this->input->get();
		$response['question_type'] = $this->db->select('b.name, count(a.question_type_id) as total')
												->join('question_type as b', 'a.question_type_id = b.id')
												->where('a.test_type_id', $get['test_type_id'])
												->where_not_in('a.question_type_id ', array(7,8))
												->group_by('a.question_type_id')
												->get('question as a')->result_array();
		$this->json_result($response);
	}
}