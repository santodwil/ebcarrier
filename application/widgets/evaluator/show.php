<style type="text/css">
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
</style>
<link href="<?php echo base_url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.css'); ?>" rel="stylesheet" type="text/css" />

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>	

	<div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0;">
    		<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Search</i></span>
							<input type="text" id="search" class="form-control">
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-group">
							<span class="input-group-addon">Tests</span>
							<select class="form-control" id="test_type">
								<option value="all">All</option>
								<?php
								$test_type = $this->db->select('id, name')->where('status', 1)->get('test_type');
								if($test_type->num_rows() > 0){
									foreach ($test_type->result_array() as $key => $value) {
										echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3 pull-right">
                        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#modal-new-evaluator"><i class="fa fa-plus"></i> New Evaluator</button>
					</div>
				</div>
    		</div>
          	<div class="panel-body" style="margin-top:-20px;"">
				<div class="row">
					<table class="table table-condensed table-striped">
						<thead>
							<th>Vacancy</th>
							<th class="text-center">Test Name</th>
							<th class="text-center">Action</th>
						</thead>
						<tbody id="section-data"></tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
          	</div>
      	</div>
	</div>

	<div class="modal fade slide-up disable-scroll in" id="modal-edit-evaluator">
    	<div class="modal-dialog">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:600px;max-height: 600px;overflow: auto;">
        			<form role="form" id="form-modify">
        				<input type="hidden" name="id">
			          	<div class="modal-body modal-body-edit-evaluator" style="margin-top: 15px;">
			          		<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Test</label>
										<select class="form-control" name="test_type" id="modify-evaluator-test" required="" disabled="">
											<?php
											$test_type = $this->db->select('id, name')->where('status', 1)->get('test_type');
											if($test_type->num_rows() > 0){
												foreach ($test_type->result_array() as $key => $value) {
													echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
												}
											}
											?>
										</select>
									</div>
									<div class="form-group">
										<label><i class="fa fa-plus"></i> Evaluator</label>
										<select class="form-control" name="choose_evaluator" id="choose_evaluator">
											<?php
												$evaluator = $this->db->select('a.first_name,a.id')->join('users_groups as b', 'a.id = b.user_id', 'left')->where('b.group_id', 3)->get('users as a');
												if($evaluator->num_rows() > 0){
													foreach ($evaluator->result_array() as $k => $v) {
														echo '<option value="'.$v['id'].'">'.$v['first_name'].'</option>';
													}
												}
											?>
										</select>
									</div>
									<hr>
									<h5 class="bold text-center">List Evaluator</h5>
									<div id="evaluator_list"></div>
									<div id="action_evaluator_list"></div>
								</div>
			          		</div>
			          	</div>
			          	<div class="modal-footer">
			          		<div class="btn-group">
								<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
								<button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Change</button>
							</div>
			          	</div>
		          	</form>
	          	</div>
          	</div>
      	</div>
  	</div>

  	<div class="modal fade slide-up disable-scroll in" id="modal-new-evaluator">
    	<div class="modal-dialog">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="max-height: 600px;overflow: auto;">
        			<form role="form" id="form-create">
			          	<div class="modal-body modal-body-edit-evaluator" style="margin-top: 15px;">
			          		<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Test</label>
										<select class="form-control" name="test_type" id="new-evaluator-test" required="">
											<?php
											$test_type = $this->db->select('id, name')->where('status', 1)->get('test_type');
											if($test_type->num_rows() > 0){
												foreach ($test_type->result_array() as $key => $value) {
													echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Evaluator</label>
										<select multiple class="form-control" name="choose_evaluator[]">
											<?php
												$evaluator = $this->db->select('a.first_name,a.id')->join('users_groups as b', 'a.id = b.user_id', 'left')->where('b.group_id', 3)->get('users as a');
												if($evaluator->num_rows() > 0){
													foreach ($evaluator->result_array() as $k => $v) {
														echo '<option value="'.$v['id'].'">'.$v['first_name'].'</option>';
													}
												}
											?>
										</select>
									</div>
								</div>
			          		</div>
			          	</div>
			          	<div class="modal-footer">
			          		<div class="btn-group">
								<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
								<button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
							</div>
			          	</div>
		          	</form>
	          	</div>
          	</div>
      	</div>
  	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') ?>"></script>