<style type="text/css">
	.bar-question{
        font-size: 15px;
        color:black;
    }
    .bar-question p{
        font-size: 15px;
        color:black;
    }
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">        	
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
    <div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
    		<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
    			<div class="row">
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Search</span>
							<input type="text" id="search" class="form-control">
						</div>
					</div>
					<div class="col-md-3 pull-right">
                        <button class="btn btn-default btn-new-data pull-right"><i class="fa fa-plus"></i> New Question</button>
					</div>
				</div>
    		</div>

          	<div class="panel-body" style="margin-top:-20px;"">
				<div class="row">
					<table class="table table-condensed table-striped">
						<thead>
							<th width="400">Name</th>
							<th width="150" class="text-center">Question</th>
							<th width="150" class="text-center">Time</th>
							<th class="text-center">Action</th>
						</thead>
						<tbody id="section-data"></tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default bold">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
          	</div>
      	</div>
	</div>

	<div class="modal fade slide-up in" id="modal-detail">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:1200px;">
        			<div class="modal-header clearfix text-left" style="padding:0px; padding-left:10px;padding-right:10px;">
	                    <h5 class="title-test bold text-center"></h5>
	                    <div class="radio radio-danger text-center radio-option">
                    	</div>
                  	</div>
                  	<input type="hidden" id="test_id">
		          	<div class="modal-body" style="border-top:2px solid #f7f7f7;">
		          	</div>
	          	</div>
          	</div>
      	</div>
  	</div>

  	<div class="modal fade slide-up disable-scroll in" id="modal-edit-question">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:1200px;max-height: 600px;overflow: auto;">
		          	<div class="modal-body modal-body-edit-question" style="margin-top: 15px;"></div>
		          	<div class="modal-footer">
		          		<div class="btn-group">
							<button class="btn btn-default btn-back-edit-question"><i class="fa fa-arrow-left"></i> Back</button>
							<button class="btn btn-danger btn-change"><i class="fa fa-edit"></i> Change</button>
						</div>
		          	</div>
	          	</div>
          	</div>
      	</div>
  	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>

<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.css') ?>" />
<script src="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.js') ?>"></script>
