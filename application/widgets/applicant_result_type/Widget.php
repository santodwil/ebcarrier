<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Controller Applicant result type
 *
 * @author SUSANTO DWI LAKSONO
 */

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        if (!$this->ion_auth->logged_in()) {
            die;
        }
        $this->load->model('result_type');
        $this->load->library('test_result');  
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Result Type',
            'id' => $get['id']
        );
        $this->render_widget($data);
    }

    public function get_applicant_result(){
        $params = $this->input->get();
        $response['success'] = TRUE;
        $this->json_result($response);
    }

}