<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('validation_form'); 
        $this->load->model('tests'); 
        $this->load->model('applicant'); 
    }

    public function index() {
        $data = array(
            'title' => 'Dashboard',
            'user' => $this->_user,
            'applicant' => $this->db->select('a.*, b.name as vacancy_division_name')
                                    ->join('vacancy_division as b', 'a.vacancy_division_id = b.id')
                                    ->where('a.user_id', $this->_user->id)
                                    ->get('applicant as a')->row_array()    
        );
        $this->render_widget($data);
    }

    public function registration_test(){
        $p = $this->input->post();
        $rules_vacancy = $this->tests->rules_vacancy($p['id'], $this->_user->id);
        $rules_transaction = $this->tests->rules_transaction($p['id'], $this->_user->id);
        if($rules_vacancy && $rules_transaction){
            $register_tests = $this->tests->registration_test($p['id'], $this->_user->id);
            $response['success'] = $register_tests ? TRUE : FALSE;
            $response['msg'] = $register_tests ? '' : 'Transaction Failed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'You dont have permission';
        }
        $this->json_result($response);
    }



    public function upload_cv(){        
        $applicant = $this->db->select('a.id, a.name, b.name as vacancy_name')->join('vacancy_division as b', 'a.vacancy_division_id = b.id')->where('a.user_id', $this->_user->id)->get('applicant as a')->row_array();
        if($_FILES) {
            $config['upload_path'] = './files/cv/';
            $config['allowed_types'] = 'doc|docx|pdf';
            $config['file_name'] = uniqid(date("his").$applicant['id']);

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('cv_file')) {
                $response['success'] = FALSE;
                $response['msg'] = $this->upload->display_errors();                    
            }
            else {
                $data = $this->upload->data();
                if($this->input->post('file_before') != '') {
                    unlink('./files/cv/'.$this->input->post('file_before'));
                    $this->applicant->update_cv($applicant['id'], $data['file_name'], $_FILES['cv_file']['name']);
                }else{
                    $this->applicant->upload_cv($applicant['id'], $data['file_name'], $_FILES['cv_file']['name']);
                }
                $response['success'] = TRUE;
                $response['msg'] = 'CV has been uploaded';
                $response['file_name'] = $data['file_name'];
                $response['temp_name'] = $_FILES['cv_file']['name'];
            }

        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'File CV Required';
        }
        $this->json_result($response);
    }

    public function download($file){
        $this->load->helper('download');
        $temp_name = $this->db->select('applicant_temp_file')->where('applicant_file', $file)->get('applicant_cv')->row_array();
        $data = file_get_contents('./files/cv/'.$file.'');
        force_download($temp_name['applicant_temp_file'], $data);
    }

}