$(function () {
 	'use restrict';

 	var series = [{
 		data : []
    }];

 //    Highcharts.getOptions().plotOptions.pie.colors = (function () {
	//     var colors = [],
	//         base = '#cfdcdb',
	//         i;

	//     for (i = 0; i < 10; i += 1) {
	//         // Start out with a darkened base color (negative brighten), and end
	//         // up with a much brighter color
	//         colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
	//     }
	//     return colors;
	// }());

    Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                    [0, color],
                    [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        })
    });

    var pie_degree = new Highcharts.Chart({
        chart : {
            renderTo : 'pie-degree',
            type : 'pie',
            margin: [0, 0, 0, 0],
            height: 250,            
	        spacingTop: 0,
	        spacingBottom: 0,
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 6
            }
        },
        title : {
            text : ''
        },
        credits: {
            enabled : false
        },legend: {
            enabled : true
        },
       tooltip: {
	        headerFormat: '',
	        pointFormat: '<span style="color:{point.color};font-weight:bold;">{point.name} <br> <b>{point.y} Orang</b> <b> ({point.percentage:.2f} %)</b></span>'
	    },
        plotOptions : {
            pie : {
                size:'90%',
                allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                distance: -20,
	                connectorColor: 'silver'
            	}
            },
            series: {
	            dataLabels: {
	                enabled: true,
	                format: '<span style="font-size:12px;">{point.name}</span>'
	            }
	        }
        },
        series : series
    });

    var pie_vacancy = new Highcharts.Chart({
        chart : {
            renderTo : 'pie-vacancy',
            type : 'pie',
            margin: [0, 0, 0, 0],
            height: 250,            
	        spacingTop: 0,
	        spacingBottom: 0,
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 6
            }
        },
        title : {
            text : ''
        },
        credits: {
            enabled : false
        },legend: {
            enabled : true
        },
       tooltip: {
	        headerFormat: '',
	        pointFormat: '<span style="color:{point.color};font-weight:bold;">{point.name} <br> <b>{point.y} Orang</b> <b> ({point.percentage:.2f} %)</b></span>'
	    },
        plotOptions : {
            pie : {
                size:'90%',
                allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                distance: -20,
	                connectorColor: 'silver'
            	}
            },
            series: {
	            dataLabels: {
	                enabled: true,
	                format: '<span style="font-size:12px;">{point.name}</span>'
	            }
	        }
        },
        series : series
    });

    var pie_gender = new Highcharts.Chart({
        chart : {
            renderTo : 'pie-gender',
            type : 'pie',
            margin: [0, 0, 0, 0],
            height: 250,            
	        spacingTop: 0,
	        spacingBottom: 0,
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 6
            }
        },
        title : {
            text : ''
        },
        credits: {
            enabled : false
        },legend: {
            enabled : true
        },
       tooltip: {
	        headerFormat: '',
	        pointFormat: '<span style="color:{point.color};font-weight:bold;">{point.name} <br> <b>{point.y} Orang</b> <b> ({point.percentage:.2f} %)</b></span>'
	    },
        plotOptions : {
            pie : {
                size:'90%',
                allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                distance: -20,
	                connectorColor: 'silver'
            	}
            },
            series: {
	            dataLabels: {
	                enabled: true,
	                format: '<span style="font-size:12px;">{point.name}</span>'
	            }
	        }
        },
        series : series
    });

    $.fn.summary = function(opt){
        var s = $.extend({
            load : true,
            sdate : false,
            edate : false
        }, opt);

        $.ajax({
            type : 'post',
            url: site_url + '/dashboard_admin/widget/get_summary',
            dataType : 'json',
            data : {
                load : s.load,
                sdate : s.sdate,
                edate : s.edate,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function(){
            	show_loading();
	      	},
            error : function(){
                alert('some error occured');
            },
            success : function(r){
                $('#csrf').val(r.csrf);
            	if(r.degree){
            		$('.no-result').remove();
            		pie_degree.series[0].setData(r.degree);
            	}else{
            		$('#pie-degree').html('<h5 class="text-center no-result font-arial">No Result</h5>');
            	}
            	if(r.gender){
            		$('.no-result').remove();
            		pie_gender.series[0].setData(r.gender);
            	}else{
            		$('#pie-gender').html('<h5 class="text-center no-result font-arial">No Result</h5>');
            	}
            	if(r.vacancy){
            		pie_vacancy.series[0].setData(r.vacancy);
            		$('.no-result').remove();
            	}else{
            		$('#pie-vacancy').html('<h5 class="text-center no-result font-arial">No Result</h5>');
            	}
                // pie_vacancy.series[0].setData(r.vacancy);
                // pie_gender.series[0].setData(r.gender);
                $('.total-applicant').html(r.total_applicant);
                $('.total-active').html(r.total_active);
                $('.total-inactive').html(r.total_inactive);
                if(s.load){
		        	hide_loading();
            	}
            }
        });
    };


    $('#range-date').daterangepicker({
        ranges: {
            'Last 7 Days': [moment().subtract('days', 7), moment()],
            'Last 30 Days': [moment().subtract('days', 30), moment()],
            'Last 3 Month': [moment().subtract('days', 90), moment()]
        },
        format: 'DD-MM-YYYY',
        startDate: moment().subtract('days', 29),
        endDate: moment(),
        isShowing: true
    },
    function (start, end) {
    	$('#range-date').html('<i class="fa fa-calendar"></i> '+start.format('DD MMM YYYY')+' sd '+end.format('DD MMM YYYY')+'');
        $(this).summary({
        	load : true,
            sdate: start.format('YYYY-MM-DD'),
            edate: end.format('YYYY-MM-DD')
        });
        // $('input[name=dstart]').val(start.format('YYYYMMDD'));
        // $('input[name=dend]').val(end.format('YYYYMMDD'));
    }
    );


    $(this).summary();


});