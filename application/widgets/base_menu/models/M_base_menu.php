<?php

class M_base_menu extends CI_Model {
    protected $_user;

    public function __construct() {
        parent::__construct();
        $this->_user = $this->ion_auth->user()->row();
    }

    public function render_menu($user_id){
        $groups_id = $this->_get_groups($user_id);
        if($groups_id){
            $this->db->where_in('group_id', $groups_id);
            $rs = $this->db->get('menu_groups');
            if($rs->num_rows() > 0){
                foreach ($rs->result_array() as $key => $value) {
                    $menu_id[] = $value['menu_id'];
                }
                return $menu_id;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function _get_groups($user_id){
        $this->db->where('user_id', $user_id);
        $rs = $this->db->get('users_groups');
        if($rs->num_rows() > 0){
            foreach ($rs->result_array() as $key => $value) {
                $group_id[] = $value['group_id'];
            }
            return $group_id;
        }else{
            return false;
        }
    }

    public function menu($id = NULL, $parent = 0, $status = 1) {
        $this->db->from('menu');
        if ($id) {
            $this->db->where_in('id', $id);
        }
        $this->db->where('parent', $parent);
        $this->db->where('status', $status);
        $this->db->order_by('sort', 'asc');
        return $this->db->get()->result_array();
    }

    private function _member_id(){
        $this->db->select('id');
        $this->db->where('users_id', $this->_user->id);
        $rs = $this->db->get('members');
        $members = $rs->row_array();
        return $this->_check_in_plan($members['id']);
    }

    private function _check_in_plan($member_id){
        $this->db->where('leader_id', $member_id);
        $rs = $this->db->count_all_results('jobs');
        if($rs > 0){
            return true;
        }else{
            return false;
        }
    }

    public function menu_groups($menu_id = NULL, $group_id = NULL) {
        if($this->_member_id()){
            $this->db->join('menu as b', 'a.menu_id = b.id');
            $this->db->where('b.role', 'leader');
            $this->db->from('menu_groups as a');
            if ($menu_id) {
                $this->db->where_in('a.menu_id', $menu_id);
            }
            if ($group_id) {
                $this->db->where_in('a.group_id', $group_id);
            }
        }else{
            $this->db->from('menu_groups');
            if ($menu_id) {
                $this->db->where_in('menu_id', $menu_id);
            }
            if ($group_id) {
                $this->db->where_in('group_id', $group_id);
            }
        }
        
        return $this->db->get()->result_array();
    }

    public function get_menu($parent = 0, $status = 1, $with_child = true) {
        $menu = $this->menu($parent, $status);
        if ($with_child) {
            foreach ($menu as $k => $v) {
                $menu[$k]['child'] = $this->get_menu($v['id']);
            }
        }
        return $menu;
    }

    public function draw_menu($theme = 'pages', $id = NULL, $parent = 0, $status = 1, $with_child = true, $is_child = false) {
        $menu = $this->menu($id, $parent, $status, $with_child);
        switch ($theme) {
            case 'kode':
                $result = '';
                $result .= $is_child ? '' : '<ul class="sidebar-panel nav"><li class="sidetitle">MAIN</li>';
                foreach ($menu as $v) {
                    $submenu = $with_child ? $this->draw_menu($theme, NULL, $v['id'], $status, $with_child, true) : false;
                    $result .= '<li>';
                    $result .= '<a href="' . site_url($v['url']) . '" data-pjax="content" class="pjax ' . (uri_string() == $v['url'] ? 'active' : '') . '">';
                    $result .= '<span class="icon">';
                    $result .= $v['icon'] ? '<i class="' . $v['icon'] . '"></i>' : '';
                    $result .= '</span>';
                    $result .= $v['name'];
                    $result .= '</a>';
                    $result .= $submenu ? '<ul>' . $submenu . '</ul>' : '';
                    $result .= '</li>';
                }
                $result .= $is_child ? '' : '</ul>';
                break;
            case 'uplon':
                $result = '';
                $result .= $is_child ? '' : '<ul>';
                foreach ($menu as $v) {
                    $result .= $v['header'] ? '<li class="text-muted menu-title">'.$v['header'].'</li>' : '';
                    $submenu = $with_child ? $this->draw_menu($theme, NULL, $v['id'], $status, $with_child, true) : false;
                    $result .= '<li class="has_sub">';
                    $result .= '<a href="' . ($v['url'] != '#' ? site_url($v['url']) : '#') . '" data-pjax="content" class="pjax waves-effect ' . (uri_string() == $v['url'] ? 'active' : '') . '">';
                    $result .= $v['icon'] ? '<i class="' . $v['icon'] . '"></i>' : '';
                    $result .= '<span>' . $v['name'] . '</span>';
                    $result .= $submenu ? '<span class="menu-arrow"></span>' : '';
                    $result .= '</a>';
                    $result .= $submenu ? '<ul>' . $submenu . '</ul>' : '';
                    $result .= '</li>';
                }
                $result .= $is_child ? '' : '</ul>';
                break;
            case 'milestone':
                $result = '';
                $result .= $is_child ? '' : '<ul class="nav">';
                foreach ($menu as $v) {
                    $submenu = $with_child ? $this->draw_menu($theme, NULL, $v['id'], $status, $with_child, true) : false;
                    $result .= '<li>';
                    $result .= '<a href="' . ($v['url'] != '#' ? site_url($v['url']) : '#') . '" data-pjax="content" class="pjax waves-effect ' . (uri_string() == $v['url'] ? 'active' : '') . '">';
                    $result .= $submenu ? '<span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span>' : '';
                    $result .= $v['icon'] ? '<i class="material-icons">' . $v['icon'] . '</i>' : '';
                    $result .= '<span>' . $v['name'] . '</span>';
                    $result .= '</a>';
                    $result .= $submenu ? '<ul class="sub-menu">' . $submenu . '</ul>' : '';
                    $result .= '</li>';
                }
                $result .= $is_child ? '' : '</ul>';
                break;
            case 'taurus':
                $result = '';
                $result .= $is_child ? '' : '<ul class="nav navbar-nav">';
                foreach ($menu as $v) {
                    $submenu = $with_child ? $this->draw_menu($theme, NULL, $v['id'], $status, $with_child, true) : false;
                    $result .= $submenu ? '<li class="dropdown">' : '<li>';
                    $result .= '<a href="' . site_url($v['url']) . '" class="'.($submenu ? 'dropdown-toggle' : '').'' . (uri_string() == $v['url'] ? 'active' : '') . '" '.($submenu ? 'data-toggle="dropdown"' : '').'>';
                    $result .= $submenu ? '<span class="icon-angle-down"></span>' : '';
                    $result .= $v['name'];
                    $result .= '</a>';
                    $result .= $submenu ? '<ul class="dropdown-menu">' . $submenu . '</ul>' : '';
                    $result .= '</li>';
                }
                $result .= $is_child ? '' : '</ul>';
                break;
            case 'pages':
                $no = 0;
                $result = '';
                $result .= $is_child ? '' : '<ul class="menu-items">';
                foreach ($menu as $v) {
                    $no++;
                    $submenu = $with_child ? $this->draw_menu($theme, NULL, $v['id'], $status, $with_child, true) : false;
                    $result .= $no == 1 ? '<li class="m-t-30" id="menu-'.$v['url'].'">' : '<li class="" id="menu-'.$v['url'].'">';
                    $result .= '<a href="'.($submenu ? 'javascript:;' : $v['url']).'">';
                    $result .= '<span class="title bold" style="width:90%;overflow:unset;font-size: 12px;text-transform:uppercase;">'.$v['name'].'</span>';
                    $result .= $submenu ? '<span class="arrow"></span>' : '';
                    $result .= '</a>';
                    $result .= '<span class="icon-thumbnail bg-info-dark"><i class="'.$v['icon'].'"></i></span>';
                    $result .= $submenu ? '<ul class="sub-menu" style="display: none;">' . $submenu . '</ul>' : '';
                    $result .= '</li>';
                }
                $result .= $is_child ? '' : '</ul>';
                break;
            default:
                $this->draw_menu('kode', $id);
                break;
        }
        return $result;
    }

}
