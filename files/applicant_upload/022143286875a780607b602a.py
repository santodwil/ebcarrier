import argparse
import sys
import traceback
from controller.monitoringdata.compareStatTwitter import CompareStatTwitter
from ConfigParser import ConfigParser
from bottle import route, run
from gevent import monkey
from helper.api import error_response, success_response, get
from datetime import datetime, timedelta

monkey.patch_all()


@route('/twitter_stream', method='GET')
def twitter_stream():
    try:
        screen_name = get('screen_name', required=True)
        user_id = get('user_id', required=True)
        date = get('date', datetime.now().strftime('%Y%m%d'))
        ddate = datetime.strptime(date, '%Y%m%d')
        if cluster == 'cloud':
            sdate = ddate - timedelta(hours=8)
        elif cluster == 'biznet':
            sdate = ddate - timedelta(hours=7)
        else:
            raise Exception('cluster unregistered')
        edate = sdate + timedelta(days=1)
        edate = edate - timedelta(seconds=1)
        fq = 'created_at:[{} TO {}]'.format(sdate.strftime('%Y-%m-%dT%H:%M:%SZ'), edate.strftime('%Y-%m-%dT%H:%M:%SZ'))
        tweet = twitter.get_count_stream_tweet(screen_name, user_id, fq)
        rtm = twitter.get_count_stream_rtm(screen_name, user_id, fq)
        return success_response({
            'tweet': tweet,
            'rtm': rtm
        })
    except Exception, e:
        print traceback.print_exc()
        return error_response(500, str(e))


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Monitoring Data Tools Main API',
                                        formatter_class=argparse.RawDescriptionHelpFormatter)
    argparser.add_argument('-c', '--config', help='Config File', metavar='', default='config.conf')
    argparser.add_argument('-p', '--port', help='Port Number. Default 8000', metavar='', default=8000)
    argparser.add_argument('-w', '--workers', help='Number of workers. Default 6', metavar='', default=6)
    argparser.add_argument('-to', '--timeout', help='Request Timeout. Default 90', metavar='', default=90)
    argparser.add_argument('-cl', '--cluster', help='Cluster', metavar='', default='cloud')
    args = argparser.parse_args()

    twitter = CompareStatTwitter(config_file=args.config, cluster=args.cluster)
    cluster = args.cluster

    config = ConfigParser()
    config.read(args.config)
    log_file = "{0}/devt_main_monitoringdata_api.log".format(config.get('path', 'logging_path'))
    sys.argv = sys.argv[0:1]

    run(host='0.0.0.0', port=args.port, server='gunicorn', workers=args.workers,
        accesslog=log_file, timeout=args.timeout, worker_class='gevent', limit_request_line=8192)
    # run(host='0.0.0.0', port=8040)
