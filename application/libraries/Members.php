<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Members{

	public function __construct() {
        $this->ci = & get_instance();
    }

    public function get_member_with_team() {
        $teams = $this->ci->db->select('id, name')->where('active', 1)->get('teams');
        foreach ($teams->result_array() as $k => $v) {
            // $result[$v['name']] = $v['name'];
            $members = $this->ci->db->select('id, name')->where('team_id', $v['id'])->get('members');
            if($members->num_rows() > 0){
                foreach ($members->result_array() as $kk => $vv) {
                    $result[$v['name']][$vv['id']] = $vv['name'];
                    // $result[$v['name']][$vv['id']]['name'] = $vv['name'];
                }
            }
        }
        return $result;
    }

}
