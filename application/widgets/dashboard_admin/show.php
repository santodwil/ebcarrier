<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>" class="font-arial"> 
    <div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default" style="margin-bottom: 0">
            <div class="panel-heading">
                <!-- <div class="panel-controls pull-left"> -->
                    <button type="button" id="range-date" class="btn btn-default btn-sm pull-left">
                        <i class="fa fa-calendar"></i> <?php echo date('d M Y') ?>
                    </button>
                <!-- </div> -->
              </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 30px;">
                        <div class="col-md-4 text-center" style="border-right: 1px solid #f3f3f3">
                            <h1 class="bold text-info no-margin total-applicant"></h1>
                            <h3 class="bold text-info no-margin font-arial">Applicant</h3>
                        </div>
                         <div class="col-md-4 text-center" style="border-right: 1px solid #f3f3f3">
                            <h1 class="bold text-success no-margin total-active"></h1>
                            <h3 class="bold text-success no-margin font-arial">Active</i></h3>
                        </div>
                         <div class="col-md-4 text-center">
                            <h1 class="bold text-danger no-margin total-inactive"></h1>
                            <h3 class="bold text-danger no-margin font-arial">Inactive</h3>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div id="pie-vacancy" style="width:100%;border:0px solid black;"></div>
                        </div>
                        <div class="col-md-4">
                            <div id="pie-degree" style="width:100%;border:0px solid black;"></div>
                        </div>
                         <div class="col-md-4">
                            <div id="pie-gender" style="width:100%;border:0px solid black;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>