$(function () {

	'use restrict';

	$.fn.get_evaluator = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            test_type : $(container + ' #test_type').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/evaluator/widget/get_evaluator',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search,
                test_type: p.test_type
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
        		var no = 0;
            	if(r.total > 0){
            		$.each(r.result, function (k,v){
                        t += '<tr>';
                            t += '<td>'+v.test_name+'</td>';
                            t += '<td class="text-center">'+v.evaluator+'</td>';
                            t += '<td class="text-center">';
                                t += '<div class="btn-group">';
                                	t += '<button data-id="'+v.id+'" class="btn btn-xs btn-danger btn-delete">Delete</button>';
                                	t += '<button data-testtypeid="'+v.test_type_id+'" data-id="'+v.id+'" class="btn btn-xs btn-default btn-edit">Modify</button>';
                                t += '</div>';
                            t += '</td>';
                        t += '</tr>';
            		});
            	}else{
            		t += '<tr><td colspan="3" class="text-center">No Result</td></tr>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	hide_loading();
            }
        });
    }

    $.fn.edit_evaluator = function(params) {
        var p = $.extend({
            id: null,
        	testtypeid: null
        }, params);
        ajaxManager.addReq({
            url: site_url + '/evaluator/widget/edit_evaluator',
            type: 'GET',
            dataType: 'JSON',
            data: {
                testtypeid: p.testtypeid
            },
            beforeSend: function(){
            	$(container + ' .btn-edit[data-id="'+p.id+'"]').addClass('disabled');
            	$(container + ' .btn-edit[data-id="'+p.id+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                var $form = $('#form-modify');
                $form.find('input[name="id"]').val(p.testtypeid);
                $form.find('select[name="test_type"]').val(p.testtypeid).trigger("change");
                if(r.evaluator){
                    t = '';
                    $.each(r.evaluator, function (k,v){
                        t += '<input type="hidden" name="available_user_'+v.user_id+'">';
                        t += '<input type="hidden" class="status_'+v.id+'" value="noaction">';
                        t += '<button type="button" style="margin:5px;" class="btn btn-default btn-xs btn-delete-evaluator" data-type="old_data" data-id="'+v.id+'" data-userid="'+v.user_id+'"><i class="fa fa-trash"></i><br>'+v.first_name+'</button>';
                    });
                    $(container + ' #evaluator_list').html(t);
                }
            },
            complete: function(){
            	$(container + ' .btn-edit[data-id="'+p.id+'"]').removeClass('disabled');
            	$(container + ' .btn-edit[data-id="'+p.id+'"]').html('Modify');
            	$('#modal-edit-evaluator').modal('show');
            }
        });
    }

	$(this).get_evaluator();

    $(this).on('click', container + ' .btn-delete-evaluator', function (e) {
        var id = $(this).data('id');
        var type = $(this).data('type');
        var userid = $(this).data('userid');
        var status = $('.status_'+id+'').val();
        if(type == 'old_data'){
            if(status == 'noaction'){
                $('.status_'+id+'').val('removed');
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(container + ' #action_evaluator_list').append('<input type="hidden" name="removed_list[]" class="removed_list_'+id+'" value="'+id+'">');
            }
            if(status == 'removed'){
                $('.status_'+id+'').val('noaction');
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(container + ' #action_evaluator_list').find('.removed_list_'+id+'').remove();
            }
        }else{
            $(this).remove();
            $('.adduser_'+userid+'').remove();
        }
        e.preventDefault();
    });

    $(this).on('change', container + ' #choose_evaluator', function (e){
        var user_id = $(this).val();
        var nm = $("#choose_evaluator option:selected").html();
        t = '';
        if($('input[name="available_user_'+user_id+'"]').length == 0){
            t += '<input type="hidden" class="adduser_'+user_id+'" name="adduser[]" value="'+user_id+'">';
            t += '<button style="margin:5px;" class="btn btn-default btn-xs btn-delete-evaluator" data-type="new_add" data-userid="'+user_id+'"><i class="fa fa-trash"></i><br>'+nm+'</button>';
            $(container + ' #evaluator_list').append(t);
        }
    });

    $(this).on('click', container + ' .btn-edit', function (e) {
        var id = $(this).data('id');
        var testtypeid = $(this).data('testtypeid');
        $(this).edit_evaluator({
            id : id,
            testtypeid : testtypeid
        });
        e.preventDefault();
    });

    $(this).on('submit', container + ' #form-create', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + 'evaluator/widget/save',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function(r) {
                $('#csrf').val(r.csrf);                
                if(r.success){
                    form.resetForm();
                    $(this).get_evaluator();
                }
                $.snackbar({
                    content: r.msg, 
                    timeout: 5000
                });
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-save"></i> Save');
            }
        });
        e.preventDefault();
    });

    $(this).on('submit', container + ' #form-modify', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + 'evaluator/widget/change',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function(r) {
                $('#csrf').val(r.csrf);                
                if(r.success){
                    $('#modal-edit-evaluator').modal('hide');
                    form.resetForm();
                    $(this).get_evaluator();
                }
                $.snackbar({
                    content: r.msg, 
                    timeout: 5000
                });
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-edit"></i> Change');
            }
        });
        e.preventDefault();
    });


    $(this).on('change', container + ' #search', function (e){
  		$(this).get_evaluator({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(this).on('change', container + ' #test_type', function (e){
  		$(this).get_evaluator({
            test_type: $(this).val()
        });
  		e.preventDefault();
  	});

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 10,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_evaluator({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

    $(container + ' #new-evaluator-test').select2({
        dropdownParent: $("#modal-new-evaluator"),
        width: '100%'
    });

    $(container + ' #modify-evaluator-test').select2({
        dropdownParent: $("#modal-edit-evaluator"),
        width: '100%'
    });

    $(container + ' #choose_evaluator').select2({
        dropdownParent: $("#modal-edit-evaluator"),
        width: '100%'
    });

    $(container + ' #test_type').select2();

    $('.tagsinput').tagsinput();

});