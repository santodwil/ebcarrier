<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>

    <div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0;">
    		<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
							<label>For Test</label>
							<select class="form-control" id="test_type">
								<?php
									foreach ($test_type as $key => $value) {
										echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
									}
								?>

							</select>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label>Answers Type</label>
							<select class="form-control" id="question_type">
								<option value="">Choose answers type...</option>
								<?php
									$type = $this->db->select('type')->get('question_type');
									foreach ($type->result_array() as $key => $value) {
										$q_type[$value['type']]['type'] = $value['type'];
										$q_type[$value['type']]['rs'] = $this->db->select('id, name')->where_not_in('id', array(7,8))->where('type', $value['type'])->where('status', 1)->get('question_type')->result_array();
									}
									foreach ($q_type as $key => $value) {
										echo '<optgroup label="'.$value['type'].'">';
											foreach ($value['rs'] as $kk => $vv) {
												echo '<option value="'.$vv['id'].'">'.$vv['name'].'</option>';
											}
										echo '</optgroup>';
									}
								?>

							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-default btn-back" style="margin-top: 25px;">Back</button>
							<button type="button" class="btn btn-danger btn-submit" style="margin-top: 25px;">Submit</button>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-body" style="margin-top:-20px;"">
				<div class="row" id="section-data" style="padding-top: 10px;">
					<div class="col-md-12" id="section-result">
						<h4 class="text-center font-arial muted bold">Choose test and question to getting start</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>