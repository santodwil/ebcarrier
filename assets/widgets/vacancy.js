$(function () {

	'use restrict';

    $('#section-data').slimScroll({height: '450px'});

    $(this).on('submit', container + ' #form-save-vacancy', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/vacancy/widget/save_vacancy',
            type: 'POST',
            dataType: 'JSON',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    $.snackbar({
                        content: r.msg, 
                        timeout: 5000
                    });
                    $(this).get_vacancy();
                }else{
                    alert(r.msg);
                }
                form.resetForm();
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('Save');
            }
        });
        e.preventDefault();
    });

	$.fn.get_vacancy = function(params) {
        var p = $.extend({
            search : $(container + ' #search').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/vacancy/widget/get_vacancy',
            type: 'GET',
            dataType: 'JSON',
            data: {
                search: p.search
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
            	if(r.total > 0){
                    t += '<ul class="nav nav-pills nav-stacked" id="tests_no" style="width:auto;">';
            		$.each(r.result, function (k,v){
            			t += '<li class="vacancy-list-'+v.id+'">';
                            t += '<a href="#" class="detail-vacancy font-arial" data-id="'+v.id+'">'+(v.status == 1 ? '<i class="fa fa-circle text-success pull-left" style="margin-top:2px;"></i>&nbsp;' : '<i class="fa fa-circle text-danger pull-left" style="margin-top:2px;"></i>&nbsp;')+' '+v.name+'</a>';
                        t += '</li>';
            		});
                    t += '</ul';
            	}else{
            		t += '<h5 class="text-center font-arial">No Result</h5>';
            	}
            	$(container + ' #section-data').html(t);
                hide_loading();
            }
        });
    }

    $(this).get_vacancy();

  	$(this).on('change', container + ' #search', function (e){
  		$(this).get_vacancy({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

    $.fn.detail_vacancy = function(params) {
        var p = $.extend({
            id : false
        }, params);
        ajaxManager.addReq({
            url: site_url + '/vacancy/widget/detail_vacancy',
            type: 'GET',
            dataType: 'JSON',
            data: {
                id: p.id,
            },
            beforeSend: function () {
                $(container +' .nav-pills').find('li').removeClass('active');
                $(container +' .nav-pills').find('.vacancy-list-'+p.id+'').addClass('active');
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                t = '';
                t += '<div class="col-md-12">';
                    t += '<ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">';
                        t += '<li class="active"><a style="font-weight: bold;" data-toggle="tab" href="#basic"><span><i class="fab fa-whmcs"></i> Config Test</span></a></li>';
                        t += '<li><a style="font-weight: bold;" data-toggle="tab" href="#config" class="rules_test" data-id="'+p.id+'"><span><i class="fab fa-whmcs"></i> Rules Test</span></a></li>';
                    t += '</ul>';
                    t += '<div class="tab-content">';
                        t += '<div class="tab-pane active" id="basic">';
                            t += '<div class="row">';
                                t += '<div class="col-md-7" style="border-right: 1px solid #f3f3f3">';
                                    t += '<div class="col-md-10">';
                                        t += '<select class="form-control input-sm" id="lists-tests" name="status">';
                                            $.each(test_type_list, function(k,v){
                                            t += '<option value="'+v.id+'">'+v.name+'</option>';
                                            });
                                        t += '</select>';
                                    t += '</div>';
                                    t += '<div class="col-md-2">';
                                        t += '<button class="btn btn-default btn-sm pull-left btn-add-test" style="margin-left: -15px;"><i class="fa fa-plus"></i> Add</button>';
                                    t += '</div>';
                                    t += '<hr>';
                                    t += '<div class="col-md-12">';
                                        t += '<table class="table table-hover table-striped">';
                                            t += '<tbody>';
                                                if(r.test_groups.length > 0){
                                                    $.each(r.test_groups, function(k,v){
                                                        t += '<tr>';
                                                            t += '<td style="font-size:13px;">'+v.name+'</td>';
                                                            t += '<td width="60">';
                                                                t += '<div class="btn-group">';
                                                                    t += '<button role="group" class="btn btn-default btn-xs btn-remove-test" data-id="'+v.id+'" data-test-type-id="'+v.test_type_id+'" style="margin-top: -10px"><i class="fa fa-trash"></i></button>';
                                                                t += '</div>';
                                                            t += '</td>';
                                                        t += '</tr>';
                                                    });
                                                }else{
                                                    t += '<tr><td colspan="3">No Result</td></tr>';
                                                }
                                            t += '</tbody>';
                                        t += '</table>';
                                    t += '</div>';
                                t += '</div>';
                                t += '<div class="col-md-5">';
                                    t += '<form id="form-edit-vacancy">';
                                        t += '<input type="hidden" id="vacancy_id" name="vacancy_id" value="'+r.vacancy_id+'">';
                                        t += '<div class="form-group">';
                                            t += '<label class="text-default">Name</label>';
                                            t += '<input type="text" name="name" class="form-control" value="'+r.result.name+'" required="">';
                                        t += '</div>';
                                        t += '<div class="form-group">';
                                            t += '<label class="text-default">Status</label>';
                                            t += '<select class="form-control" id="edit-status" name="status">';
                                                t += '<option value="1" '+(r.result.status == 1 ? 'selected' : '')+'> Active</option>';
                                                t += '<option value="0" '+(r.result.status == 0 ? 'selected' : '')+'> Inactive</option>';
                                            t += '</select>';
                                        t += '</div>';
                                        t += '<button type="submit" class="btn btn-danger btn-block"><i class="fa fa-edit"></i> Change</button>';
                                        t += '<button type="button" class="btn btn-block btn-remove-vacancy" data-vacancyid="'+r.vacancy_id+'"><i class="fa fa-trash"></i> Remove this vacancy</button>';
                                    t += '</form>';
                                t += '</div>';
                            t += '</div>';
                        t += '</div>';
                        t += '<div class="tab-pane fade" id="config">';
                        t += '</div>';
                    t += '</div>';
                t += '</div>';

                $(container + ' #section-result').html(t);
                $('#lists-tests').select2();
                $('#edit-status').select2();
                hide_loading();
            }
        });
    }

    $(this).on('click', container + ' .btn-new-vacancy', function (e){
        $('.btn-new-vacancy').hide();
        $('.row-new').show();
        $(container + ' #new-status').select2();
    });

    $(this).on('click', container + ' .detail-vacancy', function (e){
        var id = $(this).data('id');
        $(this).detail_vacancy({
            id : id
        })
        e.preventDefault();
    });

    $(this).on('click', container + ' .rules_test', function (e){
        var id = $(this).data('id');
        $(container + ' #config').html('<p class="text-center"><i class="fa fa-refresh fa-spin"></i> Please Wait...</p>');
        ajaxManager.addReq({
            url: site_url + '/vacancy/widget/detail_vacancy',
            type: 'GET',
            dataType: 'JSON',
            data: {
                id: id
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                t = '';
                t += '<form id="form-rules">';
                    t += '<input type="hidden" name="vacancy_rules_id" value="'+id+'">';
                    t += '<div class="col-md-12">';
                        t += '<table class="table table-hover table-striped">';
                            t += '<tbody>';
                                if(r.test_groups.length > 0){
                                    $.each(r.test_groups, function(k,v){
                                        t += '<tr>'                                            
                                            t += '<td style="font-size:13px;">'+v.name+'</td>';
                                            t += '<td width="260">';
                                                t += '<select name="rules['+v.id+'][vacancy_id]" class="form-control rules-status">';
                                                t += '<option value="opened" '+(v.rules == 'opened' ? 'selected' : '')+'>Always Opened</option>';
                                                    $.each(r.test_groups, function(kk,vv){
                                                        t += '<option value="'+vv.test_type_id+'" '+(vv.test_type_id == v.rules ? 'selected' : '')+'>After '+vv.name+'</option>';
                                                    });
                                                t += '</select>';
                                            t += '</td>';
                                        t += '</tr>';
                                    });
                                }else{  
                                    t += '<tr><td colspan="3">No Result</td></tr>';
                                }
                            t += '</tbody>';
                        t += '</table>';
                    t += '</div>';
                    t += '<div class="col-md-12">';
                        t += '<button type="submit" class="btn btn-danger pull-right"><i class="fa fa-save"></i> Submit</button>';
                    t += '</div>';
                t += '</form>';
                $(container + ' #config').html(t);
            }
        });
    });

    $(this).on('click', container + ' .btn-remove-test', function (e){
        var test_type_id = $(this).data('test-type-id');
        var id = $(this).data('id');
        var conf = confirm('removing this will also remove all transactions related to the test on this vacancy. Are you sure ?');
        if(conf){
            ajaxManager.addReq({
                url: site_url + '/vacancy/widget/remove_test',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    test_type_id: test_type_id,
                    id: id,
                    vacancy_id: $('#vacancy_id').val(),
                    'csrf_token_app' : $('#csrf').val()
                },
                beforeSend: function () {
                    $(container + '.btn-remove-test').addClass('disabled');
                    $(container + '.btn-remove-test').html('<i class="fa fa-refresh fa-spin"></i>');
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    $('#csrf').val(r.csrf);
                    if(r.success){
                       $.snackbar({
                            content: r.msg, 
                            timeout: 5000
                        });
                        $(this).detail_vacancy({
                            id : $('#vacancy_id').val()
                        });

                    }else{
                        alert(r.msg);
                    }
                    $(container + '.btn-remove-test').removeClass('disabled');
                    $(container + '.btn-remove-test').html('<i class="fa fa-trash"></i>');
                }
            });
        }else{
            return false;
        }
        e.preventDefault();
    });

    $(this).on('click', container + ' .btn-remove-vacancy', function (e){
        var vacancyid = $(this).data('vacancyid');
        var conf = confirm('removing this will also remove all transactions related to the test on this vacancy. Are you sure ?');
        if(conf){
            ajaxManager.addReq({
                url: site_url + '/vacancy/widget/remove_vacancy',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    vacancyid: vacancyid,
                    'csrf_token_app' : $('#csrf').val()
                },
                beforeSend: function () {
                    $('.btn-remove-vacancy').addClass('disabled');
                    $('.btn-remove-vacancy').html('<i class="fa fa-refresh fa-spin"></i>');
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    $('#csrf').val(r.csrf);
                    if(r.success){
                        $.snackbar({
                            content: r.msg, 
                            timeout: 5000
                        });
                    }else{
                        alert(r.msg);
                    }
                    t = '';
                    t += '<h2 class="text-center font-arial muted bold" style="margin-top: 13%">Click vacancy to getting start</h2>';
                    $(container + ' #section-result').html(t);
                    $(this).get_vacancy();
                }
            });
        }else{
            return false;
        }
        e.preventDefault();
    });
    

    $(this).on('click', container + ' .btn-add-test', function (e){
        var test_id = $('#lists-tests').val();
        var conf = confirm('Are you sure ?');
        if(conf){
            ajaxManager.addReq({
                url: site_url + '/vacancy/widget/insert_new_test',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    test_id: test_id,
                    vacancy_id: $('#vacancy_id').val(),
                    'csrf_token_app' : $('#csrf').val()
                },
                beforeSend: function () {
                    $('.btn-add-test').addClass('disabled');
                    $('.btn-add-test').html('<i class="fa fa-refresh fa-spin"></i>');
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    $('#csrf').val(r.csrf);
                    if(r.success){
                       $.snackbar({
                            content: r.msg, 
                            timeout: 5000
                        });
                        $(this).detail_vacancy({
                            id : $('#vacancy_id').val()
                        });
                    }else{
                        alert(r.msg);
                    }
                    $('.btn-add-test').removeClass('disabled');
                    $('.btn-add-test').html('<i class="fa fa-plus"></i> Add');
                }
            });
        }else{
            return false;
        }
        e.preventDefault();
    });
    
    $(this).on('submit', container + ' #form-edit-vacancy', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/vacancy/widget/change_vacancy',
            type: 'POST',
            dataType: 'JSON',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    $.snackbar({
                        content: r.msg, 
                        timeout: 5000
                    });
                    $(this).get_vacancy();
                    $(this).detail_vacancy({
                        id : $('#vacancy_id').val()
                    });

                }else{
                    alert(r.msg);
                }
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('Change');
            }
        });
        e.preventDefault();
    });

    $(this).on('submit', container + ' #form-rules', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/vacancy/widget/change_rules',
            type: 'POST',
            dataType: 'JSON',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    $.snackbar({
                        content: r.msg, 
                        timeout: 5000
                    });
                    $(this).detail_vacancy({
                        id : r.vacancy_id
                    });

                }else{
                    alert(r.msg);
                }
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-save"></i> Submit');
            }
        });
        e.preventDefault();
    });
    

});