<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['app_author'] = 'PT eBdesk Teknologi';
$config['app_name'] = 'Task Management';
$config['app_desc'] = 'Manage & Report Task';

// $config['app_title_separator'] = '|';
// $config['app_default_product_image'] = './image/no-image.jpg';
// $config['app_favico'] = './images/favicon-32x32.png';
// $config['app_logo'] = './images/logo.png';
// $config['app_logo_light'] = './images/logo-alt-light.png';
// $config['app_logo_dark'] = './images/logo-alt-dark.png';
// $config['app_prefix'] = 'RNS';
