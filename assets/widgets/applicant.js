$(function () {

	'use restrict';

	$.fn.get_applicant = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            created : $(container + ' #created').val(),
            status : $(container + ' #status').val(),
            vacancy : $(container + ' #vacancy').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/applicant/widget/get_applicant',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search,
                created: p.created,
                status: p.status,
                vacancy: p.vacancy
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
        		var no = 0;
            	if(r.total > 0){
            		$.each(r.result, function (k,v){
                        t += '<tr>';
                            t += '<td>';  
                                t += '<input type="checkbox" style="margin-top:-5px;" class="option_id" id="checkbox'+v.id+'" value="'+v.id+'">';
                                t += '</div>';  
                            t += '</td>';
                            t += '<td>'+v.name+'</td>';
                            t += '<td class="text-center">'+v.education_degree+'</td>';
                            t += '<td class="text-center">'+v.email+'</td>';
                            t += '<td class="text-center">'+v.vacancy_name+'</td>';
                            t += '<td class="text-center">'+v.created+'</td>';
                            t += '<td class="text-center">';
                                t += v.active_login == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';
                            t += '</td>';
                            t += '<td class="text-center">';
                                t += '<div class="btn-group">';
                                    if(v.active_login == 1){
                                        t += '<button data-id="'+v.user_id+'" data-mode="inactive" class="btn btn-xs btn-default set-status">Set Inactive</button>';
                                    }else{
                                        t += '<button data-id="'+v.user_id+'" data-mode="active" class=" btn btn-xs btn-default set-status">Set Active</button>';
                                    }
                                t += '</div>';
                            t += '</td>';
                        t += '</tr>';
            		});
            	}else{
            		t += '<p class="text-center">No Result</p>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	hide_loading();
            }
        });
    }

    $(this).get_applicant();

    $(this).on('click', container + ' .set-status', function (e) {
        var id = $(this).data('id');
        var mode = $(this).data('mode');

        ajaxManager.addReq({
            url: site_url + '/applicant/widget/change_status',
            type: 'POST',
            dataType: 'JSON',
            data: {
                mode: mode,
                id: id,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                $(container + ' .set-status[data-id="'+id+'"]').addClass('disabled');
                $(container + ' .set-status[data-id="'+id+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    $.snackbar({
                        content: 'Data Updated', 
                        timeout: 5000
                    });
                    $(this).get_applicant();
                }else{
                    alert('Function Failed');
                }
            }
        });
        
        e.preventDefault();
    });

    $(container + ' .bulk_action').on('click', function(e) {
        var mode = $(this).data('mode');
        var value = $(this).data('value');

        var data = [];
        $('.option_id').each(function () {
            if (this.checked) {
                data.push($(this).val());
            }
        });
        if (data.length > 0) {
            ajaxManager.addReq({
                url: site_url + '/applicant/widget/change_selected',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    mode: mode,
                    data: data,
                    value: value,
                    'csrf_token_app' : $('#csrf').val()
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function (r) {
                    $('#csrf').val(r.csrf);
                    $('.select_id').prop('checked', false);
                    if(r.success){
                        $.snackbar({
                            content: 'Data Updated', 
                            timeout: 5000
                        });
                        $(this).get_applicant();
                    }else{
                        alert('Function Failed');
                    }
                }
            });
        } else {
            alert('No Data Selected');
        }

        e.preventDefault();
    });
    
    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 10,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_applicant({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

	$(container + ' .date-data').daterangepicker({
		autoUpdateInput: false,
		locale: {
	      format: 'DD/MMM/YYYY'
	    },
        singleDatePicker: true,
        showDropdowns: true
    });

    $(container + ' .date-data').on('apply.daterangepicker', function(ev, picker) {
      	$(this).val(picker.startDate.format('DD/MMM/YYYY'));
      	$('#created').val(picker.startDate.format('YYYY-MM-DD'));
      	$(this).get_applicant({
            created: picker.startDate.format('YYYY-MM-DD')
        });
  	});

    $(container + ' .date-data').on('change', function(ev, picker) {
        var date = $(this).val();
        if(date == ''){
            $('#created').val('');
            $(this).get_applicant();
        }
    });

  	$(this).on('change', container + ' #search', function (e){
  		$(this).get_applicant({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(this).on('change', container + ' #status', function (e){
  		$(this).get_applicant({
            status: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(this).on('change', container + ' #vacancy', function (e){
  		$(this).get_applicant({
            vacancy: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(container + ' #vacancy').select2();
  	$(container + ' #status').select2();

  	$(container + ' .select_id').on('change', function (e) {
        if (this.checked) {
            $('.option_id').each(function () {
                this.checked = true;
            });
        } else {
            $('.option_id').each(function () {
                this.checked = false;
            });
        }
    });



});