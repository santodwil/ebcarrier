<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>

    <div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
			<div class="panel-heading" style="background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-5">
							<div class="form-group">
								<label class="text-danger">For Test</label>
								<select class="form-control" id="test_type">
									<?php
										foreach ($test_type as $key => $value) {
											echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
										}
									?>

								</select>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label class="text-danger">Question Type</label>
								<select class="form-control" id="question_type">
									<option value="">Choose question type...</option>
									<?php
										$type = $this->db->select('type')->get('question_type');
										foreach ($type->result_array() as $key => $value) {
											$q_type[$value['type']]['type'] = $value['type'];
											$q_type[$value['type']]['rs'] = $this->db->select('id, name')->where('type', $value['type'])->where('status', 1)->get('question_type')->result_array();
										}
										foreach ($q_type as $key => $value) {
											echo '<optgroup label="'.$value['type'].'">';
												foreach ($value['rs'] as $kk => $vv) {
													echo '<option value="'.$vv['id'].'">'.$vv['name'].'</option>';
												}
											echo '</optgroup>';
										}
									?>

								</select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-default btn-back" style="margin-top: 25px;">Back</button>
								<button type="button" class="btn btn-danger btn-submit" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12" id="section-result" style="min-height: 400px;">
						<h2 class="text-center font-arial muted bold" style="margin-top: 13%">Choose test and question to getting start</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade slide-up disable-scroll in" id="modal-answers-image">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content">
                  	<div class="modal-header clearfix text-left">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                  		<h5 class="bold">Choose Correct Answers</h5>
                  	</div>
                  	<form id="form-correct-answers-image">
	                  	<div class="modal-body">
	                  		<table class="table table-condensed table-hover">
	                  			<tbody id="section-data-image"></tbody>
	                  		</table>
	                  	</div>
	                  	<div class="modal-footer">
							<button type="submit" class="btn btn-danger pull-right">Submit</button>
	                  	</div>
                  	</form>
				</div>
			</div>
		</div>
	</div>              		

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>