var ajaxManager = (function() {
    var requests = [];

    return {
        addReq:  function(opt) {
            requests.push(opt);
        },
        removeReq:  function(opt) {
            if( $.inArray(opt, requests) > -1 )
                requests.splice($.inArray(opt, requests), 1);
        },
        run: function() {
            var self = this,
                oriSuc;

            if( requests.length ) {
                oriSuc = requests[0].complete;

                requests[0].complete = function() {
                     if( typeof(oriSuc) === 'function' ) oriSuc();
                     requests.shift();
                     self.run.apply(self, []);
                };   

                $.ajax(requests[0]);
            } else {
              self.tid = setTimeout(function() {
                 self.run.apply(self, []);
              }, 1000);
            }
        },
        stop:  function() {
            requests = [];
            clearTimeout(this.tid);
        }
    };
}());

function error_handle(jqXHR, status, errorThrown){
    if (jqXHR.status === 0) {
        return alert('Not connect.\n Verify Network.');
     } else if (jqXHR.status == 404) {
        return alert('Requested page not found. [404]');
     } else if (jqXHR.status == 500) {
        return alert('Internal Server Error [500].');
     } else if (errorThrown === 'parsererror') {
        return alert('Requested JSON parse failed.');
     } else if (errorThrown === 'timeout') {
        return alert('Time out error.');
     } else if (errorThrown === 'abort') {
        return alert('Ajax request aborted.');
     } else {
        alert('Login session expired...');
        // window.location.href = location;
     }
}

function show_loading(){
    $('.loading').html(loading);
    $('.loading').show();
}

function hide_loading(){
    $('.loading').hide();
}

function notifications(container, style, position, message, type, timeout){
     $(container).pgNotification({
        style: style,
        message: message,
        position: position,
        timeout: timeout,
        type: type
     }).show();
} 

$(function() {
    ajaxManager.run(); 
});