-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Inang: 192.168.150.112
-- Waktu pembuatan: 05 Jan 2018 pada 13.51
-- Versi Server: 5.5.57-38.9-log
-- Versi PHP: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `newsletter`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jobs_npm`
--

CREATE TABLE IF NOT EXISTS `jobs_npm` (
  `jobs_id` int(11) NOT NULL,
  `npm_name` varchar(255) NOT NULL,
  PRIMARY KEY (`jobs_id`,`npm_name`),
  KEY `npm_name` (`npm_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Printed News Media';

--
-- Dumping data untuk tabel `jobs_npm`
--

INSERT INTO `jobs_npm` (`jobs_id`, `npm_name`) VALUES
(165, 'bisnis+indonesia'),
(165, 'investor+daily'),
(165, 'kompas'),
(165, 'koran+kontan'),
(165, 'koran+sindo'),
(165, 'koran+tempo'),
(165, 'majalah+tempo'),
(165, 'media+indonesia'),
(165, 'rakyat+merdeka'),
(165, 'republika'),
(165, 'the+jakarta+post');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `jobs_npm`
--
ALTER TABLE `jobs_npm`
  ADD CONSTRAINT `jobs_npm_ibfk_1` FOREIGN KEY (`jobs_id`) REFERENCES `jobs` (`jobs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jobs_npm_ibfk_2` FOREIGN KEY (`npm_name`) REFERENCES `newspaper_media` (`npm_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
