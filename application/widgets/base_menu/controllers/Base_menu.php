<?php

class Base_menu extends Widgets {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_base_menu');
    }

    public function index(){
        $menu_id = $this->m_base_menu->render_menu($this->_user->id);
        echo $this->m_base_menu->draw_menu($this->theme, $menu_id);
    }
    
    public function check_as_admin(){
        return $this->db->where('group_id', 1)->where('user_id', $this->_user->id)->count_all_results('users_groups');
    }

    public function check_as_evaluator(){
        return $this->db->where('group_id', 3)->where('user_id', $this->_user->id)->count_all_results('users_groups');
    }

}
