<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('vacancy'); 
        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Vacancy',
            'test_type_list' => $this->db->select('id, name')->where('status', 1)->get('test_type')->result_array(),
        );
        $this->render_widget($data);
    }

    public function get_vacancy(){
        $params = $this->input->get();
        $response['result'] = $this->vacancy->get_vacancy('get', $params['search']);
        $response['total'] = $this->vacancy->get_vacancy('count', $params['search']);
        $this->json_result($response);
    }

    public function save_vacancy(){
        $post = $this->input->post();
        $rs = $this->db->insert('vacancy_division', array('name' => $post['name'], 
                                                     'status' => $post['status'], 
                                                     'created_date' => date('Y-m-d H:i:s')));

        if($rs){
            $response['success'] = TRUE;
            $response['msg'] = 'New Data Added';  
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transaction Failed';
        }
        
        
        $this->json_result($response);
    }

    public function change_vacancy(){
        $post = $this->input->post();
        $rows = array(
            'name' => $post['name'],
            'status' => $post['status']
        );
        $rs = $this->db->update('vacancy_division', $rows, array('id' => $post['vacancy_id']));
        if($rs){
            $response['success'] = TRUE;
            $response['msg'] = 'Data Updated';  
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transaction Failed';
        }
        
        
        $this->json_result($response);
    }

    public function detail_vacancy(){
        $params = $this->input->get();
        $rs = $this->db->get_where('vacancy_division', array('id' => $params['id']));
        if($rs->num_rows() > 0){
            $response['success'] = TRUE;
            $response['result'] = $rs->row_array();
            $response['test_groups'] = $this->db->select('a.id, a.test_type_id as test_type_id, a.rules, b.name, a.status')
                                                ->join('test_type as b', 'a.test_type_id = b.id')
                                                ->where('a.vacancy_division_id', $params['id'])
                                                ->get('test_groups as a')->result_array();
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transaction Failed';
        }
        $response['vacancy_id'] = $params['id'];
        $this->json_result($response);
    }

    public function insert_new_test(){
        $params = $this->input->post();
        $check = $this->db->where('vacancy_division_id', $params['vacancy_id'])->where('test_type_id', $params['test_id'])->get('test_groups');
        if($check->num_rows() == 0){
            $insert = $this->db->insert('test_groups', array('vacancy_division_id' => $params['vacancy_id'], 'test_type_id' => $params['test_id'], 'status' => 1));
            if($insert){
                $response['success'] = TRUE;
                $response['msg'] = 'New Data Added';
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'Transaction Failed';
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'This type of test already exists';
        }
        $this->json_result($response);
    }

    public function change_rules(){
        $post = $this->input->post();
        if(isset($post['rules'])){
            foreach ($post['rules'] as $key => $value) {
                $rs = $this->db->update('test_groups', array('rules' => $value['vacancy_id']), array('id' => $key));
            }
            $response['success'] = TRUE;
            $response['msg'] = 'Data Updated';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'No Update';
        }
        $response['vacancy_id'] = $post['vacancy_rules_id'];
        $this->json_result($response);
    }

    public function remove_test(){
        $params = $this->input->post();
        $remove = $this->vacancy->remove_test($params['id'], $params['test_type_id'], $params['vacancy_id']); 
        if($remove){
            $response['success'] = TRUE;
            $response['msg'] = 'Data Removed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transacation Failed';
        }
        $this->json_result($response);
    }

    public function remove_vacancy(){
        $params = $this->input->post();
        $remove = $this->vacancy->remove_vacancy($params['vacancyid']); 
        if($remove){
            $response['success'] = TRUE;
            $response['msg'] = 'Data Removed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transacation Failed';
        }
        $this->json_result($response);
    }



}