<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Library for counting result test
 *
 * @author SUSANTO DWI LAKSONO
 */

class Test_result{

	public function __construct() {
        $this->ci = & get_instance();
    }

    private function on_duplicate($table, $data, $exclude = array(), $db = 'default') {
        $this->ci->_db = $this->ci->load->database($db, TRUE);
        $updatestr = array();
        foreach ($data as $k => $v) {
            if (!in_array($k, $exclude)) {
//                $updatestr[] = '`' . $k . '`="' . mysql_real_escape_string($v) . '"'; // local
                // $updatestr[] = '`' . $k . '`="' . mysql_escape_string($v) . '"'; // server
               $updatestr[] = "`" . $k . "`='" . $v . "'"; // local
            }
        }
        $query = $this->ci->_db->insert_string($table, $data);
        $query .= ' ON DUPLICATE KEY UPDATE ' . implode(', ', array_filter($updatestr));
        $this->ci->_db->query($query);
        return $this->ci->_db->affected_rows();
    }

    public function generate($test_type_id, $sdate, $edate){
    	$test_trans = $this->_test_transaction($test_type_id, $sdate, $edate);
    	$result = FALSE;

    	if($test_trans){
    		foreach ($test_trans->result_array() as $k => $v) {
    			$test_type_id = $v['test_type_id'];
    			$applicant_id = $v['applicant_id'];
    			$result = json_encode($this->_test_result_applicant($v['id'], $test_type_id));
    			$tmp = array(
					'test_type_id' => $test_type_id,
					'applicant_id' => $applicant_id,
					'result' => $result,
				);
    			$this->on_duplicate('test_result', array_filter($tmp));
    		}
    	}
    	return $result;
    }

    private function _test_transaction($test_type_id, $sdate, $edate){
    	$this->ci->db->where('test_type_id', $test_type_id);
    	$this->ci->db->where('date(time_start) BETWEEN "' . $sdate . '" AND "' . $edate . '"');
    	$rs = $this->ci->db->get('test_transaction');
    	if($rs->num_rows() > 0){
    		return $rs;
    	}else{
    		return FALSE;
    	}
    }

    private function _test_result_applicant($test_transaction_id, $test_type_id){
    	$group_type_question = $this->_group_type_question($test_type_id);
    	if($group_type_question){
    		foreach ($group_type_question->result_array() as $k => $v) {
    			$result[$v['question_type_id']] = $this->_counting($test_transaction_id, $v['question_type_id'], $test_type_id);
    		}	
    		return $result;
    	}else{
    		return FALSE;
    	}
    }

    private function _counting($test_transaction_id, $question_type_id, $test_type_id){
    	$question_id = $this->_question_list($question_type_id, $test_type_id);

    	switch ($question_type_id) {
    		case '7': // Psikotes
    			return $this->_psikotes_counting($question_id, $test_transaction_id);
				break;
    			
    		default:
    			return 0;
    			break;
    	}
    }

    private function _psikotes_counting($question_id, $test_transaction_id){
    	$_i = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'I')->get('test_answers')->num_rows();
    	$_e = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'E')->get('test_answers')->num_rows();
    	$_s = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'S')->get('test_answers')->num_rows();
    	$_n = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'N')->get('test_answers')->num_rows();
    	$_t = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'T')->get('test_answers')->num_rows();
    	$_f = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'F')->get('test_answers')->num_rows();
    	$_j = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'J')->get('test_answers')->num_rows();
    	$_p = $this->ci->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'P')->get('test_answers')->num_rows();
    	
    	$_ie = $_i > $_e ? 'I' : 'E';
    	$_sn = $_s > $_n ? 'S' : 'N';
    	$_tf = $_t > $_f ? 'T' : 'F';
    	$_jp = $_j > $_p ? 'J' : 'P';

    	return $_ie." ".$_sn." ".$_tf." ".$_jp;
    }

    private function _multiplechoice_counting($question_id, $test_transaction_id){
        $correct = 0;
        foreach ($question_id as $v) {
            $this->ci->db->select('b.correct_answers_id, a.answers');
            $this->ci->db->join('question as b', 'a.question_id = b.id');
            $this->ci->db->where('a.test_transaction_id', $test_transaction_id);
            $this->ci->db->where('a.question_id', $v);
            $rs = $this->ci->db->get('test_answers as a');
            if($rs->num_rows() > 0){
                $row = $rs->row_array();
                if($row['correct_answers_id'] == $row['answers']){
                    $correct++;
                }
            }
        }
        return $correct * 100 / count($question_id);
    }

    // private function _papikostick_counting($question_id, $test_transaction_id){
    //     foreach ($this->papicard as $v) {
    //         $papi_card_counting = $this->db->where('papi_card_id', $value['id'])->get('papi_card_counting');
    //     }
    // }

    private function _count($question_id, $test_transaction_id){
        $this->ci->db->select('sum(result) as total');
        $this->ci->db->where('test_transaction_id', $test_transaction_id);
        $this->ci->db->where_in('question_id', $question_id);
        $rs = $this->ci->db->get('test_answers')->row_array();
        return $rs['total'] / count($question_id);
    }

    private function _question_list($question_type_id, $test_type_id){
    	$this->ci->db->select('id');
    	$this->ci->db->where('test_type_id', $test_type_id);
    	$this->ci->db->where('question_type_id', $question_type_id);
    	$rs = $this->ci->db->get('question');
    	if($rs->num_rows() > 0){
    		foreach ($rs->result_array() as $key => $value) {
    			$result[] = $value['id'];
    		}
    		return $result;
    	}else{
    		return FALSE;
    	}
    }

    public function _group_type_question($test_type_id){
    	$this->ci->db->select('question_type_id');
    	$this->ci->db->where('test_type_id', $test_type_id);
    	$this->ci->db->group_by('question_type_id');
    	$rs = $this->ci->db->get('question');
    	if($rs->num_rows() > 0){
    		return $rs;
    	}else{
    		return FALSE;
    	}
    }

    public function calculation_result($test_type_id, $sdate, $edate){
        $applicant = $this->_applicant($test_type_id, $sdate, $edate);
        if($applicant->num_rows() > 0){
            foreach ($applicant->result_array() as $key => $value) {
                $data[$value['id']]['test_transaction_id'] = $value['id'];
                $data[$value['id']]['result'] = $this->_result_question($test_type_id, $value['id']);
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function _result_question($test_type_id, $test_transaction_id){
        $mapping_type = $this->_mapping_type($test_type_id);
        if($mapping_type){
            foreach ($mapping_type as $key => $value) {
                $data[$value['question_type_id']]['result'] = $this->_result($value['question_type_id'], $test_type_id, $test_transaction_id);
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function _result($question_type_id, $test_type_id, $test_transaction_id){
        $question_id = $this->_question_list($question_type_id, $test_type_id); 
        if($question_id){
            switch ($question_type_id) {
                case '7':
                    return $this->_psikotes_counting($question_id, $test_transaction_id);
                    break;
                case '1':
                    return $this->_multiplechoice_counting($question_id, $test_transaction_id);
                    break;
                case '8':
                    return 'None';
                    break;
                default:
                    return $this->_count($question_id, $test_transaction_id);
            }
        }else{
            return FALSE;
        }
        
    }

    public function _mapping_type($test_type_id){
        $this->ci->db->select('question_type_id');
        $this->ci->db->where('test_type_id', $test_type_id);
        $this->ci->db->group_by('question_type_id');
        $rs = $this->ci->db->get('question as a');
        if($rs->num_rows() > 0){
            return $rs->result_array();
        }else{
            return FALSE;
        }
    }

    public function _applicant($test_type_id, $sdate, $edate){
        $this->ci->db->where('date(time_start) BETWEEN "' . $sdate . '" AND "' . $edate . '"');
        $this->ci->db->where('test_type_id', $test_type_id);
        $this->ci->db->where('status', 2);
        return $this->ci->db->get('test_transaction');
    }
}