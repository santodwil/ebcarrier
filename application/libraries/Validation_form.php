<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Validation_form{

	public function __construct() {
        $this->_ci = & get_instance();
        $this->_ci->load->library('form_validation');
    }

    public function register(){
    	$this->_ci->form_validation->set_rules('register_fullname', 'Fullname', 'required');
        $this->_ci->form_validation->set_rules('register_gender', 'Gender', 'required');
    	$this->_ci->form_validation->set_rules('register_degree', 'Education Degree', 'required');
    	$this->_ci->form_validation->set_rules('register_majors', 'School Majors', 'required');
    	$this->_ci->form_validation->set_rules('register_university', 'University', 'required');
    	$this->_ci->form_validation->set_rules('register_contact', 'Contact Number', 'required|numeric');
    	$this->_ci->form_validation->set_rules('register_email', 'Email', 'required|valid_email|is_unique[applicant.email]');
    	$this->_ci->form_validation->set_rules('register_vacancy', 'Vacancy', 'required');
    	return $this->_ci->form_validation->run();
    }

    public function upload_cv(){
        $this->_ci->form_validation->set_rules('cv_file', 'CV File', 'required');
        return $this->_ci->form_validation->run();
    }

}