$(function () {

	'use restrict';

   $(container + ' #modal-detail-result').find('.modal-body').slimScroll({height: '450px', alwaysVisible:true});

	$.fn.get_applicant = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            sdate : $(container + ' #sdate').val(),
            edate : $(container + ' #edate').val(),
            test_type_id : $(container + ' #test_type_id').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/show_result_type/widget/get_applicant',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search,
                sdate: p.sdate,
                edate: p.edate,
                test_type_id: p.test_type_id,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                $('.loading').html('<h4>Please Wait...</h4>');
	        	$('.loading').show();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);

        		t = '';
        		var no = 0;
            	if(r.total > 0){
                  t += '<div class="col-md-12">';
                     t += '<table class="table table-hover table-striped table-condensed">';
                        t += '<thead>';
                           t += '<tr>';
                              t += '<th width="400">Name</th>';
                              t += '<th>Vacancy</th>';
                              t += '<th width="500" class="text-center">Result</th>';
                           t += '</tr>';
                        t += '</thead>';
                        t += '<tbody>';
                           $.each(r.result, function (k,v){ 
                              t += '<tr>';
                                 t += '<td><b>'+v.name+'</b><br>'+v.email+'</td>';
                                 t += '<td>'+v.division_name+'</td>';
                                 t += '<td class="text-center">';
                                    $.each(v.test_result, function (kk,vv){
                                       t += '<div class="col-md-4">';
                                          t += '<h4 class="bold no-margin text-center">'+vv.question_result+'</h4>';
                                          if(vv.question_type_id == 1 || vv.question_type_id == 7 || vv.question_type_id == 8){
                                             t += '<p class="small no-margin text-center">'+vv.question_type_name+'</p>';
                                          }else{
                                             t += '<p class="small no-margin text-center"><a href="" class="detail-result" data-applicantid="'+v.id+'" data-typeid="'+vv.question_type_id+'" data-name="'+v.name+'">'+vv.question_type_name+'</a></p>';
                                          }
                                       t += '</div>';
                                    });
                                 t += '</td>';
                              t += '</tr>';
                           });
                        t += '</tbody>';
                     t += '</table>';
                  t += '</div>';
            	}else{
            		t += '<p class="text-center font-arial">No Result</p>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	$('.loading').hide();
            }
        });
    }

    $(this).get_applicant();

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 9,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_applicant({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

   $(this).on('change', container + ' #search', function (e){
  		$(this).get_applicant({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

   $(this).on('click', container + ' .detail-result', function (e){
      var applicantid = $(this).data('applicantid');
      var typeid = $(this).data('typeid');      
      var name = $(this).data('name');      
      ajaxManager.addReq({
         url: site_url + '/show_result_type/widget/detail_result',
         type: 'GET',
         dataType: 'JSON',
         data: {
             applicantid: applicantid,
             typeid: typeid,
             test_type_id: $(container + ' #test_type_id').val()
         },
         beforeSend: function () {
            $('#modal-detail-result').modal('show');
            $(container + ' #modal-detail-result .modal-body').html('<h1 class="text-center no-margin"><i class="fa fa-refresh fa-spin" style="margin-top:20%;"></i></h1>');
         },
         error: function (jqXHR, status, errorThrown) {
             error_handle(jqXHR, status, errorThrown);
         },
         success : function (r){
            t = '';
            $('#form-detail-result input[name="test_type_id"]').val($(container + ' #test_type_id').val());
            $('#form-detail-result input[name="question_type_id"]').val(typeid);
            $('#form-detail-result input[name="applicant_id"]').val(applicantid);
            $('#form-detail-result input[name="total_question"]').val(r.total_question);

            $.each(r.result, function (k,v){ 
               t += '<div class="row" style="border-bottom: 1px solid #f1efef;margin-bottom: 10px;padding-bottom: 10px;">';
                  t += '<div class="col-md-12">';
                     t += ''+v.question_text+'';
                     t += '<p style="margin-top:-10px;">';
                        t += ''+(v.question_image ? '<button class="btn btn-xs" data-image="'+v.question_image+'">View Image</button>' : '' )+'';
                        t += ''+(v.question_file ? '<button class="btn btn-xs" data-image="'+v.question_file+'">View File</button>' : '' )+'';
                     t += '</p>';
                     t += '<div class="alert alert-warning">';
                        t += ''+(v.question_answers ? v.question_answers : 'None')+'';
                        if(v.question_answers){
                           t += '<input type="number" class="form-control" name="assessment['+v.question_answers_id+']" placeholder="Range 10 sd 100" value="'+v.question_answers_result+'">';
                        }
                     t += '</div>';
                  t += '</div>';
               t += '</div>';
            });
            $(container + ' #modal-detail-result .title-result').html(name);
            $(container + ' #modal-detail-result .modal-body').html(t);
         }
      });
      e.preventDefault();
   });

   $(this).on('submit', container + ' #form-detail-result', function (e) {
      var form = $(this);
      $(this).ajaxSubmit({
         url: site_url + '/show_result_type/widget/save_result',
         data:{
             type : 'none',
             'csrf_token_app' : $('#csrf').val()
         },
         type: 'POST',
         dataType: 'JSON',
         beforeSend: function () {
            form.find('[type="submit"]').addClass('disabled');
            form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r) {
            $('#csrf').val(r.csrf);

            $('.alert-notif').removeClass('hidden');
            $('.alert-notif').html(r.msg);

            form.find('[type="submit"]').removeClass('disabled');
            form.find('[type="submit"]').html('<i class="fa fa-save"></i> Submit');
         },
         complete: function (){
            $(".alert-notif").fadeTo(2000, 500).slideUp(500, function(){
                 $(".alert-notif").slideUp(500);
             });
         }
         
      });
      e.preventDefault();
   });

   $(this).on('click', container + ' .btn-back', function (e){
      Widget.Loader('result_type', {}, 'container-content', false);
      e.preventDefault();
   });

  	$(this).on('click', container + ' .show-result', function (e){
  		var id = $(this).data('id');
  		sdate = $(container + ' #sdate').val();
      edate = $(container + ' #edate').val();
  		Widget.Loader('show_result_type', {id:id,sdate:sdate,edate:edate}, 'container-content', false);
  		e.preventDefault();
  	});

});