<style type="text/css">
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>

    <div class="col-md-12">
		<div class="panel panel-default" style="margin-bottom: 0;">
    		<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
    			<div class="row">
					<div class="col-md-12">
						<div class="input-group">
							<span class="input-group-addon">Search</i></span>
							<input type="text" id="search" class="form-control">
						</div>
					</div>
    			</div>
    		</div>
    		<div class="panel-body" style="margin-top:-20px;"">
				<div class="row" style="padding-top: 10px;">
					<div class="col-md-12">
						<table class="table table-condensed table-striped">
							<thead>
								<th>Name</th>
								<th>Degree</th>
								<th>Test</th>	
								<th>Date</th>
								<th>Status</th>
								<th width="250"></th>
							</thead>
							<tbody id="section-data"></tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
    		</div>
		</div>
    </div>
    <div class="col-md-5">

    </div>
	
	<div class="modal fade slide-up disable-scroll in" id="modal-view-detail">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:1200px;max-height: 600px;overflow: auto;">
		          	<div class="modal-body modal-body-view-detail" style="margin-top: 15px;"></div>
		          	<div class="modal-footer">
		          		<div class="btn-group">
							<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
							<button class="btn btn-success btn-change"><i class="fa fa-edit"></i> Submit</button>
						</div>
		          	</div>
	          	</div>
          	</div>
      	</div>
  	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<link rel="stylesheet" href="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.css') ?>" />
<script src="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.js') ?>"></script>