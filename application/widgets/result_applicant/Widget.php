<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        $this->load->model('applicant');
        $this->load->library('assessment');
        $this->load->library('psikotes');

        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Result by Applicant'
        );
        $this->render_widget($data);
    }

    public function applicant_result(){
        $params = $this->input->get(); 
        $response['result'] = $this->applicant->applicant_result('get', $params);
        $response['total'] = $this->applicant->applicant_result('count', $params);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function detail_result(){
        $params = $this->input->get(); 
        $response['applicant'] = $this->applicant->detail_applicant($params);
        $response['assessment'] = $this->assessment->summary($params);
        $response['psikotes'] = $this->assessment->psikotes($params);
        $response['papi_kostick'] = $this->assessment->papi_kostick($params);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function cv_download($file){
        $this->load->helper('download');
        $temp_name = $this->db->select('temp_name')->where('file', $file)->get('question_file')->row_array();
        $data = file_get_contents('./files/question_file/'.$file.'');
        force_download($temp_name['temp_name'], $data);
    }

    public function change_status(){
        $params = $this->input->get();
        $update = $this->db->update('applicant', array('status' => $params['status']), array('id' => $params['applicant_id']));
        $response['success'] = $update ? TRUE : FALSE;
        $this->json_result($response);
    }
}