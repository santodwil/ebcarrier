<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('applicant');
        if (!$this->ion_auth->logged_in()) {
            return '{"msg":"success"}';
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Applicant',
        );
        $this->render_widget($data);
    }

    public function get_applicant(){
        $params = $this->input->get(); 
        $response['result'] = $this->applicant->get_applicant('get', $params);
        $response['total'] = $this->applicant->get_applicant('count', $params);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function change_status(){
        $post = $this->input->post();
        if(isset($post['mode']) && isset($post['id'])){
            if($post['mode'] == 'active'){
                $update = $this->db->update('users', array('active' => 1), array('id' => $post['id']));
            }else{
                $update = $this->db->update('users', array('active' => 0), array('id' => $post['id']));
            }
            if($update){
                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
            }
        }else{
            $response['success'] = FALSE;
        }
        $this->json_result($response);
    }

    public function change_selected(){
        $post = $this->input->post();
        if($post['mode'] == 'tests'){
            $this->db->where_in('applicant_id', $post['data']);
            $this->db->where_in('test_type_id', $post['value']);
            $this->db->delete('test_transaction');
        }else{
            $user_id = $this->db->select('user_id')->where_in('id', $post['data'])->get('applicant');
            if($user_id->num_rows() > 0){
                foreach ($user_id->result_array() as $key => $value) {
                    $rows = array(
                        'active' => $post['value']
                    );
                    $this->db->where('id', $value['user_id']);
                    $this->db->update('users', $rows);
                }
            }
            
        }
        $response['success'] = TRUE;
        $this->json_result($response);
    }

}