<style type="text/css">
    .btn-danger{
        background-color: #f11818;
    }
    .text-danger{
        color: #f11818 !important;
    }
    .bg-danger{
        background-color: #f11818;
    }    
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
    
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-transparent" style="margin-bottom: 0;">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-user"></i> Applicant Profil</div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td width="170" class="bold">Name</td>
                                        <td ><?php echo $applicant['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Email</td>
                                        <td><?php echo $applicant['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Contact Number</td>
                                        <td><?php echo $applicant['contact_number'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="190" class="bold">Education Degree</td>
                                        <td><?php echo $applicant['education_degree'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">School Majors</td>
                                        <td><?php echo $applicant['school_majors'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">University</td>
                                        <td><?php echo $applicant['university'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Apply As</td>
                                        <td><?php echo $applicant['vacancy_division_name'] ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-transparent" style="margin-bottom: 0">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-file"></i> Upload CV</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <?php
                                    $cv = $this->db->where('applicant_id', $applicant['id'])->get('applicant_cv');
                                    if($cv->num_rows() > 0){
                                        $cv_name = $cv->row_array();
                                        $mode = $cv_name['applicant_file'];
                                    }else{
                                        $mode = '';
                                    }
                                ?>
                                <form id="form-upload-cv" enctype="multipart/form-data">
                                    <div class="col-md-9">
                                        <input type="hidden" name="file_before" value="<?php echo $mode ?>">
                                        <input type="file" name="cv_file" class="form-control input-sm">
                                    </div>
                                    <div class="col-md-3 no-padding sm-m-t-10 sm-text-center">
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-upload"></i> Upload</button>
                                    </div>
                                </form> 
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 result-cv">
                                    <?php if($mode != '') { ?>
                                        <p>Your CV : <a href="<?php echo site_url('dashboard/widget/download/'.$cv_name['applicant_file'].'') ?>"><?php echo $cv_name['applicant_temp_file'] ?></a></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-transparent" style="margin-bottom: 0">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-edit"></i> List of Test</div>
                        </div>
                        <div class="panel-body">
                           <?php
                            $vacancy = $this->db->select('vacancy_division_id, id')->where('user_id', $user->id)->get('applicant')->row_array();
                            $test_list = $this->db->select('a.rules, b.*,c.*')
                                        ->join('test_type as b', 'a.test_type_id = b.id', 'left')
                                        ->join('(select status as status_register, test_type_id from test_transaction where applicant_id = '.$vacancy['id'].') as c', 'b.id = c.test_type_id', 'left')
                                        ->where('a.vacancy_division_id', $vacancy['vacancy_division_id'])
                                        ->where('a.status', 1)
                                        ->order_by('a.id', 'asc')
                                        ->get('test_groups as a');

                            if($test_list->num_rows() > 0){
                                foreach ($test_list->result_array() as $key => $value) {

                                    if($value['rules'] != 'opened'){
                                        $check = $this->db->where('applicant_id', $vacancy['id'])
                                                              ->where('test_type_id', $value['rules'])
                                                              ->where('status', 2)
                                                              ->get('test_transaction')->num_rows();
                                    }else{
                                        $check = 1;
                                    }
                                    

                                    echo '<div class="card share full-width">';
                                        if($value['status_register']){
                                            if($value['status_register'] == 1){
                                                if($check > 0){
                                                    echo '<button class="btn btn-danger btn-sm pull-right apply-test" style="margin-top:13px;margin-right:10px;" data-id="'.$value['id'].'">Continue</button>';
                                                }
                                            }else{
                                                if($check > 0){
                                                    echo '<label class="label label-success pull-right" style="font-size:20px;margin-top:15px;margin-right:10px;"><i class="fa fa-check"></i></label>';
                                                }
                                            }
                                        }else{
                                            if($check > 0){
                                                echo '<button class="btn btn-danger btn-sm pull-right apply-test" style="margin-top:13px;margin-right:10px;" data-id="'.$value['id'].'">Apply</button>';
                                            }
                                        }
                                        echo '<div class="card-header clearfix" style="cursor: default;">';
                                            // echo '<div class="user-pic">';
                                            //     if($value['type'] == 'Basic'){
                                            //         echo '<img width="150" height="150" src="'.base_url().'/assets/img/basic.png">';
                                            //     }else{
                                            //         echo '<img width="150" height="150" src="'.base_url().'/assets/img/teknis.png">';
                                            //     }
                                            // echo '</div>';
                                            echo '<h5>'.$value['name'].'</h5>';
                                            echo '<h6>'.$value['total_question'].' Question in '.$value['time'].' Minutes</h6>';
                                        echo '</div>';
                                        echo '<div class="card-description">';
                                            echo ''.$value['guide_tests'].'';
                                        echo '</div>';
                                    echo '</div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>