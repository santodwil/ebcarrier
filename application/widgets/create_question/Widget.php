<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('questions'); 
        $this->load->library('upload'); 
        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'New Question',
            'question_type' => $this->db->get('question_type')->result_array(),
            'test_type' => $this->db->get('test_type')->result_array(),
            'question_type' => $this->db->where('id !=', 4)->get('question_type')->result_array()
        );
        $this->render_widget($data);
    }

    public function save_question(){
        $post = $this->input->post();
        $count_answers = 0;
        $count_answers_image = 0;

        //Insert to table question
        $question = $this->questions->save_question($post);

        if($question){
            $question_id = $this->db->insert_id();

            //Insert to question image
            if (!empty($_FILES['question_image']['name'])){
                $config['upload_path']  = './files/question_image/';
                $config['allowed_types'] = '|png|jpg|jpeg|gif|';
                $config['file_name'] = $question_id.'_'.$_FILES['question_image']['name'];

                $this->upload->initialize($config);
                if (!$this->upload->do_upload('question_image')) {
                    $this->db->delete('question', array('id' => $question_id));
                    $response['success'] = FALSE;
                    $response['msg'] = $this->upload->display_errors();
                    $this->json_result($response);
                    exit();        
                }else{
                    $datafile = $this->upload->data();
                    $this->db->insert('question_image', array('question_id' => $question_id, 'image' => $datafile['file_name']));
                }
            }

            //Insert to question file
            if (!empty($_FILES['question_file']['name'])){
                $configs['upload_path']  = './files/question_file/';
                $configs['allowed_types'] = '|doc|docx|pdf|txt|xls|xlsx';
                $configs['file_name'] = $question_id.'_'.$_FILES['question_file']['name'];

                $this->upload->initialize($configs);
                if (!$this->upload->do_upload('question_file')) {
                    $this->db->delete('question', array('id' => $question_id));
                    $response['success'] = FALSE;
                    $response['msg'] = $this->upload->display_errors();
                    $this->json_result($response);
                    exit();        
                }else{
                    $datafile = $this->upload->data();
                    $this->db->insert('question_file', array('question_id' => $question_id, 'file' => $datafile['file_name']));
                }
            }

            //Insert to question answers (type papi kostick)
            if($post['type_question'] == 8){
                if(isset($post['answers'])){
                    foreach ($post['answers'] as $value) {
                        $answers = $this->questions->save_answers_psikotes($question_id, $value['answers_text'], $value['answers_card']);
                        $answers ? $count_answers++ : FALSE; 
                    }
                    if($count_answers != count($post['answers'])){
                        $this->db->delete('question', array('id' => $question_id));
                        $response['success'] = FALSE;
                        $response['msg'] = 'Transaction For Save Answers Papi Kostick Failed';
                        $this->json_result($response);
                        exit();
                    }
                }
            }
            
            //Insert to question answers (type multiple choice / multiple answers / Psikotes)
            if($post['type_question'] == 1 || $post['type_question'] == 4 || $post['type_question'] == 7){
                //Insert type text answers
                if($post['option_type_answers'] == 'text'){
                    if($post['type_question'] == 1){
                        if(isset($post['answers']) && isset($post['correct'])){
                            foreach ($post['answers'] as $key => $value) {
                                $answers = $this->questions->save_question_answers($question_id, $key, $value);
                                $answers ? $count_answers++ : FALSE; 
                            }

                            if($count_answers == count($post['answers'])){
                                $rs_correct_answers_image = $this->questions->save_correct_answers($question_id, $post['correct']);      
                                if(!$rs_correct_answers_image){
                                    $this->db->delete('question', array('id' => $question_id));
                                    $response['success'] = FALSE;
                                    $response['msg'] = 'Transaction For Save Correct Answers Failed';
                                    $this->json_result($response);
                                    exit();
                                }   
                            }else{
                                $this->db->delete('question', array('id' => $question_id));
                                $response['success'] = FALSE;
                                $response['msg'] = 'Transaction For Save Answers Failed';
                                $this->json_result($response);
                                exit();
                            }
                        }
                    }
                    if($post['type_question'] == 7){
                        if(isset($post['answers'])){
                            foreach ($post['answers'] as $value) {
                                $answers = $this->questions->save_answers_psikotes($question_id, $value['answers_text'], $value['answers_nilai']);
                                $answers ? $count_answers++ : FALSE; 
                            }
                        }
                    }
                }

                //Insert type image answers
                if($post['option_type_answers'] == 'image'){
                    if(isset($_FILES['userfile'])){
                        $files = $_FILES;
                        $filesCount = count($_FILES['userfile']['name']);
                        for($i = 0; $i < $filesCount; $i++){
                            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                            $conf_image_answers['upload_path']  = './files/answers_image/';
                            $conf_image_answers['allowed_types'] = '|png|jpg|jpeg|gif|';
                            $conf_image_answers['file_name'] = $question_id.'_'.$_FILES['userfile']['name'];
                            
                            $this->upload->initialize($conf_image_answers);
                            if ($this->upload->do_upload('userfile')){
                                $count_answers_image++;
                                $fileData = $this->upload->data();
                                $uploadData[$i]['question_id'] = $question_id;
                                $uploadData[$i]['question_answers'] = $fileData['file_name'];
                                $uploadData[$i]['question_type'] = $post['option_type_answers'];
                            }else{
                                $this->db->delete('question', array('id' => $question_id));
                                $response['success'] = FALSE;
                                $response['msg'] = 'File '.$_FILES['userfile']['name'].' : '.strip_tags($this->upload->display_errors());
                                foreach ($uploadData as $key => $value) {
                                    unlink($conf_image_answers['upload_path'].$value['question_answers']);
                                }
                                $this->json_result($response);
                                exit();
                            }
                        }
                        if(count($uploadData) == $filesCount){
                            $this->db->insert_batch('question_answers', $uploadData);
                        }
                    }else{
                        $response['success'] = FALSE;
                        $response['msg'] = 'Answers Required';
                    }
                }
            }

            $response['success'] = TRUE;
            $response['option_type_answers'] = isset($post['option_type_answers']) ? $post['option_type_answers'] : '';
            $response['question_id'] = $question_id;
            $response['msg'] = 'Question successfully added';
        }
        $this->json_result($response);
    }

    public function get_answers_image(){
        $post = $this->input->post();
        $response['result'] = $this->db->where('question_id', $post['question_id'])->get('question_answers')->result_array();
        $response['success'] = TRUE;
        $response['question_id'] = $post['question_id'];
        $this->json_result($response);
    }

    public function save_correct_answers_image(){
        $post = $this->input->post();
        $rs_correct_answers_image = $this->questions->save_correct_answers($post['question_id'], $post['correct'], '_by_id'); 
        $response['success'] = $rs_correct_answers_image ? TRUE : FALSE;     
        $this->json_result($response);
    }
}