<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');
        
        $this->load->model('tests'); 

        if (!$this->ion_auth->logged_in()) {
            die;
        }else{
            $this->_applicant_id = $this->tests->_get_applicant_id($this->_user->id);
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'id' => $get['id'],
            'detail_tests' => $this->tests->detail_tests($get['id'], $this->_user->id),
        );
        $this->render_widget($data);
    }

    public function register_tests(){
        $p = $this->input->post();
        $rules = $this->tests->rules_tests($p['reg_id'], $this->_user->id);
        if($rules){
            $register_tests = $this->tests->register_tests($p['reg_id'], $p['time_test'], $this->_user->id);
            if($register_tests){
                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'Register failed';
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'You dont have permissions'; 
        }
        $this->json_result($response);
    }

    public function finish_tests(){
        $p = $this->input->get();
        $this->load->model('assessment');
        $check_question_type_id = $this->db->where('test_type_id', $p['test_id'])->where_in('question_type_id', array(1,7,8))->get('question');
        if($check_question_type_id->num_rows() > 0){
            $check = $this->db->select('id')->where('test_type_id', $p['test_id'])->get('question');
            foreach ($check->result_array() as $key => $value) {
                $question_id[] = $value['id'];
            }
            $total_question = $check->num_rows();
            $check_total_answers = $this->db->where('test_transaction_id', $p['test_transaction_id'])->where_in('question_id', $question_id)->get('test_answers');
            if($total_question == $check_total_answers->num_rows()){
                $finish_tests = $this->tests->finish_tests($p['test_id'], $p['test_transaction_id']);
                $counting = $this->assessment->counting_result($p['test_id'], $p['test_transaction_id']);
                if($finish_tests && $counting){
                    $response['success'] = TRUE;
                }else{
                    $response['success'] = FALSE;
                    $response['msg'] = 'Register for finishing tests failed';
                }
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'You must complete all the question';
            }            
        }else{
            $finish_tests = $this->tests->finish_tests($p['test_id'], $p['test_transaction_id']);
            if($finish_tests){
                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'Register for finishing tests failed';
            }
        }
        $this->json_result($response);
    }

    public function get_questions(){
        $p = $this->input->get();
        if($p['question_id'] || is_numeric($p['question_id'])){
            $question = $this->tests->get_question($p['question_id']);
            if($question){
                $response['question'] = $question;
                
                $previous_id = $this->db->select('max(id) as previous_id')->where('id < ', $question['id'])->where('test_type_id', $question['test_type_id'])->get('question')->row_array('previous_id');
                $next_id = $this->db->select('min(id) as next_id')->where('id > ', $question['id'])->where('test_type_id', $question['test_type_id'])->get('question')->row_array('previous_id');
                $max_soal = $this->db->select('max(id) as soal_terakhir')->where('test_type_id', $question['test_type_id'])->get('question')->row_array();
                $min_soal = $this->db->select('min(id) as soal_pertama')->where('test_type_id', $question['test_type_id'])->get('question')->row_array();

                $response['previous_id'] = $previous_id['previous_id'];
                $response['next_id'] = $next_id['next_id'];
                $response['max_soal'] = $max_soal['soal_terakhir'];
                $response['min_soal'] = $min_soal['soal_pertama'];

                $answers_question = $this->db->select('answers,answers_temp_file')->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['question_id'])->get('test_answers')->row_array();
                // $response['answers_question'] = $answers_question['answers'] ? $answers_question['answers'] : FALSE;
                
                if($question['question_type_id'] == 5 || $question['question_type_id'] == 6){
                    $response['answers_question'] = $answers_question['answers'] ? json_decode($answers_question['answers'], TRUE) : FALSE;
                }else if($question['question_type_id'] == 2){
                    $response['answers_question'] = $answers_question['answers'] ? $answers_question['answers'] : FALSE;
                    $response['answers_question_temp_file'] = $answers_question['answers_temp_file'] ? $answers_question['answers_temp_file'] : FALSE;
                }else{
                    $response['answers_question'] = $answers_question['answers'] ? $answers_question['answers'] : FALSE;
                }

                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'Question Not Found';
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Restrict';
        }
        $this->json_result($response);
    }



    public function save_answers(){
        $p = $this->input->post();
        if(isset($p['previous_id'])){
            $correct_answers_checking = $p['question_type_id'] == 2 ? $_FILES['correct_answers']['name'] : $p['correct_answers'];
            if(isset($correct_answers_checking)){
                if($p['type'] == 'save'){
                    if($p['question_type_id'] == 2){
                        if (!empty($_FILES['correct_answers']['name'])){
                            $config['upload_path']  = './files/applicant_upload/';
                            $config['allowed_types'] = '*';
                            $rep_name = str_replace("_", "-", $_FILES['correct_answers']['name']);
                            $config['file_name'] = uniqid(date("his").$this->_applicant_id.$p['q_id']);

                            $this->load->library('upload', $config);
                            $checking_file = $this->tests->checking_file($p['q_id'], $p['test_transaction_id']);
                            if($checking_file){
                                unlink('./files/applicant_upload/'.$checking_file);
                            }
                            if (!$this->upload->do_upload('correct_answers')) {
                                $response['success'] = FALSE;
                                $response['msg'] = $this->upload->display_errors();   
                                $this->json_result($response);
                                exit();                 
                            }else{
                                $datafile = $this->upload->data();
                                $correct_answers =  $datafile['file_name'];
                                $answers_file_temp_name =  $_FILES['correct_answers']['name'];
                                $response['answers_file'] = $datafile['file_name'];
                                $response['answers_file_temp_name'] = $_FILES['correct_answers']['name'];
                            }
                        }else{
                            $response['success'] = FALSE;
                            $response['msg'] = 'Upload your file';   
                            $this->json_result($response);
                            exit();                 
                        }
                    }else{
                        $correct_answers = $p['correct_answers'];
                        $answers_file_temp_name = NULL;
                    }

                    $result = $this->db->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['q_id'])->get('test_answers');
                    if($p['question_type_id'] == 1){
                        $answers_assessment = $p['correct_answers'] == $p['correct_answers_question'] ? 100 : 0;
                    }else{
                        $answers_assessment = NULL;
                    }
                    if($result->num_rows() == 0){
                        $ins = array(
                            'question_id' => $p['q_id'],
                            'test_transaction_id' => $p['test_transaction_id'],
                            'answers' => $correct_answers,
                            'answers_temp_file' => $answers_file_temp_name,
                            'result' => $answers_assessment
                        );
                        $save = $this->db->insert('test_answers', $ins);
                        if($save){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }else{
                        $upd = array(
                            'answers' => $correct_answers,
                            'answers_temp_file' => $answers_file_temp_name,
                            'result' => $answers_assessment
                        );
                        $update = $this->db->update('test_answers', $upd, array('test_transaction_id' => $p['test_transaction_id'], 'question_id' => $p['q_id']));
                        if($update){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }
                }else{
                    $response['success'] = TRUE;
                }
                $response['answered'] = TRUE;
            }else{
                $response['success'] = TRUE;
                $response['answered'] = FALSE;
            }
        }else{
            $response['success'] = TRUE;
        }
        $response['previous_id'] = $p['previous_id'];
        $response['next_id'] = $p['next_id'];
        $response['mode'] = $p['mode'];
        $response['question_id'] = $p['q_id']; 

        $this->json_result($response);
    }

    public function next_step(){
        $p = $this->input->post();
        if($p['question_type_id'] == 5 || $p['question_type_id'] == 6){
            if($p['question_type_id'] == 5){
                if(isset($p['sh'])){
                    $no = 0;
                    foreach ($p['sh']['statement'] as $key => $value) {
                        $d[$value]['statement'] = $value;
                        $d[$value]['holder'] = $p['sh']['holder'][$no];
                        $no++;
                    }
                    foreach ($d as $key => $value) {
                        $statholder[] = $value;
                    }
                    $correct_answers = json_encode($statholder);
                    $result = $this->db->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['q_id'])->get('test_answers');
                    if($result->num_rows() == 0){
                        $ins = array(
                            'question_id' => $p['q_id'],
                            'test_transaction_id' => $p['test_transaction_id'],
                            'answers' => $correct_answers
                        );
                        $save = $this->db->insert('test_answers', $ins);
                        if($save){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }else{
                        $upd = array(
                            'answers' => $correct_answers
                        );
                        $update = $this->db->update('test_answers', $upd, array('test_transaction_id' => $p['test_transaction_id'], 'question_id' => $p['q_id']));
                        if($update){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }
                    $response['answered'] = TRUE;
                }else{
                    $response['success'] = TRUE;
                    $response['answered'] = FALSE;
                    $this->db->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['q_id'])->delete('test_answers');
                }
            }
            if($p['question_type_id'] == 6){
                if(isset($p['ner'])){
                    $correct_answers = json_encode($p['ner']);
                    $result = $this->db->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['q_id'])->get('test_answers');
                    if($result->num_rows() == 0){
                        $ins = array(
                            'question_id' => $p['q_id'],
                            'test_transaction_id' => $p['test_transaction_id'],
                            'answers' => $correct_answers
                        );
                        $save = $this->db->insert('test_answers', $ins);
                        if($save){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }else{
                        $upd = array(
                            'answers' => $correct_answers
                        );
                        $update = $this->db->update('test_answers', $upd, array('test_transaction_id' => $p['test_transaction_id'], 'question_id' => $p['q_id']));
                        if($update){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }
                }else{
                    $response['success'] = TRUE;
                    $response['answered'] = FALSE;
                }
            }
        }else{
            $check_answers = $this->db->where('question_id', $p['q_id'])->where('test_transaction_id', $p['test_transaction_id'])->count_all_results('test_answers');
            if($check_answers > 0){
                $response['answered'] = TRUE;
            }else{
                $response['answered'] = FALSE;
            }
            $response['success'] = TRUE;
        }
        $response['previous_id'] = $p['previous_id'];
        $response['next_id'] = $p['next_id'];
        $response['mode'] = $p['mode'];
        $response['question_id'] = $p['q_id'];
        $this->json_result($response);
    }

    public function applicant_download($file){
        $this->load->helper('download');
        $temp_name = $this->db->select('answers_temp_file')->where('answers', $file)->get('test_answers')->row_array();
        $data = file_get_contents('./files/applicant_upload/'.$file.'');
        force_download($temp_name['answers_temp_file'], $data);
    }
}