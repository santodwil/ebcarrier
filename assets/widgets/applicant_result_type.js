$(function () {

	'use restrict';

	$.fn.get_applicant = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            sdate : $(container + ' #sdate').val(),
            edate : $(container + ' #edate').val(),
            test_type_id : $(container + ' #test_type_id').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/applicant_result_type/widget/get_applicant_result',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search,
                sdate: p.sdate,
                edate: p.edate,
                test_type_id: p.test_type_id,
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		// t = '';
        		// var no = 0;
          //   	if(r.total > 0){
          //         t += '<div class="col-md-12">';
          //            t += '<table class="table table-hover table-striped table-condensed">';
          //               t += '<thead>';
          //                  t += '<tr>';
          //                     t += '<th width="400">Name</th>';
          //                     t += '<th>Vacancy</th>';
          //                     t += '<th width="500" class="text-center">Result</th>';
          //                  t += '</tr>';
          //               t += '</thead>';
          //               t += '<tbody>';
          //                  $.each(r.result, function (k,v){ 
          //                     t += '<tr>';
          //                        t += '<td><b>'+v.name+'</b><br>'+v.email+'</td>';
          //                        t += '<td>'+v.division_name+'</td>';
          //                        t += '<td class="text-center">';
          //                           $.each(v.test_result, function (kk,vv){
          //                              t += '<div class="col-md-4">';
          //                                 t += '<h4 class="bold no-margin text-center">'+vv.question_result+'</h4>';
          //                                 if(vv.question_type_id == 1 || vv.question_type_id == 7 || vv.question_type_id == 8){
          //                                    t += '<p class="small no-margin text-center">'+vv.question_type_name+'</p>';
          //                                 }else{
          //                                    t += '<p class="small no-margin text-center"><a href="" class="detail-result" data-applicantid="'+v.id+'" data-typeid="'+vv.question_type_id+'" data-name="'+v.name+'">'+vv.question_type_name+'</a></p>';
          //                                 }
          //                              t += '</div>';
          //                           });
          //                        t += '</td>';
          //                     t += '</tr>';
          //                  });
          //               t += '</tbody>';
          //            t += '</table>';
          //         t += '</div>';
          //   	}else{
          //   		t += '<p class="text-center font-arial">No Result</p>';
          //   	}
          //   	$(container + ' #section-total').html(r.total + ' Data Found');
          //   	$(container + ' #section-data').html(t);
          //   	$(container + ' #section-pagination').paging({
          //           items: r.total,
          //           currentPage: p.currentPage
          //       });
	        	hide_loading();
            }
        });
    }

    $(this).get_applicant();

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 9,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_applicant({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

    $('#range-date').daterangepicker({
        ranges: {
            'Last 7 Days': [moment().subtract('days', 7), moment()],
            'Last 30 Days': [moment().subtract('days', 30), moment()],
            'Last 3 Month': [moment().subtract('days', 90), moment()]
        },
        format: 'DD-MM-YYYY',
        startDate: moment().subtract('days', 29),
        endDate: moment(),
        isShowing: true
    },
	    function (start, end) {
	    	$('#range-date').html('<i class="fa fa-calendar"></i> '+start.format('DD MMM YYYY')+' sd '+end.format('DD MMM YYYY')+'');
	    	$('#sdate').val(start.format('YYYY-MM-DD'));
	        $('#edate').val(end.format('YYYY-MM-DD'));
	        // $(this).get_tests({
	        // 	load : true
	        // });
	    }
    );

});