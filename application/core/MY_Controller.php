<?php

class MY_Controller extends MX_Controller {
	protected $theme = 'pages';
    protected $theme_path = 'themes/pages/admin/';
    protected $theme_path_user = 'themes/pages/user/';
    protected $_user;
    protected $_role_id;
    protected $_message;
    protected $_post, $_get;

    public function __construct() {
        parent::__construct();
        if($this->config->item('maintenance_mode') == TRUE) {
            $this->load->view('maintenance_view');
            die();
        }
        $this->load->add_package_path(APPPATH . 'third_party/ion_auth/');
        $this->load->library('ion_auth');
        $this->_user = $this->ion_auth->user()->row();

        if($this->ion_auth->logged_in()){
            $this->_role_id = $this->db->select('group_id')->where('user_id', $this->_user->id)->get('users_groups')->row_array();
        }

        $this->_message = array(
            'message' => 'Some Error Occured, Please Try Again',
            'label' => 'danger'
        );
        $this->_post = $this->input->post();
        $this->_get = $this->input->get();
    }

    public function render_page($data = array(), $view = 'index') {
        $data['_user'] = $this->_user;
        $data['_post'] = $this->_post;
        $data['_get'] = $this->_get;
        if($this->_role_id['group_id'] == 1){
            $theme_dir = $this->theme_path;
        }else if($this->_role_id['group_id'] == 3){
            $theme_dir = $this->theme_path;
        }else{
            $theme_dir = $this->theme_path_user;
        }
        if (array_key_exists('HTTP_X_PJAX', $_SERVER) && $_SERVER['HTTP_X_PJAX'] && $view != 'login') {
            echo $this->load->view($theme_dir . 'pjax', $data, TRUE);
        } else {
            $this->load->view($theme_dir . $view, $data);
        }
    }

     public function render_widget($data, $css = FALSE) {
        $data['container'] = $this->input->get('container');
        $data['uniqid'] = uniqid();
        $data['widget_name'] = str_replace('/', '_', uri_string());
        if ($css) {
            $result['css'] = $this->router->fetch_module();
        }
        $result['html'] = $this->load->widget_view($this->router->fetch_module(), $data, TRUE);
        $result['sess'] = $this->ion_auth->logged_in() ? TRUE : FALSE;
        $this->json_result($result);
    }

    public function json_result($data) {
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            $data['sessionapp'] = FALSE;
        }else{
            $data['sessionapp'] = TRUE;
            $data['csrf'] = $this->security->get_csrf_hash();
        }
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }
}