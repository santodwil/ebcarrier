<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Library for Papi Kostick
 *
 * @author SUSANTO DWI LAKSONO
 */

class Papi_kostick{

	public function __construct() {
        $this->ci = & get_instance();
        $this->papi_card = array('G','L','I','T','V','S','R','D','C','E','N','A','P','X','B','O','Z','K','F','W');
        $this->master_result = array(
        	'G' => array(
				'3' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'4' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'5' => 'kemauan bekerja keras tinggi',
				'6' => 'kemauan bekerja keras tinggi',
				'7' => 'kemauan bekerja keras tinggi',
				'8' => 'kemauan bekerja keras tinggi',
				'9' => 'kemauan bekerja keras tinggi'
			),
			'L' => array(
				'0' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'1' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'2' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'3' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'4' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'5' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'6' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'7' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'8' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'9' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya'
			),
			'I' => array(
				'0' => 'ragu – menolak mengambil keputusan',
				'1' => 'ragu – menolak mengambil keputusan',
				'2' => 'ragu – menolak mengambil keputusan',
				'3' => 'berhati hati membuat keputusan',
				'4' => 'berhati hati membuat keputusan',
				'5' => 'berhati hati – lancar dan mudah mengambil keputusan',
				'6' => 'berhati hati – lancar dan mudah mengambil keputusan',
				'7' => 'berhati hati – lancar dan mudah mengambil keputusan',
				'8' => 'tidak ragu dalam mengambil keputusan',
				'9' => 'tidak ragu dalam mengambil keputusan'
			),
			'T' => array(
				'0' => 'melakukan segala sesuatu menurut kemauannya sendiri',
				'1' => 'melakukan segala sesuatu menurut kemauannya sendiri',
				'2' => 'melakukan segala sesuatu menurut kemauannya sendiri',
				'3' => 'tergolong aktif secara internal dan mental',
				'4' => 'tergolong aktif secara internal dan mental',
				'5' => 'tergolong aktif secara internal dan mental',
				'6' => 'tergolong aktif secara internal dan mental'
			),
			'V' => array(
				'0' => 'cenderung pasif',
				'1' => 'cenderung pasif',
				'2' => 'cenderung pasif',
				'3' => 'cenderung pasif',
				'4' => 'cenderung pasif',
				'5' => 'aktif secara fisik, cenderung sportif',
				'6' => 'cenaktif secara fisik, cenderung sportif',
				'7' => 'aktif secara fisik, cenderung sportif'
			),
			'S' => array(
				'0' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'1' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'2' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'3' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'4' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'5' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'6' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social',
				'7' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social',
				'8' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social',
				'9' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social'
			),
			'R' => array(
				'0' => 'kurang perhatian, bersifat praktis',
				'1' => 'kurang perhatian, bersifat praktis',
				'2' => 'kurang perhatian, bersifat praktis',
				'3' => 'kurang perhatian, bersifat praktis',
				'4' => 'kurang perhatian, bersifat praktis',
				'5' => 'nilai nilai penalaran tergolong tinggi',
				'6' => 'nilai nilai penalaran tergolong tinggi',
				'7' => 'nilai nilai penalaran tergolong tinggi',
				'8' => 'nilai nilai penalaran tergolong tinggi',
				'9' => 'nilai nilai penalaran tergolong tinggi'
			),
			'D' => array(
				'0' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'1' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'2' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'3' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'4' => 'minat tinggi untuk bekerja secara detail',
				'5' => 'minat tinggi untuk bekerja secara detail',
				'6' => 'minat tinggi untuk bekerja secara detail',
				'7' => 'minat tinggi untuk bekerja secara detail',
				'8' => 'minat tinggi untuk bekerja secara detail',
				'9' => 'minat tinggi untuk bekerja secara detail'
			),
			'C' => array(
				'0' => 'fleksibel – tidak teratur',
				'1' => 'fleksibel – tidak teratur',
				'2' => 'fleksibel – tidak teratur',
				'3' => 'teratur tetapi tidak tergolong fleksibel',
				'4' => 'teratur tetapi tidak tergolong fleksibel',
				'5' => 'teratur tetapi tidak tergolong fleksibel',
				'6' => 'keteraturan tinggi cenderung kaku',
				'7' => 'keteraturan tinggi cenderung kaku',
				'8' => 'keteraturan tinggi cenderung kaku',
				'9' => 'keteraturan tinggi cenderung kaku'
			),
			'E' => array(
				'0' => 'terbuka, cepat bereaksi, tidak normative',
				'1' => 'terbuka, cepat bereaksi, tidak normative',
				'2' => 'terbuka',
				'3' => 'terbuka',
				'4' => 'punya pendekatan emosional seimbang ,mampu mengendalikan',
				'5' => 'punya pendekatan emosional seimbang ,mampu mengendalikan',
				'6' => 'punya pendekatan emosional seimbang ,mampu mengendalikan',
				'7' => 'sangat normative, kebutuhan pengendalian diri yang berlebihan',
				'8' => 'sangat normative, kebutuhan pengendalian diri yang berlebihan',
				'9' => 'sangat normative, kebutuhan pengendalian diri yang berlebihan'
			),
			'N' => array(
				'0' => 'menunda atau menghindari pekerjaan',
				'1' => 'menunda atau menghindari pekerjaan',
				'2' => 'menunda atau menghindari pekerjaan',
				'3' => 'berhati hati atau ragu dalam bekerja',
				'4' => 'berhati hati atau ragu dalam bekerja',
				'5' => 'cukup bertanggung jawab pada pekerjaan',
				'6' => 'cukup bertanggung jawab pada pekerjaan',
				'7' => 'tekun, tanggung jawab tinggi',
				'8' => 'tekun, tanggung jawab tinggi',
				'9' => 'tekun, tanggung jawab tinggi'
			),
			'A' => array(
				'0' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'1' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'2' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'3' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'4' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'5' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'6' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi',
				'7' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi',
				'8' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi',
				'9' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi'
			),
			'P' => array(
				'0' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'1' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'2' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'3' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'4' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'5' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'6' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'7' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'8' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'9' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.'
			),
			'X' => array(
				'0' => 'cenderung pemalu',
				'1' => 'cenderung pemalu',
				'2' => 'rendah hati, tulus',
				'3' => 'rendah hati, tulus',
				'4' => 'memiliki pola perilaku yang unik',
				'5' => 'memiliki pola perilaku yang unik',
				'6' => 'membutuhkan perhatian nyata',
				'7' => 'membutuhkan perhatian nyata',
				'8' => 'membutuhkan perhatian nyata',
				'9' => 'membutuhkan perhatian nyata'
			),
			'B' => array(
				'0' => 'selektif',
				'1' => 'selektif',
				'2' => 'selektif',
				'3' => 'selektif',
				'4' => 'butuh diterima, tapi tidak mudah dipengaruhi kelompok',
				'5' => 'butuh diterima, tapi tidak mudah dipengaruhi kelompok',
				'6' => 'butuh disukai dan diakui, mudah dipengaruhi',
				'7' => 'butuh disukai dan diakui, mudah dipengaruhi',
				'8' => 'butuh disukai dan diakui, mudah dipengaruhi',
				'9' => 'butuh disukai dan diakui, mudah dipengaruhi'
			),
			'O' => array(
				'0' => 'tidak suka hubungan perorangan',
				'1' => 'tidak suka hubungan perorangan',
				'2' => 'tidak suka hubungan perorangan',
				'3' => 'sadar akan hubungan perorangan, tapi tidak terlalu tergantung',
				'4' => 'sadar akan hubungan perorangan, tapi tidak terlalu tergantung',
				'5' => 'sangat tergantung, butuh penerimaan diri',
				'6' => 'sangat tergantung, butuh penerimaan diri',
				'7' => 'sangat tergantung, butuh penerimaan diri',
				'8' => 'sangat tergantung, butuh penerimaan diri',
				'9' => 'sangat tergantung, butuh penerimaan diri'
			),
			'Z' => array(
				'0' => 'tidak suka berubah',
				'1' => 'tidak suka berubah',
				'2' => 'tidak suka berubah',
				'3' => 'tidak suka perubahan jika dipaksakan',
				'4' => 'tidak suka perubahan jika dipaksakan',
				'5' => 'mudah menyesuaikan diri',
				'6' => 'mudah menyesuaikan diri',
				'7' => 'membuat perubahan yang selektif, berfikir jauh kedepan',
				'8' => 'mudah gelisah, frustasi, karena segala sesuatu tidak berjalan fantastis',
				'9' => 'mudah gelisah, frustasi, karena segala sesuatu tidak berjalan fantastis'
			),
			'K' => array(
				'0' => 'menhindari masalah, menulak, untuk mengenali situasi sebagai masalah',
				'1' => 'menhindari masalah, menulak, untuk mengenali situasi sebagai masalah',
				'2' => 'menhindari masalah, menulak, untuk mengenali situasi sebagai masalah',
				'3' => 'suka lingkungan tanang, menghindari konflik',
				'4' => 'suka lingkungan tanang, menghindari konflik',
				'5' => 'keras kepala',
				'6' => 'agresi berhubungan dengan kerja, dorongan semangat bersaing',
				'7' => 'agresi berhubungan dengan kerja, dorongan semangat bersaing',
				'8' => 'agresif, cendering defensive',
				'9' => 'agresif, cendering defensive'
			),
			'F' => array(
				'0' => 'cenderung egois, kemungkinan bisa memberontak',
				'1' => 'cenderung egois, kemungkinan bisa memberontak',
				'2' => 'mengurus kepentingan sendiri',
				'3' => 'mengurus kepentingan sendiri',
				'4' => 'setia terhadap perusahaan',
				'5' => 'setia terhadap perusahaan',
				'6' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis',
				'7' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis',
				'8' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis',
				'9' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis'
			),
			'W' => array(
				'0' => 'berorientasi pada tujuan, mandiri',
				'1' => 'berorientasi pada tujuan, mandiri',
				'2' => 'berorientasi pada tujuan, mandiri',
				'3' => 'berorientasi pada tujuan, mandiri',
				'4' => 'kebutuhan akan pengarahan dan harapan yang dirumuskan untuknya',
				'5' => 'kebutuhan akan pengarahan dan harapan yang dirumuskan untuknya',
				'6' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas',
				'7' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas',
				'8' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas',
				'9' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas'
			)
    	);
    }

    public function counting($test_transaction_id){
        foreach ($this->papi_card as $v) {
            $data[$v] = $this->ci->db->where('answers', $v)->where('test_transaction_id', $test_transaction_id)->count_all_results('test_answers');
        }
        return $data;
    }


    
    


}