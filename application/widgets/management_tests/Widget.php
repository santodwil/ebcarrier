<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('management_tests'); 
        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Tests',
        );
        $this->render_widget($data);
    }

    public function get_tests(){
        $params = $this->input->get();
        $response['result'] = $this->management_tests->get_tests('get', $params);
        $response['total'] = $this->management_tests->get_tests('count', $params);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function change_status(){
        $post = $this->input->post();
        if(isset($post['mode']) && isset($post['id'])){
            if($post['mode'] == 'active'){
                $update = $this->db->update('test_type', array('status' => 1), array('id' => $post['id']));
            }else{
                $update = $this->db->update('test_type', array('status' => 0), array('id' => $post['id']));
            }
            if($update){
                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
            }
        }else{
            $response['success'] = FALSE;
        }
        $this->json_result($response);
    }

    public function change_selected(){
        $post = $this->input->post();
        $rows = array(
            'status' => $post['value']
        );
        $this->db->where_in('id', $post['data']);
        $this->db->update('test_type', $rows);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function save(){
        $post = $this->input->post();
        $guidetest = isset($post['guidetest']) && $post['guidetest'] != '' ? $post['guidetest'] : NULL;
        $entity = array(
            'name' => $post['name'],
            'total_question' => $post['question'],
            'time' => $post['times'],
            'description' => NULL,
            'guide_tests' => $guidetest,
            'status' => 1,
            'sort' => 5
        );
        $insert = $this->db->insert('test_type', $entity);
        if($insert){
            $response['success'] = TRUE;
            $response['msg'] = 'New Data Added';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transaction Failed';
        }
        
        $this->json_result($response);
    }

    public function change(){
        $post = $this->input->post();
        $description = isset($post['description']) && $post['description'] != '' ? $post['description'] : NULL;
        $guidetest = isset($post['guidetest']) && $post['guidetest'] != '' ? $post['guidetest'] : NULL;
        $entity = array(
            'name' => $post['name'],
            'total_question' => $post['question'],
            'time' => $post['times'],
            'description' => $description,
            'guide_tests' => $guidetest,
            'status' => 1,
            'sort' => 5
        );
        $update = $this->db->update('test_type', $entity, array('id' => $post['id']));
        if($update){
            $response['success'] = TRUE;
            $response['msg'] = 'Data Updated';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transaction Failed';
        }
        
        $this->json_result($response);
    }

    public function modify_tests(){
        $params = $this->input->post();
        $response['test_type'] = $this->db->where('id', $params['id'])->get('test_type')->row_array();
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function remove_test(){
        $params = $this->input->post();
        $remove = $this->management_tests->remove_test($params['id']); 
        if($remove){
            $response['success'] = TRUE;
            $response['msg'] = 'Data Removed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Transacation Failed';
        }
        $this->json_result($response);
    }

}