from ConfigParser import ConfigParser
from controller.compareMediaController import CompareMediaController
from datetime import datetime, timedelta
from dateutil import rrule
from time import sleep
import argparse
import calendar

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Main Compare Topic',
                                        formatter_class=argparse.RawDescriptionHelpFormatter)
    argparser.add_argument('-m', '--mode', help='Mode', metavar='', default=None, type=str)
    argparser.add_argument('-c', '--config', help='Config', metavar='', default='config.conf', type=str)
    argparser.add_argument('-d', '--date', help='Date (YYYYMMDD)', metavar='', default=None, type=str)
    argparser.add_argument('-sd', '--sdate', help='Start Date (YYYYMMDD)', metavar='', default=None, type=str)
    argparser.add_argument('-ed', '--edate', help='End Date (YYYYMMDD)', metavar='', default=None, type=str)
    argparser.add_argument('-cl', '--cluster', help='Cluster [bigdata, cloud]', metavar='', default='bigdata', type=str)
    argparser.add_argument('-tid', '--topic_id', help='Topic ID', metavar='', default=None, type=str)
    argparser.add_argument('-wid', '--workspace_id', help='Workspace ID', metavar='', default=None, type=str)
    argparser.add_argument('-t', '--type', help='Type', metavar='', default='statement', type=str)
    argparser.add_argument('-mo', '--month', help='Month', metavar='', default=None, type=str)
    argparser.add_argument('-ye', '--year', help='Year', metavar='', default=None, type=str)
    argparser.add_argument('-sl', '--sleep', help='Sleep', metavar='', default=20, type=int)
    args = argparser.parse_args()

    config = ConfigParser()
    config.read(args.config)
    compareMedia = CompareMediaController(config_file=args.config, log_name='compare_media')

    sdate = datetime.now()
    edate = datetime.now()

    if args.month and args.year:
        _, days_in_month = calendar.monthrange(int(args.year), int(args.month))
        sdate = datetime.now().replace(year=int(args.year), month=int(args.month), day=1)
        edate = sdate + timedelta(days=days_in_month - 1)

    if args.edate:
        if int(args.edate) > 0:
            edate = datetime.strptime(args.edate, '%Y%m%d')
        else:
            edate = datetime.now() + timedelta(days=int(args.edate))

    if args.sdate:
        if int(args.sdate) > 0:
            sdate = datetime.strptime(args.sdate, '%Y%m%d')
        else:
            sdate = edate + timedelta(days=int(args.sdate))

    if args.date:
        edate = datetime.strptime(args.date, '%Y%m%d')
        sdate = datetime.strptime(args.date, '%Y%m%d')

    daterange = [d.strftime('%Y%m%d') for d in list(rrule.rrule(rrule.DAILY, dtstart=sdate, until=edate))]

    if args.mode == "media":
        for date in daterange:
            compareMedia.compare_media(args.cluster, date)
            sleep(int(args.sleep))
    else:
        print argparser.print_help()
