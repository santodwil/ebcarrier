<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Library for Assessment result
 *
 * @author SUSANTO DWI LAKSONO
 */

class Assessment{

	public function __construct() {
        $this->ci = & get_instance();
        $this->_master_psikotes_rules = array(
        	'ENFJ' => 'Bersifat hangat, berempati, pendengar yang baik, dan bertanggung jawab. Sangat menyesuaikan diri dengan emosi, kebutuhan, dan motivasi dari orang lain. Mereka melihat potensi pada setiap orang, dan ingin membantu mereka untuk mencapai potensi mereka. Dapat bertindak sebagai pendorong untuk pertumbuhan individu dan kelompok. Loyal, mau mendengarkan pujian ataupun kritik. Suka bergaul, memudahkan orang lain dalam kelompok mereka, dan menghadirkan kepemimpinan yang bersemangat.',
        	'ENFP' => 'Sosok hangat yang memiliki ansuiasme dan imaginatif. Melihat kehidupan sebagai sesuatu yang penuh kemungkinan. Mampu memahami hubungan antara kejadian dan informasi dengan sangat mudah, dan percaya diri melakukan sesuatu berdasarkan pola yang mereka lihat. Menginginkan banyak pengakuan dari orang lain, dan siap memberikan apresiasi dan dukungan. Spontan dan fleksibel, seringkali mengandalkan kemampuan mereka dalam berimprovisasi dan kefasihan lisan mereka.',
        	'ENTJ' => 'Berterus terang, menentukan, siap memikul kepemimpinan. Dengan mudah melihat prosedur atau kebijakan yang tidak efisien dan tidak logis, mampu mengembangkan dan mengimplementasikan sistem yang luas untuk menyelesaikan masalah-masalah organisasi. Menyukai rencana jangka panjang, dan penetappan tujuan. Biasanya berpengetahuan luas, pembaca yang baik, senang mengembangkan pengetahuan mereka dan menyampaikannya kepada orang lain. Terkadang memaksa dalam menyajikan ide-ide mereka.',
        	'ENTP' => 'Cepat, berbakat, pendorong, siaga, and blak-blakan. Sanggup untuk memecahkan masalah yang menantang. Dapat menganalisa kemungkinan secara strategis. Mampu membaca orang lain. Jenuh dengan rutinitas, tidak tertarik melakukan hal yg sama berulang-ulang, lalu mencoba hal yang menarik minatnya. Good at reading other people.',
        	'ESFJ' => 'Bersahabat, bersungguh-sungguh, dan dapat bekerja sama. Menginginkan keharmonisan dalam lingkungan mereka, mereka bekerja dengan kebulatan tekad. Senang bekerja dengan orang lain untuk menyelesaikan tugas-tugas dengan teliti, dan tepat waktu. Loyal, melaksanakan tugas hingga hal-hal kecil sekalipun. Memperhatikan apa yang menjadi kebutuhan orang lain dalam keseharian mereka dan, mencoba untuk memenuhi itu semua. Ingin dihargai untuk siapa diri mereka dan segala kontribusi mereka.',
        	'ESFP' => 'Ramah, bersahabat, dan menerima. Sosok yang mencintai kehidupan, orang, dan kenyamanan materi. Senang bekerja dengan orang lain dalam menyelesaikan sesuatu. menggunakan akal sehat dan pendekatan realistis dalam pekerjaan, dan menjadikan pekerjaan sebagai sebuah kesenangan. Fleksibel dan spontan, beradaptasi dengan mudah ke orang-orang dan lingkungan baru . Cara belajar terbaik mereka adalah dengan mencoba keterampilan baru bersama orang lain.',
        	'ESTJ' => 'Praktis, realistis, berpegang pada fakta. Tegas, dengan cepat mengimplementasikan keputusan. Untuk menyelesaikan sesuatu mereka mampu mengatur pekerjaan, dan orang lain, terfokus untuk mendapatkan hasil dengan cara yang memungkinkan dan paling efisien. Selalu menjaga detail rutinitas. Memiliki standart logika yang jelas, yang secara sistematis menuntun mereka, dan mereka ingin orang-oranglain juga menggunakan standart logika itu. Terkadang memaksa agar rencana mereka dapat terimplementasikan.',
        	'ESTP' => 'Fleksibel dan Toleran, mereka mengambil pendekatan praktis yang berfokus pada hasil yang langsung. Teori dan penjelasan konseptual membosankan bagi mereka ? mereka biasanya bertindak energik untuk memecahkan masalah. Berfokus pada dimana mereka saat ini, dan sekarang, bersifat sponta, menikmati saat-saat dimana mereka bisa aktif berinteraksi dengan orang lain. Menikmati kenyamanan material dan gaya. Mereka belajar sangat baik belajar dengan melakukan sesuatu.',
            'INFJ' => 'Pencari makna dan hubungannya dengan ide-ide, hubungan sosial, dan pekerjaan. Selalu ingin memahami tentang pola pikir orang lain, dan belajar tentang apa yang memotivasi seseorang dalam kehidupan. Seorang yang bersungguh-sungguh dan berkomitmen dengan apa yang mereka kerjakan. Memiliki misi yang jelas tentang bagaimana cara terbaik untuk melayani kepentingan umum. Terorganisir dan Tegas dalam melaksanakan visi mereka.',
            'INFP' => 'Idealis, setia kepada prinsip yang mereka genggam serta orang-orang yang penting bagi mereka. Ingin kehidupan yang selaras dengan prinsip yang mereka genggam. Ingin tahu, mudah untuk melihat kemungkinan, bisa menjadi pemicu untuk memulai mewujudkan ide-ide. Berusaha untuk memahami orang lain, dan membantu mereka untuk mencapai potensi mereka. Mudah beradaptasi, fleksibel, dan mudah menerima, kecuali jika itu bertentangan dengan prinsip-prinsip mereka.',
            'ISFJ' => 'Tenang, ramah, bertanggung jawab, dan teliti. Berkomitmen dan bersungguh-sungguh dalam memenuhi kewajibannya. Cermat, telaten, dan akurat. Loyal, baik hati, perhatian dan selalu mengingat secara spesifik tentang orang-orang yang penting bagi mereka, peduli dengan perasaan orang lain. Berupaya untuk menciptakan lingkungan yang tertib dan harmonis di tempat kerja maupun di rumah.',
            'ISFP' => 'Tenang, ramah, sensitif, dan baik hati. Menikmati apa yang sedang terjadi saat ini, apa yang terjadi di sekitar mereka. Ingin memiliki ruang mereka sendiri dan bekerja dalam rentang waktu mereka sendiri. Loyal dan berkomitmen untuk prinsip yang mereka genggam serta orang-orang yang penting bagi mereka. Tidak menyukai perselisihan dan konflik, tidak memaksakan pendapat atau prinsip mereka pada orang lain.',
            'ISTJ' => 'Tenang, serius, meraih kesuksesannya dengan ketelitian dan kehandalannya dalam bekerja. Praktis, orienatsi pada fakta, realistis, dan bertanggung jawab. Memutuskan apa yang harus dilakukan dengan logis, dan bekerja dengan tekun pada satu hal, tanpa mempedulikan gangguan. Mereka merasa senang dengan segala sesuatu yang tertib dan teratur pada pekerjaan mereka, rumah mereka, dan kehidupan mereka. Memegang nilai-nilai tradisi dan loyalitas.',
            'ISTP' => 'Bersikap toleran, dan fleksibel, pengamat yang tenang sampai masalah tampak, kemudian bertindak cepat untuk menemukan solusi yang terbaik. Menganalisis apa yang membuat sesuatu bekerja, dan mampu dengan segera menemukan berbagai data untuk mengisolasi inti dari masalah. Tertarik dengan sebab dan akibat, mengolah fakta menggunakan prinsip masuk akal, dan efisien.',
            'INTP' => 'Berusaha membangun penjelasan yang masuk akal untuk segala sesuatu yang menarik bagi mereka. Teoritis dan abstrak, lebih tertarik pada ide-ide daripada interaksi sosial. Tenang, cerdas, fleksibel, dan mudah beradaptasi. Memiliki kemampuan yang tidak biasa untuk fokus dan mendalami pemecahan masalah pada bidang yang menjadi minat mereka. Skeptis, terkadang kritis, dan selalu analitis.',
            'INTJ' => 'Memiliki pemikiran yang orisinil dan motivasi yang kuat untuk menerapkan ide-ide mereka hingga mencapai tujuan. Cepat melihat pola dalam peristiwa yang terjadi disekitar mereka, dan mampu menyusun perspektif jangka panjang yang jelas. Bila hendak melakukan sesuatu, ia akan mengorganisir lalu segera melaksanakannya. Sering kali bersifat skeptis namun mandiri, memiliki standar kompetensi dan kinerja yang tinggi untuk diri mereka sendiri maupun orang lain.',
    	);
		$this->master_papi_kostick_rules = array(
        	'G' => array(
				'0' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'1' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'2' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'3' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'4' => 'bekerja untuk kesenangan saja, bukan hasil optimal',
				'5' => 'kemauan bekerja keras tinggi',
				'6' => 'kemauan bekerja keras tinggi',
				'7' => 'kemauan bekerja keras tinggi',
				'8' => 'kemauan bekerja keras tinggi',
				'9' => 'kemauan bekerja keras tinggi'
			),
			'L' => array(
				'0' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'1' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'2' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'3' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'4' => 'cendurung tidak secara aktif menggunakan orang lain dalam bekerja',
				'5' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'6' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'7' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'8' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya',
				'9' => 'yaitu tingkat dimana seseorang memproyeksikan dirinya sebagai pemimpin suatu tingkat dimana ia mencoba menggunakan orang lain untuk mencapai tujuannya'
			),
			'I' => array(
				'0' => 'ragu – menolak mengambil keputusan',
				'1' => 'ragu – menolak mengambil keputusan',
				'2' => 'ragu – menolak mengambil keputusan',
				'3' => 'berhati hati membuat keputusan',
				'4' => 'berhati hati membuat keputusan',
				'5' => 'berhati hati – lancar dan mudah mengambil keputusan',
				'6' => 'berhati hati – lancar dan mudah mengambil keputusan',
				'7' => 'berhati hati – lancar dan mudah mengambil keputusan',
				'8' => 'tidak ragu dalam mengambil keputusan',
				'9' => 'tidak ragu dalam mengambil keputusan'
			),
			'T' => array(
				'0' => 'melakukan segala sesuatu menurut kemauannya sendiri',
				'1' => 'melakukan segala sesuatu menurut kemauannya sendiri',
				'2' => 'melakukan segala sesuatu menurut kemauannya sendiri',
				'3' => 'tergolong aktif secara internal dan mental',
				'4' => 'tergolong aktif secara internal dan mental',
				'5' => 'tergolong aktif secara internal dan mental',
				'6' => 'tergolong aktif secara internal dan mental'
			),
			'V' => array(
				'0' => 'cenderung pasif',
				'1' => 'cenderung pasif',
				'2' => 'cenderung pasif',
				'3' => 'cenderung pasif',
				'4' => 'cenderung pasif',
				'5' => 'aktif secara fisik, cenderung sportif',
				'6' => 'cenaktif secara fisik, cenderung sportif',
				'7' => 'aktif secara fisik, cenderung sportif'
			),
			'S' => array(
				'0' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'1' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'2' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'3' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'4' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'5' => 'perhatian rendah terhadap hubungan social, kurang percaya pada orang lain',
				'6' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social',
				'7' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social',
				'8' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social',
				'9' => 'kepercayaan tinggu dalam hubungan social, suka interaksi social'
			),
			'R' => array(
				'0' => 'kurang perhatian, bersifat praktis',
				'1' => 'kurang perhatian, bersifat praktis',
				'2' => 'kurang perhatian, bersifat praktis',
				'3' => 'kurang perhatian, bersifat praktis',
				'4' => 'kurang perhatian, bersifat praktis',
				'5' => 'nilai nilai penalaran tergolong tinggi',
				'6' => 'nilai nilai penalaran tergolong tinggi',
				'7' => 'nilai nilai penalaran tergolong tinggi',
				'8' => 'nilai nilai penalaran tergolong tinggi',
				'9' => 'nilai nilai penalaran tergolong tinggi'
			),
			'D' => array(
				'0' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'1' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'2' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'3' => 'menyadari kebutuhan akan kecermatan, tetapi tidak berminat bekerja detail',
				'4' => 'minat tinggi untuk bekerja secara detail',
				'5' => 'minat tinggi untuk bekerja secara detail',
				'6' => 'minat tinggi untuk bekerja secara detail',
				'7' => 'minat tinggi untuk bekerja secara detail',
				'8' => 'minat tinggi untuk bekerja secara detail',
				'9' => 'minat tinggi untuk bekerja secara detail'
			),
			'C' => array(
				'0' => 'fleksibel – tidak teratur',
				'1' => 'fleksibel – tidak teratur',
				'2' => 'fleksibel – tidak teratur',
				'3' => 'teratur tetapi tidak tergolong fleksibel',
				'4' => 'teratur tetapi tidak tergolong fleksibel',
				'5' => 'teratur tetapi tidak tergolong fleksibel',
				'6' => 'keteraturan tinggi cenderung kaku',
				'7' => 'keteraturan tinggi cenderung kaku',
				'8' => 'keteraturan tinggi cenderung kaku',
				'9' => 'keteraturan tinggi cenderung kaku'
			),
			'E' => array(
				'0' => 'terbuka, cepat bereaksi, tidak normative',
				'1' => 'terbuka, cepat bereaksi, tidak normative',
				'2' => 'terbuka',
				'3' => 'terbuka',
				'4' => 'punya pendekatan emosional seimbang ,mampu mengendalikan',
				'5' => 'punya pendekatan emosional seimbang ,mampu mengendalikan',
				'6' => 'punya pendekatan emosional seimbang ,mampu mengendalikan',
				'7' => 'sangat normative, kebutuhan pengendalian diri yang berlebihan',
				'8' => 'sangat normative, kebutuhan pengendalian diri yang berlebihan',
				'9' => 'sangat normative, kebutuhan pengendalian diri yang berlebihan'
			),
			'N' => array(
				'0' => 'menunda atau menghindari pekerjaan',
				'1' => 'menunda atau menghindari pekerjaan',
				'2' => 'menunda atau menghindari pekerjaan',
				'3' => 'berhati hati atau ragu dalam bekerja',
				'4' => 'berhati hati atau ragu dalam bekerja',
				'5' => 'cukup bertanggung jawab pada pekerjaan',
				'6' => 'cukup bertanggung jawab pada pekerjaan',
				'7' => 'tekun, tanggung jawab tinggi',
				'8' => 'tekun, tanggung jawab tinggi',
				'9' => 'tekun, tanggung jawab tinggi'
			),
			'A' => array(
				'0' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'1' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'2' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'3' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'4' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'5' => 'ketidakpastian tujuan, kepuasan dalam suatu pekerjaan, tidak ada usaha lebih',
				'6' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi',
				'7' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi',
				'8' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi',
				'9' => 'tujuan jelas, kubutuhan sukses dan ambisi tinggi'
			),
			'P' => array(
				'0' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'1' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'2' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'3' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'4' => 'menurunnya keinginan untuk bertanggung jawab pada pekerjaan dan tindakan orang lain.',
				'5' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'6' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'7' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'8' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.',
				'9' => 'tingkat kebutuhan untuk menerima tanggung jawab orang lain, menjadi orang yang bertanggung jawab.'
			),
			'X' => array(
				'0' => 'cenderung pemalu',
				'1' => 'cenderung pemalu',
				'2' => 'rendah hati, tulus',
				'3' => 'rendah hati, tulus',
				'4' => 'memiliki pola perilaku yang unik',
				'5' => 'memiliki pola perilaku yang unik',
				'6' => 'membutuhkan perhatian nyata',
				'7' => 'membutuhkan perhatian nyata',
				'8' => 'membutuhkan perhatian nyata',
				'9' => 'membutuhkan perhatian nyata'
			),
			'B' => array(
				'0' => 'selektif',
				'1' => 'selektif',
				'2' => 'selektif',
				'3' => 'selektif',
				'4' => 'butuh diterima, tapi tidak mudah dipengaruhi kelompok',
				'5' => 'butuh diterima, tapi tidak mudah dipengaruhi kelompok',
				'6' => 'butuh disukai dan diakui, mudah dipengaruhi',
				'7' => 'butuh disukai dan diakui, mudah dipengaruhi',
				'8' => 'butuh disukai dan diakui, mudah dipengaruhi',
				'9' => 'butuh disukai dan diakui, mudah dipengaruhi'
			),
			'O' => array(
				'0' => 'tidak suka hubungan perorangan',
				'1' => 'tidak suka hubungan perorangan',
				'2' => 'tidak suka hubungan perorangan',
				'3' => 'sadar akan hubungan perorangan, tapi tidak terlalu tergantung',
				'4' => 'sadar akan hubungan perorangan, tapi tidak terlalu tergantung',
				'5' => 'sangat tergantung, butuh penerimaan diri',
				'6' => 'sangat tergantung, butuh penerimaan diri',
				'7' => 'sangat tergantung, butuh penerimaan diri',
				'8' => 'sangat tergantung, butuh penerimaan diri',
				'9' => 'sangat tergantung, butuh penerimaan diri'
			),
			'Z' => array(
				'0' => 'tidak suka berubah',
				'1' => 'tidak suka berubah',
				'2' => 'tidak suka berubah',
				'3' => 'tidak suka perubahan jika dipaksakan',
				'4' => 'tidak suka perubahan jika dipaksakan',
				'5' => 'mudah menyesuaikan diri',
				'6' => 'mudah menyesuaikan diri',
				'7' => 'membuat perubahan yang selektif, berfikir jauh kedepan',
				'8' => 'mudah gelisah, frustasi, karena segala sesuatu tidak berjalan fantastis',
				'9' => 'mudah gelisah, frustasi, karena segala sesuatu tidak berjalan fantastis'
			),
			'K' => array(
				'0' => 'menhindari masalah, menulak, untuk mengenali situasi sebagai masalah',
				'1' => 'menhindari masalah, menulak, untuk mengenali situasi sebagai masalah',
				'2' => 'menhindari masalah, menulak, untuk mengenali situasi sebagai masalah',
				'3' => 'suka lingkungan tanang, menghindari konflik',
				'4' => 'suka lingkungan tanang, menghindari konflik',
				'5' => 'keras kepala',
				'6' => 'agresi berhubungan dengan kerja, dorongan semangat bersaing',
				'7' => 'agresi berhubungan dengan kerja, dorongan semangat bersaing',
				'8' => 'agresif, cendering defensive',
				'9' => 'agresif, cendering defensive'
			),
			'F' => array(
				'0' => 'cenderung egois, kemungkinan bisa memberontak',
				'1' => 'cenderung egois, kemungkinan bisa memberontak',
				'2' => 'mengurus kepentingan sendiri',
				'3' => 'mengurus kepentingan sendiri',
				'4' => 'setia terhadap perusahaan',
				'5' => 'setia terhadap perusahaan',
				'6' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis',
				'7' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis',
				'8' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis',
				'9' => 'bersikap setia dan membantu, kemungkinan bantuannya bersifat politis'
			),
			'W' => array(
				'0' => 'berorientasi pada tujuan, mandiri',
				'1' => 'berorientasi pada tujuan, mandiri',
				'2' => 'berorientasi pada tujuan, mandiri',
				'3' => 'berorientasi pada tujuan, mandiri',
				'4' => 'kebutuhan akan pengarahan dan harapan yang dirumuskan untuknya',
				'5' => 'kebutuhan akan pengarahan dan harapan yang dirumuskan untuknya',
				'6' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas',
				'7' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas',
				'8' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas',
				'9' => 'meningkatnya orientasi terhadap tugas dan membutuhkan instruksi yang jelas'
			)
    	);
	
    }


    public function summary($params){
    	$test_groups = $this->test_groups($params['vacancy_id']);
    	foreach ($test_groups as $k => $v) {
    		$test_transaction_id = $this->get_transaction($params['applicant_id'], $v['test_type_id']);
    		$result[$v['test_type_id']]['name'] = $v['name'];
    		$result[$v['test_type_id']]['result'] = $test_transaction_id ? $this->get_result($test_transaction_id['id']) : FALSE;
    		$result[$v['test_type_id']]['calculation'] = $test_transaction_id ? $this->calculation($test_transaction_id['id']) : FALSE;
    	}
    	return $result;
    }

    public function test_groups($vacancy_id){
    	$this->ci->db->select('a.test_type_id, b.name');
    	$this->ci->db->join('test_type as b', 'a.test_type_id = b.id');
    	$this->ci->db->where_not_in('a.test_type_id', array(1,4));
    	$this->ci->db->where('a.vacancy_division_id', $vacancy_id);
    	$rs = $this->ci->db->get('test_groups as a');
    	return $rs->num_rows() > 0 ? $rs->result_array() : FALSE;
    }

    public function get_transaction($applicant_id, $test_type_id){
		$this->ci->db->select('id');
		$this->ci->db->where('applicant_id', $applicant_id);
		$this->ci->db->where('test_type_id', $test_type_id);
		$rs = $this->ci->db->get('test_transaction');
		return $rs->num_rows() > 0 ? $rs->row_array() : FALSE;
    }

    public function get_result($test_transaction_id){
    	$this->ci->db->select('a.*, b.name as answers_type_name');
    	$this->ci->db->join('question_type as b', 'a.question_type_id = b.id');
    	$this->ci->db->where('a.test_transaction_id', $test_transaction_id);
    	$rs = $this->ci->db->get('test_assessment as a');
    	if($rs->num_rows() > 0){
    		foreach ($rs->result_array() as $k => $v) {
    			$result[$v['id']]['answers_type_name'] = $v['answers_type_name'];
    			$result[$v['id']]['assessment'] = $v['assessment'];
    		}
    		return $result;
    	}else{
    		return FALSE;
    	}
    }

    public function calculation($test_transaction_id){
    	$this->ci->db->select('sum(assessment) as calculation');
		$this->ci->db->where('test_transaction_id', $test_transaction_id);
    	$rs = $this->ci->db->get('test_assessment')->row_array();
    	return $rs['calculation'];
    }

    public function psikotes($params){
    	$test_transaction_id = $this->get_transaction($params['applicant_id'], 1);
    	if($test_transaction_id){
    		$assessment = $this->psikotes_result($test_transaction_id['id']);
    		return $assessment ? $this->_master_psikotes_rules[$assessment] : FALSE;
    	}else{
    		return FALSE;
    	}
    }

    public function papi_kostick($params){
    	$test_transaction_id = $this->get_transaction($params['applicant_id'], 4);
    	if($test_transaction_id){
    		$assessment = $this->psikotes_result($test_transaction_id['id']);
    		if($assessment){
    			foreach (json_decode($assessment) as $key => $value) {
    				$data[] = $this->master_papi_kostick_rules[$key][$value];
    			}
    			return $data;
    		}else{
    			return FALSE;
    		}
    	}else{
    		return FALSE;
    	}
    }

    public function psikotes_result($test_transaction_id){
    	$this->ci->db->select('assessment');
		$this->ci->db->where('test_transaction_id', $test_transaction_id);
    	$rs = $this->ci->db->get('test_assessment')->row_array();
    	return $rs['assessment'];
    }

}