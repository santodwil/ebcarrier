$(function () {

	'use restrict';

	$.fn.applicant_result = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            vacancy : $(container + ' #vacancy').val(),
            sdate : $(container + ' #sdate').val(),
            edate : $(container + ' #edate').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/result_applicant/widget/applicant_result',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search,
                vacancy: p.vacancy,
                sdate: p.sdate,
                edate: p.edate
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
        		var no = 0;
            	if(r.total > 0){
                   $.each(r.result, function (k,v){ 
                   	p.offset++;
                      t += '<tr>';
                         t += '<td>'+v.name+'</td>';
                         t += '<td>'+v.vacancy_name+'</td>';
                         t += '<td>'+v.contact_number+'</td>';
                         t += '<td>'+v.email+'</td>';
                         t += '<td>'+v.created+'</td>';
                         if(v.status){
                            if(v.status == 1){
                                t += '<td><span class="label label-danger bold">Not Pass</span></td>';
                            }
                             if(v.status == 2){
                                t += '<td><span class="label label-success bold">Pass</span></td>';
                            }
                         }else{
                            t += '<td><span class="label label-default">None</span></td>';
                         }
                         t += '<td class="text-center">';
                            t += '<button type="button" class="btn btn-danger btn-detail btn-sm" data-id="'+v.id+'" data-vacancy="'+v.vacancy_id+'">Result</button>';
                         t += '</td>';
                      t += '</tr>';
                   });
            	}else{
            		t += '<tr><td colspan="7"><p class="text-center">No Result</p></td></tr>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	hide_loading();
            }
        });
    }

    $.fn.detail_result = function(params) {
        var p = $.extend({
        	applicant_id: null,
        	vacancy_id: null
        }, params);
        ajaxManager.addReq({
            url: site_url + '/result_applicant/widget/detail_result',
            type: 'GET',
            dataType: 'JSON',
            data: {
                vacancy_id: p.vacancy_id,
                applicant_id: p.applicant_id
            },
            beforeSend: function () {
                $(container + ' .btn-detail[data-id="'+p.applicant_id+'"]').addClass('disabled');
                $(container + ' .btn-detail[data-id="'+p.applicant_id+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
            	$(container +' .applicant-name').html(r.applicant.name);
            	$(container +' .applicant-email').html(r.applicant.email);
            	$(container +' .applicant-contact-number').html(r.applicant.contact_number);
            	$(container +' .applicant-education-degree').html(r.applicant.education_degree);
            	$(container +' .applicant-school-majors').html(r.applicant.school_majors);
            	$(container +' .applicant-university').html(r.applicant.university);
            	$(container +' .applicant-vacancy').html(r.applicant.vacancy_name);
            	if(r.applicant.applicant_file){
            		$(container +' .applicant-cv').html('<a href="'+site_url+'/dashboard/widget/download/'+r.applicant.applicant_file+'">'+r.applicant.applicant_temp_file+'</a>');
            	}

            	t = '';
            	t += '<ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">';
	            	t += '<li class="active">';
	            		t += '<a class="text-danger" style="font-weight: bold;" data-toggle="tab" href="#assessment" aria-expanded="false"><span><i class="fa fa-edit"></i> Assessment</span></a>';
	            	t += '</li>';
	            	t += '<li>';
	            		t += '<a class="text-danger" style="font-weight: bold;" data-toggle="tab" href="#personality" aria-expanded="false"><span><i class="fa fa-edit"></i> Personality</span></a>';
	            	t += '</li>';
            	t += '</ul>';
            	t += '<div class="tab-content" style="height: 400px;overflow: auto;">';
            		t += '<div class="tab-pane active" id="assessment">';
            			$.each(r.assessment, function (k,v){
        					t += '<div class="row" style="border: 2px solid #f7f7f7;border-radius: 5px;padding-left: 10px;padding-bottom: 10px;margin:5px;">';
        						t += '<h5 class="bold" style="font-size:16px;">'+v.name+'</h5>';
        						if(v.result){
        							$.each(v.result, function (kk,vv){
	        							t += '<div class="col-md-3">';
	        								t += '<h3 class="bold text-success no-margin text-center">'+vv.assessment+'</h1>';
	        								t += '<h6 class="bold font-arial no-margin text-center">'+vv.answers_type_name+'</h3>';
	            						t += '</div>';
	        						});
	        						if(v.calculation){
		        						t += '<div class="col-md-3">';
	        								t += '<h3 class="bold text-danger no-margin text-center">'+v.calculation+'</h1>';
	        								t += '<h6 class="bold font-arial no-margin text-center">Calculation</h3>';
	            						t += '</div>';	
            						}
        						}else{
        							t += '<h6 style="font-size: 15px;">No Result</h6>';
        						}
            				t += '</div>';
            			}); 
            		t += '</div>';
            		t += '<div class="tab-pane fade" id="personality">';
            			if(r.psikotes){
            				t += '<h5 class="bold">Type Personality</h5>';
            				t += '<ul class="lg-icon">';
            					t += '<li><span>'+r.psikotes+'</span></li>';
            				t += '</ul>';
            			}
            			if(r.papi_kostick){
        					t += '<h5 class="bold">Self Report Inventory</h5>';
        					t += '<ul class="lg-icon">';
	            				$.each(r.papi_kostick, function (k,v){
	            					t += '<li><span>'+v+'</span></li>';
	            				});
            				t += '</ul>';
            			}
            		t += '</div>';
            	t += '</div>';
            	$(container +' .result-detail').html(t);
                $(container + ' .btn-detail[data-id="'+p.applicant_id+'"]').removeClass('disabled');
                $(container + ' .btn-detail[data-id="'+p.applicant_id+'"]').html('Result');
                $(container + ' #applicant_id').val(p.applicant_id);

            },
            complete: function(){
            	$(container + ' #modal-view-detail').modal('show');
            }
        });
    }

    $(this).applicant_result();

    $(this).on('change', container + ' #vacancy', function (e){
  		$(this).applicant_result({
            vacancy: $(this).val()
        });
  		e.preventDefault();
  	});

    $(this).on('change', container + ' #search', function (e){
  		$(this).applicant_result({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(this).on('click', container + ' .btn-detail', function (e){
  		var id = $(this).data('id');
  		var vacancy = $(this).data('vacancy');
  		$(this).detail_result({
            applicant_id: id,
            vacancy_id: vacancy
        });
  		e.preventDefault();
  	});

    $(this).on('click', container + ' .btn-status', function (e){
        var status = $(this).data('status');
        ajaxManager.addReq({
            url: site_url + '/result_applicant/widget/change_status',
            type: 'GET',
            dataType: 'JSON',
            data: {
                status: status,
                applicant_id: $(container + ' #applicant_id').val()
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                if(r.success){
                    $.snackbar({
                        content: 'Data Updated', 
                        timeout: 5000
                    });
                    $(this).applicant_result();
                }else{
                    alert('Function Failed');
                }
            },
            complete: function(){
                $(container + ' #modal-view-detail').modal('hide');
            }
        });
        e.preventDefault();
    });

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 10,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).applicant_result({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

	$('#range-date').daterangepicker({
        ranges: {
            'Last 7 Days': [moment().subtract('days', 7), moment()],
            'Last 30 Days': [moment().subtract('days', 30), moment()],
            'Last 3 Month': [moment().subtract('days', 90), moment()]
        },
        format: 'DD-MM-YYYY',
        startDate: moment().subtract('days', 29),
        endDate: moment(),
        isShowing: true
    },
	    function (start, end) {
	    	$('#range-date').html('<i class="fa fa-calendar"></i> '+start.format('DD MMM YYYY')+' sd '+end.format('DD MMM YYYY')+'');
	    	$('#sdate').val(start.format('YYYY-MM-DD'));
	        $('#edate').val(end.format('YYYY-MM-DD'));
	        $(this).applicant_result();
	    }
    );

	$(container + ' #vacancy').select2();

});