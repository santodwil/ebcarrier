from controller.monitoringdata.editorLogCrawler import EditorLogCrawler
from ConfigParser import ConfigParser
from helper.helper import parse_date
import argparse

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Main Crawler',
                                        formatter_class=argparse.RawDescriptionHelpFormatter)
    argparser.add_argument('-m', '--mode', help='Mode', metavar='', default=None, type=str)
    argparser.add_argument('-c', '--config', help='Config', metavar='', default='config.conf', type=str)
    argparser.add_argument('-id', '--id', help='ID', metavar='', default=None, type=str)
    argparser.add_argument('-d', '--date', help='Date', metavar='', default=None, type=str)
    argparser.add_argument('-sd', '--sdate', help='Start Date', metavar='', default=None, type=str)
    argparser.add_argument('-ed', '--edate', help='End Date', metavar='', default=None, type=str)
    argparser.add_argument('-mo', '--month', help='Month', metavar='', default=None, type=str)
    argparser.add_argument('-ye', '--year', help='Year', metavar='', default=None, type=str)
    args = argparser.parse_args()

    config = ConfigParser()
    config.read(args.config)

    sdate, edate = parse_date(args.date, args.sdate, args.edate, args.month, args.year)

    if args.mode == 'editor_log':
        editorLog = EditorLogCrawler(config_file=args.config, log_name='crawler')
        editorLog.crawler()
    elif args.mode == 'editor_log_by_date':
        editorLog = EditorLogCrawler(config_file=args.config, log_name='crawler')
        editorLog.crawler_by_date(sdate.strftime('%Y-%m-%d'), edate.strftime('%Y-%m-%d'))
    else:
        print argparser.print_help()
