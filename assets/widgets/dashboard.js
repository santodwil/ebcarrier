$(function () {
 	'use restrict';

 	$(this).on('submit', container + ' #form-upload-cv', function (e){
        var form = $(this);
        $(this).ajaxSubmit({
           url: site_url + '/dashboard/widget/upload_cv',
           type: 'POST',
           data: {
            'csrf_token_app' : $('#csrf').val()
            },
           dataType: 'JSON',
            beforeSend: function(){
               form.find('[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i> Uploading...');
               form.find('[type="submit"]').attr('disabled', 'disabled');
            },
            success: function(r) {
               // if(r.sessionapp){
                  $('#csrf').val(r.csrf);
                 	form.find('[type="submit"]').html('Upload');
                 	form.find('[type="submit"]').removeAttr('disabled');
                 	if(r.success){
                 		form.find('input[name="file_before"]').val(r.file_name);
                 		$('.result-cv').html('<p>Your CV : <a href="'+site_url+'/dashboard/widget/download/'+r.file_name+'">'+r.temp_name+'</a></p>');
                    	$('.container-fluid').pgNotification({
                       	style: 'flip',
                       	message: r.msg,
                       	position: 'top-right',
                       	timeout: 2000,
                       	type: 'success'
                    	}).show();
                    	form.resetForm();
                  }else{
                     $('.container-fluid').pgNotification({
                        style: 'flip',
                        message: r.msg,
                        position: 'top-right',
                        timeout: 2000,
                        type: 'danger'
                     }).show();
                  }
               // }else{
               //    window.location.href = location;
               // }
            },
           error: function() {
               form.find('[type="submit"]').html('Upload');
               form.find('[type="submit"]').removeAttr('disabled');
           }
        });
        e.preventDefault();
 	});

 	$(this).on('click', container + ' .apply-test', function (e){
	   var id = $(this).data('id');
      ajaxManager.addReq({
         url: site_url + '/dashboard/widget/registration_test',
         type: 'POST',
         dataType: 'JSON',
         data: {
            id: id,
            'csrf_token_app' : $('#csrf').val()
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r) {
            $('#csrf').val(r.csrf);

            if(r.success) {
               Widget.Loader('test', {id : id}, 'container-content', false);
            }else{
               alert(r.msg);
            }
         }
      });
      e.preventDefault(); 		
 	});

});