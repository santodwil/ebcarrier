$(function () {

	'use restrict';

	$.fn.get_applicant = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/dashboard_evaluator/widget/get_applicant',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
            	if(r.total > 0){
            		$.each(r.result, function (k,v){
                    	t += '<tr>';
	                    	t += '<td>'+v.name+'</td>';
	                    	t += '<td>'+v.education_degree+'</td>';
	                    	t += '<td>'+v.test_type_name+'</td>';
	                    	t += '<td>'+v.time_start+'</td>';
                            if(v.checked_transaction == 0){
                                t += '<td><span class="label label-default">Not Check</span></td>';
                            }
                            if(v.checked_transaction == 1){
                                t += '<td><span class="label label-success">Checked</span></td>';
                            }
	                    	t += '<td>';
                                t += '<div class="btn-group">'
                                    t += '<button class="btn btn-default btn-sm view-detail" data-transaction="'+v.id+'" data-testtype="'+v.test_type_id+'"><i class="fa fa-edit"></i> Detail</button>';
                                    if(v.checked_transaction == 0){
                                        t += '<button class="btn btn-default btn-sm set-status" data-status="1" data-id="'+v.id+'">Set Checked</button>';
                                    }
                                    if(v.checked_transaction == 1){
                                        t += '<button class="btn btn-default btn-sm set-status" data-status="0" data-id="'+v.id+'">Set Not Checked</button>';
                                    }
                                t += '</div>';
                            t += '</td>';
                    	t += '</tr>';
            		});
            	}else{
            		t += '<tr><td class="text-center" colspan="6">No Result</td></tr>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	hide_loading();
            }
        });
    }

    $(this).on('click', container + ' .set-status', function (e) {
        var status = $(this).data('status');
        var id = $(this).data('id');

        ajaxManager.addReq({
            url: site_url + '/dashboard_evaluator/widget/set_status',
            type: 'GET',
            dataType: 'JSON',
            data: {
                status: status,
                id: id
            },
            beforeSend: function () {
                $(container + ' .set-status[data-id="'+id+'"]').addClass('disabled');
                $(container + ' .set-status[data-id="'+id+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $(container + ' .set-status[data-id="'+id+'"]').removeClass('disabled');
                if(r.checked_transaction == 0){
                    $(container + ' .set-status[data-id="'+id+'"]').html('Set Checked');
                }
                if(r.checked_transaction == 1){
                    $(container + ' .set-status[data-id="'+id+'"]').html('Set Not Checked');
                }
            },
            complete: function() {
                $(this).get_applicant();
            }
        });
        
        e.preventDefault();

    });

    $(this).on('click', container + ' .view-detail', function (e) {
        var transaction = $(this).data('transaction');
        var test_type = $(this).data('testtype');

        ajaxManager.addReq({
            url: site_url + '/dashboard_evaluator/widget/detail',
            type: 'GET',
            dataType: 'JSON',
            data: {
                transaction: transaction,
                test_type: test_type
            },
            beforeSend: function () {
                $(container + ' .view-detail[data-transaction="'+transaction+'"][data-testtype="'+test_type+'"]').addClass('disabled');
                $(container + ' .view-detail[data-transaction="'+transaction+'"][data-testtype="'+test_type+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
            	t = '';
            	t += '<form id="form-assessment">';
                    t += '<input type="hidden" id="test_type_id" name="test_type_id" value="'+test_type+'">';
                    t += '<input type="hidden" id="transaction_id" name="transaction_id" value="'+r.transaction_id+'">';
	            	t += '<div class="col-md-12 panel-list-question" style="margin-top:10px;">';
	            		if(r.result){
	            			$.each(r.result, function(k,v){
	                            t += '<div class="row" style="border-bottom: 1px solid #f1efef;margin-bottom: 10px;padding-bottom: 10px;">';
				                  	t += '<div class="col-md-12">';
				                     	t += ''+v.question_text+'';
				                     	t += '<p style="margin-top:-10px;">';
				                        t += ''+(v.question_image ? '<a data-fancybox="gallery" href="'+base_url+'files/question_image/'+v.question_image.dir+'">View Image</a>&nbsp;' : '' )+'';
				                        t += ''+(v.question_file ? '<a href="'+base_url+'files/question_file/'+v.question_file.dir+'">View File</a>' : '' )+'';
				                     t += '</p>';
				                     t += '<div class="alert alert-info">';
				                     	if(v.question_answers){
			                     			if(v.question_type_id == 2){
                                                if(v.question_answers.filetype == 'image'){
                                                    t += '<p><a data-fancybox="gallery" href="'+base_url+'/files/applicant_upload/'+v.question_answers.answers+'">'+v.question_answers.answers+'</a></p>';
                                                }
                                                if(v.question_answers.filetype == 'file'){
                                                    t += '<p><a href="'+site_url+'dashboard_evaluator/widget/download/'+v.question_answers.answers+'">'+v.question_answers.answers+'</a></p>';
                                                }
				                     		}
				                     		if(v.question_type_id == 3){
			                     				t += v.question_answers.answers;
				                     		}
				                     		if(v.question_type_id == 5){
				                     			t += '<table class="table table-condensed">';
				                     				t += '<tr>';
				                     					t += '<th>Statement</th>';
				                     					t += '<th>Holder</th>';
			                     					t += '</tr>';
					                     			$.each(v.question_answers.answers, function(kk,vv){
				                     					t += '<tr>';
						                     				t += '<td>'+vv.statement+'</td>';
						                     				t += '<td>'+vv.holder+'</td>';
					                     				t += '</tr>';
					                     			});
			                     				t += '</table>';
				                     		}
				                     		if(v.question_type_id == 6){
			                     				$.each(v.question_answers.answers, function(kk,vv){
		                     						t += '<h5 class="font-arial bold" style="font-size:14px;">'+kk+'</h5>';
		                     						t += '<p>'+vv+'</p>';
		                     					});
				                     		} 
				                     	}else{
				                     		t += 'No Answers';
				                     	}
				                        if(v.question_answers){
                                           t += '<input type="number" class="form-control assessment-check" data-id="'+v.question_id+'" name="assessment['+v.question_type_id+'][]" placeholder="Range 10 sd 100" value="'+v.question_answers.result+'">';
				                           t += '<input type="hidden" class="assessment-result-'+v.question_id+'" name="assessment_result['+v.question_id+']" value="'+v.question_answers.result+'">';
				                        }
				                     t += '</div>';
				                  t += '</div>';
				               	t += '</div>';
	        				});
	            		}
	            	t += '</div>';
            	t += '</form>';
            	$(container + ' .modal-body-view-detail').html(t);
            	$(container + ' .view-detail[data-transaction="'+transaction+'"][data-testtype="'+test_type+'"]').removeClass('disabled');
                $(container + ' .view-detail[data-transaction="'+transaction+'"][data-testtype="'+test_type+'"]').html('<i class="fa fa-edit"></i> Detail');
                $(container + ' #modal-view-detail').modal('show');
            }
        });
        
        e.preventDefault();
    });

	$(this).on('click', container + ' .btn-change', function (e) {
    	$('#form-assessment').submit();
    	e.preventDefault();
    });

    $(this).on('change', container + ' .assessment-check', function (e) {
        var id = $(this).data('id');
        var data = $(this).val();
        $('.assessment-result-'+id+'').val(data);
        e.preventDefault();
    });

	$(this).on('submit', container + ' #form-assessment', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/dashboard_evaluator/widget/change',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                $(container + ' .btn-change').addClass('disabled');
                $(container + ' .btn-change').html('<i class="fa fa-refresh fa-spin"></i> Please Wait...');
            },
            success: function(r) {
            	$('#csrf').val(r.csrf);
                hide_loading();
                $.snackbar({
                    content: r.msg, 
                    timeout: 5000
                });
            },
            complete: function(){
                $(container + ' .btn-change').removeClass('disabled');
                $(container + ' .btn-change').html('<i class="fa fa-edit"></i> Change');
            }
        });
        e.preventDefault();
    });

    $(this).get_applicant();

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 10,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_applicant({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

    $(this).on('change', container + ' #search', function (e){
  		$(this).get_applicant({
            search: $(this).val()
        });
  		e.preventDefault();
  	});


});