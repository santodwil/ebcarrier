<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        $this->load->model('applicant');
        $this->load->model('assessment');

        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Dashboard'
        );
        $this->render_widget($data);
    }

    public function get_applicant(){
        $params = $this->input->get(); 
        $response['result'] = $this->applicant->get_applicant_evaluator('get', $params, $this->_user->id);
        $response['total'] = $this->applicant->get_applicant_evaluator('count', $params, $this->_user->id);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function detail(){
        $params = $this->input->get(); 
        $question = $this->applicant->get_question($params['test_type']);
        if($question->num_rows() > 0){
            foreach ($question->result_array() as $k => $v) {
                $result[$v['id']]['question_id'] = $v['id'];
                $result[$v['id']]['question_text'] = $v['question_text'];
                $result[$v['id']]['question_type_id'] = $v['question_type_id'];
                $result[$v['id']]['question_file'] = $this->applicant->question_dir($v['id'], 'question_file');  
                $result[$v['id']]['question_image'] = $this->applicant->question_dir($v['id'], 'question_image');
                $result[$v['id']]['question_answers'] = $this->applicant->get_answers($v['id'], $params['transaction'], $v['question_type_id']); 
            }
        }else{
            $result = FALSE;
        }
        $response['transaction_id'] = $params['transaction'];
        $response['success'] = TRUE;
        $response['result'] = $result;
        $this->json_result($response);
    }

    public function download($file){
        $this->load->helper('download');
        $data = file_get_contents('./files/applicant_upload/'.$file.'');
        force_download($file, $data);
    }

    public function change(){
        $params = $this->input->post();
        $count = 0;
        
        //check total question in test type
        $total_question = $this->assessment->total_question($params['test_type_id']);

        // Update assessment to each question
        foreach ($params['assessment_result'] as $k => $v) {
            $this->assessment->test_answers_assessment(array('result' => $v), array('question_id' => $k, 'test_transaction_id' => $params['transaction_id']));
        }

        // Update/Insert assessment to result all assessment group by question type id
        $result = array();
        foreach ($params['assessment'] as $k => $v) {
            $result[$k] = array_sum($v);
        }
        foreach ($result as $k => $v) {
            $check = $this->assessment->checking_assessment($params['transaction_id'], $k);
            $assessment = $v / $total_question;

            if($check > 0){
                $this->assessment->update_assessment(array('assessment' => round($assessment, 2)), array('test_transaction_id' => $params['transaction_id'], 'question_type_id' => $k));

            }else{
                $tmp = array(
                    'test_transaction_id' => $params['transaction_id'],
                    'question_type_id' => $k,
                    'assessment' => round($assessment, 2)
                );
                $this->assessment->insert_assessment($tmp);
            }
        }
        $response['msg'] = 'Data updated';
        $this->json_result($response);
    }

    public function set_status(){
        $params = $this->input->get();
        $update = $this->db->update('test_transaction', array('checked_transaction' => $params['status']), array('id' => $params['id']));
        $response['status'] = $update ? TRUE : FALSE;
        $response['checked_transaction'] = $params['status'];
        $this->json_result($response);
    }
}