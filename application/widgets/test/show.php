<link href="<?php echo base_url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.css'); ?>" rel="stylesheet" type="text/css" />
<style type="text/css">
    .bar-question{
        font-size: 15px;
        color:black;
    }
    .bar-question > p{
        font-size: 15px;
        color:black;
    }
    .btn-danger{
        background-color: #f11818;
    }
    .text-danger{
        color: #f11818 !important;
    }
    .radio.radio-danger input[type=radio]:checked + label:before{
        border-color: #f11818;
    }
</style>
<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">
	<div class="row" style="margin-top: 10px;">
		<div class="col-md-3" style="border-right: 0px solid #e5e8e9;">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-transparent" style="margin-bottom: 0">
                      <div class="panel-body">
                      	<h4 class="text-center" style="font-weight:bold;">Test Name</h4>
                        <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;">
                        	<?php echo $detail_tests['name'] ?>
                    	</h5>
                    	<hr style="border-color: #f5f3f3">

                        <h4 class="text-center" style="font-weight:bold;">Question</h4>
                        <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;">
                        	<?php echo $detail_tests['total_question'] ?>
                    	</h5>
                    	<hr style="border-color: #f5f3f3">

                        <h4 class="text-center" style="font-weight:bold;">Start</h4>
                        <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;">
                        	<?php echo date('H:i', strtotime($detail_tests['time_start'])) ?> sd <?php echo date('H:i', strtotime($detail_tests['time_end'])) ?>
                    	</h5>
                        <hr style="border-color: #f5f3f3">

                        <h4 class="text-center" style="font-weight:bold;">Time Left</h4>
                        <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;" id="clock-time"></h5>
                        <hr style="border-color: #f5f3f3">
                        
                        <button class="btn btn-default btn-block btn-back"><i class="fa fa-arrow-left"></i> Back To List</button>
                        <button class="btn btn-default btn-block" data-target="#modal-instructions" data-toggle="modal"><i class="fa fa-info"></i> Test Instructions</button>
                    	<button class="btn btn-danger btn-block btn-finish-test"><i class="fa fa-check"></i> Finish Test</button>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        	<div class="panel panel-transparent panel-list-question" style="margin-bottom: 0;">
    		 	<div class="panel-body">
	            	<ul class="nav nav-pills nav-stacked" id="tests_no" style="width:150px;">
	            		<?php 
	            		$answers = $this->db->select('max(a.question_id) as last_question_id')
	            					->join('question as b', 'a.question_id = b.id')
	            					->where('b.test_type_id', $detail_tests['test_id'])
	            					->get('test_answers as a')->row_array();

	            		$questions = $this->db->select('id')
	            					->where('test_type_id', $detail_tests['test_id'])
            						->order_by('id', 'asc')
            						->get('question');

						$first_question = $this->db->select('min(id) as first_question')
	            									->where('test_type_id', $detail_tests['test_id'])
            										->get('question')->row_array();;

	            		$list = $this->db->select('question_id')
	            						->where('test_transaction_id', $detail_tests['id'])
										->get('test_answers')->result_array();

	            		if(count($list) > 0){
                            foreach ($list as $v) {
                                $listdata[] = $v['question_id'];
                            }
                        }else{
                            $listdata = array();
                        }
	            		if($questions->num_rows() > 0){
	            			$no = 0;
	            			foreach ($questions->result_array() as $key => $value) {
	            				$icon  = in_array($value['id'], $listdata) ? '<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>&nbsp;' : '<i class="fa fa-circle text-danger pull-left" style="margin-top:5px;"></i>&nbsp;';
	            				$no++;
	            				if($answers['last_question_id']){
	            					if($value['id'] <= $answers['last_question_id']){
	            						echo '<li>';
			            					echo '<a style="border: 1px solid #efe9e9;border-radius: 5px;cursor: pointer;" class="detail-soal" data-id="'.$value['id'].'">'.$icon.' No '.$no.'</a>';
			            				echo '</li>';
	            					}else{
            							echo '<li>';
			            					echo '<a class="detail-soal" data-id="'.$value['id'].'" style="border: 1px solid #efe9e9;border-radius: 5px;background-color:#f7f7f7;pointer-events: none;cursor: default;">'.$icon.' No '.$no.'</a>';
			            				echo '</li>';
	            					}
	            				}else{
	            					if($value['id'] == $first_question['first_question']){
	            						echo '<li>';
			            					echo '<a style="border: 1px solid #efe9e9;border-radius: 5px;pointer-events: none;cursor: default;font-weight:bold;" class="detail-soal" data-id="'.$value['id'].'">'.$icon.' No '.$no.'</a>';
			            				echo '</li>';
	            					}else{
	            						echo '<li>';
			            					echo '<a class="detail-soal" data-id="'.$value['id'].'" style="border: 1px solid #efe9e9;border-radius: 5px;background-color:#f7f7f7;pointer-events: none;cursor: default;">'.$icon.' No '.$no.'</a>';
			            				echo '</li>';
	            					}
	            					
	            				}
	            			}
	            		}
	            		?>
	            	</ul>
            	</div>
        	</div>
        </div>
        <div class="col-md-7">
        	<form id="form-save-answers">
            	<div class="panel" style="margin-bottom: 0;margin-top: 10px;">
            		<input type="hidden" id="question_id" value="<?php echo $first_question['first_question'] ?  $first_question['first_question'] : '' ?>">
        		 	<div class="panel-body question-section">
        		 	</div>
    		 	</div>
		 	</form>
        </div>
    </div>

    <div class="modal fade fill-in" id="modal-instructions" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close"></i></button>
        <div class="modal-dialog" style="">
            <div class="modal-content">
               	<div class="modal-header">
                  <h5 class="text-left"><span class="bold">Test Instructions</span></h5>
               	</div>
               	<div class="modal-body">
                  	<div class="panel panel-default">
                     	<div class="panel-body">
                        	<h5 class="font-arial" style="font-size: 16px;"><?php echo $detail_tests['guide_tests'] ?></h1>
                     	</div>
                  	</div>
               </div>
            </div>
        </div>
  	</div>
	      	
</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var reg_id = '<?php echo $id; ?>';
    var now_date = '<?php echo date('Y-m-d H:i:s'); ?>';
    var time_test = '<?php echo $detail_tests['time']; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
    var end_time = '<?php echo $detail_tests['time_end']; ?>';
    var test_transaction_id = '<?php echo $detail_tests['test_transaction_id']; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.css') ?>" />
<script src="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.js') ?>"></script>