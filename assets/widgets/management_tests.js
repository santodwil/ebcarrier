$(function () {

	'use restrict';

    $.fn.modify_tests = function(params) {
        var p = $.extend({
            id: false,
        }, params);
        ajaxManager.addReq({
            url: site_url + '/management_tests/widget/modify_tests',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: p.id,
                'csrf_token_app' : $('#csrf').val()
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                $('.btn-edit[data-id="' + p.id + '"]').addClass('disabled');
                $('.btn-edit[data-id="' + p.id + '"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    var $form = $('#form-modify');
                    $form.find('input[name="id"]').val(r.test_type.id);
                    $form.find('input[name="name"]').val(r.test_type.name);
                    $form.find('input[name="question"]').val(r.test_type.total_question);
                    $form.find('input[name="times"]').val(r.test_type.time);
                    if(r.test_type.guide_tests){
                        tinyMCE.get('edit_guidetest').setContent(r.test_type.guide_tests);
                    }
                    $('#modal-modify-data').modal('show');
                }
                $('.btn-edit[data-id="' + p.id + '"]').removeClass('disabled');
                $('.btn-edit[data-id="' + p.id + '"]').html('Modify');
            }
        });
    }

	$.fn.get_tests = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            status : $(container + ' #status').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/management_tests/widget/get_tests',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search,                
                status: p.status,
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
        		var no = 0;
            	if(r.total > 0){
            		$.each(r.result, function (k,v){
                        t += '<tr>';
                            t += '<td>';  
                                t += '<input type="checkbox" style="margin-top:-5px;" class="option_id" id="checkbox'+v.id+'" value="'+v.id+'">';
                                t += '</div>';  
                            t += '</td>';
                            t += '<td>'+v.name+'</td>';
                            t += '<td class="text-center">'+v.total_question+'</td>';
                            t += '<td class="text-center">'+v.time+'</td>';
                            t += '<td class="text-center">';
                                t += v.status == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';
                            t += '</td>';
                            t += '<td class="text-center">';
                                t += '<div class="btn-group">';
                                    if(v.status == 1){
                                         t += '<button data-id="'+v.id+'" data-mode="inactive" type="button" class="btn btn-xs set-status">Set Inactive</button>';
                                    }else{
                                        t += '<button data-id="'+v.id+'" data-mode="active" type="button" class="btn btn-xs set-status">Set Active</button>';
                                    }
                                    t += '<button data-id="'+v.id+'" type="button" class="btn btn-xs btn-edit">Modify</button>';
                                    t += '<button data-id="'+v.id+'" type="button" class="btn btn-xs btn-delete">Delete</button>';
                                t += '</div>';
                            t += '</td>';
                        t += '</tr>';

                        // t += '<div class="col-md-3">';
                        //     t += '<div class="card share full-width" style="padding-bottom:10px;">';
                        //         t += '<div class="checkbox pull-right" style="left: 15px;bottom: 2px;">';
                        //             t += '<input type="checkbox" class="option_id" id="checkbox'+v.id+'" value="'+v.id+'">';
                        //             t += '<label for="checkbox'+v.id+'"></label>';
                        //         t += '</div>';
                                
                        //         t += '<h5 class=" font-arial bold" style="font-size: 12.5px;padding: 0px;margin: 0px;padding-left:10px;">';
                        //             t += v.name;
                        //         t += '</h5>';
                                
                        //         t += '<h5 class="font-arial" style="font-size: 12px;margin-top: 1px;padding-left:10px;">';
                        //             t += ''+v.total_question+' Question in '+v.time+' minutes ';
                        //             t += v.status == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';
                        //         t += '</h5>';
                                    
                        //         t += '<div class="col-md-12 text-center">';
                        //             t += '<div class="btn-group">';
                        //                 if(v.status == 1){
                        //                      t += '<button data-id="'+v.id+'" data-mode="inactive" type="button" class="btn btn-xs set-status" style="margin-top:-3px;">Set Inactive</button>';
                        //                 }else{
                        //                     t += '<button data-id="'+v.id+'" data-mode="active" type="button" class="btn btn-xs set-status" style="margin-top:-3px;">Set Active</button>';
                        //                 }
                        //                 t += '<button data-id="'+v.id+'" type="button" class="btn btn-xs btn-edit" style="margin-top:-3px;">Modify</button>';
                        //                 t += '<button data-id="'+v.id+'" type="button" class="btn btn-xs btn-delete" style="margin-top:-3px;">Delete</button>';
                        //             t += '</div>';
                        //         t += '</div>';

                        //     t += '</div>';
                        // t += '</div>';
            		});
            	}else{
            		t += '<tr><td colspan="9" class="text-center">No Result</td></tr>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	hide_loading();
            }
        });
    }

    $(this).get_tests();

    $(this).on('click', container + ' .set-status', function (e) {
        var id = $(this).data('id');
        var mode = $(this).data('mode');

        ajaxManager.addReq({
            url: site_url + '/management_tests/widget/change_status',
            type: 'POST',
            dataType: 'JSON',
            data: {
                mode: mode,
                id: id,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                $(container + ' .set-status[data-id="'+id+'"]').addClass('disabled');
                $(container + ' .set-status[data-id="'+id+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    $.snackbar({
                        content: 'Status Updated', 
                        timeout: 5000
                    });
                    $(this).get_tests();
                }else{
                    alert('Function Failed');
                }
            }
        });
        e.preventDefault();
    });

    $(container + ' .bulk_action').on('click', function(e) {
        var value = $(this).data('value');
        var data = [];
        $('.option_id').each(function () {
            if (this.checked) {
                data.push($(this).val());
            }
        });
        if (data.length > 0) {
            ajaxManager.addReq({
                url: site_url + '/management_tests/widget/change_selected',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    data: data,
                    value: value,
                    'csrf_token_app' : $('#csrf').val()
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function (r) {
                    $('#csrf').val(r.csrf);
                    $('.select_id').prop('checked', false);
                    if(r.success){
                        $.snackbar({
                            content: 'Data Updated', 
                            timeout: 5000
                        });
                        $(this).get_tests();
                    }else{
                        alert('Function Failed');
                    }
                }
            });
        } else {
            alert('No Data Selected');
        }

        e.preventDefault();
    });
    
    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 10,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_tests({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

	$(container + ' .date-data').daterangepicker({
		autoUpdateInput: false,
		locale: {
	      format: 'DD/MMM/YYYY'
	    },
        singleDatePicker: true,
        showDropdowns: true
    });

    $(container + ' .date-data').on('apply.daterangepicker', function(ev, picker) {
      	$(this).val(picker.startDate.format('DD/MMM/YYYY'));
      	$('#created').val(picker.startDate.format('YYYY-MM-DD'));
      	$(this).get_applicant({
            created: picker.startDate.format('YYYY-MM-DD')
        });
  	});


  	$(this).on('change', container + ' #search', function (e){
  		$(this).get_tests({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(this).on('change', container + ' #status', function (e){
  		$(this).get_tests({
            status: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(container + ' #status').select2();

  	$(container + ' .select_id').on('change', function (e) {
        if (this.checked) {
            $('.option_id').each(function () {
                this.checked = true;
            });
        } else {
            $('.option_id').each(function () {
                this.checked = false;
            });
        }
    });


    $(container + ' .new-data').on('click', function(e) {
        $('#modal-new-data').modal('show');
        e.preventDefault();
    });

    $(this).on('click', container + ' .btn-edit', function (e) {
        var id = $(this).data('id');
        $(this).modify_tests({
            id : id
        });
        e.preventDefault();
    });

    $(this).on('click', container + ' .btn-delete', function (e) {
        var id = $(this).data('id');
        var conf = confirm('removing this will also remove all transactions related on this tests. Are you sure ?');
        if(conf){
            ajaxManager.addReq({
                url: site_url + '/management_tests/widget/remove_test',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    id: id,
                    'csrf_token_app' : $('#csrf').val()
                },
                beforeSend: function () {
                    $(container + ' .btn-delete[data-id="'+id+'"]').addClass('disabled');
                    $(container + ' .btn-delete[data-id="'+id+'"]').html('<i class="fa fa-refresh fa-spin"></i>');
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    $('#csrf').val(r.csrf);
                    if(r.success){
                        $.snackbar({
                            content: r.msg, 
                            timeout: 5000
                        });
                        $(this).get_tests();
                    }else{
                        alert(r.msg);
                    }
                    $(container + ' .btn-delete[data-id="'+id+'"]').removeClass('disabled');
                    $(container + ' .btn-delete[data-id="'+id+'"]').html('Delete');
                }
            });
        }else{
            return false;
        }
        e.preventDefault();
    });

    tinymce.init({
        selector: 'textarea',
        menubar: false,
        style: "border:5px solid black;",
        mobile: { theme: 'mobile' }
    });

    $(this).on('submit', container + ' #form-create', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/management_tests/widget/save',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                
                if(r.success){
                    form.resetForm();
                    $(this).get_tests();
                }
                $('.alert-save').show();
                $('.alert-save').html(r.msg);
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-save"></i> Save');
            },
            complete: function(){
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });
            }
        });
        e.preventDefault();
    });

    $(this).on('submit', container + ' #form-modify', function (e) {
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/management_tests/widget/change',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-edit"></i> Change');
                if(r.success){
                    $(this).get_tests();
                    $('#modal-modify-data').modal('hide');
                    $.snackbar({
                        content: r.msg, 
                        timeout: 5000
                    });
                }
                $('#modal-modify-data .alert-save').show();
                $('#modal-modify-data .alert-save').html(r.msg);
            },
            complete: function(){
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });
            }
        });
        e.preventDefault();
    });

});