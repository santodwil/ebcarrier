<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
    <div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default" style="margin-bottom: 0">
					<div class="panel-heading" style="background-color: #fbfbfb;">
						<div class="row">
							<div class="col-md-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
									<input type="text" id="search" class="form-control input-sm" placeholder="Search...">
								</div>
							</div>
							<div class="col-md-2">
								<button title="New Question" type="button" class="btn btn-danger btn-sm btn-new" style="margin-left:-20px;"><i class="fa fa-plus"></i></button>
							</div>
						</div>
					</div>
  					<div class="panel-body" id="section-data" style="height: 420px;overflow: auto">
						<ul class="nav nav-pills nav-stacked" id="tests_no" style="width:auto;"></ul>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default" style="margin-bottom: 0">
					<div class="panel-body" id="section-result" style="height: 410px;overflow: auto">
						<h2 class="text-center font-arial" style="margin-top: 13%">Click test to getting start</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade slide-up disable-scroll in" id="modal-new-data" style="overflow: auto;">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="margin-top: 5px;width: 1300px;">
        			<div class="modal-header clearfix text-left">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                  	</div>
		          	<div class="modal-body" style="height: auto;">
		          		<div class="row">
							<div class="col-md-3">
								<ul class="nav nav-pills nav-stacked" id="container-question-type" style="width:auto;">
									<?php
									foreach ($question_type as $key => $value) {
										echo '<li>';
											echo '<a style="padding:5px;" href="#" class="detail-questions-type font-arial" data-id="'.$value['id'].'">'.$value['name'].'</a>';
										echo '</li>';
									}
									?>
								</ul>
							</div>
							<div class="col-md-9" id="section-question-type-form">
								<h3 class="text-center font-arial muted" style="margin-top: 13%">Click question type to getting start</h3>
							</div>
						</div>


		            	<!-- <form role="form" id="form-create">
			                <div class="row">
	                  	 		<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Question</label>
											<textarea class="form-control" name="question"></textarea>
										</div>
									</div>
				                </div>
			                </div>
			            </form> -->
		          	</div>
        		</div>
      		</div>
    	</div>
  	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>