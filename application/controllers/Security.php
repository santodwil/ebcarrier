<?php

class Security extends MX_Controller {

    protected $theme_path = 'themes/pages/';

    public function __construct() {
        parent::__construct();
        $this->load->add_package_path(APPPATH . 'third_party/ion_auth/');
        $this->load->library('ion_auth');
        $this->load->library('validation_form');    
    }

    private function json_result($result = array()) {
        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function index(){
        if ($this->ion_auth->logged_in()) {
            redirect('main');
        }
    	$data = array(
            'title' => 'Beranda'
    	);
    	$this->load->view($this->theme_path . 'login', $data);
    }

    public function ja_request(){
    	$data = array(
            'title' => 'Job Assignment Request',
            'member' => $this->members->get_member_with_team(),
            'job_type' => $this->db->select('id,name')->get('job_type')->result_array(),
    	);
    	$this->load->view($this->theme_path . 'ja_request', $data);
    }

    public function login() {
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $remember = true;
            $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'main';

            $login = $this->ion_auth->login($username, $password, $remember);
            if ($login) {
                redirect(urldecode($redirect));
            } else {
                $this->_message = array(
                    'message' => 'Kesalahan Username atau Password',
                    'label' => 'danger'
                );
                $this->session->set_flashdata('message', $this->_message);
                redirect('security?redirect=' . urlencode($redirect));
            }
        } else {
            redirect('security');
        }
    }

    public function logout() {
        $this->session->unset_userdata('cart');
        $this->ion_auth->logout();
        redirect('security');
    }

    public function register(){        
        if($this->validation_form->register($this->input->post())){
            $p = $this->input->post();
            $user = array(
                'active' => 0,
                'first_name' => NULL,
                'last_name' => NULL,
                'phone' => NULL,
                'status' => NULL,
                'avatar' => NULL,
            );

            $user['id'] = $this->ion_auth->register(uniqid(date("his")), 'password', $p['register_email'], $user, array(2));
            if ($user['id']) {
                $applicant['user_id'] = $user['id'];
                $applicant['name'] = $p['register_fullname'];
                $applicant['gender'] = $p['register_gender'];
                $applicant['vacancy_division_id'] = $p['register_vacancy'];
                $applicant['education_degree'] = $p['register_degree'];
                $applicant['school_majors'] = $p['register_majors'];
                $applicant['university'] = $p['register_university'];
                $applicant['contact_number'] = $p['register_contact'];
                $applicant['email'] = $p['register_email'];
                $applicant['created_date'] = date('Y-m-d H:i:s');
                $insert_applicant = $this->db->insert('applicant', $applicant);

                $response['success'] = $insert_applicant ? TRUE : FALSE;
                $response['msg'] = $insert_applicant ? '<b>Registration Successfully</b> Wait for the administrator to enable your login access' : 'Registration Failed';
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = validation_errors();
        }
        $response['csrf'] = $this->security->get_csrf_hash();
        $this->json_result($response);
    }

    public function check_username(){      
        $p = $this->input->post();
        $user = $this->db->select('username, active')->where('email', $p['email_check'])->get('users');
        if($user->num_rows() > 0){
            $response['userdata'] = $user->row_array();
        }else{
            $response['userdata'] = FALSE;
        }
        $response['csrf'] = $this->security->get_csrf_hash();
        $this->json_result($response);
    }
}