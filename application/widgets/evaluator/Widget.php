<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('vacancy');
        if (!$this->ion_auth->logged_in()) {
            return '{"msg":"success"}';
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Evaluator',
        );
        $this->render_widget($data);
    }

    public function get_evaluator(){
        $params = $this->input->get(); 
        $response['result'] = $this->vacancy->get_evaluator('get', $params);
        $response['total'] = $this->vacancy->get_evaluator('count', $params);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function edit_evaluator(){
        $params = $this->input->get(); 
        $response['evaluator'] = $this->db->select('a.id, a.user_id, b.first_name')->join('users as b', 'a.user_id = b.id')->where('a.test_type_id', $params['testtypeid'])->get('evaluator_tests as a')->result_array();
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function save(){
        $post = $this->input->post();
        $check = $this->db->where('test_type_id', $post['test_type'])->get('evaluator_tests');
        if($check->num_rows() > 0){
            $response['success'] = FALSE;
            $response['msg'] = 'Test type already available';
        }else{
            if($post['choose_evaluator']){
                foreach ($post['choose_evaluator'] as $key => $value) {
                    $entity = array(
                        'user_id' => $value,
                        'test_type_id' => $post['test_type']
                    );
                    $insert = $this->db->insert('evaluator_tests', $entity);
                }
                $response['success'] = TRUE;
                $response['msg'] = 'New Data Added';
            }else{
                $response['success'] = TRUE;
                $response['msg'] = 'No Choose evaluator';
            }
        }
        $this->json_result($response);
    }

    public function change(){
        $params = $this->input->post();
        if(isset($params['removed_list'])){
            foreach ($params['removed_list'] as $key => $value) {
                $this->db->delete('evaluator_tests', array('id' => $value));
            }
        }
        if(isset($params['adduser'])){
            foreach ($params['adduser'] as $key => $value) {
                $this->db->insert('evaluator_tests', array('test_type_id' => $params['id'], 'user_id' => $value));
            }
        }
        $response['success'] = TRUE;
        $response['msg'] = 'Data Updated';
        $this->json_result($response);
    }

}