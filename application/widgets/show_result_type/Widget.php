<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Controller Result Type
 *
 * @author SUSANTO DWI LAKSONO
 */

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        if (!$this->ion_auth->logged_in()) {
            die;
        }
        $this->load->model('result_type');
        $this->load->library('test_result');  
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Result Type',
            'id' => $get['id'],
            'sdate' => $get['sdate'],
            'edate' => $get['edate']
        );
        $this->render_widget($data);
    }

    public function get_applicant(){
        $post = $this->input->get();
        $sdate = isset($post['sdate']) && $post['sdate'] != '' ? $post['sdate'] : date('Y-m-d');
        $edate = isset($post['edate']) && $post['edate'] != '' ? $post['edate'] : date('Y-m-d');

        $response['result'] = $this->result_type->get_applicant('get', $post['test_type_id'], $post['search'], $sdate, $edate, $post['offset']);
        $response['total'] = $this->result_type->get_applicant('count', $post['test_type_id'], $post['search'], $sdate, $edate, $post['offset']);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function detail_result(){
        $get = $this->input->get();
        $response['result'] = $this->result_type->detail_result($get['applicantid'], $get['typeid'], $get['test_type_id']);
        $response['total_question'] = count($response['result']);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function save_result(){
        $params = $this->input->post();
        $count = 0;
        $count_result = 0;
        if($params['assessment']){
            foreach ($params['assessment'] as $key => $value) {
                $count_result += $value;
                $update = $this->db->update('test_answers', array('result' => $value), array('id' => $key));
                $update ? $count++ : FALSE;
            }
        }

        $test_result = $this->db->where('test_type_id', $params['test_type_id'])->where('applicant_id', $params['applicant_id'])->get('test_result');
        if($test_result->num_rows() > 0){
            $row_test_result = $test_result->row_array();
            $r = json_decode($row_test_result['result']);
            foreach ($r as $key => $value) {
                $result[$key] = $value;
            }

            //calculation result
            $question_result = $count_result / $params['total_question']; 
            $result[$params['question_type_id']] = round($question_result, 2);
            //update test result
            $this->db->update('test_result', array('result' => json_encode($result)), array('test_type_id' => $params['test_type_id'], 'applicant_id' => $params['applicant_id']));
        }else{
            $group_question_id = $this->test_result->_group_type_question($params['test_type_id']);
            if($group_question_id){
                foreach ($group_question_id->result_array() as $k => $v) {
                    $result[$v['question_type_id']] = 0;
                }   
            }

            //calculation result
            $question_result = $count_result / $params['total_question']; 
            $result[$params['question_type_id']] = round($question_result, 2);
            //insert test result
            $this->db->insert('test_result', array('test_type_id' => $params['test_type_id'], 'applicant_id' => $params['applicant_id'], 'result' => json_encode($result)));
        }

        $response['msg'] = $count.' Data Updated';
        $response['count_result'] = $count_result;
        $response['result'] = $result;
        $response['success'] = TRUE;
        $this->json_result($response);
    }
}