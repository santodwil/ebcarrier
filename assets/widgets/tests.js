$(function () {
 	'use restrict';

    var _question_id = $('#question_id').val();

    $.fn.toggleQuestion = function(id) {
        $('.detail-soal[data-id="' + id + '"]').removeAttr('style');
        for (var i = 1; i <= id; i++) {
            $('.detail-soal').css("font-weight", "");
            $('.detail-soal[data-id="' + i + '"]').attr('style', 'background-color:#ffffff;cursor: pointer;');
        }
        $('.detail-soal[data-id="' + id + '"]').attr('style', 'cursor: pointer;font-weight:bold;');
    };

    $.fn.get_questions = function(params) {
        var p = $.extend({
            question_id : _question_id,
            test_transaction_id : test_transaction_id,
            load : true
        }, params);

        ajaxManager.addReq({
            url: site_url + '/tests/widget/get_questions',
            type: 'POST',
            dataType: 'JSON',
            data: {
                question_id: p.question_id,
                test_transaction_id: test_transaction_id,
                'csrf_token_app' : $('#csrf').val()

            },
            beforeSend: function () {
                $(this).toggleQuestion(p.question_id);
                if(p.load){
                    $(container + ' .question-section').html('<h6 class="text-center font-arial"><i class="fa fa-spinner fa-spin"></i> Please Wait...</h6>');
                    // $('.loading').html('<h4>Render Question...</h4>');
                }
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                if(r.success){
                    t = '';
                    t += '<input type="hidden" name="test_transaction_id" value="'+test_transaction_id+'">';
                    t += '<input type="hidden" name="mode" class="mode-saving">';
                    t += '<input type="hidden" name="q_id" value="'+r.question.id+'">';
                    t += '<input type="hidden" name="previous_id" value="'+r.previous_id+'">';
                    t += '<input type="hidden" name="next_id" value="'+r.next_id+'">';
                    t += '<input type="hidden" name="question_type_id" value="'+r.question.question_type_id+'">';
                    
                    t += '<h5 class="font-arial text-info" style="font-weight: bold;">Question</h5>';
                    t += '<h5 class="font-arial" style="font-size:15px;">'+(r.question.question_text ? r.question.question_text : '')+'</h5>';
                    if(r.question.question_file){
                        t += '<h5 class="font-arial text-info" style="font-weight: bold;">Question File</h5>';
                        $.each(r.question.question_file, function (k,v){
                            t += '<p><a href="'+base_url+'/files/question_file/'+v.file+'">'+v.file+'</a></p>';
                            t += '<p><a href="'+base_url+'/files/question_file/'+v.file+'">'+v.file+'</a></p>';
                        });
                    }
                    if(r.question.question_image){
                        t += '<h5 class="font-arial text-info" style="font-weight: bold;">Question Image</h5>';
                        $.each(r.question.question_image, function (k,v){
                            t += '<img class="img-thumbnail" src="'+base_url+'/files/question_image/'+v.image+'" style="min-width:550px;margin-bottom:10px;">';
                        });
                    }
                    if(r.question.question_answers){
                        switch(r.question.question_type_id){
                            case '1':
                                t += '<div class="radio radio-success">';
                                    $.each(r.question.question_answers, function (k,v){
                                        t += '<input type="radio" value="'+v.answers_id+'" name="correct_answers" class="choose_answers" id="opt_'+v.answers_id+'" '+(v.answers_id === r.answers_question ? 'checked' : '')+'>';
                                        t += '<label for="opt_'+v.answers_id+'" style="margin-bottom: 10px;font-size:15px;color:black;">'+v.question_answers+'</label><br>';
                                    });
                                t += '</div>';  
                                break;
                            case '3':
                                t += '<textarea class="form-control">'+(r.answers_question ? r.answers_question : '')+'</textarea>';
                                break;
                            case '2':
                                t += '<input type="file" name="correct_answers" class="form-control" id="correct_answers_file">';
                                t += '<p id="your-file">Your File : <a href="'+base_url+'files/applicant_upload/'+r.answers_question+'">'+r.answers_question+'</a></p>';
                                break;
                            case '5':
                                t += '<a href="#" class="btn-more" data-id="'+r.question.id+'" style="margin-bottom:5px;"><i class="fa fa-plus"></i> Add Statement & Holder</a>';
                                t += '<div id="statement-holder">';
                                    if(r.answers_question){
                                        $.each(r.answers_question, function (k,v){
                                            t += '<div class="row statement-holder-row" style="margin-bottom:5px;">';
                                                t += '<div class="col-md-5">';
                                                    t += '<input type="text" name="sh[statement][]" class="form-control" placeholder="Add Statement..." value="'+v.statement+'">';
                                                t += '</div>';
                                                t += '<div class="col-md-5">';
                                                    t += '<input type="text" name="sh[holder][]" class="form-control tagsinput" placeholder="Add Holder..." value="'+v.holder+'">';
                                                t += '</div>';
                                                t += '<div class="col-md-2">';
                                                    t += '<button class="btn btn-danger btn-sm btn-remove" type="button"><i class="fa fa-remove"></i></button>';
                                                t += '</div>';
                                            t += '</div>';
                                        });
                                    }
                                t += '</div>';
                                t += '<h6 class="font-arial text-muted">*Use enter to lock your answers</h6>';
                                break;

                            case '6':
                                t += '<div id="statement-holder">';
                                    t += '<div class="row statement-holder-row" style="margin-bottom:5px;">';
                                        t += '<div class="col-md-12">';
                                            t += '<h6 class="font-arial">Person</h6>';
                                            t += '<input type="text" name="ner[person]" class="tagsinput form-control" value="'+(r.answers_question.person ? r.answers_question.person : '')+'">';
                                        t += '</div>';
                                    t += '</div>';
                                    t += '<div class="row statement-holder-row" style="margin-bottom:5px;">';
                                        t += '<div class="col-md-12">';
                                            t += '<h6 class="font-arial">Organization</h6>';
                                            t += '<input type="text" name="ner[organization]" class="tagsinput form-control" value="'+(r.answers_question.organization ? r.answers_question.organization : '')+'">';
                                        t += '</div>';
                                    t += '</div>';
                                    t += '<div class="row statement-holder-row" style="margin-bottom:5px;">';
                                        t += '<div class="col-md-12">';
                                            t += '<h6 class="font-arial">Location</h6>';
                                            t += '<input type="text" name="ner[location]" class="tagsinput form-control" value="'+(r.answers_question.location ? r.answers_question.location : '')+'">';
                                        t += '</div>';
                                    t += '</div>';
                                    t += '<h6 class="font-arial text-muted">*Use enter to lock your answers</h6>';
                                t += '</div>';
                                break;

                            case '7':
                                t += '<div class="radio radio-success">';
                                    $.each(r.question.question_answers, function (k,v){
                                        var re_answers = v.question_answers_alias ? v.question_answers_alias : v.answers_id;
                                        t += '<input type="radio" value="'+re_answers+'" name="correct_answers" class="choose_answers" id="opt_'+v.answers_id+'" '+(re_answers === r.answers_question ? 'checked' : '')+'>';
                                        t += '<label for="opt_'+v.answers_id+'" style="margin-bottom: 10px;font-size:15px;color:black;">'+v.question_answers+'</label><br>';
                                    });
                                t += '</div>';  
                                break;
                        }
                    }else{
                        t += '<h5 class="font-arial text-center text-danger">No Result Answers</h5>';
                    }
                    t += '<hr>';
                    if(r.question.id != r.min_soal){
                        t += '<button style="margin-top:10px;" name="mode" class="btn btn-default saving-answers pull-left" type="submit" value="previous"><i class="fa fa-chevron-left"></i> Previous</button>&nbsp;';    
                    }
                    if(r.question.id != r.max_soal){
                        t += '<button style="margin-top:10px;" name="mode" class="btn btn-default saving-answers pull-right" type="submit" value="next">Next <i class="fa fa-chevron-right"></i></button>';
                    }

                    $(container + ' .question-section').html(t);
                    $('.tagsinput').tagsinput();
                    if(r.question.question_type_id == 3){
                        tinymce.init({
                            selector: 'textarea',
                            setup:function(ed) {
                               ed.on('change', function(e) {
                                    $('#form-save-answers').ajaxSubmit({
                                        url: site_url + '/tests/widget/save_answers',
                                        type: 'POST',
                                        data:{
                                            type : 'save',
                                            correct_answers : ed.getContent()
                                        },
                                        dataType: 'JSON',
                                        success: function(r) {
                                            if(r.answered){
                                                $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                                                $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:2px;"></i>');
                                            }
                                            if(!r.success){
                                                alert('Failed to save your answers');
                                            }
                                        },
                                        error: function() {
                                            alert('function failed');
                                        }
                                    });
                               });
                            }
                        });
                    }
                }else{
                    alert(r.msg);
                }
                $('#csrf').val(r.csrf);
                
            }
        });
    }
    
    if(_question_id !== ''){
        $(this).get_questions();
    }

    $(this).on('click', container + ' .btn-more', function (e){
        var id = $(this).data('id');
        t = '';
        t += '<div class="row statement-holder" style="margin-bottom:5px;">';
            t += '<div class="col-md-5">';
                t += '<input type="text" name="sh[statement][]" class="form-control" placeholder="Add Statement...">';
            t += '</div>';
            t += '<div class="col-md-5">';
                t += '<input type="text" name="sh[holder][]" class="form-control tagsinput" placeholder="Add Holder..." data-role="tagsinput">';
            t += '</div>';
            t += '<div class="col-md-2">';
                t += '<button class="btn btn-danger btn-sm btn-remove" type="button"><i class="fa fa-remove"></i></button>';
            t += '</div>';
        t += '</div>';
        $('#statement-holder').append(t);
        $('.tagsinput').tagsinput('refresh');
        e.preventDefault();
    });

    $(this).on('click', container + ' .btn-remove', function (e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    
    $(this).on('change', container + ' #correct_answers_file', function (e){
        if( document.getElementById("correct_answers_file").files.length !== 0 ){
            $('#form-save-answers').attr('enctype', 'multipart/form-data');

            $('#form-save-answers').ajaxSubmit({
                url: site_url + '/tests/widget/save_answers',
                type: 'POST',
                data:{
                    type : 'save',
                    csrf_token_app : csrf_name
                },
                dataType: 'JSON',
                beforeSend: function(){
                     $('#your-file').html('<h6 class="font-arial"><i class="fa fa-spinner fa-spin"></i> Please Wait...</h6>');
                },
                success: function(r) {
                    if(r.answered){
                        $('#your-file').html('Your File : <a href="'+base_url+'files/applicant_upload/'+r.answers_file+'">'+r.answers_file+'</a></p>');
                        $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                        $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:2px;"></i>');
                    }
                    if(!r.success){
                        $('.container-fluid').pgNotification({
                            style: 'flip',
                            message: r.msg,
                            position: 'top-right',
                            timeout: 2000,
                            type: 'danger'
                        }).show();
                    }
                    $('#csrf').val(r.csrf);
                    
                },
                error: function() {
                    alert('function failed');
                }
            });
        }
    });
    
    $(this).on('click', container + ' .choose_answers', function (e){
        $('#form-save-answers').ajaxSubmit({
            url: site_url + '/tests/widget/save_answers',
            type: 'POST',
            data:{
                type : 'save',
                'csrf_token_app' : $('#csrf').val()
            },
            dataType: 'JSON',
            success: function(r) {
                if(r.answered){
                    $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                    $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:2px;"></i>');
                }
                if(!r.success){
                    $('.container-fluid').pgNotification({
                        style: 'flip',
                        message: 'Failed to save your answers',
                        position: 'top-right',
                        timeout: 2000,
                        type: 'danger'
                    }).show();
                }
                $('#csrf').val(r.csrf);
            },
            error: function() {
                alert('function failed');
            }
        });
    });


    $(this).on('submit', container + ' #form-save-answers', function (e) {
        var form = $(this);
        var mode = $(this).data('mode');
        $(this).ajaxSubmit({
            url: site_url + '/tests/widget/next_step',
            data:{
                type : 'none',
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function () {
                form.find('')
            },
            success: function(r) {
                if(r.success){
                    if(r.mode === 'previous'){
                        $(this).get_questions({
                            question_id : r.previous_id,
                            load : true
                        });
                    }
                    if(r.mode === 'next'){
                        $(this).get_questions({
                            question_id : r.next_id,
                            load : true
                        });
                    }
                }else{
                    $('.container-fluid').pgNotification({
                        style: 'flip',
                        message: 'Failed to save your answers',
                        position: 'top-right',
                        timeout: 2000,
                        type: 'danger'
                    }).show();
                }
            },
            error: function() {
                alert('function failed');
            }
        });
        e.preventDefault();
    });

    $(this).on('click', container + ' .saving-answers', function (e){
        $('.mode-saving').val($(this).val());
    });

 	$(container + ' #clock-time').countdown(end_time)
        .on('update.countdown', function (event) {
            $(this).html(event.strftime('%H:%M:%S'));
        })
        .on('finish.countdown', function (event) {
            $(this).finish_tests();
            return false;
        });

 	$(this).on('click', container + ' .btn-back', function (e){
 		Widget.Loader('dashboard', {}, 'container-content', false);
	});

    $(this).on('click', container + ' .btn-finish-test', function (e){
        var conf = confirm('Are you sure ?');
        if(conf){
            $(this).finish_tests({
                default : false
            });
        }else{
            return false;
        }
    });

    $.fn.finish_tests = function(params) {
        var p = $.extend({
            default : true
        }, params);

        ajaxManager.addReq({
            url: site_url + '/tests/widget/finish_tests',
            type: 'POST',
            dataType: 'JSON',
            data: {
                test_id: reg_id,
                test_transaction_id: test_transaction_id,
                csrf_token_carrier : csrf_name
            },
            beforeSend: function () {
                if(p.default){
                    alert('Time has run out');
                }
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                if(r.success){
                    Widget.Loader('dashboard', {}, 'container-content', false);
                }else{
                    alert(r.msg);
                }
            }
        });
    }

	$(this).on('click', container + ' .btn-start-test', function (e){
		ajaxManager.addReq({
            url: site_url + '/tests/widget/register_tests',
            type: 'POST',
            dataType: 'JSON',
            data: {
                reg_id: reg_id,
                time_test: time_test,
                csrf_token_carrier : csrf_name
            },
            error: function (jqXHR, status, errorThrown) {
	         	error_handle(jqXHR, status, errorThrown);
	      	},
            success: function(r) {
            	var t = '';
                if(r.success) {
                	Widget.Loader('tests', {id:reg_id}, 'container-content', false);
                }else{
                	alert(r.msg);
                }
        	}
    	});

    	e.preventDefault();
   	});

    $(this).on('click', container + ' .detail-soal', function (e){
        var id = $(this).data('id');
        var question_type_id = $('#form-save-answers input[name="question_type_id"]').val();
        if(question_type_id == 5 || question_type_id == 6){
            $('#form-save-answers').ajaxSubmit({
                url: site_url + '/tests/widget/next_step',
                data:{
                    type : 'none',
                    csrf_token_carrier : csrf_name
                },
                type: 'POST',
                dataType: 'JSON',
                success: function(r) {
                    if(r.success){
                        $(this).get_questions({
                            question_id : id,
                            load : true
                        });
                    }else{
                        $('.container-fluid').pgNotification({
                            style: 'flip',
                            message: 'Failed to save your answers',
                            position: 'top-right',
                            timeout: 2000,
                            type: 'danger'
                        }).show();
                    }
                },
                error: function() {
                    alert('function failed');
                }
            });
        }else{
            $(this).get_questions({
                question_id : id,
                load : true
            });
            e.preventDefault();
        }
    });

})