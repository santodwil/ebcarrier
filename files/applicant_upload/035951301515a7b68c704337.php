<?php

class Board extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    private function get_member_info($field) {
        $this->load->model('member_model');
        $member_info = $this->member_model
            ->fields('team_id,nik')
            ->get(array(
                'users_id' => $this->_user->id
            ));
        return $member_info[$field];
    }

    private function get_plan() {
        $this->load->model('plan_model');
        $this->load->model('member_model');
        $this->load->model('plan_shared_model');

        $plan = $this->plan_model
            ->where('team_id', $this->get_member_info('team_id'))
            ->get_all();

        $plan_shared = $this->plan_shared_model
            ->with_plan()
            ->where('team_id', $this->get_member_info('team_id'))
            ->get_all();

        $shared_plan = array();
        if($plan_shared) {
            foreach($plan_shared as $v) {
                if (array_key_exists('plan', $v)){
                    $shared_plan[] = $v['plan'];
                }
            }
        }
        if($plan) {
            return array_merge($plan, $shared_plan);
        }
    }

    public function index() {
        $this->load->model('plan_model');
        $id = $this->input->get('plan_id');

        $plan = $this->get_plan();
        $plan_id = array();
        if($plan) {
            foreach ($plan as $key => $row) {
                $plan_type[$key]  = $row['plan_type_id'];
                $name[$key] = $row['name'];
                $plan_id[] = $row['id'];
            }
            array_multisort($plan_type, SORT_DESC, $name, SORT_ASC, $plan);
        }

        if($id) {
            if (!in_array($id, $plan_id)) {
                redirect('');
            }
        }
        $data = array(
            'title' => 'Board',
            'content' => 'board/board',
            '_css' => array(
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/uplon/plugins/select2/css/select2.min.css'
            ),
            '_js' => array(
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/plugins/sortable/sortable.js',
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/pages/board/board.js?v='.time()
            ),
            'menu_list' => $this->get_menu(),
            'plan' => $plan ? array_map('unserialize', array_unique(array_map('serialize', $plan))) : null,
            'active_plan' => $id ? $id : $plan[0]['id']
        );
        $this->render_page($data);
    }

    public function add_board() {
        $this->load->model('board_model');
        $name = $this->input->post('name');
        $description = $this->input->post('description');

        $data = array(
            'name' => $name,
            'description' => $description
        );
        $board_id = $this->board_model->insert($data);

        if($board_id) {
            $result = array(
                'status' => true,
                'message' => 'Success',
                'data' => array(
                    'id' => $board_id,
                    'name' => $name
                )
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed',
                'data' => false
            );
        }
        $this->json_result($result);
    }
    
    public function add_board_to_plan() {
        $this->load->model('board_model');
        $this->load->model('plan_board_model');
        $this->load->model('task_status_model');
        $this->load->model('board_task_status_model');

        $id = $this->input->post('board_id');
        $plan_id = $this->input->post('plan_id');
        $highest_sort = $this->plan_board_model
                    ->fields('sort')
                    ->where('plan_id', $plan_id)
                    ->order_by('sort', 'DESC')
                    ->get();

        if($id) {
            $sort = $highest_sort['sort'];
            $data = array();
            $board_status = array();
            foreach($id as $v) {
                $sort++;
                $data[] = array(
                    'board_id' => $v,
                    'plan_id' => $plan_id,
                    'sort' => $sort
                );
                $cur_board_status = $this->board_task_status_model->get_all($v);
                if($cur_board_status) {
                    foreach($cur_board_status as $kk => $vv) {
                        $board_status[] = array(
                            'board_id' => $v,
                            'task_status_id' => $vv['task_status_id']
                        );
                    }
                } else {
                    $board_status[] = array(
                        'board_id' => $v,
                        'task_status_id' => 1
                    );
                }
            }
            $update = $this->plan_board_model->insert($data);
            if($update) {
                $this->board_task_status_model->insert($board_status, TRUE);
                $task_status = $this->task_status_model->get_all();
                $board = $this->board_model
                    ->where('id', $id)
                    ->with_board_task_status()
                    ->get_all();
                if($board) {
                    foreach($board as $k => $v) {
                        foreach($v['board_task_status'] as $kk => $vv) {
                            $board[$k]['task_status_id'][] = $vv['task_status_id'];
                        }
                    }
                }

                $result = array(
                    'status' => true,
                    'message' => 'Success',
                    'data' => array(
                        'board' => $board,
                        'plan_id' => $plan_id,
                        'task_status' => $task_status
                    )
                );
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Failed',
                    'data' => false
                );
            }
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed',
                'data' => false
            );
        }
        $this->json_result($result);
    }

    public function save_board() {
        $this->load->model('plan_board_model');
        $this->load->model('board_task_status_model');
        $plan_id = $this->input->post('plan_id');
        $boards = $this->input->post('board');

        if($boards) {
            $data = array();
            $data_status = array();
            foreach($boards as $board) {
                foreach($board as $k => $v) {
                    $v['plan_id'] = $plan_id;
                    $v['sort'] = $k+1;
                    $data[] = $v;
                    if(array_key_exists('task_status_id', $v)) {
                        foreach($v['task_status_id'] as $kk => $vv) {
                            $board_status = array(
                                'board_id' => $v['board_id'],
                                'task_status_id' => $vv
                            );
                            $data_status[] = $board_status;
                        }
                    }
                    $this->board_task_status_model->force_delete($v['board_id']);
                }
            }

            $insert = $this->plan_board_model->insert($data, FALSE, TRUE);
            if($insert) {
                if($data_status) {
                    $insert_board_status = $this->board_task_status_model->insert($data_status, FALSE, TRUE);
                } else {
                    $insert_board_status = true;
                }
                if($insert_board_status) {
                    $result = array(
                        'status' => true,
                        'message' => 'Success'
                    );
                } else {
                    $result = array(
                        'status' => false,
                        'message' => 'Failed to set board status'
                    );
                }
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Failed'
                );
            }
        } else {
            $result = array(
                'status' => false,
                'message' => 'No change detected'
            );
        }

        $this->json_result($result);
    }

    public function edit_board() {
        $this->load->model('board_model');

        $id = $this->input->post('board_id');
        $name = $this->input->post('name');
        $description = $this->input->post('description');

        $data = array(
            'name' => $name,
            'description' => $description
        );
        $update = $this->board_model->update($data, $id);
        if($update) {
            $result = array(
                'status' => true,
                'message' => 'Success'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to edit board'
            );
        }
        $this->json_result($result);
    }

    public function hide_board() {
        $this->load->model('plan_board_model');
        $this->load->model('board_task_status_model');
        $id = $this->input->post('board_id');

        $board_task_status = $this->board_task_status_model
                            ->where('board_id', $id)
                            ->fields('*count*')
                            ->get();
        $update = $this->plan_board_model->update(array(
            'hide' => 1
        ), array(
            'board_id' => $id
        ));
        if($update) {
            $result = array(
                'status' => true,
                'message' => 'Success',
                'data' => array(
                    'board_id' => $id,
                    'task_status_count' => $board_task_status['counted_rows']
                )
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to hide board'
            );
        }
        $this->json_result($result);
    }

    public function add_swimlane() {
        $this->load->model('swimlane_model');
        $name = $this->input->post('name');
        $description = $this->input->post('description');

        $data = array(
            'name' => $name,
            'description' => $description
        );
        $swimlane_id = $this->swimlane_model->insert($data);

        if($swimlane_id) {
            $result = array(
                'status' => true,
                'message' => 'Success',
                'data' => array(
                    'id' => $swimlane_id,
                    'name' => $name
                )
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed',
                'data' => false
            );
        }
        $this->json_result($result);
    }

    public function add_swimlane_to_plan() {
        $this->load->model('swimlane_model');
        $this->load->model('plan_swimlane_model');

        $id = $this->input->post('swimlane_id');
        $plan_id = $this->input->post('plan_id');
        $highest_sort = $this->plan_swimlane_model
            ->fields('sort')
            ->where('plan_id', $plan_id)
            ->order_by('sort', 'DESC')
            ->get();

        if($id) {
            $sort = $highest_sort['sort'];
            $data = array();
            foreach($id as $v) {
                $sort++;
                $data[] = array(
                    'swimlane_id' => $v,
                    'plan_id' => $plan_id,
                    'sort' => $sort
                );
            }
            $update = $this->plan_swimlane_model->insert($data);
            if($update) {
                $swimlane = $this->swimlane_model
                    ->where('id', $id)
                    ->get_all();

                $result = array(
                    'status' => true,
                    'message' => 'Success',
                    'data' => array(
                        'swimlane' => $swimlane,
                        'plan_id' => $plan_id
                    )
                );
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Failed',
                    'data' => false
                );
            }
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed',
                'data' => false
            );
        }
        $this->json_result($result);
    }

    public function save_swimlane() {
        $this->load->model('plan_swimlane_model');
        $plan_id = $this->input->post('plan_id');
        $swimlanes = $this->input->post('swimlane');

        if($swimlanes) {
            $data = array();
            foreach($swimlanes as $swimlane) {
                foreach($swimlane as $k => $v) {
                    $v['plan_id'] = $plan_id;
                    $v['sort'] = $k+1;
                    $data[] = $v;
                }
            }

            $insert = $this->plan_swimlane_model->insert($data, FALSE, TRUE);
            if($insert) {
                $result = array(
                    'status' => true,
                    'message' => 'Success'
                );
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Failed'
                );
            }
        } else {
            $result = array(
                'status' => false,
                'message' => 'No change detected'
            );
        }

        $this->json_result($result);
    }

    public function add_task() {
        $this->load->model('task_model');
        $plan_id = $this->input->post('plan_id');
        $name = $this->input->post('task_name');
        $description = $this->input->post('task_description');
        $type = $this->input->post('task_type');
        $status = $this->input->post('task_status');
        $start_at = $this->input->post('start_at');
        $due_date = $this->input->post('due_date');
        $assign_to = $this->input->post('assign_to') ? $this->input->post('assign_to') : null;

        $data = array(
            'name' => $name,
            'description' => $description,
            'plan_id' => $plan_id,
            'task_type_id' => $type,
            'status' => $status,
            'start_at' => $start_at ? date('Y-m-d', strtotime($start_at)) : null,
            'due_at' => $due_date ? date('Y-m-d H:i:s', strtotime($due_date)) : null,
            'team_id' => $this->get_member_info('team_id'),
            'nik' => $this->get_member_info('nik'),
            'assign' => $assign_to
        );

        $task_id = $this->task_model->insert($data);

        if($task_id) {
            $task = $this->task_model
                    ->with_task_type()
                    ->get($task_id);
            $result = array(
                'status' => true,
                'message' => 'Success',
                'task' => $task
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed'
            );
        }

        $this->json_result($result);
    }

    public function edit_swimlane() {
        $this->load->model('swimlane_model');

        $id = $this->input->post('swimlane_id');
        $name = $this->input->post('name');
        $description = $this->input->post('description');

        $data = array(
            'name' => $name,
            'description' => $description
        );
        $update = $this->swimlane_model->update($data, $id);
        if($update) {
            $result = array(
                'status' => true,
                'message' => 'Success'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to edit swimlane'
            );
        }
        $this->json_result($result);
    }

    public function update_task_board_position() {
        $this->load->model('task_board_model');
        $this->load->model('task_board_log_model');
        $this->load->model('task_model');

        $board_id = $this->input->post('board_id');
        $task_status_id = $this->input->post('task_status_id');
        $swimlane_id = $this->input->post('swimlane_id') ? $this->input->post('swimlane_id') : null;
        $task_id = $this->input->post('task_id');

        $data = array(
            'board_id' => $board_id,
            'swimlane_id' => $swimlane_id
        );
        $this->task_board_model->update($data, $task_id);
        $this->task_model->update(array(
            'status' => $task_status_id
        ), $task_id);

        $data_log = array(
            'task_id' => $task_id,
            'board_id' => $board_id,
            'swimlane_id' => $swimlane_id,
            'nik' => $this->get_member_info('nik')
        );
        $this->task_board_log_model->insert($data_log);

        $result = array(
            'status' => true,
            'message' => 'Success'
        );
        $this->json_result($result);
    }

    public function save_task() {
        $this->load->model('task_board_model');

        $board_id = $this->input->post('board_id');
        $task_id = $this->input->post('task_id') ? $this->input->post('task_id') : NULL;

        if($task_id) {
            $data = array();
            foreach($task_id as $k => $v) {
                $data[] = array(
                    'task_id' => $v,
                    'board_id' => $board_id
                );
            }

            $this->task_board_model->insert($data);
            $result = array(
                'status' => true,
                'message' => 'Success'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'No task selected'
            );
        }

        $this->json_result($result);
    }

    public function share_plan() {
        $this->load->model('plan_shared_model');

        $plan_id = $this->input->post('plan_id');
        $team_id = $this->input->post('team_id');

        $this->plan_shared_model->delete($plan_id);
        if($team_id) {
            $data = array();
            foreach($team_id as $k => $v) {
                $data[] = array(
                    'plan_id' => $plan_id,
                    'team_id' => $v
                );
            }
            $this->plan_shared_model->insert($data);
        }
        $result = array(
            'status' => true,
            'message' => 'Success'
        );
        $this->json_result($result);
    }

    public function hide_swimlane() {
        $this->load->model('plan_swimlane_model');
        $id = $this->input->post('swimlane_id');

        $update = $this->plan_swimlane_model->update(array(
            'hide' => 1
        ), array(
            'swimlane_id' => $id
        ));
        if($update) {
            $result = array(
                'status' => true,
                'message' => 'Success'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to hide swimlane'
            );
        }
        $this->json_result($result);
    }

    public function update_start_at() {
        $this->load->model('task_model');

        $task_id = $this->input->post('task_id');
        $date = date('Y-m-d', strtotime($this->input->post('date')));

        $this->task_model
            ->update(array('start_at' => $date), $task_id);
    }

    public function remove_start_at() {
        $this->load->model('task_model');

        $task_id = $this->input->post('task_id');

        $this->task_model
            ->update(array(
                'start_at' => null
            ), $task_id);
    }

    public function update_assign_to() {
        $this->load->model('task_model');

        $task_id = $this->input->post('task_id');
        $member_id = $this->input->post('member_id');

        $update = $this->task_model
            ->update(array(
                'assign' => $member_id
            ), $task_id);

        if($update) {
            $result = array(
                'status' => true,
                'message' => 'Success'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Error'
            );
        }

        $this->json_result($result);
    }

    public function analytics() {
        $id = $this->input->get('plan_id');

        $plan = $this->get_plan();
        $plan_id = array();
        foreach ($plan as $key => $row) {
            $plan_type[$key]  = $row['plan_type_id'];
            $name[$key] = $row['name'];
            $plan_id[] = $row['id'];
        }
        array_multisort($plan_type, SORT_DESC, $name, SORT_ASC, $plan);

        if($id) {
            if (!in_array($id, $plan_id)) {
                redirect('');
            }
        }

        $data = array(
            'title' => 'Board Analytics',
            'content' => 'board/board_analytics',
            '_css' => array(
                'assets/plugins/highchart/css/highcharts.css',
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/uplon/plugins/bootstrap-daterangepicker/daterangepicker.css',
                'assets/uplon/plugins/select2/css/select2.min.css'
            ),
            '_js' => array(
                'assets/plugins/highchart/highcharts.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/plugins/sortable/sortable.js',
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/uplon/plugins/bootstrap-daterangepicker/daterangepicker.js',
                'assets/pages/board/board_analytics.js?v='.time()
            ),
            'menu_list' => $this->get_menu(),
            'plan' => array_map('unserialize', array_unique(array_map('serialize', $plan))),
            'active_plan' => $id ? $id : $plan[0]['id']
        );
        $this->render_page($data);
    }

    public function get_task_distribution() {
        $this->load->model('task_distribution_model');
        $this->load->model('board_cumulative_flow_model');

        if(php_sapi_name() != 'cli'){
            redirect('');
        }

        $task_distribution = $this->task_distribution_model->cumulative();
        if($task_distribution) {
            foreach($task_distribution as $k => $v) {
                $task_distribution[$k]['d_day'] = date('Ymd');
                $task_distribution[$k]['d_month'] = date('Ym');
                $task_distribution[$k]['d_year'] = date('Y');
            }
        }

        $this->board_cumulative_flow_model->insert($task_distribution, FALSE, TRUE);
    }

    public function test() {
        $this->load->model('task_board_model');

        $board = $this->task_board_model
                ->get_all();

        $this->json_result($board);
    }
}