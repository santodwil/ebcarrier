<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">
<style type="text/css">
	/*.table tbody tr td{
		padding:0;
		margin:0;
		border-bottom: 0px;
		font-size: 13px;
		margin-top: 5px;
	}	
	.table thead tr th{
		padding:0;
		border:0px;
		color:#797777;
		font-family: Arial;
	}*/
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
	/*.input-group-addon{
		background:none;
		color:black;
	}*/
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
    <div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
    		<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Search</span>
							<input type="text" id="search" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Status</span>
							<select class="form-control" id="status">
								<option value="all">All</option>
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>
					<div class="col-md-3 pull-right">
						<div class="btn-group dropdown-default pull-right"> 
							<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#" style="width: auto;" aria-expanded="true"> <i class="fa fa-gear"></i> <span class="caret"></span> 
							</a>
                          	<ul class="dropdown-menu " style="width: 150px;">
								<li style="font-size: 13px;"><a href="#" class="bulk_action" data-value="1">Set Active</a></li>  	
								<li style="font-size: 13px;"><a href="#" class="bulk_action" data-value="0">Set Inactive</a></li>
                          </ul>
                        </div>
                        <button class="btn btn-default new-data pull-right"><i class="fa fa-plus"></i> New Data</button>
					</div>
				</div>
    		</div>
          	<div class="panel-body" style="margin-top:-20px;"">
				<div class="row">
					<table class="table table-condensed table-striped">
						<thead>
							<th width="20">
			                    <input type="checkbox" class="select_id" id="checkbox_choose">
		                      	<label for="checkbox_choose"></label>
							</th>
							<th width="400">Name</th>
							<th width="150" class="text-center">Question</th>
							<th width="150" class="text-center">Time</th>
							<th class="text-center">Status</th>
							<th class="text-center">Action</th>
						</thead>
						<tbody id="section-data"></tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
          	</div>
      	</div>
	</div>

	<div class="modal fade slide-up disable-scroll in" id="modal-new-data" style="overflow: auto;">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content">
		          	<div class="modal-body" style="margin-top: 15px;">
		          		<div class="alert alert-info alert-save" role="alert" style="display: none;"></div>
		            	<form role="form" id="form-create">
			                <div class="row">
			                  <div class="col-sm-12">
			                    <div class="form-group">
			                      	<label>Name</label>
			                      	<input type="text" class="form-control" name="name" required="">
			                    </div>
			                  </div>
			                </div>	
			                <div class="row">
			                  	<div class="col-sm-6">
			                    	<div class="form-group">
			                      		<label>Question</label>
		                      			<input type="number" class="form-control" name="question" required="">
			                    	</div>
			                  	</div>
			                  	<div class="col-sm-6">
			                    	<div class="form-group">
			                      		<label>Times</label>
			                      		<div class="input-group">
			                      			<input type="number" class="form-control" name="times" required="">
			                      			<span class="input-group-addon default">
				                                Minutes
				                            </span>
		                      			</div>
			                    	</div>
			                  	</div>
			                </div>
			                <div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Guide Test</label>
										<textarea class="form-control" name="guidetest"></textarea>
									</div>
								</div>
			                </div>
			                <div class="row">
								<div class="col-sm-12">
									<div class="btn-group pull-right">
					                	<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Back</button>
					                	<button type="submit" class="btn btn-danger"><i class="fa fa-save"></i> Save</button>
				              		</div>
				              	</div>
			                </div>
			            </form>
		          	</div>
        		</div>
      		</div>
    	</div>
  	</div>

  	<div class="modal fade slide-up disable-scroll in" id="modal-modify-data" style="overflow: auto;">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content">
		          	<div class="modal-body" style="margin-top: 15px;">
		          		<div class="alert alert-info alert-save" role="alert" style="display: none;"></div>
		            	<form role="form" id="form-modify">
		            		<input type="hidden" name="id">
			                <div class="row">
			                  <div class="col-sm-12">
			                    <div class="form-group">
			                      	<label>Name</label>
			                      	<input type="text" class="form-control" name="name" required="">
			                    </div>
			                  </div>
			                </div>	
			                <div class="row">
			                  	<div class="col-sm-6">
			                    	<div class="form-group">
			                      		<label>Question</label>
		                      			<input type="number" class="form-control" name="question" required="">
			                    	</div>
			                  	</div>
			                  	<div class="col-sm-6">
			                    	<div class="form-group">
			                      		<label>Times</label>
			                      		<div class="input-group">
			                      			<input type="number" class="form-control" name="times" required="">
			                      			<span class="input-group-addon default">
				                                Minutes
				                            </span>
		                      			</div>
			                    	</div>
			                  	</div>
			                </div>
			                <div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Guide Test</label>
										<textarea class="form-control" name="guidetest" id="edit_guidetest"></textarea>
									</div>
								</div>
			                </div>
			                <div class="row">
								<div class="col-sm-12">
									<div class="btn-group pull-right">
					                	<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Back</button>
					                	<button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Change</button>
				                	</div>
				              	</div>
			                </div>
			            </form>
		          	</div>
        		</div>
      		</div>
    	</div>
  	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>