<?php

class Hrm_migration extends MY_Controller {

	public $_filepath;

    public function __construct() {
    	parent::__construct();
    	$this->_filepath = 'hrm_files';
    	$this->load->model('migration_model');
    	$this->migration_model->_database_connection = 'hrm';
        $this->migration_model->_set_connection();
	}

	private function _excel_reader($url = NULL, $filepath = './hrm_files/', $filename = '_get_file') {
        set_time_limit(0);
        $fullpath = $filepath . $filename;
        if ($url) {
            $file = file_get_contents($url);
            file_put_contents($fullpath, $file);
        }
        $this->load->library('PHPExcel');
        try {
            $result['status'] = TRUE;
            $result['result'] = PHPExcel_IOFactory::load($fullpath);
        } catch (PHPExcel_Reader_Exception $e) {
            $result['status'] = FALSE;
            $result['result'] = 'Error loading file "' . pathinfo($fullpath, PATHINFO_BASENAME) . '": ' . $e->getMessage();
        }

        return $result;
    }

    private function on_duplicate($table, $data, $exclude = array(), $db = 'hrm') {
        $this->_db = $this->load->database($db, TRUE);
        $updatestr = array();
        foreach ($data as $k => $v) {
            if (!in_array($k, $exclude)) {
               // $updatestr[] = '`' . $k . '`="' . mysql_real_escape_string($v) . '"'; // local
                // $updatestr[] = '`' . $k . '`="' . mysql_escape_string($v) . '"'; // server
               $updatestr[] = '`' . $k . '`="' . $v . '"'; // local
            }
        }
        $query = $this->_db->insert_string($table, $data);
        $query .= ' ON DUPLICATE KEY UPDATE ' . implode(', ', array_filter($updatestr));
        $this->_db->query($query);
        return $this->_db->affected_rows();
    }

    private function _check_name($name){
		$this->_db = $this->load->database('hrm', TRUE);
		$this->_db->where('nama_lengkap', $name);
		return $this->_db->count_all_results('eb_pegawai');
	}

    public function migration_member_tm($filename){
        $excel_reader = $this->_excel_reader(NULL, './migration/tm', $filename.'.xlsx');
        if ($excel_reader['status']) {
            $objPHPExcel = $excel_reader['result'];
            $data = array();
            $count_new = 0;
            $count_old = 0;
            $objPHPExcel->setActiveSheetIndex(0);   
            $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            foreach ($worksheet as $value) {
                if ($row != 1) {
                    $id = isset($value['A']) ? $value['A'] : NULL;
                    $email = isset($value['B']) ? $value['B'] : NULL;
                    $checking = $this->db->where('id', $id)->get('member');

                    if($checking->num_rows() > 0){
                        $update['email'] = $email;
                        $this->db->update('member', $update, array('id' => $id));
                        $count_old++;
                    }
                }
            }
            if($count_old > 0){
                echo 'Update Data : '.$count_old;
            }
        }else{
            echo 'failed reading file';
        }
    }

    public function migration_employee($filename){
        $this->db = $this->load->database('hrm', TRUE);
        $excel_reader = $this->_excel_reader(NULL, './migration/', $filename.'.xlsx');
        if ($excel_reader['status']) {
            $objPHPExcel = $excel_reader['result'];
            $data = array();
            $i = 0;
            $row = 1;
            $count_new = 0;
            $count_old = 0;
            $objPHPExcel->setActiveSheetIndex(0);   
            $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            foreach ($worksheet as $value) {
                if ($row != 1) {
                    $nik = isset($value['A']) ? $value['A'] : NULL;
                    $nama_lengkap = isset($value['B']) ? $value['B'] : NULL;
                    $nama_panggilan = isset($value['C']) ? $value['C'] : NULL;
                    $tempat_lahir = isset($value['D']) ? $value['D'] : NULL;
                    $tgl_lahir = isset($value['E']) ? $value['E'] : NULL;
                    $kelamin = isset($value['F']) ? $value['F'] : NULL;
                    $agama = isset($value['G']) ? $value['G'] : NULL;
                    $golongan_darah = isset($value['H']) ? $value['H'] : NULL;
                    $kewarganegaraan = isset($value['I']) ? $value['I'] : NULL;
                    $status_pernikahan = isset($value['J']) ? $value['J'] : NULL;
                    $nomor_npwp = isset($value['K']) ? $value['K'] : NULL;
                    $nomor_jamsostek = isset($value['L']) ? $value['L'] : NULL;
                    $ptkp_status = isset($value['M']) ? $value['M'] : NULL;
                    $ktp_nomor = isset($value['N']) ? $value['N'] : NULL;
                    $ktp_alamat = isset($value['O']) ? $value['O'] : NULL;
                    $ktp_kodepos = isset($value['P']) ? $value['P'] : NULL;
                    $ktp_rt = isset($value['Q']) ? $value['Q'] : NULL;
                    $ktp_rw = isset($value['R']) ? $value['R'] : NULL;
                    $ktp_kecamatan = isset($value['S']) ? $value['S'] : NULL;
                    $ktp_kelurahan = isset($value['T']) ? $value['T'] : NULL;
                    $ktp_kota = isset($value['U']) ? $value['U'] : NULL;
                    $ktp_provinsi = isset($value['V']) ? $value['V'] : NULL;
                    $tinggal_alamat = isset($value['W']) ? $value['W'] : NULL;
                    $tinggal_kodepos = isset($value['X']) ? $value['X'] : NULL;
                    $tinggal_rt = isset($value['Y']) ? $value['Y'] : NULL;
                    $tinggal_rw = isset($value['Z']) ? $value['Z'] : NULL;
                    $tinggal_kecamatan = isset($value['AA']) ? $value['AA'] : NULL;
                    $tinggal_kelurahan = isset($value['AB']) ? $value['AB'] : NULL;
                    $status_pegawai = isset($value['AC']) ? $value['AC'] : NULL;
                    $lokasi_kerja = isset($value['AD']) ? $value['AD'] : NULL;
                    $tanggal_masuk = isset($value['AE']) ? $value['AE'] : NULL;
                    $tanggal_masuk_ei = isset($value['AF']) ? $value['AF'] : NULL;

                    $checking = $this->_checking($nik, $nama_lengkap);

                    //Insert Mode////////////////////////////////////////////////////////////////
                    if(!$checking){
                        $insert['nik'] = $nik;
                        $insert['updated'] = date('Y-m-d');
                        $insert['nama_lengkap'] = $nama_lengkap;
                        $insert['nama_panggilan'] = $nama_panggilan;
                        $insert['tempat_lahir'] = $tempat_lahir;
                        $insert['tanggal_lahir'] = $tgl_lahir;
                        if($kelamin){
                            if($kelamin == 'L') { $insert['jenis_kelamin'] = 'Laki-Laki';}
                            if($kelamin == 'P') { $insert['jenis_kelamin'] = 'Perempuan';}
                        }
                        $insert['agama'] = $agama;
                        $insert['golongan_darah'] = $golongan_darah;
                        $insert['kewarganegaraan'] = $kewarganegaraan;
                        $insert['status_pernikahan'] = $status_pernikahan;
                        $insert['nomor_npwp'] = $nomor_npwp;
                        $insert['nomor_jamsostek'] = $nomor_jamsostek;
                        $insert['ptkp_status'] = $ptkp_status;
                        $insert['ktp_nomor'] = $ktp_nomor;
                        $insert['ktp_alamat'] = $ktp_alamat;
                        $insert['ktp_kodepos'] = $ktp_kodepos;
                        $insert['ktp_rt'] = $ktp_rt;
                        $insert['ktp_rw'] = $ktp_rw;
                        $insert['ktp_kecamatan'] = $ktp_kecamatan;
                        $insert['ktp_kelurahan'] = $ktp_kelurahan;
                        $insert['ktp_kota'] = $ktp_kota;
                        $insert['ktp_provinsi'] = $ktp_provinsi;
                        $insert['tinggal_alamat'] = $tinggal_alamat;
                        $insert['tinggal_kodepos'] = $tinggal_kodepos;
                        $insert['tinggal_rt'] = $tinggal_rt;
                        $insert['tinggal_rw'] = $tinggal_rw;
                        $insert['tinggal_kecamatan'] = $tinggal_kecamatan;
                        $insert['tinggal_kelurahan'] = $tinggal_kelurahan;
                        if($status_pegawai){
                            if($status_pegawai == 'P') { $insert['status_pegawai'] = 'Permanent';}
                            if($status_pegawai == 'C') { $insert['status_pegawai'] = 'Contract';}
                        }
                        $insert['lokasi_kerja'] = $lokasi_kerja;
                        $insert['tanggal_masuk'] = $tanggal_masuk;
                        $insert['jabatan'] = 19; // Cyber force
                        $insert['divisi'] = 18;

                        $this->on_duplicate('eb_pegawai', array_filter($insert));

                        $new_data[] = $checking.' - '.$nama_lengkap;
                        $count_new++;
                    //Update Mode////////////////////////////////////////////////////////////////
                    }else{
                        $update['nik'] = $nik;
                        $update['updated'] = date('Y-m-d');
                        $update['nama_lengkap'] = $nama_lengkap;
                        $update['nama_panggilan'] = $nama_panggilan;
                        $update['tempat_lahir'] = $tempat_lahir;
                        $update['tanggal_lahir'] = $tgl_lahir;
                        if($kelamin){
                            if($kelamin == 'L') { $update['jenis_kelamin'] = 'Laki-Laki';}
                            if($kelamin == 'P') { $update['jenis_kelamin'] = 'Perempuan';}
                        }
                        $update['agama'] = $agama;
                        $update['golongan_darah'] = $golongan_darah;
                        $update['kewarganegaraan'] = $kewarganegaraan;
                        $update['status_pernikahan'] = $status_pernikahan;
                        $update['nomor_npwp'] = $nomor_npwp;
                        $update['nomor_jamsostek'] = $nomor_jamsostek;
                        $update['ptkp_status'] = $ptkp_status;
                        $update['ktp_nomor'] = $ktp_nomor;
                        $update['ktp_alamat'] = $ktp_alamat;
                        $update['ktp_kodepos'] = $ktp_kodepos;
                        $update['ktp_rt'] = $ktp_rt;
                        $update['ktp_rw'] = $ktp_rw;
                        $update['ktp_kecamatan'] = $ktp_kecamatan;
                        $update['ktp_kelurahan'] = $ktp_kelurahan;
                        $update['ktp_kota'] = $ktp_kota;
                        $update['ktp_provinsi'] = $ktp_provinsi;
                        $update['tinggal_alamat'] = $tinggal_alamat;
                        $update['tinggal_kodepos'] = $tinggal_kodepos;
                        $update['tinggal_rt'] = $tinggal_rt;
                        $update['tinggal_rw'] = $tinggal_rw;
                        $update['tinggal_kecamatan'] = $tinggal_kecamatan;
                        $update['tinggal_kelurahan'] = $tinggal_kelurahan;
                        if($status_pegawai){
                            if($status_pegawai == 'P') { $update['status_pegawai'] =  'Permanent';}
                            if($status_pegawai == 'C') { $update['status_pegawai'] = 'Contract';}
                        }
                        $update['lokasi_kerja'] = $lokasi_kerja;
                        $update['tanggal_masuk'] = $tanggal_masuk;
                        $update['jabatan'] = 19; // Cyber force
                        $update['divisi'] = 18;
                        
                        if($checking == 1){
                            $this->db->update('eb_pegawai', $update, array('nik' => $nik));
                        }
                        if($checking == 2){
                            $this->db->update('eb_pegawai', $update, array('nama_lengkap' => $nama_lengkap));
                        }
                        if($checking == 3){
                            $this->db->update('eb_pegawai', $update, array('nik' => $nik, 'nama_lengkap' => $nama_lengkap));
                        }

                        $old_data[] = $checking.' - '.$nama_lengkap;
                        $count_old++;
                    }
                }
                $row++;
                $i++;
            }
            if($count_new > 0){
                echo 'New Data : '.$count_new;
                echo '<br>';
                foreach ($new_data as $key => $value) {
                    echo '- '.$value;
                    echo '<br>';
                }
            }
            
            echo '<hr>';

            if($count_old > 0){
                echo 'Update Data : '.$count_old;
                echo '<br>';
                foreach ($old_data as $key => $value) {
                    echo '- '.$value;
                    echo '<br>';
                }
            }
        }else{
            echo 'failed reading file';
        }
    }

    private function _checking($nik, $nama_lengkap){
        if($this->_check_nik($nik) > 0){
            return 1;
        }else{
            if($this->_check_nama_lengkap($nama_lengkap) > 0){
                return 2;
            }else{
                if($this->_check_nik_and_name($nik, $nama_lengkap) > 0){
                    return 3;
                }else{
                    return FALSE;
                } 
            }
        }
    }

    private function _check_nik($nik){
        $this->db = $this->load->database('hrm', TRUE);
        return $this->db->where('nik', $nik)->count_all_results('eb_pegawai');
    }

    private function _check_nama_lengkap($nama_lengkap){
        $this->db = $this->load->database('hrm', TRUE);
        $sql = "SELECT * FROM eb_pegawai WHERE nama_lengkap = '".$nama_lengkap."' OR nama_lengkap LIKE '".$nama_lengkap."'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    private function _check_nik_and_name($nik, $nama_lengkap){
        $this->db = $this->load->database('hrm', TRUE);
        $sql = "SELECT * FROM eb_pegawai WHERE nik = '".$nik."' AND nama_lengkap = '".$nama_lengkap."' OR nama_lengkap LIKE '".$nama_lengkap."'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	public function employee($filename){
		// $this->migration_model->tablename = 'eb_pegawai';
		// $pegawai = $this->migration_model->get(NULL, NULL, $params, NULL, 'ASC')->result_array();
		// echo $this->_filepath;
		$excel_reader = $this->_excel_reader(NULL, './hrm_files/', $filename);
        if ($excel_reader['status']) {
            $objPHPExcel = $excel_reader['result'];
            $data = array();
            $email = array();
            $i = 0;
            $row = 1;
            $objPHPExcel->setActiveSheetIndex(0);   
            $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            foreach ($worksheet as $value) {
                if ($row != 1) {
					$data[$i]['rc'] = isset($value['A']) ? $value['A'] : NULL;
					$data[$i]['tanggal_masuk'] = isset($value['B']) ? $value['B'] : NULL;
					$data[$i]['nik'] = isset($value['C']) ? $value['C'] : NULL;
					$data[$i]['new_nik'] = isset($value['D']) ? $value['D'] : NULL;
					$data[$i]['email'] = array($value['E'],$value['F']);
                }
                $row++;
                $i++;
            }
            $count = 0;
            $count_update = 0;
         	foreach ($data as $v) {
         		$total_name = $this->_check_name($v['nama_lengkap']);
         		if($total_name == 0){
	         		$tmp = array(
						'nama_lengkap' => $v['nama_lengkap'],
						'nik' => $v['nik'] !== '' ? $v['nik'] : $v['new_nik'],
						'new_nik' => $v['new_nik'],
						'tanggal_masuk' => $v['tanggal_masuk'],
						'divisi' => 79,
						'updated' => date('Y-m-d H:i:s')
					);
					$insert = $this->on_duplicate('eb_pegawai', array_filter($tmp));
					$insert ? $count++ : FALSE;

					if($v['email']){
						foreach ($v['email'] as $key => $value) {
							$tmp_email = array(
								'nik' => $v['nik'],
								'email' => $value,
								'tipe' => 'Kantor'
							);
							$inserts = $this->on_duplicate('eb_email', array_filter($tmp_email));
						}
					}
				}else{
					$this->_db = $this->load->database('hrm', TRUE);
					$pegawai = $this->_db->select('nik')->where('nama_lengkap', $v['nama_lengkap'])->get('eb_pegawai')->row_array();

					$update['new_nik'] = $v['new_nik'];
					$update['tanggal_masuk'] = $v['tanggal_masuk'];
					$update['divisi'] = 79;
					$update['updated'] = date('Y-m-d H:i:s');
					$rs_update = $this->_db->update('eb_pegawai', $update, array('nama_lengkap' => $v['nama_lengkap']));
					$rs_update ? $count_update++ : FALSE;

					if($v['email']){
						foreach ($v['email'] as $key => $value) {
							$tmp_email = array(
								'nik' => $pegawai['nik'],
								'email' => $value,
								'tipe' => 'Kantor'
							);
							$inserts = $this->on_duplicate('eb_email', array_filter($tmp_email));
						}
					}
				}
         	}
         	echo $count.' Data Inserted to database';
         	echo '<br>';
         	echo $count_update.' Data Updated to database';
        }else{
        	echo 'failed reading file';
        }
	}
}