$(function () {

	'use restrict';

	key_correct_answers = 1;

	$('#test_type').select2();
	$('#question_type').select2();

	$(this).on('click', container + ' .btn-submit', function (e){
		if($('#question_type').val() !== ''){
			$(this).render_question($('#test_type').val(), $('#question_type').val());
		}else{
			$.snackbar({
                content: 'Choose question type', 
                timeout: 5000
            });
		}
		e.preventDefault();
	});

	$.fn.render_question = function(test_type, question_type){
	 	t = '';
	 	var test_name = $( "#test_type option:selected" ).text();
	 	var question_type_name = $( "#question_type option:selected" ).text();
	 	
	 	t += '<form role="form" id="form-create" enctype="multipart/form-data">';
	 		t += '<input type="hidden" id="type_question" name="type_question" value="'+question_type+'">';
			t += '<input type="hidden" name="type_test" value="'+test_type+'">';
			t += '<h5 class="font-arial bold text-center">Mode</h5>';
			t += '<h6 class="font-arial text-center"><span class="label label-default">'+test_name+'</span> <span class="label label-default">'+question_type_name+'</span></h5>';
			t += '<hr>';

			if(question_type == 8 || question_type == 7){ // Special Mode
				for (i = 0; i < 2; i++) {
   					t += '<div class="row" style="margin-bottom:10px;padding-bottom:15px;">';
                        t += '<div class="col-md-6">';
                            t += '<label>Answers Option</label>';
                            t += '<input type="text" class="form-control" name="answers['+i+'][question_answers]" required="" value="">';
                        t += '</div>';
                        t += '<div class="col-md-6">';
                        	if(question_type == 7){
                            	t += '<label>Personality Types</label>';
                        	}
                    	 	if(question_type == 8){
                    	 		t += '<label>Value</label>';	
                    	 	}
                            t += '<select name="answers['+i+'][question_answers_alias]" class="form-control question_answers_alias" required="">';
                                if(question_type == 7){
                                	t += '<option value=""></option>';
	                                t += '<option value="I">Introvert</option>';
	                                t += '<option value="S">Sensing</option>';
	                                t += '<option value="T">Thinking</option>';
	                                t += '<option value="J">Judging</option>';
	                                t += '<option value="E">Ekstrovert</option>';
	                                t += '<option value="N">Intuition</option>';
	                                t += '<option value="F">Feeling</option>';
	                                t += '<option value="P">Perceiving</option>';
                                }
                                if(question_type == 8){
                            		t += '<option value=""></option>';
                                	t += '<option value="G">G</option>';
		                   			t += '<option value="L">L</option>';
		                   			t += '<option value="I">I</option>';
		                   			t += '<option value="J">T</option>';
		                   			t += '<option value="V">V</option>';
		                   			t += '<option value="S">S</option>';
		                   			t += '<option value="R">R</option>';
		                   			t += '<option value="D">D</option>';
		                   			t += '<option value="C">C</option>';
		                   			t += '<option value="E">E</option>';
		                   			t += '<option value="N">N</option>';
		                   			t += '<option value="A">A</option>';
		                   			t += '<option value="P">P</option>';
		                   			t += '<option value="X">X</option>';
		                   			t += '<option value="B">B</option>';
		                   			t += '<option value="O">O</option>';
		                   			t += '<option value="Z">Z</option>';
		                   			t += '<option value="K">K</option>';
		                   			t += '<option value="V">V</option>';
		                   			t += '<option value="W">W</option>';
                                }
                            t += '</select>';
                        t += '</div>';
                	t += '</div>';
                }
			}else{ // Basic Mode
				tinymce.remove();
   				t += '<div class="row">';
                    t += '<div class="col-md-8">';
                        t += '<h5 class="bold">Question Information</h5>';
                    	t += '<div class="form-group">';
							t += '<label>Question</label>';
							t += '<textarea class="form-control" name="question_text" id="question_text"></textarea>';
						t += '</div>';
						t += '<div class="form-group">';
							t += '<label>Description</label>';
							t += '<textarea class="form-control" name="question_description"></textarea>';
						t += '</div>';
                    t += '</div>';
                    t += '<div class="col-md-4">';
                        t += '<h5 class="bold">Additional Information</h5>';
                        t += '<div class="form-group">';
							t += '<label>Image</label>';
							t += '<input type="file" name="question_image" class="form-control">';
						t += '</div>';
						t += '<div class="form-group">';
							t += '<label>File</label>';
							t += '<input type="file" name="question_file" class="form-control">';
						t += '</div>';
                    t += '</div>';
                    if(question_type == 1){
	                    t += '<div class="col-md-12">';
	                    	t += '<h5 class="bold">Question Answers</h5>';
	                    	t += '<a href="" id="add-answers"><i class="fa fa-plus"></i> Add More Answers</a>';
	                    	t += '<div id="section-answers">';
	                    		t += '<div class="input-group" style="margin-bottom:10px;">';
	                    			t += '<div class="input-group-btn">';
				                        t += '<label class="btn btn-default btn-sm active" style="height:35px;">';
				                        	t += '<div class="radio radio-danger" style="margin:0;padding:0">'
				                            	t += '<input name="correct" value="0" type="radio" id="cr-0">';
				                            	t += '<label for="cr-0" style="margin:0;padding:0">Correct</label>';
				                   			t += '</div>';
				                        t += '</label>';
				                   	t += '</div>';
		                    		t += '<input type="text" class="form-control" name="answers[0]" required="" placeholder="Answers text...">';
					            t += '</div>';
					            t += '<div class="input-group" style="margin-bottom:10px;">';
					             	t += '<div class="input-group-btn">';
				                        t += '<label class="btn btn-default btn-sm active" style="height:35px;">';
				                            t += '<div class="radio radio-danger" style="margin:0;padding:0">'
				                            	t += '<input name="correct" value="1" type="radio" id="cr-1">';
				                            	t += '<label for="cr-1" style="margin:0;padding:0">Correct</label>';
				                   			t += '</div>';
				                        t += '</label>';
				                   	t += '</div>';
		                    		t += '<input type="text" class="form-control" name="answers[1]" required="" placeholder="Answers text...">';
					            t += '</div>';
	                 		t += '</div>';
	                 	t += '</div>';
                 	}
                t += '</div>';
			}
			
			t += '<div class="col-md-12 text-center" style="border-top:1px solid #e4e4e4;">';
				t += '<button type="submit" class="btn btn-danger btn-block" style="margin-top:20px;"><i class="fa fa-save"></i> Save Question</button>';
			t += '</div>';
	 	t += '</form>';

	 	$(container + ' #section-result').html(t);

	 	$('.question_answers_alias').select2();

	 	tinymce.init({
	        selector: '#question_text',
	        menubar: false
	    });

	    $(container + ' #add-answers').on('click', function (e) {
	    	key_correct_answers += 1;
	    	ans = '';
	    	ans += '<div class="input-group" style="margin-bottom:10px;">';
	    		ans += '<div class="input-group-btn">';
                    ans += '<label class="btn btn-default btn-sm active" style="height:35px;">';
                    	ans += '<div class="radio radio-danger" style="margin:0;padding:0">'
                        	ans += '<input name="correct" value="'+key_correct_answers+'" type="radio" id="cr-'+key_correct_answers+'">';
                        	ans += '<label for="cr-'+key_correct_answers+'" style="margin:0;padding:0">Correct</label>';
               			ans += '</div>';
                    ans += '</label>';
               	ans += '</div>';
        		ans += '<input type="text" class="form-control" name="answers['+key_correct_answers+']" required="" placeholder="Answers...">';
        		ans += '<div class="input-group-btn">';
        			ans += '<button type="button" class="btn btn-remove-answers"><i class="fa fa-trash"></i></button>';
    			ans += '</div>';
            ans += '</div>';
            $(container + ' #section-answers').append(ans);
	    	e.preventDefault();

	    	$(container + ' .btn-remove-answers').on('click', function (e) {
				$(this).parents('.input-group').remove();
		    	e.preventDefault();
		    });
	    });
 	};




 	$(this).on('submit', container + ' #form-create', function (e) {
        var form = $(this);
        var mode = $(this).data('mode');
        $(this).ajaxSubmit({
            url: site_url + '/new_question/widget/save',
            type: 'POST',
            dataType: 'JSON',
            data:{
            	'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('Submit');
            },
            success: function(r) {
            	$('#csrf').val(r.csrf);
                if(r.success){
                    form.resetForm();
                    $(".question_answers_alias").select2("val", "");
                }

                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-save"></i> Save Question');
                $.snackbar({
                    content: r.msg, 
                    timeout: 5000
                });
            },
        });
        e.preventDefault();
    });

	$(this).on('click', container + ' .btn-back', function (e) {
        Widget.Loader('questions', {}, 'container-content');
    });
});