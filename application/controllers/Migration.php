<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    private function excel_reader($url = NULL, $filepath = './migration/', $filename = '_get_file') {
        set_time_limit(0);
        $fullpath = $filepath . $filename;
        if ($url) {
            $file = file_get_contents($url);
            file_put_contents($fullpath, $file);
        }
        $this->load->library('PHPExcel');
        try {
            $result['status'] = TRUE;
            $result['result'] = PHPExcel_IOFactory::load($fullpath);
        } catch (PHPExcel_Reader_Exception $e) {
            $result['status'] = FALSE;
            $result['result'] = 'Error loading file "' . pathinfo($fullpath, PATHINFO_BASENAME) . '": ' . $e->getMessage();
        }


        return $result;
    }

    public function question_logika(){
        $filepath = './migration/';
        $filename = 'question_logika.xlsx';
        $excel_reader = $this->excel_reader(NULL, $filepath, $filename);
        if ($excel_reader['status']) {
            $objPHPExcel = $excel_reader['result'];
            $data = array();
            $i = 0;
            $row = 1;
            $objPHPExcel->setActiveSheetIndex(0);   
            $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            foreach ($worksheet as $value) {
                $data[$i]['question'] = isset($value['A']) ? str_replace("_x000D_", '<br>', utf8_encode($value['A'])) : NULL;
                $data[$i]['ans']['ans_a'] = isset($value['B']) ? $value['B'] : NULL;
                $data[$i]['ans']['ans_b'] = isset($value['C']) ? $value['C'] : NULL;
                $data[$i]['ans']['ans_c'] = isset($value['D']) ? $value['D'] : NULL;
                $data[$i]['ans']['ans_d'] = isset($value['E']) ? $value['E'] : NULL;
                $data[$i]['ans']['ans_e'] = isset($value['F']) ? $value['F'] : NULL;
                $data[$i]['correct'] = isset($value['G']) ? $value['G'] : NULL;
                $row++;
                $i++;
            }
            $count = 0;

            foreach ($data as $v) {
                $ins = array(
                    'question_text' => $v['question'],
                    'test_type_id' => 3,
                    'question_type_id' => 1
                );
                $this->db->insert('question', $ins);
                $question_id = $this->db->insert_id();

                foreach ($v['ans'] as $kk => $vv) {
                    $ins_answers = array(
                        'question_id' => $question_id,
                        'question_answers' => $vv,
                        'question_type' => 'text'
                    );
                    $this->db->insert('question_answers', $ins_answers);
                }
                switch ($v['correct']) {
                    case 'A':
                        $answers_correct_id = $this->db->select('id')->where('question_id', $question_id)->where('question_answers', $v['ans']['ans_a'])->get('question_answers')->row_array();
                        $this->db->update('question', array('correct_answers_id' => $answers_correct_id['id']), array('id' => $question_id));
                        break;
                    case 'B':
                        $answers_correct_id = $this->db->select('id')->where('question_id', $question_id)->where('question_answers', $v['ans']['ans_b'])->get('question_answers')->row_array();
                        $this->db->update('question', array('correct_answers_id' => $answers_correct_id['id']), array('id' => $question_id));
                        break;
                    case 'C':
                        $answers_correct_id = $this->db->select('id')->where('question_id', $question_id)->where('question_answers', $v['ans']['ans_c'])->get('question_answers')->row_array();
                        $this->db->update('question', array('correct_answers_id' => $answers_correct_id['id']), array('id' => $question_id));
                        break;
                    case 'D':
                        $answers_correct_id = $this->db->select('id')->where('question_id', $question_id)->where('question_answers', $v['ans']['ans_d'])->get('question_answers')->row_array();
                        $this->db->update('question', array('correct_answers_id' => $answers_correct_id['id']), array('id' => $question_id));
                        break;
                    case 'E':
                        $answers_correct_id = $this->db->select('id')->where('question_id', $question_id)->where('question_answers', $v['ans']['ans_e'])->get('question_answers')->row_array();
                        $this->db->update('question', array('correct_answers_id' => $answers_correct_id['id']), array('id' => $question_id));
                        break;
                }
            }
            echo 'Data Migrated';
        } else {
            echo 'failed reading file';
        }
    }

    public function question_papi(){
        $filepath = './migration/';
        $filename = 'papi_kostick.xlsx';
        $excel_reader = $this->excel_reader(NULL, $filepath, $filename);
        if ($excel_reader['status']) {
            $objPHPExcel = $excel_reader['result'];
            $data = array();
            $i = 0;
            $row = 1;
            $objPHPExcel->setActiveSheetIndex(0);   
            $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            foreach ($worksheet as $value) {
                $data[$value['A']]['statement'][] = isset($value['B']) ? $value['C'].':'.$value['B'] : NULL;
            }
            foreach ($data as $v) {
                $ins = array(
                    'test_type_id' => 4,
                    'question_type_id' => 8
                );
                $this->db->insert('question', $ins);
                $question_id = $this->db->insert_id();

                foreach ($v['statement'] as $kk => $vv) {
                    $st = explode(':', $vv);
                    $ins_answers = array(
                        'question_id' => $question_id,
                        'question_answers' => $st[1],
                        'question_type' => 'text',
                        'question_answers_alias' => $st[0]
                    );
                    $this->db->insert('question_answers', $ins_answers);
                }
            }
            echo 'Data Migrated';
        }else {
            echo 'failed reading file';
        }
    }
}