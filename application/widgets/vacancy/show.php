<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">

<style type="text/css">
	.table tbody tr td{
		padding:10px;
	}
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
	
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default" style="margin-bottom: 0">
					<div class="panel-heading" style="background-color: #fbfbfb;">
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
									<input type="text" id="search" class="form-control input-sm" placeholder="Search...">
								</div>
							</div>
						</div>
					</div>
  					<div class="panel-body" id="section-data">
						<ul class="nav nav-pills nav-stacked" id="tests_no" style="width:auto;">
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="panel panel-default" style="margin-bottom: 0">
					<div class="panel-heading" style="background-color: #fbfbfb;">
						<button type="button" class="btn btn-default btn-block btn-new-vacancy"><i class="fa fa-plus"></i> new Vacancy</button>
						<div class="row row-new" style="display: none;">
							<form id="form-save-vacancy">
								<div class="col-md-5">
									<div class="form-group">
										<label>Name</label>
										<input type="text" name="name" class="form-control" required="">
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label>Status</label>
										<select class="form-control" id="new-status" name="status">
											<option value="1"> Active</option>
											<option value="0"> Inactive</option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-danger btn-block" style="margin-top:25px;"><i class="fa fa-save"></i> Save</button>
								</div>
							</form>
						</div>
					</div>
					<hr style="margin:0">
					<div class="panel-body" id="section-result" style="height: 446px;overflow: auto">
						<h2 class="text-center font-arial muted bold" style="margin-top: 13%">Click vacancy to getting start</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
    var test_type_list = <?php echo json_encode($test_type_list); ?>;
</script>

<script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>