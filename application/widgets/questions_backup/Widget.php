<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('questions'); 
        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Questions',
            'question_type' => $this->db->get('question_type')->result_array(),
        );
        $this->render_widget($data);
    }

    public function get_tests(){
        $post = $this->input->post();
        $response['result'] = $this->questions->get_tests('get', $post['search']);
        $this->json_result($response);
    }

    public function detail_questions(){
        $post = $this->input->post();
        $response['questions'] = $this->db->select('a.*, b.name as question_type_name')
                                            ->join('question_type as b', 'a.question_type_id = b.id')
                                            ->where('a.test_type_id', $post['test_type_id'])
                                            ->get('question as a')->result_array();
        $response['test_type_id'] = $post['test_type_id'];
        $this->json_result($response);
    }

    public function review_question(){
        $post = $this->input->post();
        $response['question_id'] = $post['question_id'];
        $response['result'] = $this->questions->get_review($post['question_id']);
        $this->json_result($response);
    }

    public function save_question(){
        $post = $this->input->post();
        switch ($post['type_question']) {
            case '1':
                if(isset($post['answers']) && isset($post['correct'])){
                    $insert_question = array(
                        'question_text' => $post['question_text'],
                        'test_type_id' => $post['type_test'],
                        'question_type_id' => $post['type_question'],
                        'question_description' => $post['question_description'] != '' ? $post['question_description'] : NULL,
                    );
                    $insert = $this->db->insert('question', $insert_question);
                    if($insert){
                        $question_id = $this->db->insert_id();
                        foreach ($post['answers'] as $key => $value) {
                            $insert_answers = array(
                                'question_id' => $question_id,
                                'question_answers' => $value,
                                'question_type' => 'text',
                                'answers_sort' => $key
                            );
                            $rs_insert_answers = $this->db->insert('question_answers', $insert_answers);
                            if($rs_insert_answers){
                                $correct_answers = $this->db->select('id')->where('question_id', $question_id)->where('answers_sort', $post['correct'])->get('question_answers')->row_array();
                                $update_question_answers = $this->db->update('question', array('correct_answers_id' => $correct_answers['id']), array('id' => $question_id));
                                if($update_question_answers){   
                                    $response['success'] = TRUE;
                                    $response['msg'] = 'Question Successfully Added';
                                }else{
                                    $response['success'] = FALSE;
                                    $response['msg'] = 'Transaction For Correct Answers Failed';
                                }
                            }else{
                                $response['success'] = FALSE;
                                $response['msg'] = 'Transaction Answers Failed';
                            }
                        }
                    }else{
                        $response['success'] = FALSE;
                        $response['msg'] = 'Transaction Question Failed';
                    }
                }else{
                    $response['success'] = FALSE;
                    $response['msg'] = 'Answers option and correct answers required';
                }
                break;
        }
        $this->json_result($response);
    }

}