<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">
<style type="text/css">
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
	<div class="row" style="">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>

    <div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0;">
    		<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Search</i></span>
							<input type="text" id="search" class="form-control">
						</div>
					</div>
					<div class="col-md-2">
						<div class="input-group">
							<span class="input-group-addon">Date</span>
							<input type="hidden" id="created" value="">
							<input type="text" class="form-control date-data" value="">
						</div>
					</div>
					<div class="col-md-2">
						<div class="input-group">
							<span class="input-group-addon">Status</span>
							<select class="form-control" id="status">
								<option value="all">All</option>
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Vacancy</span>
							<select class="form-control" id="vacancy">
								<option value="all">All</option>
								<?php
								$vacancy = $this->db->where('status', 1)->get('vacancy_division');
								if($vacancy->num_rows() > 0){
									foreach ($vacancy->result_array() as $key => $value) {
										echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-1 pull-right">
						<div class="btn-group dropdown-default pull-right"> 
							<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#" style="width: auto;" aria-expanded="true"> <i class="fa fa-gear"></i> <span class="caret"></span> 
							</a>
                          	<ul class="dropdown-menu " style="width: auto;">
                          		<li style="font-weight: bold;text-align: center;">Set Status</li>
								<li style="font-size: 13px;"><a href="#" class="bulk_action" data-mode="status" data-value="1">Set Active</a></li>  	
								<li style="font-size: 13px;"><a href="#" class="bulk_action" data-mode="status" data-value="0">Set Inactive</a></li>
								<div class="divider"></div>

								<li style="font-weight: bold;text-align: center;">Reset Test</li>                     	
                      			<?php 
                      				$test_list = $this->db->select('id, name')->where('status', 1)->get('test_type');
                      				foreach ($test_list->result_array() as $key => $value) {
                      					echo '<li style="font-size: 13px;"><a href="#" class="bulk_action" data-mode="tests" data-value="'.$value['id'].'">'.$value['name'].'</a></li>';
                      				}
                      			?>
                          </ul>
                        </div>
					</div>
				</div>
    		</div>
          	<div class="panel-body" style="margin-top:-20px;"">
				<div class="row table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
							<tr>
								<th width="20">
			                       	<input type="checkbox" class="select_id" id="checkbox_choose">
		                      		<label for="checkbox_choose"></label>
								</th>
								<th width="200">Name</th>
								<th class="text-center" width="150">Degree</th>
								<th class="text-center" width="200">Email</th>
								<th class="text-center" width="200">Vacancy</th>
								<th class="text-center">Register</th>
								<th class="text-center" width="150">Status</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody id="section-data" class="table-result"></tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
          	</div>
      	</div>
	</div>
	
<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>" type="text/javascript"></script>