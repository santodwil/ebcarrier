<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->basic = array(1,2,3,5,6);
        $this->load->model('questions'); 
        $this->load->library('upload');
        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $data = array(
            'title' => 'Questions'
        );
        $this->render_widget($data);
    }

    public function get_tests(){
        $params = $this->input->get();
        $response['result'] = $this->questions->get_tests('get', $params);
        $response['total'] = $this->questions->get_tests('count', $params);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function question(){
        $params = $this->input->get();
        $response['result'] = $this->questions->question($params['test_id'], $params['question_type_id']);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function detail_test(){
        $params = $this->input->get();
        $response['group_question'] = $this->questions->group_question($params['test_id'])->result_array();
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function edit_question(){
        $params = $this->input->get();
        $response['result'] = $this->questions->edit_question($params['question_id'], $params['questiontypeid']);
        $response['success'] = TRUE;
        $this->json_result($response);
    }

    public function file_download($file){
        $this->load->helper('download');
        $temp_name = $this->db->select('temp_name')->where('file', $file)->get('question_file')->row_array();
        $data = file_get_contents('./files/question_file/'.$file.'');
        force_download($temp_name['temp_name'], $data);
    }

    public function change_question(){
        $params = $this->input->post();
        $response['msg'] = 'Data updated';

        if($params['question_type_id'] == 7 || $params['question_type_id'] == 8){ // Special Mode
            foreach ($params['answers'] as $key => $value) {
                if($value['question_answers_alias'] == $value['question_answers_alias_before']){
                    $this->db->update('question_answers', array('question_answers' => $value['question_answers']), array('id' => $key));
                }else{
                    $this->db->update('question_answers', array('question_answers' => $value['question_answers'], 'question_answers_alias' => $value['question_answers_alias']), array('id' => $key));
                    $this->db->update('test_answers', array('answers' => $value['question_answers_alias']), array('question_id' => $value['question_id'], 'answers' => $value['question_answers_alias_before']));
                    $response['msg'] = 'Data Update & you must regenerate test result for this test because personality type has change';
                }
            }
        }else{ // Basic Mode
            // Basic Information
            $_entity_update = array(
                'question_text' => $params['question_text'],
                'question_description' => $params['question_description']
            );
            $update_question = $this->db->update('question', $_entity_update, array('id' => $params['question_id']));
            if(!$update_question){
                $response['msg'] = 'Failed update basic information question';
            }

            //Insert to question file
            if (!empty($_FILES['question_file']['name'])){
                $configs['upload_path'] = './files/question_file/';
                $configs['allowed_types'] = '|doc|docx|pdf|txt|xls|xlsx';
                $configs['file_name'] = uniqid(date("his").$params['question_id']);

                $this->upload->initialize($configs);
                if (!$this->upload->do_upload('question_file')) {
                    $response['msg'] = $this->upload->display_errors();
                    $response['question_type_id'] = $params['question_type_id'];
                    $this->json_result($response);
                    exit();        
                }else{
                    $datafile = $this->upload->data();
                    if(isset($params['file_before']) && $params['file_before'] != ''){
                        $update_file = $this->questions->update_question_file($datafile['file_name'], $_FILES['question_file']['name'], $params['file_id']);
                        if($update_file){
                            if (file_exists('./files/question_file/'.$params['file_before'].'')) {
                                unlink('./files/question_file/'.$params['file_before'].'');
                            }
                        }
                    }else{
                        $this->questions->insert_question_file($datafile['file_name'], $_FILES['question_file']['name'], $params['question_id']);
                    }
                }
            }

            //Insert to question image
            if (!empty($_FILES['question_image']['name'])){
                $configs['upload_path']  = './files/question_image/';
                $configs['allowed_types'] = '|png|jpg|jpeg|gif|';
                $configs['file_name'] = uniqid(date("his").$params['question_id']);

                $this->upload->initialize($configs);
                if (!$this->upload->do_upload('question_image')) {
                    $response['msg'] = $this->upload->display_errors();
                    $response['question_type_id'] = $params['question_type_id'];
                    $this->json_result($response);
                    exit();        
                }else{
                    $datafile = $this->upload->data();
                    if(isset($params['image_before']) && $params['image_before'] != ''){
                        $update_image = $this->questions->update_question_image($datafile['file_name'], $_FILES['question_image']['name'], $params['image_id']);
                        if($update_image){
                            if (file_exists('./files/question_image/'.$params['image_before'].'')) {
                                unlink('./files/question_image/'.$params['image_before'].'');
                            }
                        }
                    }else{
                        $this->questions->insert_question_image($datafile['file_name'], $_FILES['question_image']['name'], $params['question_id']);
                    }
                }
            }

            //Update answers
            if(isset($params['answers'])){
                foreach ($params['answers'] as $key => $value) {
                    $this->db->update('question_answers', array('question_answers' => $value['question_answers']), array('id' => $key));
                }
                if($params['correct_answers_id_before'] != $params['correct']){
                    $this->db->update('question', array('correct_answers_id' => $params['correct']), array('id' => $params['question_id']));
                    $response['msg'] = 'Data Update & you must regenerate test result for this test because correct answers has change';
                }
            }
        }
        
        $response['question_type_id'] = $params['question_type_id'];
        $this->json_result($response);
    }


     public function change(){
        $params = $this->input->post();
        $response['question_type_id'] = $params['question_type_id'];
        $response['msg'] = 'Data updated';

        if(in_array($params['question_type_id'], $this->basic)){ // Condition basic question type
            // Update basic Information
            $_entity_update = array(
                'question_text' => $params['question_text'],
                'question_description' => $params['question_description']
            );
            $update_question = $this->db->update('question', $_entity_update, array('id' => $params['question_id']));
            if(!$update_question){
                $response['msg'] = 'Failed update basic information question';
            }
        }else{ // Condition special question type

        }

        //Insert to question file
        if (!empty($_FILES['question_file']['name'])){
            $configs['upload_path'] = './files/question_file/';
            $configs['allowed_types'] = '|doc|docx|pdf|txt|xls|xlsx';
            $configs['file_name'] = uniqid(date("his").$params['question_id']);

            $this->upload->initialize($configs);
            if (!$this->upload->do_upload('question_file')) {
                $response['msg'] = $this->upload->display_errors();
                $this->json_result($response);
                exit();        
            }else{
                $datafile = $this->upload->data();
                if(isset($params['file_before']) && $params['file_before'] != ''){
                    $update_file = $this->db->update('question_file', array('file' => $datafile['file_name'], 
                                                                            'temp_name' => $_FILES['question_file']['name']), 
                                                                        array('id' => $params['file_id']));
                    if($update_file){
                        if (file_exists('./files/question_files/'.$params['file_before'].'')) {
                            unlink('./files/question_files/'.$params['file_before'].'');
                        }
                    }
                }else{
                    $this->db->insert('question_file', array('file' => $datafile['file_name'], 
                                                            'temp_name' => $_FILES['question_file']['name'], 
                                                            'question_id' => $params['question_id']));
                }
            }
        }

        //Insert to question image
        if (!empty($_FILES['question_image']['name'])){
            $configs['upload_path']  = './files/question_image/';
            $configs['allowed_types'] = '|png|jpg|jpeg|gif|';
            $configs['file_name'] = uniqid(date("his").$params['question_id']);

            $this->upload->initialize($configs);
            if (!$this->upload->do_upload('question_image')) {
                $response['msg'] = $this->upload->display_errors();
                $this->json_result($response);
                exit();        
            }else{
                $datafile = $this->upload->data();
                if(isset($params['image_before']) && $params['image_before'] != ''){
                    $update_image = $this->db->update('question_image', array('image' => $datafile['file_name'], 
                                                                            'temp_name' => $_FILES['question_image']['name']), 
                                                                        array('id' => $params['image_id']));
                    if($update_image){
                        if (file_exists('./files/question_image/'.$params['image_before'].'')) {
                            unlink('./files/question_image/'.$params['image_before'].'');
                        }
                    }
                }else{
                    $this->db->insert('question_image', array('image' => $datafile['file_name'], 
                                                            'temp_name' => $_FILES['question_image']['name'], 
                                                            'question_id' => $params['question_id']));
                }
            }
        }

        
        $this->json_result($response);
    }

}