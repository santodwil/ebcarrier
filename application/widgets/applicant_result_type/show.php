<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  

	<div class="row">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>

    <div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
	    	<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-9">
						<div class="input-group">
							<input type="hidden" id="test_type_id" value="<?php echo $id; ?>">
							<input type="hidden" id="sdate" value="<?php echo date('Y-m-d'); ?>">
							<input type="hidden" id="edate" value="<?php echo date('Y-m-d'); ?>">
						 	<button type="button" id="range-date" class="btn btn-default">
							 	<i class="fa fa-calendar"></i> <?php echo date('d M Y') ?>
						 	</button>
					 	</div>
					</div>
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" id="search" class="form-control" placeholder="Search...">
						</div>
					</div>
				</div>
	    	</div>
	       	<div class="panel-body">
				 <table class="table table-hover table-striped">
					<thead>
						<th>No</th>
						<th>Name</th>
						<th>Result</th>
					</thead>
					<tbody id="section-data"></tbody>
				</table>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default bold">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
	       	</div>
   		</div>
	</div>

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>