$(function () {

	'use restrict';

	$.fn.get_tests = function(params) {
        var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/questions/widget/get_tests',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search
            },
            beforeSend: function () {
                show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
        		t = '';
        		var no = 0;
            	if(r.total > 0){
            		$.each(r.result, function (k,v){
            			p.offset++;
                         t += '<tr>';
                            t += '<td>'+v.name+'</td>';
                            t += '<td class="text-center">'+v.total_question+'</td>';
                            t += '<td class="text-center">'+v.time+'</td>';
                            t += '<td class="text-center">';
                                t += '<div class="btn-group">';
                                    t += '<button data-id="'+v.id+'" data-name="'+v.name+'" type="button" class="btn btn-xs btn-view-detail"><i class="fa fa-search"></i></button>';
                                t += '</div>';
                            t += '</td>';
                        t += '</tr>';
            		});
            	}else{
            		t += '<tr><td colspan="4"><h5 class="text-center font-arial">No Result</h5></td></tr>';
            	}
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
	        	hide_loading();
            }
        });
    }

    $.fn.question = function(params) {
    	var overlay = '<h4 class="font-arial bold text-center"><i class="fa fa-sync-alt fa-spin"></i></h4>';
        
        var p = $.extend({
        	question_type_id : null,
        	test_id : null
        }, params);
        ajaxManager.addReq({
            url: site_url + '/questions/widget/question',
            type: 'GET',
            dataType: 'JSON',
            data: {
                question_type_id: p.question_type_id,
                test_id : p.test_id
            },
            beforeSend: function () {
                $(container + ' #modal-detail').find('.modal-body').html(overlay);
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
            	t = '';
            	t += '<div class="col-md-12 panel-list-question" style="margin-top:10px;">';
	            	if(p.question_type_id == 1 || p.question_type_id == 2 || p.question_type_id == 3 || p.question_type_id == 5 || p.question_type_id == 6){
	            		if(r.result){
	            				$.each(r.result, function(k,v){
                                    t += '<h6 class="font-arial bar-question">'+v.question_text+'</h6>';
                                    t += '<a href="" class="btn-edit-question" data-questionid="'+v.question_id+'" data-questiontypeid="'+v.question_type_id+'"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;';
                                    t += '<a href="" class="text-danger btn-delete-question" data-questionid="'+v.question_id+'" data-questiontypeid="'+v.question_type_id+'"><i class="fa fa-trash"></i> Delete</a>';
                                    t += '<hr>';
	            				});
	            		}else{
	            			t += 'No Result';
	            		}
	            	}else{
	            		if(r.result){
            				$.each(r.result, function(k,v){
								$.each(v.question_answers, function(kk,vv){
									t += '<h6 class="font-arial bar-question">'+vv.question_answers+'</h6>';
								});
								t += '<a href="" class="btn-edit-question" data-questionid="'+k+'" data-questiontypeid="'+p.question_type_id+'"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;';
								t += '<a href="" class="btn-delete-question" data-questionid="'+k+'" data-questiontypeid="'+p.question_type_id+'"><i class="fa fa-trash"></i> Delete</a>';
                                t += '<hr>';
            				});
	            		}else{
	            			t += 'No Result';
	            		}
	            	}

            	t += '</div>';
                $(container + ' #modal-detail').find('.modal-body').html(t);
            	$('.panel-list-question').slimScroll({height: '450px', alwaysVisible:true});
            }
        });
    }

    $(this).get_tests();

    $(this).on('click', container + ' .btn-edit-question', function (e) {
		var question_id = $(this).data('questionid');
		var questiontypeid = $(this).data('questiontypeid');

    	ajaxManager.addReq({
            url: site_url + '/questions/widget/edit_question',
            type: 'GET',
            dataType: 'JSON',
            data: {
                question_id : question_id,
                questiontypeid : questiontypeid
            },
            beforeSend: function (){
        		tinymce.remove();
            	$(container + ' .btn-edit-question[data-questionid="'+question_id+'"]').addClass('disabled');
                $(container + ' .btn-edit-question[data-questionid="'+question_id+'"]').html('<i class="fa fa-sync-alt fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
            	t = '';
        		t += '<form role="form" id="form-edit" enctype="multipart/form-data">';
                    t += '<input type="hidden" name="question_type_id" value="'+questiontypeid+'">';
                    
                    if(questiontypeid == 8 || questiontypeid == 7){ // Special Mode
                        t += '<div class="section-answers">';
                            $.each(r.result.question_answers, function (k,v){
                                t += '<input type="hidden" name="answers['+v.id+'][question_answers_alias_before]" value="'+v.question_answers_alias+'">';
                                t += '<input type="hidden" name="answers['+v.id+'][question_id]" value="'+v.question_id+'">';

                                t += '<div class="row" style="margin-bottom:10px;border-bottom:1px solid #f3f3f3;padding-bottom:15px;">';
                                    t += '<div class="col-md-6">';
                                        t += '<label>Answers Option</label>';
                                        t += '<input type="text" class="form-control" name="answers['+v.id+'][question_answers]" required="" value="'+v.question_answers+'">';
                                    t += '</div>';
                                    t += '<div class="col-md-6">';
                                        if(questiontypeid == 7){
                                            t += '<label>Personality Types</label>';
                                        }
                                        if(questiontypeid == 8){
                                            t += '<label>Value</label>';    
                                        }
                                        t += '<select name="answers['+v.id+'][question_answers_alias]" class="form-control question_answers_alias">';
                                            if(questiontypeid == 7){
                                                t += '<option value="I" '+(v.question_answers_alias == 'I' ? 'selected' : '')+'>Introvert</option>';
                                                t += '<option value="S" '+(v.question_answers_alias == 'S' ? 'selected' : '')+'>Sensing</option>';
                                                t += '<option value="T" '+(v.question_answers_alias == 'T' ? 'selected' : '')+'>Thinking</option>';
                                                t += '<option value="J" '+(v.question_answers_alias == 'J' ? 'selected' : '')+'>Judging</option>';
                                                t += '<option value="E" '+(v.question_answers_alias == 'E' ? 'selected' : '')+'>Ekstrovert</option>';
                                                t += '<option value="N" '+(v.question_answers_alias == 'N' ? 'selected' : '')+'>Intuition</option>';
                                                t += '<option value="F" '+(v.question_answers_alias == 'F' ? 'selected' : '')+'>Feeling</option>';
                                                t += '<option value="P" '+(v.question_answers_alias == 'P' ? 'selected' : '')+'>Perceiving</option>';
                                            }
                                            if(questiontypeid == 8){
                                                t += '<option value="G" '+(v.question_answers_alias == 'G' ? 'selected' : '')+'>G</option>';
                                                t += '<option value="L" '+(v.question_answers_alias == 'L' ? 'selected' : '')+'>L</option>';
                                                t += '<option value="I" '+(v.question_answers_alias == 'I' ? 'selected' : '')+'>I</option>';
                                                t += '<option value="J" '+(v.question_answers_alias == 'T' ? 'selected' : '')+'>T</option>';
                                                t += '<option value="V" '+(v.question_answers_alias == 'V' ? 'selected' : '')+'>V</option>';
                                                t += '<option value="S" '+(v.question_answers_alias == 'S' ? 'selected' : '')+'>S</option>';
                                                t += '<option value="R" '+(v.question_answers_alias == 'R' ? 'selected' : '')+'>R</option>';
                                                t += '<option value="D" '+(v.question_answers_alias == 'D' ? 'selected' : '')+'>D</option>';
                                                t += '<option value="C" '+(v.question_answers_alias == 'C' ? 'selected' : '')+'>C</option>';
                                                t += '<option value="E" '+(v.question_answers_alias == 'E' ? 'selected' : '')+'>E</option>';
                                                t += '<option value="N" '+(v.question_answers_alias == 'N' ? 'selected' : '')+'>N</option>';
                                                t += '<option value="A" '+(v.question_answers_alias == 'A' ? 'selected' : '')+'>A</option>';
                                                t += '<option value="P" '+(v.question_answers_alias == 'P' ? 'selected' : '')+'>P</option>';
                                                t += '<option value="X" '+(v.question_answers_alias == 'X' ? 'selected' : '')+'>X</option>';
                                                t += '<option value="B" '+(v.question_answers_alias == 'B' ? 'selected' : '')+'>B</option>';
                                                t += '<option value="O" '+(v.question_answers_alias == 'O' ? 'selected' : '')+'>O</option>';
                                                t += '<option value="Z" '+(v.question_answers_alias == 'Z' ? 'selected' : '')+'>Z</option>';
                                                t += '<option value="K" '+(v.question_answers_alias == 'K' ? 'selected' : '')+'>K</option>';
                                                t += '<option value="V" '+(v.question_answers_alias == 'V' ? 'selected' : '')+'>V</option>';
                                                t += '<option value="W" '+(v.question_answers_alias == 'W' ? 'selected' : '')+'>W</option>';
                                            }
                                        t += '</select>';
                                    t += '</div>';
                                t += '</div>';
                            });
                        t += '</div>';
                    }else{ // Basic Mode
                        t += '<input type="hidden" name="question_id" value="'+r.result.question_id+'">';
                        t += '<div class="row">';
                            t += '<div class="col-md-8">';
                                t += '<h5 class="bold">Question Information</h5>';
                                t += '<div class="form-group">';
                                     t += '<label>Question</label>';
                                     t += '<textarea class="form-control" name="question_text" id="question_text">';
                                         t += r.result.question_text ? r.result.question_text : '';
                                     t += '</textarea>';
                                 t += '</div>';
                                 t += '<div class="form-group">';
                                     t += '<label>Description</label>';
                                     t += '<textarea class="form-control" name="question_description">';
                                         t += r.result.question_description ? r.result.question_description : '';
                                     t += '</textarea>';
                                 t += '</div>';
                            t += '</div>';
                            t += '<div class="col-md-4">';
                                t += '<h5 class="bold">Additional Information</h5>';
                                    t += '<div class="form-group">';
                                         t += '<label>Image</label>';
                                         t += '<input type="file" name="question_image" class="form-control">';
                                         if(r.result.question_image){
                                             t += '<input type="hidden" name="image_before" value="'+r.result.question_image.dir+'">';
                                             t += '<input type="hidden" name="image_id" value="'+r.result.question_image.dir_id+'">';
                                             t += '<h6 class="font-arial">';
                                                 t += '<a data-fancybox="gallery" href="'+base_url+'/files/question_image/'+r.result.question_image.dir+'"><i class="fa fa-image"></i> '+r.result.question_image.dir_temp+'</a>';
                                             t += '</h6>';
                                         }
                                     t += '</div>';
                                     t += '<div class="form-group">';
                                         t += '<label>File</label>';
                                         t += '<input type="file" name="question_file" class="form-control">';
                                         if(r.result.question_file){
                                             t += '<input type="hidden" name="file_before" value="'+r.result.question_file.dir+'">';
                                             t += '<input type="hidden" name="file_id" value="'+r.result.question_file.dir_id+'">';
                                             t += '<h6 class="font-arial">'; 
                                                 t += '<a href="'+site_url+'/questions/widget/file_download/'+r.result.question_file.dir+'"><i class="fa fa-file"></i> '+r.result.question_file.dir_temp+'</a>';
                                             t += '</h6>';
                                         }
                                     t += '</div>';
                            t += '</div>';
                        t += '</div>';
                        if(questiontypeid == 1){
                            t += '<input type="hidden" name="question_id" value="'+r.result.question_id+'">';
                            t += '<input type="hidden" name="correct_answers_id_before" value="'+r.result.correct_answers_id+'">';
                            t += '<div class="row">';
                                t += '<div class="col-md-12">';
                                    t += '<h5 class="bold">Question Answers</h5>';
                                    if(r.result.question_answers){
                                        t += '<div class="section-answers-multiple-choice">';
                                            $.each(r.result.question_answers, function (k,v){
                                                t += '<div class="input-group" style="margin-bottom:10px;">';                   
                                                    t += '<div class="input-group-btn">';
                                                        t += '<label class="btn btn-default btn-sm active">';
                                                            t += '<div class="radio radio-danger" style="margin:0;padding:0">'
                                                                t += '<input name="correct" value="'+v.id+'" '+(v.id === r.result.correct_answers_id ? 'checked' : '')+' type="radio" id="cr-'+v.id+'">';
                                                                t += '<label for="cr-'+v.id+'"" style="margin:0;padding:0">Correct</label>';
                                                            t += '</div>';
                                                        t += '</label>';
                                                    t += '</div>';
                                                    t += '<input type="text" class="form-control" name="answers['+v.id+'][question_answers]" required="" value="'+v.question_answers+'">';
                                                t += '</div>';
                                            });
                                        t += '</div>';
                                    }
                                t += '</div>';
                            t += '</div>';
                        }   
                    }
                t += '</form>';
            	$(container + ' .modal-body-edit-question').html(t);

                $('.question_answers_alias').select2({
                    dropdownParent: $("#modal-edit-question"),
                    width: '100%'
                });

            	tinymce.init({
			        selector: '#question_text',
			        menubar: false,
			        style: "border:5px solid black;",
			        mobile: { theme: 'mobile' }
			    });
            },
            complete: function(){
            	$(container + ' .btn-edit-question[data-questionid="'+question_id+'"]').removeClass('disabled');
                $(container + ' .btn-edit-question[data-questionid="'+question_id+'"]').html('<i class="fa fa-edit"></i> Edit');
            	$('#modal-detail').modal('hide');
            	$('#modal-edit-question').modal('show');
            }
        });

    	e.preventDefault();
    });
    
    $(this).on('click', container + ' .btn-view-detail', function (e) {
       var test_id = $(this).data('id');
       var name = $(this).data('name');

       ajaxManager.addReq({
            url: site_url + '/questions/widget/detail_test',
            type: 'GET',
            dataType: 'JSON',
            data: {
                test_id: test_id
            },
            beforeSend: function (){
                $(container + ' .btn-view-detail[data-id="'+test_id+'"]').addClass('disabled');
                $(container + ' .btn-view-detail[data-id="'+test_id+'"]').html('<i class="fa fa-sync-alt fa-spin"></i>');
            	$(container + ' #modal-detail').find('.modal-body').html('<h4 class="font-arial text-center muted"> Choose type to getting start</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
            	$('.title-test').html(name);
            	t = '';
            	// t += '<option value="">Choose Type</option>';
            	if(r.group_question.length > 0){
            		$.each(r.group_question, function (k,v){	
	            		// t += '<option value="'+v.question_type_id+'">'+v.question_name+'</option>';
                        t += '<input type="radio" value="'+v.question_type_id+'" class="choose-type" name="optiontype" id="type-'+v.question_type_id+'">';
                        t += '<label for="type-'+v.question_type_id+'">'+v.question_name+'</label>';
	            	});
            	}
            	$('.radio-option').html(t);
            	$('#test_id').val(test_id);
            },
            complete: function(){
                $(container + ' .btn-view-detail[data-id="'+test_id+'"]').removeClass('disabled');
                $(container + ' .btn-view-detail[data-id="'+test_id+'"]').html('<i class="fa fa-search"></i>');
            	$('#modal-detail').modal('show');
            }
        });
    });

    $(this).on('change', container + ' .choose-type', function (e) {
    	var question_type_id = $(this).val();
    	var test_id = $('#test_id').val();
    	$(this).question({
    		question_type_id : question_type_id,
    		test_id : test_id
    	});
    	e.preventDefault();
    });

    $(this).on('submit', container + ' #form-edit', function (e) {
        var form = $(this);
        tinyMCE.triggerSave();
        $(this).ajaxSubmit({
            url: site_url + '/questions/widget/change_question',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            type: 'POST',
            dataType: 'JSON',
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            beforeSend: function () {
                $(container + ' .btn-change').addClass('disabled');
             	$(container + ' .btn-change').html('<i class="fa fa-sync-alt fa-spin"></i>');
            },
            success: function(r) {
            	$('#csrf').val(r.csrf);
                $(container + ' .btn-change').removeClass('disabled');
             	$(container + ' .btn-change').html('<i class="fa fa-edit"></i> Change');
             	$(this).question({
		    		question_type_id : r.question_type_id,
		    		test_id : $('#test_id').val()
		    	});
                $.snackbar({
                    content: r.msg, 
                    timeout: 5000
                });
            },
            complete: function(){
            	$('#modal-edit-question').modal('hide');
        		$('#modal-detail').modal('show');
            }
        });
        e.preventDefault();
    });

 	$(this).on('click', container + ' .btn-change', function (e) {
    	$('#form-edit').submit();
    	e.preventDefault();
    });


	$(this).on('click', container + ' .btn-back-edit-question', function (e) {
		$('#modal-edit-question').modal('hide');
		$('#modal-detail').modal('show');
    });

    $(this).on('click', container + ' .btn-new-data', function (e) {
        Widget.Loader('new_question', {}, 'container-content', false);
    });

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 9,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_tests({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

    $(this).on('change', container + ' #search', function (e){
  		$(this).get_tests({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

});