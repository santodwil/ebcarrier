<?php

class Group extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_group');
        $this->load->model('groups_model');
        $this->load->helper('global');
    }

    public function index() {
        $search = $this->input->get('search');
        $group = $this->m_group->get(NULL, $search);

        $data = array(
            'title' => 'Group',
            'group' => $group->result_array(),
            'search' => $search,
            'content' => 'group/group',
            'menu_list' => $this->get_menu(),
            '_css' => array(
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/nprogress/js/nprogress.js'
            )
        );
        $this->render_page($data);
    }

    public function add() {
        $post = $this->input->post();
        if ($post) {
            $this->save();
        }

        $data = array(
            'title' => 'Tambah Group',
            'content' => 'group/add',
            'menu_list' => $this->get_menu(),
            '_css' => array(
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/nprogress/js/nprogress.js'
            )
        );
        $this->render_page($data);
    }

    public function edit() {
        $post = $this->input->post();
        if ($post) {
            $this->update();
        }

        $id = $this->input->get('id');
        $group = $this->m_group->get($id)->row_array();

        if (!$group) {
            show_404();
        }

        $data = array(
            'title' => $group['name'],
            'group' => $group,
            'content' => 'group/edit',
            'menu_list' => $this->get_menu(),
            '_css' => array(
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/nprogress/js/nprogress.js'
            )
        );
        $this->render_page($data);
    }

    private function save() {
        $post = $this->input->post();
        $name = $this->input->post('name');
        $description = $this->input->post('description') ? $post['description'] : NULL;

        $group = array(
            'name' => $name,
            'description' => $description
        );
        $group['id'] = $this->ion_auth->create_group($name, $description);

        if ($group['id']) {
            $this->_message = array(
                'message' => $this->ion_auth->messages(),
                'label' => 'success'
            );
            $this->session->set_flashdata('message', $this->_message);
            redirect('group/edit?id=' . $group['id']);
        } else {
            $this->_message = array(
                'message' => $this->ion_auth->errors(),
                'label' => 'danger'
            );
            $this->session->set_flashdata('message', $this->_message);
            redirect('group/add');
        }
    }

    private function update() {
        $post = $this->input->post();
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $description = $this->input->post('description') ? $post['description'] : NULL;

        $update = $this->ion_auth->update_group($id, $name, $description);

        if ($update) {
            $this->_message = array(
                'message' => $this->ion_auth->messages(),
                'label' => 'success'
            );
        } else {
            $this->_message = array(
                'message' => $this->ion_auth->errors(),
                'label' => 'danger'
            );
        }

        $this->session->set_flashdata('message', $this->_message);
        redirect('group/edit?id=' . $id);
    }

    public function delete() {
        $id = $this->input->get('id');
        $delete = $this->ion_auth->delete_group($id);
        if ($delete) {
            $this->_message = array(
                'message' => $this->ion_auth->messages(),
                'label' => 'success'
            );
        } else {
            $this->_message = array(
                'message' => $this->ion_auth->errors(),
                'label' => 'danger'
            );
        }

        $this->session->set_flashdata('message', $this->_message);
        redirect('group');
    }

}
