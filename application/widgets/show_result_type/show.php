
<style type="text/css">
	.table thead tr th{
		color:#F55753;
	}
	.table tbody tr td{
		font-size: 13px;
	}	
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  

	<div class="row">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
	
	<input type="hidden" id="sdate" value="<?php echo $sdate ?>">
	<input type="hidden" id="edate" value="<?php echo $edate ?>">
	<input type="hidden" id="test_type_id" value="<?php echo $id ?>">

	<div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
	       	<div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-search"></i></span>
								<input type="text" id="search" class="form-control" placeholder="Search...">
							</div>
						</div>
						<div class="col-md-9">
							<button class="btn btn-danger pull-right btn-back">Back</button>
						</div>
					</div>
					<br>
					<div class="row" id="section-data"></div>
					<div class="row">
						<div class="col-md-3 pull-left">
							<span id="section-total" class="label label-danger">0 Data Found</span>
						</div>
						<div class="col-md-9">
							<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
						</div>
					</div>
	       	</div>
	   	</div>
	</div>
	
	<div class="modal fade slide-up disable-scroll in" id="modal-detail-result">
    	<div class="modal-dialog modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:1100px;">
                  	<div class="modal-header clearfix text-left">
	                    <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button> -->
                  		<h4 class="text-center"><span class="bold">Test Result</span></h4>
                  		<h5 class="text-center title-result font-arial" style="margin-top: -15px;margin-bottom: 10px;"></h5>
                  		<h6 class="alert alert-success bold alert-notif hidden" style="margin-top: 5px;"></h6>
                  	</div>
                  	<form id="form-detail-result">
                  		<input type="hidden" name="test_type_id">
                  		<input type="hidden" name="applicant_id">
                  		<input type="hidden" name="question_type_id">
                  		<input type="hidden" name="total_question">
                  		s
	                  	<div class="modal-body"></div>
	                  	<div class="modal-footer">
							<button type="submit" class="btn btn-info pull-right" style="margin-top: 10px;"><i class="fa fa-save"></i> Submit</button>
	                  	</div>
                  	</form>
				</div>
			</div>
		</div>
	</div>   

 </div>

 <script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>