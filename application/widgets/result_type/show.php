<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">
<style type="text/css">
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
	.daterangepicker{z-index:1151 !important;}
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  

	<div class="row">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
	
	<div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
	    	<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" id="search" class="form-control" placeholder="Search...">
						</div>
					</div>
				</div>
	    	</div>
	       	<div class="panel-body">
				<div class="row table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
							<th width="350">Name</th>
							<th width="200" class="text-center">Complete Question</th>
							<th width="200" class="text-center">Register Applicant</th>
							<th width="200" class="text-center">Finished Test</th>
							<th class="text-center">Action</th>
						</thead>
						<tbody id="section-data"></tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default bold">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
	       	</div>
   		</div>
	</div>
	
	<div class="modal fade slide-up in" id="modal-generate-result">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:1200px;">
		          	<div class="modal-body" style="margin-top: 20px;">
	          			<div class="row">
							<div class="col-md-5">
								<div class="col-md-12">
									<h6 class="font-arial bold" style="font-size: 15px;">Applicant Register</h6>
									<div class="input-group" style="margin:10px;">
										<span class="input-group-addon">Start Date</span>
										<input type="hidden" id="sdate" value="">
										<input type="text" class="form-control sdate" value="">
									</div>
									<div class="input-group" style="margin:10px;">
										<span class="input-group-addon">End Date</span>
										<input type="hidden" id="edate" value="">
										<input type="text" class="form-control edate" value="">
									</div>
									<div class="btn-group pull-right" style="margin:10px;">
										<input type="hidden" id="test_type_id" value="">
										<button class="btn btn-default btn-view-result">View Result</button>
										<button class="btn btn-default btn-generate-result">Generate Result</button>
									</div>
								</div>
								<div class="col-md-12">
									<h6 class="font-arial bold" style="font-size: 15px;">Type Answers In This Test</h6>
									<div class="section-type-question" style="margin:10px;">
									</div>
								</div>
							</div>
							<div class="col-md-7" style="height:500px; overflow: auto;">
								<?php for ($i=0; $i < 10; $i++) {  ?>
									<div class="row" style="border-bottom: 1px solid #eae5e5;padding-bottom: 10px;padding-top:5px;">
										<div class="col-md-3">
											<h5 class="font-arial" style="font-size:14px;margin:0;padding: 0">Susanto Dwi L</h5>
											<h6 class="font-arial" style="margin: 0;padding:0">susanto@gmail.com</h6>
										</div>
										<div class="col-md-9 text-left" style="padding:10px;">
											<div class="label label-default">textarea : 20</div>
											<div class="label label-default">textarea : 20</div>
											<div class="label label-default">textarea : 20</div>
											<div class="label label-default">textarea : 20</div>
										</div>
									</div>	
								<?php } ?>
							</div>
	          			</div>
		          	</div>
	          	</div>
          	</div>
      	</div>
  	</div>

 </div>

 <script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>