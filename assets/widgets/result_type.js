$(function () {

	'use restrict';

	$.fn.get_tests = function(params) {
      var p = $.extend({
        	offset: 0,
            currentPage: 1,
            search : $(container + ' #search').val(),
            sdate : $(container + ' #sdate').val(),
            edate : $(container + ' #edate').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/result_type/widget/get_tests',
            type: 'GET',
            dataType: 'JSON',
            data: {
                offset: p.offset,
                search: p.search
            },
            beforeSend: function () {
               show_loading();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
           		t = '';
           		var no = 0;
            	if(r.total > 0){
            		$.each(r.result, function (k,v){
                     t += '<tr>';
                        t += '<td>'+v.name+'<br>'+v.total_question+' Question in '+v.time+' minutes</td>';
                        t += '<td class="text-center">'+v.complete_question+'</td>';
                        t += '<td class="text-center">'+v.registered+'</td>';
                        t += '<td class="text-center">'+v.finished+'</td>';
                        t += '<td class="text-center"><button class="btn btn-xs btn-default show-result" data-id="'+v.id+'"><i class="fa fa-eye"></i> Show Result</button></td>';
                     t += '</tr>';
            		});
            	}else{
            		t += '<p class="text-center font-arial">No Result</p>';
            	} 
            	$(container + ' #section-total').html(r.total + ' Data Found');
            	$(container + ' #section-data').html(t);
            	$(container + ' #section-pagination').paging({
                    items: r.total,
                    currentPage: p.currentPage
                });
   	        	hide_loading();
            }
        });
    }

    $(this).get_tests();

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 9,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_tests({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

    $(this).on('change', container + ' #search', function (e){
  		$(this).get_tests({
            search: $(this).val()
        });
  		e.preventDefault();
  	});

  	$(this).on('click', container + ' .show-result', function (e){
  		var id = $(this).data('id');
  		sdate = $(container + ' #sdate').val();
      edate = $(container + ' #edate').val();
  		Widget.Loader('applicant_result_type', {id:id}, 'container-content', true);
  		e.preventDefault();
  	});

  	$(this).on('click', container + ' .generate-result', function (e){
  		var id = $(this).data('id');
  		ajaxManager.addReq({
         url: site_url + '/result_type/widget/show_result',
         type: 'GET',
         dataType: 'JSON',
         data: {
            test_type_id: id
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r) {
            if(r.question_type){
               t = '';
               $.each(r.question_type, function (k,v){
                  t += '<h5 class="font-arial" style="font-size:14px"><div class="label label-default">'+v.total+'</div> '+v.name+'</h5>';
               });
               $(container + ' .section-type-question').html(t);
               $(container + ' #test_type_id').val(id);
             	$(container + ' #modal-generate-result').modal('show');
            }else{
             	alert('Failed');
            }
         }
      });
  		e.preventDefault();
  	});

   $.fn.calculation_result = function(params) {
      var p = $.extend({
         test_type_id : $('#test_type_id').val(),
         sdate : null,
         edate : null
      }, params);
     ajaxManager.addReq({
         url: site_url + '/result_type/widget/calculation_result',
         type: 'GET',
         dataType: 'JSON',
         data: {
            test_type_id: p.test_type_id,
            sdate: p.sdate,
            edate: p.edate
         },
         beforeSend: function () {
            
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r){

         }
      });
   }

   $(this).on('click', container + ' .btn-generate-result', function (e){
      var sdate = $('#sdate').val();
      var edate = $('#edate').val();
      if(sdate !== '' && edate !== ''){
         $(this).calculation_result({
            sdate : sdate,
            edate : edate
         });
      }else{
         $.snackbar({
            content: 'Choose start date & end date', 
            timeout: 5000
         });
      }
   });
   
   $(container + ' .sdate').daterangepicker({
      autoUpdateInput: false,
      locale: {
         format: 'DD/MMM/YYYY'
       },
        singleDatePicker: true,
        showDropdowns: true
    });

    $(container + ' .edate').daterangepicker({
        autoUpdateInput: false,
        locale: {
          format: 'DD/MMM/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true
    });

   $(container + ' .date-data').daterangepicker({
      autoUpdateInput: false,
      locale: {
         format: 'DD/MMM/YYYY'
       },
        singleDatePicker: true,
        showDropdowns: true
   });

   $(container + ' .sdate').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MMM/YYYY'));
      $('#sdate').val(picker.startDate.format('YYYY-MM-DD'));
   });

   $(container + ' .edate').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MMM/YYYY'));
      $('#edate').val(picker.startDate.format('YYYY-MM-DD'));
   });



	// $('#range-date').daterangepicker({
 //        ranges: {
 //            'Last 7 Days': [moment().subtract('days', 7), moment()],
 //            'Last 30 Days': [moment().subtract('days', 30), moment()],
 //            'Last 3 Month': [moment().subtract('days', 90), moment()]
 //        },
 //        format: 'DD-MM-YYYY',
 //        startDate: moment().subtract('days', 29),
 //        endDate: moment(),
 //        isShowing: true
 //    },
 //    function (start, end) {
 //    	$('#range-date').html('<i class="fa fa-calendar"></i> '+start.format('DD MMM YYYY')+' sd '+end.format('DD MMM YYYY')+'');
 //    	$('#sdate').val(start.format('YYYY-MM-DD'));
 //        $('#edate').val(end.format('YYYY-MM-DD'));
 //        $(this).get_tests({
 //        	load : true
 //        });
        
 //    }
 //    );
});