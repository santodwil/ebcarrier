$(function () {

	'use restrict';

	$('#test_type').select2();
	$('#question_type').select2();

	$(this).on('click', container + ' .btn-next-step', function (e){
		$('#section-option').slideUp('fast');
		$('#section-question-form').slideDown('fast');
	});
	
	$(this).on('click', container + ' .btn-submit', function (e){
		var test_type = $('#test_type').val();
		var question_type = $('#question_type').val();
		if(question_type !== ''){
			$(this).render_question(test_type, question_type);
		}
		e.preventDefault();
	});

	$.fn.render_question = function(test_type, question_type){
	 	t = '';
	 	$(container + ' #section-result').html('<h2 class="text-center"><i class="fa fa-refresh fa-spin text-center"></i></h2>');
	 	tinymce.remove();
		t += '<form role="form" id="form-create" enctype="multipart/form-data">';
			t += '<input type="hidden" id="type_question" name="type_question" value="'+question_type+'">';
			t += '<input type="hidden" name="type_test" value="'+test_type+'">';
			
			t += '<div class="col-md-12">';
				t += '<ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">';
					t += '<li class="active"><a style="font-weight: bold;" data-toggle="tab" href="#basic"><span>Basic Information</span></a></li>';
					t += '<li><a style="font-weight: bold;" data-toggle="tab" href="#question"><span>Answers Question</span></a></li>';
					t += '<li><a style="font-weight: bold;" data-toggle="tab" href="#additional"><span>Additional Information</span></a></li>';
              	t += '</ul>';
              	t += '<div class="tab-content">';
                	t += '<div class="tab-pane active" id="basic">';
                		t += '<div class="row">';
                            t += '<div class="col-md-12">';
                        		t += '<div class="form-group">';
									t += '<label class="text-danger">Question</label>';
									t += '<textarea class="form-control" name="question_text" id="question_text"></textarea>';
								t += '</div>';
								t += '<div class="form-group">';
									t += '<label class="text-danger">Description</label>';
									t += '<textarea class="form-control" name="question_description"></textarea>';
								t += '</div>';
                        	t += '</div>';
                    	t += '</div>';
                	t += '</div>';
                	t += '<div class="tab-pane fade" id="question">';
                		if(question_type == 8){
                			t += '<div class="row">';
	                            t += '<div class="col-md-12">';
	                            	t += '<div class="input-group" style="margin-bottom:10px;">';
						            	t += '<span class="input-group-addon default">';                                
					                        t += 'A';
					                    t += '</span>';	            	
					            		t += '<input type="text" class="form-control" name="answers[0][answers_text]" required="" placeholder="Answers...">';
				                        t += '<select name="answers[0][answers_card]" class="form-control">';
				                   			t += '<option value="G">G</option>';
				                   			t += '<option value="L">L</option>';
				                   			t += '<option value="I">I</option>';
				                   			t += '<option value="J">T</option>';
				                   			t += '<option value="V">V</option>';
				                   			t += '<option value="S">S</option>';
				                   			t += '<option value="R">R</option>';
				                   			t += '<option value="D">D</option>';
				                   			t += '<option value="C">C</option>';
				                   			t += '<option value="E">E</option>';
				                   			t += '<option value="N">N</option>';
				                   			t += '<option value="A">A</option>';
				                   			t += '<option value="P">P</option>';
				                   			t += '<option value="X">X</option>';
				                   			t += '<option value="B">B</option>';
				                   			t += '<option value="O">O</option>';
				                   			t += '<option value="Z">Z</option>';
				                   			t += '<option value="K">K</option>';
				                   			t += '<option value="V">V</option>';
				                   			t += '<option value="W">W</option>';
				                   		t += '</select>';
						            t += '</div>';
						            t += '<div class="input-group" style="margin-bottom:10px;">';
						            	t += '<span class="input-group-addon default">';                                
					                        t += 'B';
					                    t += '</span>';	            	
					            		t += '<input type="text" class="form-control" name="answers[1][answers_text]" required="" placeholder="Answers...">';
				                        t += '<select name="answers[1][answers_card]" class="form-control">';
				                   			t += '<option value="G">G</option>';
				                   			t += '<option value="L">L</option>';
				                   			t += '<option value="I">I</option>';
				                   			t += '<option value="J">T</option>';
				                   			t += '<option value="V">V</option>';
				                   			t += '<option value="S">S</option>';
				                   			t += '<option value="R">R</option>';
				                   			t += '<option value="D">D</option>';
				                   			t += '<option value="C">C</option>';
				                   			t += '<option value="E">E</option>';
				                   			t += '<option value="N">N</option>';
				                   			t += '<option value="A">A</option>';
				                   			t += '<option value="P">P</option>';
				                   			t += '<option value="X">X</option>';
				                   			t += '<option value="B">B</option>';
				                   			t += '<option value="O">O</option>';
				                   			t += '<option value="Z">Z</option>';
				                   			t += '<option value="K">K</option>';
				                   			t += '<option value="V">V</option>';
				                   			t += '<option value="W">W</option>';
				                   		t += '</select>';
						            t += '</div>';
                            	t += '</div>';
	                    	t += '</div>';
                		}
                		else if(question_type == 1 || question_type == 4 || question_type == 7){
                			t += '<div class="row">';
	                            t += '<div class="col-md-4">';
	                            	t += '<div class="form-group">';
		                        		t += '<label class="text-danger">Type</label>';
	                                    t += '<select class="form-control" id="option_type_answers" name="option_type_answers">';
	                                    	t += '<option value="">Choose type option answers...</option>';
	                                    	t += '<option value="text">Text</option>';
	                                    	if(question_type != 7){
	                                    		t += '<option value="image">Image</option>';
	                                    	}
	                                    t += '</select>';
	                       			t += '</div>';
		                        	t += '<div class="form-group">';
		                        		t += '<label class="text-danger">Total Answers</label>';
	                                    t += '<input type="text" class="form-control" id="option_total_answers" placeholder="Only type text...">';
	                       			t += '</div>';
	                       			t += '<div class="form-group pull-right">';
		                        		t += '<button type="button" class="btn btn-default" id="btn-add-total-answers"><i class="fa fa-plus"></i> Add Answers</button>';
	                       			t += '</div>';
	                   			t += '</div>';
	                   			t += '<div class="col-md-8">';
	                   				t += '<div id="answers-list" style="margin-top:10px;"><h2 class="text-center font-arial muted bold">Choose type answers</h2></div>';
	                   			t += '</div>';
	                    	t += '</div>';
                		}else{
                			t += '<h3>No Option</h3>';
                		}
                	t += '</div>';
                	t += '<div class="tab-pane fade" id="additional">';
                		t += '<div class="row">';
                            t += '<div class="col-md-12">';
	                        	t += '<div class="form-group">';
									t += '<label class="text-danger">Image</label>';
									t += '<input type="file" name="question_image" class="form-control">';
								t += '</div>';
								t += '<div class="form-group">';
									t += '<label class="text-danger">File</label>';
									t += '<input type="file" name="question_file" class="form-control">';
								t += '</div>';
                        	t += '</div>';
                    	t += '</div>';
                	t += '</div>';
            	t += '</div>';
			t += '</div>';
			t += '<div class="col-md-12 text-center" style="border-top:1px solid #e4e4e4;">';
				t += '<button type="submit" class="btn btn-danger btn-lg" style="margin-top:20px;"><i class="fa fa-save"></i> Save Question</button>';
			t += '</div>';
		t += '</form>';

		$(container + ' #section-result').html(t);
		
		tinymce.init({
	        selector: '#question_text',
	        menubar: false
	    });


	    $(container + ' #btn-add-total-answers').on('click', function (e) {
	        var option_total_answers = $('#option_total_answers').val();
	        var option_type_answers = $('#option_type_answers').val();
	        var type_question = $('#type_question').val();
	        ans = '';
	        if(option_type_answers == 'text'){
	        	for (i = 0; i < option_total_answers; i++) {
	        		if(type_question == 1){
	        			ans += '<div class="input-group" style="margin-bottom:10px;">';
			            	ans += '<span class="input-group-addon default">';                                
		                        ans += i;
		                    ans += '</span>';	            	
		            		ans += '<input type="text" class="form-control" name="answers['+i+']" required="" placeholder="Answers...">';
		                   	ans += '<div class="input-group-btn">';
		                        ans += '<label class="btn btn-success btn-sm active">';
		                            ans += '<input name="correct" value="'+i+'" type="radio"> Correct';
		                        ans += '</label>';
		                   	ans += '</div>';
			            ans += '</div>';
        			}
    			 	if(type_question == 7){
    			 		ans += '<div class="input-group" style="margin-bottom:10px;">';
			            	ans += '<span class="input-group-addon default">';                                
		                        ans += i;
		                    ans += '</span>';	            	
		            		ans += '<input type="text" class="form-control" name="answers['+i+'][answers_text]" required="" placeholder="Answers...">';
	                        ans += '<select name="answers['+i+'][answers_nilai]" class="form-control answers_nilai_psikotes" required="">';
	                   			ans += '<option value="I">Introvert</option>';
	                   			ans += '<option value="S">Sensing</option>';
	                   			ans += '<option value="T">Thinking</option>';
	                   			ans += '<option value="J">Judging</option>';
	                   			ans += '<option value="E">Ekstrovert</option>';
	                   			ans += '<option value="N">Intuition</option>';
	                   			ans += '<option value="F">Feeling</option>';
	                   			ans += '<option value="P">Perceiving</option>';
	                   		ans += '</select>';
			            ans += '</div>';
        			}
		        }
	        }
	        if(option_type_answers == 'image'){
	        	ans += '<div class="form-group" style="margin-top:10px;">';
	        		ans += '<label class="text-danger">Multiple File</label>';	            	
            		ans += '<input type="file" class="form-control" name="userfile[]" multiple="multiple"/>';
	            ans += '</div>';
	        	ans += '<h6 class="font-arial">Correct answers after save question</h6>';	            	
	        }
	        
	        $('#answers-list').html(ans);
	        $('.answers_nilai_psikotes').select2();
	    });

	}


	$(this).on('click', container + ' .btn-back', function (e) {
        Widget.Loader('questions', {}, 'container-content');
    });
	
	$.fn.get_answers_image = function(question_id){
		ajaxManager.addReq({
            url: site_url + '/create_question/widget/get_answers_image',
            type: 'POST',
            dataType: 'JSON',
            data: {
                question_id: question_id,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                $('#modal-answers-image').modal('show');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
            	$('#csrf').val(r.csrf);
        		t = '';
        		if(r.result.length > 0){
    				t += '<input type="hidden" name="question_id" value="'+r.question_id+'">'
        			$.each(r.result, function(k,v){
        				t += '<tr>';
        				t += '<td>'+v.question_answers+'</td>';
        				t += '<td>';
        					t += '<div class="radio radio-success">';
	                      		t += '<input type="radio" value="'+v.id+'" name="correct" id="yes_'+v.id+'">';
		                      	t += '<label for="yes_'+v.id+'">Correct</label>';
		                    t += '</div>';
        				t += '</td>';
        				t += '</tr>';
        			});
        		}else{
        			t += '<tr><td colspan="3">No Result</td></tr>';
        		}
        		$('#section-data-image').html(t);
            }
        });
	}

	$(this).on('submit', container + ' #form-create', function (e) {
        var form = $(this);
        var mode = $(this).data('mode');
        $(this).ajaxSubmit({
            url: site_url + '/create_question/widget/save_question',
            type: 'POST',
            dataType: 'JSON',
            data:{
            	'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('Submit');
            },
            success: function(r) {
            	$('#csrf').val(r.csrf);
                alert(r.msg);
                if(r.success){
                    form.resetForm();
                    form.find('#answers-list').html('');
                    if(r.option_type_answers == 'image'){
	            		$(this).get_answers_image(r.question_id);
	                }
                }

                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-save"></i> Save Question');
            },
        });
        e.preventDefault();
    });

    $(this).on('submit', container + ' #form-correct-answers-image', function (e) {
        var form = $(this);
        var mode = $(this).data('mode');
        $(this).ajaxSubmit({
            url: site_url + '/create_question/widget/save_correct_answers_image',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('Submit');
            },
            success: function(r) {
            	$('#csrf').val(r.csrf);
                if(r.success){
                    $('#modal-answers-image').modal('hide');
                }

                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-save"></i> Submit');
            },
        });
        e.preventDefault();
    });

});