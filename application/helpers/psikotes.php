<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of helper psikotes
 *
 * @author SUSANTO DWI LAKSONO
 */

if (!function_exists('get_member_with_team')) {

    function get_member_with_team() {
        $teams = $this->db->select('id, name')->where('active', 1)->get('teams');
        foreach ($teams->result_array() as $k => $v) {
            $result[$v['id']][$v['name']] = $v['name'];
            $members = $this->db->select('id, name')->where('team_id', $v['id'])->get('members');
            if($members->num_rows() > 0){
                foreach ($members->result_array() as $kk => $vv) {
                    $result[$v['id']][$v['name']]['id'] = $vv['id'];
                    $result[$v['id']][$v['name']]['name'] = $vv['name'];
                }
            }
        }
        return $result;
    }

}