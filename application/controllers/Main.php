<?php

class Main extends MY_Controller {
    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Beranda',
            'role_id' => $this->_role_id['group_id'],
            'content' => 'main',
    	);
    	$this->render_page($data);
    }

}