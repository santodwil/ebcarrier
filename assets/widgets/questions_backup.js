$(function () {

	'use restrict';

    $('#section-data').slimScroll({height: '450px'});

	$.fn.get_tests = function(params) {
        var p = $.extend({
            search : $(container + ' #search').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/questions/widget/get_tests',
            type: 'POST',
            dataType: 'JSON',
            data: {
                search: p.search,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                $('.loading').html('<h4>Please Wait...</h4>');
	        	$('.loading').show();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
        		t = '';
            	if(r.result.length > 0){
                    t += '<ul class="nav nav-pills nav-stacked" id="tests_no" style="width:auto;">';
            		$.each(r.result, function (k,v){
            			t += '<li>';
                            t += '<a style="padding:5px;" href="#" class="detail-questions font-arial" data-id="'+v.id+'">'+(v.status == 1 ? '<i class="fa fa-circle text-success pull-left" style="margin-top:4px;"></i>&nbsp;' : '<i class="fa fa-circle text-danger pull-left" style="margin-top:4px;"></i>&nbsp;')+' '+v.name+'</a>';
                        t += '</li>';
            		});
                    t += '</ul';
            	}else{
            		t += '<h5 class="text-center font-arial">No Result</h5>';
            	}
            	$(container + ' #section-data').html(t);
                $('.loading').hide();
            }
        });
    }

    $(this).get_tests();

     $.fn.detail_questions = function(params) {
        var p = $.extend({
            test_type_id : false
        }, params);
        ajaxManager.addReq({
            url: site_url + '/questions/widget/detail_questions',
            type: 'POST',
            dataType: 'JSON',
            data: {
                test_type_id: p.test_type_id,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                $('.loading').html('<h4>Please Wait...</h4>');
                $('.loading').show();
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
            	t = '';
            	t += '<div class="row">';
            		t += '<div class="col-md-8">';
            			t += '<h4 class="bold text-danger" style="margin-top:-5px;">List Question</h4>';
            		t += '</div>';
                    t += '<div class="col-md-4">';
            			t += '<input type="text" id="search_question" class="form-control input-sm" placeholder="Search Question...">';
            		t += '</div>';
            	t += '</div>';
            	t += '<div class="row">';
            		t += '<div class="col-md-12">';
            			t += '<ul id="media-content-list" class="nav nav-pills nav-stacked">';
            				$.each(r.questions, function(k,v){
        						t += '<li class="news-item">';
        							t += '<a href="#'+v.id+'" data-toggle="collapse" role="button">';
        								t += '<span class="label label-danger pull-right">'+v.question_type_name+'</span>';
        								t += $(v.question_text).text().substr(0, 200)
    								t += '</a>';
    								t += '<div class="collapse question-content" id="'+v.id+'" data-content="false"></div>';
        						t += '<li>';
            				});
            			t += '</ul>';
            		t += '</div>';
            	t += '</div>';
            	$(container + ' #section-result').html(t);
            	$('.collapse').collapse({
                    toggle: false
                });
                $('.collapse').on('show.bs.collapse', function(){
                	var question_id = $(this).attr('id');
                    var content = $(this).data('content');
                    if(!content){
                        $(this).review_question(question_id);
                    }
            	});
            	$('.loading').hide();
            }
        });
    }

    $.fn.review_question = function(question_id){
        ajaxManager.addReq({
            type: 'POST',
            url: site_url + '/questions/widget/review_question',
            type: 'POST',
            dataType: 'JSON',
            data: {
                question_id: question_id,
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function(){
                $('.question-content[id="'+question_id+'"]').html('<p class="text-center no-margin"><i class="fa fa-refresh fa-spin"></i> Loading</p>');
            },
            error: function(){
                $('.question-content[id="'+question_id+'"]').html('<p class="text-center no-margin"><i class="fa fa-times"></i> Failed load question detail</p>');
            },
            success: function(r){
                $('#csrf').val(r.csrf);
            	$('.question-content[id="'+question_id+'"]').data('content',true);
                $('.question-content[id="'+question_id+'"]').html(r.result);
            }
        });
    };

    $(this).on('click', container + ' .detail-questions', function (e){
        var test_type_id = $(this).data('id');
        $(this).detail_questions({
            test_type_id : test_type_id
        })
        e.preventDefault();
    });


    $(this).on('click', container + ' .btn-new', function (e) {
        Widget.Loader('create_question', {}, 'container-content', false);
    });

    $(this).on('click', container + ' .detail-questions-type', function (e){
        var id = $(this).data('id');
        t = '';
        switch(id){
            case 1:
                tinymce.remove();
                t += '<form role="form" id="form-create">';
                    t += '<input type="hidden" name="type_question" value="'+id+'">';
                    t += '<div class="row">';
                        t += '<div class="col-md-12">';
                            t += '<div class="form-group">';
                                t += '<label>Question</label>';
                                t += '<textarea class="form-control" name="question_text" id="question_text"></textarea>';
                            t += '</div>';
                            t += '<div class="form-group">';
                                t += '<label>Answers</label>';
                                t += '<div class="row">';
                                    t += '<div class="col-md-8">';
                                        t += '<input type="text" class="form-control" id="option_total_answers" placeholder="Total Answers...">';
                                    t += '</div>';
                                    t += '<div class="col-md-4">';
                                        t += '<button type="button" class="btn btn-default" id="btn-add-total-answers">Add</button>';
                                    t += '</div>';
                                t += '</div>';
                            t += '</div>';
                            t += '<div id="answers-list" style="margin-botttom:10px;"></div>';
                            t += '<div class="form-group">';                                
                                t += '<label>Description</label>';
                                t += '<textarea class="form-control" name="question_description" id="question_description"></textarea>';
                            t += '</div>';
                            t += '<div class="form-group">';
                                t += '<button type="submit" class="btn btn-danger">Submit</button>';
                            t += '</div>';
                        t += '</div>';
                    t += '</div>';
                t += '</form>';
                $('#section-question-type-form').html(t);
                $(container + ' #btn-add-description-question').on('click', function (e) {
                    $('#question_description_container').show();
                });
                $(container + ' #btn-add-total-answers').on('click', function (e) {
                    var option_total_answers = $('#option_total_answers').val();
                    ans = '';
                    for (i = 0; i < option_total_answers; i++) { 
                        ans += '<div class="input-group">';
                            ans += '<input type="text" class="form-control" name="answers['+i+']" required="" placeholder="Answers...">';
                               ans += '<div class="input-group-btn">';
                                    ans += '<label class="btn btn-default btn-sm active">';
                                        ans += '<input name="correct" value="'+i+'" type="radio"> Correct';
                                    ans += '</label>';
                               ans += '</div>';
                        ans += '</div>';
                    }
                    $('#answers-list').html(ans);
                });
                
                tinymce.init({
                    selector: '#question_text',
                    menubar: false
                });
            break;
            case 2:
                tinymce.remove();
                t += '<form role="form" id="form-create">';
                    t += '<input type="hidden" name="type_question" value="'+id+'">';
                    t += '<div class="row">';
                        t += '<div class="col-md-12">';
                            t += '<div class="form-group">';
                                t += '<label>Question</label>';
                                t += '<textarea class="form-control" name="question"></textarea>';
                            t += '</div>';
                        t += '</div>';
                    t += '</div>';
                t += '</form>';
                $('#section-question-type-form').html(t);
                 tinymce.init({
                    selector: 'textarea',
                    menubar: false
                });
            break;
        }
        e.preventDefault();
    });
    
    $(this).on('submit', container + ' #form-create', function (e) {
        var form = $(this);
        var mode = $(this).data('mode');
        $(this).ajaxSubmit({
            url: site_url + '/questions/widget/save_question',
            type: 'POST',
            dataType: 'JSON',
            data:{
                'csrf_token_app' : $('#csrf').val()
            },
            beforeSend: function () {
                form.find('[type="submit"]').addClass('disabled');
                form.find('[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                $('#csrf').val(r.csrf);
                if(r.success){
                    alert(r.msg);
                    form.resetForm();
                    form.find('#answers-list').html('');
                }
                form.find('[type="submit"]').removeClass('disabled');
                form.find('[type="submit"]').html('Submit');
            },
        });
        e.preventDefault();
    });
    
    tinymce.init({
        selector: 'textarea',
        menubar: false,
        mobile: { theme: 'mobile' }
    });


});