<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array(
            'title' => 'Dashboard',
            'user' => $this->_user,
        );
        $this->render_widget($data);
    }

    public function get_summary(){
        $p = $this->input->post();
        $sdate = isset($p['sdate']) && $p['sdate'] != '' ? $p['sdate'] : date('Y-m-d');
        $edate = isset($p['edate']) && $p['edate'] != '' ? $p['edate'] : date('Y-m-d');
        $response['vacancy'] = $this->_get_vacancy($sdate, $edate);
        $response['degree'] = $this->_get_degree($sdate, $edate);
        $response['gender'] = $this->_get_gender($sdate, $edate);
        $response['total_applicant'] = $this->db->where('date(created_date) BETWEEN "' . $sdate . '" AND "' . $edate . '"')->count_all_results('applicant');
        $response['total_active'] = $this->db->join('users as b', 'a.user_id = b.id')->where('date(a.created_date) BETWEEN "' . $sdate . '" AND "' . $edate . '"')->where('b.active', 1)->count_all_results('applicant as a');
        $response['total_inactive'] = $this->db->join('users as b', 'a.user_id = b.id')->where('date(a.created_date) BETWEEN "' . $sdate . '" AND "' . $edate . '"')->where('b.active', 0)->count_all_results('applicant as a');
        $this->json_result($response);
    }

    private function _get_vacancy($sdate, $edate){
        $this->db->select('b.name as vacancy_name, count(a.vacancy_division_id) as total');
        $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
        $this->db->where('date(a.created_date) BETWEEN "' . $sdate . '" AND "' . $edate . '" ');
        $this->db->group_by('a.vacancy_division_id');
        $rs = $this->db->get('applicant as a');
        if($rs->num_rows() > 0){
            foreach ($rs->result_array() as $key => $value) {
                $data[] = array('name' => $value['vacancy_name'], 'y' => (int) $value['total']);
            }
            return $data;
        }else{
            return false;
        }

    }

    private function _get_degree($sdate, $edate){
        $this->db->select('a.education_degree, count(a.education_degree) as total');
        $this->db->where('date(a.created_date) BETWEEN "' . $sdate . '" AND "' . $edate . '" ');
        $this->db->group_by('a.education_degree');
        $rs = $this->db->get('applicant as a');
        if($rs->num_rows() > 0){
            foreach ($rs->result_array() as $key => $value) {
                $data[] = array('name' => $value['education_degree'], 'y' => (int) $value['total']);
            }
            return $data;
        }else{
            return false;
        }
    }

    private function _get_gender($sdate, $edate){
        $this->db->select('a.gender, count(a.gender) as total');
        $this->db->where('date(a.created_date) BETWEEN "' . $sdate . '" AND "' . $edate . '" ');
        $this->db->group_by('a.gender');
        $rs = $this->db->get('applicant as a');
        if($rs->num_rows() > 0){
            foreach ($rs->result_array() as $key => $value) {
                $data[] = array('name' => $value['gender'] == 'L' ? 'Laki-Laki' : 'Perempuan', 'y' => (int) $value['total']);
            }
            return $data;
        }else{
            return false;
        }
    }
}