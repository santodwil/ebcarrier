<?php

class Home extends MY_Controller {
    public function __construct() {
        parent::__construct();
    }
    private function get_member_info($field) {
        $this->load->model('member_model');
        $member_info = $this->member_model
                ->fields('team_id,nik')
                ->get(array(
                    'users_id' => $this->_user->id
                ));
        return $member_info[$field];
    }
    
    private function get_plan($plan_id = null, $plan_type = null) {
        $this->load->model('plan_model');
        $this->load->model('member_model');
        $this->load->model('plan_shared_model');
        if($plan_id) {
            $plan = $this->plan_model
                ->where('team_id', $this->get_member_info('team_id'))
                ->with_exposure()    
                ->get_all($plan_id);
            $plan_shared = $this->plan_shared_model
                    ->with_plan(array('with'=>array('relation'=>'exposure')))
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->where('plan_id', $plan_id)
                    ->get_all();
        } else {
            if($plan_type && $plan_type != 3) {
                $plan = $this->plan_model
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->where('plan_type_id', $plan_type)
                    ->with_exposure()
                    ->get_all();
                $plan_shared = $this->plan_shared_model
                        ->with_plan(array(
                            'with'=> array(
                                'relation'=>'exposure'
                                ),
                            'where' => array(
                                'plan_type_id' => $plan_type
                                )
                            ))
                        ->where('team_id', $this->get_member_info('team_id'))
                        ->get_all();
            } else {
                $plan = $this->plan_model
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->with_exposure()
                    ->get_all();
                $plan_shared = $this->plan_shared_model
                        ->with_plan(array('with'=>array('relation'=>'exposure')))
                        ->where('team_id', $this->get_member_info('team_id'))
                        ->get_all();
            }
        }
        $shared_plan = array();
        if($plan_shared) {
            foreach($plan_shared as $v) {
                if (array_key_exists('plan', $v)){
                $shared_plan[] = $v['plan'];
                }
            }
        }
        if($plan_type != 3) {
            if($plan AND $shared_plan) {
                return array_merge($plan, $shared_plan);
            }
            else if($plan AND !$shared_plan) {
                return $plan;
            }
            else if(!$plan AND $shared_plan) {
                return $shared_plan;
            }
            else {
                return null;
            }
        } else if($plan_type == 3) {
            return $shared_plan;
        } else {
            if($plan AND $shared_plan) {
                return array_merge($plan, $shared_plan);
            }
            else if($plan AND !$shared_plan) {
                return $plan;
            }
            else if(!$plan AND $shared_plan) {
                return $shared_plan;
            }
            else {
                return null;
            }
        }
    }

    public function get_job_assignment($plan_id = null) {
        $this->load->model('ja_request_model');
        $this->load->model('plan_model');

        $nik = $this->get_member_info('nik');
        $get_ja = $this->ja_request_model
                ->fields('plan_id')
                ->where('plan_id', $plan_id)
                ->where('nik', $nik)
                ->where('status', '1')
                ->where('leader', $nik, NULL, TRUE)
                ->group_by('plan_id')
                ->get_all();

        $plan_id = array();
        if($get_ja) {
            foreach($get_ja as $k => $v) {
                $plan_id[] = $v['plan_id'];
            }
        }

        $plan = array();
        if($plan_id) {
            $plan = $this->plan_model
                    ->where('id', $plan_id)
                    ->with_exposure()
                    ->get_all();
        }

        return $plan;
//        $this->json_result($plan);
    }
    
    private function hierarchical_array(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->hierarchical_array($elements, $element['id']);
                if ($children) {
                    $element['child'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

//    public function index() {
//        $this->load->model('Member_model');
//
//        $id = $this->Member_model
//            ->fields('team_id')
//            ->get(array(
//                'users_id'=>$this->_user->id
//            ));
//
//        $nik = $this->Member_model
//            ->fields('nik')
//            ->get(array(
//                'users_id'=>$this->_user->id
//            ));
//        $this->load->model('Activity_model');
//
//        $act = $this->Activity_model
//                    ->get_all(array(
//                        'nik' => $nik['nik']
//            ));
//        $this->load->model('team_model');
//        $team_list = $this->team_model
//                    ->order_by('name', 'ASC')
//                    ->get_all();
//
//        $team = $this->hierarchical_array($team_list);
//
//        $this->load->model('quotes_model');
//        $quotes = $this->quotes_model
//                ->where('status', '1')
//                ->order_by('rand()')
//                ->get();
//
//        $data = array(
//            'title' => 'Beranda',
//            'content' => 'home/beranda',
//            '_css' => array(
//                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
//                'assets/plugins/nprogress/css/nprogress.css',
//                'assets/uplon/plugins/toastr/toastr.min.css',
//                'assets/uplon/plugins/select2/css/select2.min.css'
//            ),
//            '_js' => array(
//                'assets/plugins/ajaxform/ajaxform.min.js',
//                'assets/plugins/momentjs/momentjs.min.js',
//                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
//                'assets/plugins/nprogress/js/nprogress.js',
//                'assets/uplon/plugins/toastr/toastr.min.js',
//                'assets/plugins/simplepaginationjs/js/jquery.simplePagination.js',
//                'assets/uplon/plugins/select2/js/select2.full.min.js',
//                'assets/pages/home/beranda.js?v='.time()
//            ),
//            'menu_list' => $this->get_menu(),
//            'quotes' => $quotes,
//            'team' => $team,
//            'id'   => $id,
//            'act'  => $act,
//        );
//        $this->render_page($data);
//    }

    public function index() {
        $this->load->model('menu_model');
        $this->load->model('team_model');
        if(!$this->ion_auth->in_group(1)) {
            $member = $this->db->select('nik')->where('users_id', $this->_user->id)->get('member')->row_array();
            $mode = $this->db->where('leader', $member['nik'])->or_where('leader_2', $member['nik'])->get('plan')->num_rows();
            $leader_resource_pool = $this->team_model
                                    ->where('leader', $member['nik'])
                                    ->get();
            if($mode > 0 || $leader_resource_pool){
                // $menu_dashboard = $this->db->get('menu')->result_array();
                $menu_dashboard = $this->db
                                        ->where_in('role', array('leader','all'))
                                        ->where('show_in_dashboard', 1)
                                        ->get('menu')->result_array();
            } else{
                $menu_dashboard = $this->db
                                        ->where_in('role', array('all'))
                                        ->where('show_in_dashboard', 1)
                                        ->get('menu')->result_array();
            }
        }
        else{
            $menu_dashboard = $this->db
                                    ->where('show_in_dashboard', 1)
                                    ->get('menu')->result_array();
        }
        $data = array(
            'title' => 'Beranda',
            'content' => 'home/n_beranda',
            '_css' => array(
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/uplon/plugins/select2/css/select2.min.css',
                'assets/uplon/plugins/bootstrap-daterangepicker/daterangepicker.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
//                'assets/plugins/simplepaginationjs/js/jquery.simplePagination.js',
                'assets/plugins/simplepaginationjs/js/custom.jquery.simplePagination.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/plugins/jquery.sticky.js',
                'assets/plugins/sortable/sortable.js',
                'assets/uplon/plugins/bootstrap-daterangepicker/daterangepicker.js',
                'assets/pages/home/n_beranda.js?v='.time()
            ),
            'menu_list' => $this->get_menu(),
            'menu_dashboard' => $menu_dashboard
        );
        $this->render_page($data);
    }

    public function get_plan_list() {
        $type = $this->input->post('plan_type') ? $this->input->post('plan_type') : null;
//        $plan = $this->get_plan(null, $type);
        $plan = $this->get_job_assignment();
        if($plan) {
            foreach ($plan as $key => $row) {
                $plan_type[$key]  = $row['plan_type_id'];
                $name[$key] = $row['name'];
            }
            array_multisort($plan_type, SORT_DESC, $name, SORT_ASC, $plan);

            $plan = array_map('unserialize', array_unique(array_map('serialize', $plan)));
        }
        
        $data = array(
            'plan' => $plan,
            'type' => $type
        );
        
        $result = array(
            'status' => true,
            'message' => 'success',
            'view' => $this->load->view('pages/home/widget_plan_list', $data, true)
        );
        $this->json_result($result);
    }
    
    public function my_task() {
        $this->load->model('task_model');
        $this->load->model('task_contribution_view_model');
        $this->load->model('task_type_model');
        $this->load->model('task_status_model');
        $this->load->model('activity_model');

        $offset = $this->input->get('offset_task');
        $task_id_list = $this->input->get('task_id');
//        if($offset) {
//            $this->db->where('created_at BETWEEN "'.date('Y-m-d G:i:s', strtotime('-7 days')).'" AND "'.date('Y-m-d G:i:s').'"');
//        }
//        $activity = $this->activity_model
//                    ->fields('created_at, task_id')
//                    ->where('nik', $this->get_member_info('nik'))
//                    ->get_all();
//        $my_task = $this->task_contribution_view_model
//                ->where('nik', $this->get_member_info('nik'))
//                ->fields('task_id')
//                ->get_all();
//        $task_id = array();
//        if($my_task) {
//            foreach($my_task as $v){
//                $task_id[] = $v['task_id'];
//            }
//        }
//        $task = array();
//        $limit = 10;
//        if($task_id) {
//            if(!array_key_exists('offset_task', $this->input->get())) {
//                $this->db->where('updated_at BETWEEN "'.date('Y-m-d G:i:s', strtotime('-7 days')).'" AND "'.date('Y-m-d G:i:s').'"');
//            }
//            $task = $this->task_model
//                    ->with_exposure()
//                    ->with_status()
//                    ->with_task_type()
//                    ->with_plan()
//                    ->where('id', $task_id)
//                    ->limit($limit,$offset)
//                    ->order_by('updated_at', 'DESC')
//                    ->get_all();
//
//            if(!$task) {
//                $task = $this->task_model
//                    ->where('id', $task_id)
//                    ->limit($limit,$offset)
//                    ->get_all();
//            }
//        }

//        if(!$this->ion_auth->in_group(1)) {
        if(false) {
            $is_all = false;
            $this_week = false;
            $nik = $this->get_member_info('nik');
            if(!$offset) {
                $this->db->where('created_at BETWEEN "'.date('Y-m-d G:i:s', strtotime('-7 days')).'" AND "'.date('Y-m-d G:i:s').'"');
            } else {
                $is_all = true;
            }
            get_activity:
            if($task_id_list) {
                $task_id_not = array();
                foreach($task_id_list as $k => $v) {
                    foreach($v as $vv) {
                        $task_id_not[] = $vv;
                    }
                }
                $this->db->where_not_in('task_id', $task_id_not);
            }
            if($is_all) {
                $this->db->limit('10', $offset);
            }
            $activity = $this->activity_model
                ->where('nik', $nik)
                ->fields('nik, created_at, task_id')
                ->group_by('task_id')
                ->order_by('created_at', 'DESC')
                ->get_all();

            if(!$offset && !$activity) {
                $is_all = TRUE;
                $this_week = true;
                goto get_activity;
            }

            $task_id = array();
            if($activity) {
                foreach ($activity as $v) {
                    $task_id[] = $v['task_id'];
                }
            }

            $this->load->model('task_model');
            $task = null;
            if($task_id) {
                $task = $this->task_model
                    ->where('id', $task_id)
                    ->with_exposure()
                    ->with_status()
                    ->with_task_type()
                    ->with_plan()
                    ->order_by('created_at', 'DESC')
                    ->get_all();
            }

            $data = array(
                'task' => $task,
                'offset' => $offset,
                'this_week' => $this_week
            );

            $result = array(
                'status' => true,
                'message' => 'success',
                'view' => $this->load->view('pages/home/widget_my_task', $data, true),
                'additional_data' => array(
                    'task_id' => $task_id
                )
            );
        } else {
            $result = array(
                'status' => true,
                'message' => 'Not Allowed',
                'view' => '',
                'additional_data' => array(
                    'task_id' => 0
                )
            );
        }
        
        $this->json_result($result);
    }
    
    public function addplan(){
        $this->load->model('Plan_model');
        
        $name = $this->Plan_model
                ->get_all();
        
        $plan = $this->Plan_model
                ->get(array(
                'team_id'=>$this->input->post('team_id') ? $this->input->post('team_id') : $this->input->post('belongto'),
                'name'=>$this->input->post('name'),
                ));
            if($plan){
                $a = array();
                foreach($name as $t){
                   if($t['name'] == $plan['name']){
                   $a[] = $t['sequence']; 
                   }
                }
                $max = max($a) + 1;
                    $addplan = array(
                    'team_id'    =>$this->input->post('team_id') ? $this->input->post('team_id') : $this->input->post('belongto'),
                    'name'       =>$this->input->post('name'),
                    'description'=>$this->input->post('description'),
                    'sequence'   =>$max,
                    'plan_type_id' => $this->input->post('plantype')
                );
                $planid = $this->Plan_model->insert($addplan);
                
                if ($this->input->post('share')){
                $shared = $this->input->post('share');    
                $shared = explode(";", $shared);
                foreach ($shared as $t){
                    $shareplan = array(
                        'plan_id'    =>$planid,
                        'team_id'    =>$t
                    );
                    $this->db->insert('plan_shared',$shareplan);
                }
            }
            }else{
                $max = 1;
                    $addplan = array(
                    'team_id'    =>$this->input->post('team_id') ? $this->input->post('team_id') : $this->input->post('belongto'),
                    'name'       =>$this->input->post('name'),
                    'description'=>$this->input->post('description'),
                    'sequence'   =>$max,
                    'plan_type_id' => $this->input->post('plantype')
                );
                $planid = $this->Plan_model->insert($addplan);  
                
                if ($this->input->post('share')){
                $share = $this->input->post('share');
                $shared = explode(";", $share);
                foreach ($shared as $t){
                    $shareplan = array(
                    'plan_id'    =>$planid,
                    'team_id'    =>$t
                );
                $this->db->insert('plan_shared',$shareplan);
                }
            }
        }
        $this->load->view('pages/home/beranda');
    }

   
    public function manage() {
        $this->load->model('plan_model');
        $this->load->model('task_type_model');
        $this->load->model('task_status_model');
        $plan = $this->get_job_assignment();
        if($plan) {
            foreach ($plan as $key => $row) {
                $plan_type[$key]  = $row['plan_type_id'];
                $name[$key] = $row['name'];
            }
            array_multisort($plan_type, SORT_DESC, $name, SORT_ASC, $plan);
        }
        $check_plan = $this->input->get('default_plan') != 'no-plan' ? $this->get_job_assignment($this->input->get('default_plan')) : 'No Plan';
        if(!$check_plan) {
            redirect('home');
        }
        
        $task_type = $this->task_type_model->order_by('name', 'asc')->get_all();
        $type = array();
        foreach($task_type as $v) {
            $type[$v['parent']][] = $v;
        }
        
        $task_status = $this->task_status_model->get_all();
        
        $data = array(
            'title' => 'Manage',
            'content' => 'home/home',
            'default_plan' => $this->input->get('default_plan'),
            '_css' => array(
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/plugins/simplepaginationjs/css/simplePagination.css',
                'assets/uplon/plugins/select2/css/select2.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/simplepaginationjs/js/jquery.simplePagination.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/pages/home/home.js?v='.time()
            ),
            'menu_list' => $this->get_menu(),
            'plan' => $plan ? array_map('unserialize', array_unique(array_map('serialize', $plan))) : null,
            'active_plan' => is_array($check_plan) ? $check_plan[0]['name'].' #'.$check_plan[0]['sequence'] : $check_plan,
            'task_type_list' => $type,
            'task_status_list' => $task_status
        );
        $this->render_page($data);
    }
    
    public function task_list($type = null) {
        $this->load->model('task_model');
        $this->load->model('activity_model');
        $this->load->model('task_type_model');
        $this->load->model('task_contribution_view_model');
        
        $post = $this->input->post();
        $plan_id = $post['plan_id'] != 'no-plan' ? $post['plan_id'] : null;
        
        $task_type_list = $this->task_type_model
                ->get_all();
        $task_type = array();
        foreach($task_type_list as $v) {
            if($v['parent'] == $type OR $v['id'] == $type) {
                $task_type[] = $v['id'];
            }
        }
        
        $search = $this->input->post('search') ? json_decode($this->input->post('search'), true) : null;
        
        $offset = array_key_exists('offset_task', $post) ? $post['offset_task'] : null;
        $limit = 10;
        if(!empty($search)) {
            $task_contribution = array();
            if(!empty($search['task_contribution'][0])) {
                $task_contribution_list = $this->task_contribution_view_model
                                        ->where('nik', $search['task_contribution'])
                                        ->where('plan_id', $plan_id)
                                        ->fields('task_id')
                                        ->get_all();
                if($task_contribution_list) {
                    foreach($task_contribution_list as $v) {
                        $task_contribution[] = $v['task_id'];
                    }
                }
            }
            $task_type_child = null;
            if($search['task_type'] != 0) {
                $is_parent = $this->task_type_model
                            ->where('parent', 0)
                            ->get($search['task_type']);
                if($is_parent) {
                    $get_child = $this->task_type_model
                                ->where('parent', $is_parent['id'])
                                ->get_all();
                    if($get_child) {
                        $task_type_child = array();
                        foreach($get_child as $v) {
                            $task_type_child[] = $v['id'];
                        }
                    }
                    if($task_type_child) {
                        array_push($task_type_child, $is_parent['id']);
                    }
                }
            }
            $this->db->group_start();
                $this->db->like('name', $search['task_name'], 'both');
                if($search['task_type'] != 0) {
                    if($task_type_child) {
                        $this->db->where_in('task_type_id', $task_type_child);
                    } else {
                        $this->db->where('task_type_id',  $search['task_type']);
                    }
                } else {
                    $this->db->where_in('task_type_id', $task_type);
                }
                if($search['task_status'] != 0) {
                    $this->db->where('status', $search['task_status']);
                }
                if($task_contribution) {
                    $this->db->where_in('id', $task_contribution);
                }
            $this->db->group_end();
        } else {
            if(!array_key_exists('offset_task', $post)) {
                $this->db->where('updated_at BETWEEN "'.date('Y-m-d G:i:s', strtotime('-7 days')).'" AND "'.date('Y-m-d G:i:s').'"');
            }
            $this->db->where_in('task_type_id', $task_type);
        }
        $task_list = $this->task_model
                ->with_task_type()
                ->with_team()
                ->with_exposure()
                ->with_status()
                ->where(array(
                    'plan_id' => $plan_id
                ))
                ->limit($limit,$offset)
                ->order_by('updated_at', 'DESC')
                ->get_all();
            
        if(!array_key_exists('offset_task', $post) && !$task_list) {
            $limit = 10;
            $task_list = $this->task_model
                ->with_task_type()
                ->with_team()
                ->with_exposure()
                ->with_status()
                ->where(array(
                    'plan_id' => $plan_id
                ))
                ->where('task_type_id', $task_type)
                ->limit($limit)
                ->order_by('updated_at', 'DESC')
                ->get_all();
        }
        
        $task = array();
        if($task_list) {
            foreach($task_list as $v) {
                if($v['task_type']['parent'] == $type || $v['task_type']['id'] == $type) {
                    $activity = $this->activity_model
                            ->fields('id')
                            ->with_activity_file('fields:fullpath,extension')
                            ->order_by('id', 'desc')
                            ->get(array('task_id' => $v['id']));
                    $file = array();
                    if($activity) {
                        if(array_key_exists('activity_file', $activity)) {
                            if($activity['activity_file']) {
                                $file[] = current($activity['activity_file']);
                            }
                        }
                    }
                    $task[] = array(
                        'task' => $v,
                        'file' => $file
                        );
                }
            }
        }
        $data = array(
            'task_list' => $task,
            'type' => $type,
            'search' => $search,
            'offset' => $offset
        );
        $result = array(
            'status' => true,
            'message' => 'success',
            'view' => $this->load->view('pages/home/widget_task_list', $data, TRUE)
            );
        $this->json_result($result);
    }
    
    public function plan_details() {
        $this->load->model('plan_model');
        $this->load->model('team_model');
        
        $plan_id = $this->input->get('plan_id') != 'no-plan' ? $this->input->get('plan_id') : 0;
        $plan_details = $this->plan_model
                ->with_team()
                ->with_plan_shared()
                ->get($plan_id);
        $plan_shared = $plan_details['plan_shared'];
        
        $target_team = array();
        if($plan_shared) {
            foreach($plan_shared as $k => $v) {
                $target_team[] = $v['id'];
            }
        }
        
        $shared_team = array();
        foreach($target_team as $v) {
            $shared_team[] = $this->team_model
                    ->get($v);
        }
        
        $data = array(
            'plan_details' => $plan_details,
            'shared_team' => $shared_team
        );
        
        $result = array(
            'status' => true,
            'message' => 'success',
            'view' => $this->load->view('pages/home/widget_plan_details', $data, TRUE)
        );
        $this->json_result($result);
    }
    
    public function plan_attachment() {
        $this->load->model('task_model');
        $this->load->model('activity_model');
        $this->load->model('activity_file_model');
        $this->load->model('file_model');
        $offset = $this->input->get('offset') ? $this->input->get('offset') : 20; 
        $start = $this->input->get('start') ? $this->input->get('start') : 0; 
        $task = $this->task_model
                ->where('plan_id', $this->input->get('plan_id'))
                ->fields('id')
                ->get_all();
        $file_id = array();
        if($task) {
            foreach($task as $v) {
                $activity = $this->activity_model
                        ->where('task_id', $v['id'])
                        ->fields('id')
                        ->get_all();
                if($activity) {
                    foreach($activity as $vv) {
                        $activity_file = $this->activity_file_model
                            ->where('activity_id', $vv['id'])
                            ->get_all();
                        if($activity_file) {
                            foreach($activity_file as $vvv) {
                                $file_id[] = $vvv['file_id'];
                            }
                        }
                    }
                }
            }
        }
        $file = array();
        if($file_id) {
            foreach($file_id as $v) {
                $file_list = $this->file_model
                        ->where('extension', array('.jpg', '.jpeg', '.png', '.gif'))
                        ->get($v);
                if($file_list) {
                    $file[] = $file_list;
                }
            }
        }
        
        $data = array(
            'file' => array_slice($file, $start, $offset)
        );
        
        $result = array(
            'status' => true,
            'message' => 'success',
            'view' => $this->load->view('pages/home/widget_plan_attachment', $data, true),
            'total' => count($file)
        );
        
        $this->json_result($result);
    }
    
    public function plan_contribution() {
        $plan_id = $this->input->get('plan_id') != 'no-plan' ? $this->input->get('plan_id') : 0;
        $this->load->model('plan_contribution_view_model');
        $contribution = $this->plan_contribution_view_model
            ->with_member(array(
                    'fields' => 'name,avatar,email,team_id',
                    'with' => array(
                        'relation' => 'team',
                        'fields' => 'id,name'
                    )
                ))
            ->get_all($plan_id);
        
        $grouping = array();
        if($contribution) {
            foreach($contribution as $v) {
                if(array_key_exists('member', $v)) {
                    $grouping[$v['member']['team']['id']][] = array(
                                                            'team_name' => $v['member']['team']['name'],
                                                            'member' => $v['member']
                                                        );
                }
            }
        }
        
        $data = array(
            'contribution' => $contribution,
            'grouping' => $grouping
        );
        
        $result = array(
            'status' => true,
            'message' => 'success',
            'view' => $this->load->view('pages/home/widget_plan_contributions', $data, TRUE),
            'person_list' => $this->load->view('pages/home/widget_search_person', $data, TRUE)
        );
        $this->json_result($result);
    }
    
    public function edit_password_modal() {
        echo $this->load->view('pages/home/widget_edit_password_modal', true);
    }
    
    public function edit_password() {
        echo $this->load->view('pages/home/widget_edit_password', true);
    }
    public function edit_password_update(){
        $this->load->model('users_model');
        $users = $this->users_model
                ->get(array(
                    'id' => $this->_user->id
                ));
        $id = $users['id'];
        $password = $this->input->post('current');
        $password_matches = $this->ion_auth->hash_password_db($id, $password);
        if($password_matches == 1){
            if($this->input->post('password') == $this->input->post('confirm')){
                $update_data = array(
                        'password' => $this->input->post('password')
                );
                $this->ion_auth->update($id, $update_data);
                $result = array('status' => true);
            }else{
                $result = array('status' => false);
            }
        }
        $this->json_result($result);
    }
    
    
    public function edit_profile_modal() {
        echo $this->load->view('pages/home/widget_edit_profile_modal', true);
    }
    
    public function edit_profile() {
        $this->load->model('users_model');
        
        $users = $this->users_model
                ->get(array(
                    'id' => $this->_user->id
                ));
        
        $data = array(
            'users' => $users
        );
        echo $this->load->view('pages/home/widget_edit_profile', $data, true);
//        echo '<pre>';
//        print_r($users);
//        echo '</pre>';
    }
    
    public function edit_profile_update(){
        $this->load->model('users_model');
        $users = $this->users_model
                ->get(array(
                    'id' => $this->_user->id
                ));
        
        $id = $users['id'];
        $avatar = $this->do_upload('edit-avatar');
        $update_data = array(
            'username' => $this->input->post('username__'), 
            'email' => $this->input->post('email__'), 
            'first_name' => $this->input->post('firstname__'),
            'last_name' => $this->input->post('lastname__')
                );
        if ($avatar) {
            $update_data['avatar'] = $avatar;
        }
        $this->db->update('users', $update_data, array('id'=>$id));
        
        $this->load->model('member_model');
        $member = $this->member_model
                ->get(array(
                    'users_id' => $this->_user->id
                ));
        $name = $this->input->post('firstname__') .' '. $this->input->post('lastname__');
        
        $userid = $member['nik'];
        $data = array(
            'name' => $name,
            'email' => $this->input->post('email__'),
                );
        if ($avatar){
            $data['avatar'] = $avatar;
        }
        
        $this->member_model->update($data, $userid);
        
        $result = array(
            'status' => 'success'
        );
        
        $this->json_result($result);
    }
    
    
    
    public function do_upload($index = NULL) {
        $config = array(
            'upload_path' => './images/user/',
            'allowed_types' => 'gif|jpg|png'
        );

        $this->load->library('upload', $config);
        if ($this->upload->do_upload($index)) {
            $data = $this->upload->data();
            return $config['upload_path'] . $data['file_name'];
        } else {
            return NULL;
        }
    }

    public function task_activity_modal() {
        $this->load->model('task_status_model');
        $this->load->model('task_type_model');
        $this->load->model('task_type_status_model');
        
        $task_id = $this->input->get('task_id');
        $get_task_status = $this->input->get('task_status') ? $this->input->get('task_status') : null;
        $task_status = $this->task_status_model
                ->get($get_task_status);
        
        $task_type_id = $this->input->get('task_type_id');
        $task_type = $this->task_type_model
                        ->get($task_type_id);
        if($task_type['parent'] != 0) {
            $task_type2 = $task_type['parent'];
        } else {
            $task_type2 = $task_type['id'];
        }
        $task_type_status = $this->task_type_status_model
                            ->where('task_type_id', $task_type2)
                            ->order_by('task_type_id', 'ASC')
                            ->get_all();
        $task_status_id = array();
        if($task_type_status) {
            foreach($task_type_status as $v) {
                $task_status_id[] = $v['task_status_id'];
            }
        }
        $task_status_list = $this->task_status_model
                ->where('id', $task_status_id)
                ->order_by('sort', 'DESC')
                ->get_all();

        $data = array(
            'task_id' => $task_id,
            'task_status' => $task_status,
            'task_status_list' => $task_status_list,
            'hide' => !$get_task_status ? true : false
        );
	echo $this->load->view('pages/home/widget_task_activity_modal',$data,true);
//        $this->json_result($task_status_list);
    }
    
    public function task_activity() {
        $this->load->model('task_model');
        $this->load->model('activity_model');

        $task_id = $this->input->post('task_id');        
        $task_detail = $this->task_model
                ->with_team()
                ->with_task_type()
                ->with_plan()
                ->with_exposure()
                ->with_contribution(array(
                    'with' => array(
                        'relation' => 'member',
                        'fields' => 'name,avatar'
                    )
                ))
                ->get($task_id);
        
        $offset = $this->input->post('offset_activity') ? $this->input->post('offset_activity') : null;
        $limit = 10;
        $activity = $this->activity_model
                ->with_member()
                ->with_activity_file('fields:name,description,path,fullpath,extension,size')
                ->order_by('created_at', 'desc')
                ->limit($limit, $offset)
                ->get_all(array(
                    'task_id' => $task_id
                ));
        
        $data = array(
            'task_detail' => $task_detail,
            'activity' => $activity,
            'task_id' => $task_id,
            'offset' => $offset,
            'nik' => $this->get_member_info('nik')
        );
        
        $result = array(
            'status' => true,
            'message' => 'success',
            'view_activity' => $this->load->view('pages/home/widget_task_activity', $data, true)
        );
        
        $this->json_result($result);
    }
    
    public function task_activity_info() {
        $this->load->model('task_model');
        $this->load->model('member_model');

        $task_id = $this->input->get('task_id');        
        $task_detail = $this->task_model
                ->with_team()
                ->with_task_type()
                ->with_plan()
                ->with_exposure()
                ->with_contribution(array(
                    'with' => array(
                        'relation' => 'member',
                        'fields' => 'name,avatar'
                    )
                ))
                ->get($task_id);
        if($task_detail['assign']) {
            $assign_member = $this->member_model->get($task_detail['assign']);
            $task_detail['assign'] = $assign_member['name'];
        }
        
        $data = array(
            'task_id' => $task_id,
            'task_detail' => $task_detail,
            'source' => $this->input->get('source')
        );
        
        $result = array(
            'status' => true,
            'message' => 'success',
            'view' => $this->load->view('pages/home/widget_task_activity_info', $data, true),
        );
        
        $this->json_result($result);
    }

    public function update_activity() {
        $this->load->model('activity_file_model');
        $this->load->model('activity_model');
        $this->load->model('file_model');

        $id = $this->input->post('id');
        $description = $this->input->post('description');
        $file_id = $this->input->post('file_id');

        if($id) {
            if ($file_id) {
                foreach ($file_id as $v) {
                    $file = $this->file_model->fields('id,fullpath')->get($v);
                    $this->file_model->delete($v);
                    unlink($file['fullpath']);
                }
            }
            if (array_key_exists('attachment', $_FILES)) {
                for ($i = 0; $i < count($_FILES['attachment']['name']); $i++) {
                    $_FILES['userFile']['name'] = $_FILES['attachment']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['attachment']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                    $_FILES['userFile']['size'] = $_FILES['attachment']['size'][$i];

                    $uploadPath = './uploads/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png|doc|docx|xls|xlsx|ppt|pptx|pdf|zip|rar';

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
                        $file = $this->upload->data();
                        $data_file = array(
                            'name' => $file['raw_name'],
                            'path' => $uploadPath,
                            'fullpath' => $uploadPath . $file['file_name'],
                            'extension' => $file['file_ext'],
                            'size' => $file['file_size'],
                            'nik' => $this->get_member_info('nik')
                        );
                        $file_id = $this->file_model->insert($data_file);
                        $data_activity_file = array(
                            'file_id' => $file_id,
                            'activity_id' => $id
                        );
                        $this->db->insert('activity_file', $data_activity_file);
                    }
                }
            }

            $file = $this->activity_file_model
                ->where('activity_id', $id)
                ->with_file('fields:id,name,fullpath,extension')
                ->get_all();
            $file_info = array();
            if ($file) {
                $image = array('.jpg', '.jpeg', '.png', '.gif');
                $document = array('.doc', '.docx');
                $excel = array('.xls', '.xlsx');
                $presentation = array('.ppt', '.pptx');

                foreach ($file as $vv) {
                    if (in_array(strtolower($vv['file']['extension']), $image)) {
                        $url = base_url($vv['file']['fullpath']);
                        $with_link = false;
                    } else if (in_array(strtolower($vv['file']['extension']), $document)) {
                        $url = base_url('images/Microsoft-Word-New.png');
                        $with_link = true;
                    } else if (in_array(strtolower($vv['file']['extension']), $excel)) {
                        $url = base_url('images/Microsoft-Excel-2013-01.png');
                        $with_link = true;
                    } else if (in_array(strtolower($vv['file']['extension']), $presentation)) {
                        $url = base_url('images/Microsoft-PowerPoint.png');
                        $with_link = true;
                    } else if (strtolower($vv['file']['extension']) == '.pdf') {
                        $url = base_url('images/pdf.png');
                        $with_link = true;
                    } else if (strtolower($vv['file']['extension']) == '.zip') {
                        $url = base_url('images/File-Format-ZIP.png');
                        $with_link = true;
                    } else {
                        $url = base_url('images/Document Help-WF.png');
                        $with_link = true;
                    }
                    $file_info[] = array(
                        'file_id' => $vv['file']['id'],
                        'url' => $url,
                        'alt' => $vv['file']['name'] . $vv['file']['extension'],
                        'full_path' => base_url() . $vv['file']['fullpath'],
                        'with_link' => $with_link
                    );
                }
            }

            $data = array(
                'description' => $description
            );

            $this->activity_model->update($data, $id);

            $result = array(
                'status' => true,
                'message' => 'Success',
                'description' => $description,
                'file' => $file_info
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Unknown error occured'
            );
        }

        $this->json_result($result);
    }

    public function share_plan_modal() {
        $this->load->model('team_model');
        $this->load->model('plan_model');
        $this->load->model('plan_shared_model');
        
        $plan_id = $this->input->get('plan_id');
        $team_id = $this->input->get('team_id');
        $team_list = $this->team_model->get_all();
        
        $team = $this->hierarchical_array($team_list);
        $team_shared = $this->plan_shared_model->fields('team_id')->get_all($plan_id);
        $shared_team_id = array();
        if($team_shared) {
            foreach($team_shared as $v) {
                $shared_team_id[] = $v['team_id'];
            }
        }
        
        $data = array(
            'plan_id' => $plan_id,
            'team_list' => $team,
            'team_plan' => $team_id,
            'team_shared' => $shared_team_id
        );
    	echo $this->load->view('pages/home/widget_share_plan_modal',$data,true);
//        $this->json_result($team_child);
    }
    
    public function share_plan() {
        $team_id = $this->input->post('team_id');
        $plan_id = $this->input->post('plan_id');
        
        $this->load->model('plan_shared_model');
        $this->plan_shared_model->delete($plan_id);
        
        foreach($team_id as $v) {
            $data = array(
                'plan_id' => $plan_id,
                'team_id' => $v
            );
            $this->db->insert('plan_shared',$data);
        }
    }
    
    public function add_task_modal() {
        $this->load->model('task_type_model');
        $this->load->model('team_model');
        
        $plan_id = $this->input->get('plan_id');
        
        $task_type = $this->task_type_model
                    ->order_by('name', 'asc')
                    ->get_all();
        
        $type = array();
        foreach($task_type as $v) {
            $type[$v['parent']][] = $v;
        }
        
        $team = $this->team_model
                ->get_all();
                
        $data = array(
            'type' => $type,
            'team' => $team,
            'plan_id' => $plan_id
        );
        
        echo $this->load->view('pages/home/widget_add_task_modal', $data, true);
    }
    
    public function add_task() {
        $this->load->model('task_model');
        $task_name = $this->input->post('name');
        $task_description = $this->input->post('description');
        $task_type = $this->input->post('type');
        $task_due_date = $this->input->post('due_date') ? date('Y-m-d', strtotime($this->input->post('due_date'))) : null;
        $task_plan_id = $this->input->post('plan_id');
//        $task_belong_to = $this->input->post('belong_to');
        
        $data = array(
            'name' => $task_name,
            'description' => $task_description,
            'due_at' => $task_due_date,
            'status' => 6,
            'task_type_id' => $task_type,
            'plan_id' => $task_plan_id,
//            'team_id' => $task_belong_to,
            'nik' => $this->get_member_info('nik'),
            'updated_at' => date('Y-m-d G:i:s')
        );
        
        $this->task_model->insert($data);
    }
    
    public function new_activity() {
        $this->load->model('activity_model');
        $this->load->model('file_model');
        $this->load->model('task_model');
        
        $task_description = $this->input->post('task_description');
        $task_id = $this->input->post('task_id');
        $created_at = date('Y-m-d', strtotime($this->input->post('created_at')));
        $data = array(
            'description' => $task_description,
            'created_at' => $created_at . ' ' . date('G:i:s'),
            'task_id' => $task_id,
            'nik' => $this->get_member_info('nik')
        );
        $this->task_model
             ->update(array('updated_at' => $created_at), $task_id);
        $activity_id = $this->activity_model->insert($data);
        
        if(array_key_exists('attachment', $_FILES)) {
            for($i=0; $i < count($_FILES['attachment']['name']); $i++) {
                $_FILES['userFile']['name'] = $_FILES['attachment']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['attachment']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                $_FILES['userFile']['size'] = $_FILES['attachment']['size'][$i];
                
                $uploadPath = './uploads/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|doc|docx|xls|xlsx|ppt|pptx|pdf|zip|rar';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $file = $this->upload->data();
                    $data_file = array(
                        'name' => $file['raw_name'],
                        'path' => $uploadPath,
                        'fullpath' => $uploadPath.$file['file_name'],
                        'extension' => $file['file_ext'],
                        'size' => $file['file_size'],
                        'nik' => $this->get_member_info('nik')
                    );
                    $file_id = $this->file_model->insert($data_file);
                    $data_activity_file = array(
                        'file_id' => $file_id,
                        'activity_id' => $activity_id
                    );
                    $this->db->insert('activity_file' ,$data_activity_file);
                    
                    $result = array(
                        'status' => true,
                        'message' => 'success'
                    );
                }
                else {
                    $result = array(
                        'status' => false,
                        'message' => $this->upload->display_errors()
                    );
                    $this->activity_model->delete($activity_id);
                }
            }
        }
        else {
            $result = array(
                'status' => true,
                'message' => 'success'
            );
        }
        $this->json_result($result);
    }
    
    public function update_task_status() {
        $this->load->model('task_model');

        $id = $this->input->post('id');
        $task_id = $this->input->post('task_id');
        
        $this->task_model
             ->update(array('status' => $id), $task_id);
    }
    
    public function update_due_date() {
        $this->load->model('task_model');
        
        $task_id = $this->input->post('task_id');
        $date = date('Y-m-d', strtotime($this->input->post('date')));
        
        $this->task_model
             ->update(array('due_at' => $date), $task_id);
    }
    
    public function remove_due_date() {
        $this->load->model('task_model');
        
        $task_id = $this->input->post('task_id');
        
        $this->task_model
             ->update(array(
                 'due_at' => null
             ), $task_id);
    }
    
    public function search_task() {
        $task_name = $this->input->post('task-name');
        $task_type = $this->input->post('task-type');
        $task_status = $this->input->post('task-status');
        $task_person = $this->input->post('task-person');
        
        $data = array(
            'task_name' => $task_name,
            'task_type' => $task_type,
            'task_status' => $task_status,
            'task_person' => $task_person
        );
        print_r($data);
    }
    
    public function team() {
        if(!$this->ion_auth->in_group(1)) {
            redirect('home');
        }
        $this->load->model('team_model');
        $team_list = $this->team_model->where('active', 1)->get_all();
        
        $team = $this->hierarchical_array($team_list);
        $data = array(
            'title' => 'Team',
            'content' => 'home/team',
            'team' => $team,
            '_css' => array(
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/plugins/simplepaginationjs/css/simplePagination.css',
                'assets/uplon/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/uplon/plugins/select2/css/select2.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/simplepaginationjs/js/jquery.simplePagination.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/uplon/plugins/datatables/jquery.dataTables.min.js',
                'assets/uplon/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/pages/home/team.js?v='.time()
            ),
            'menu_list' => $this->get_menu()
        );
        $this->render_page($data);
    }
    
    public function team_data() {
        $this->load->model('team_model');
        $list = $this->team_model->get_datatables();
        $team_list = $this->hierarchical_array($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($team_list as $v) {
            $no++;
            switch ($v['flag']) {
                case '0':
                    $flag = 'Resource Pool';
                break;
                case '1':
                    $flag = 'Research Group';
                break;
                default:
                    $flag = 'No Flag';
                break;
            }
            $parent = $this->team_model->get($v['parent']);
            $row = array();
            $row[] = $v['name'];
            $row[] = $v['decription'];
            $row[] = $parent['name'];
            $row[] = $flag;
            $row[] = '<center><button class="btn btn-sm btn-success waves-effect waves-light tooltips" data-toggle="modal" id="edit-team-modal" data-id="'.$v['id'].'" data-toggle="tooltip" data-placement="top" title="Edit Team"><i class="zmdi zmdi-edit"></i></button>
                      <button class="btn btn-sm btn-danger waves-effect waves-light tooltips" data-toggle="modal" id="delete-team-modal" data-id="'.$v['id'].'" data-toggle="tooltip" data-placement="top" title="Remove Team"><i class="zmdi zmdi-close"></i></button></center>';
            $data[] = $row;

            if(array_key_exists('child', $v)) {
                foreach($v['child'] as $vv) {
                    $parents = $this->team_model->get($vv['parent']);
                    $row = array();
                    $row[] = '- '.$vv['name'];
                    $row[] = $vv['decription'];
                    $row[] = $parents['name'];
                    $row[] = $flag;
                    $row[] = '<center><button class="btn btn-sm btn-success waves-effect waves-light tooltips" data-toggle="modal" id="edit-team-modal" data-id="'.$vv['id'].'" data-toggle="tooltip" data-placement="top" title="Edit Team"><i class="zmdi zmdi-edit"></i></button>
                              <button class="btn btn-sm btn-danger waves-effect waves-light tooltips" data-toggle="modal" id="delete-team-modal" data-id="'.$vv['id'].'" data-toggle="tooltip" data-placement="top" title="Remove Team"><i class="zmdi zmdi-close"></i></button></center>';
                    $data[] = $row;
                    
                    if(array_key_exists('child', $vv)) {
                        foreach($vv['child'] as $vvv) {
                            $parents = $this->team_model->get($vvv['parent']);
                            $row = array();
                            $row[] = '-- '.$vvv['name'];
                            $row[] = $vvv['decription'];
                            $row[] = $parents['name'];
                            $row[] = $flag;
                            $row[] = '<center><button class="btn btn-sm btn-success waves-effect waves-light tooltips" data-toggle="modal" id="edit-team-modal" data-id="'.$vvv['id'].'" data-toggle="tooltip" data-placement="top" title="Edit Team"><i class="zmdi zmdi-edit"></i></button>
                                      <button class="btn btn-sm btn-danger waves-effect waves-light tooltips" data-toggle="modal" id="delete-team-modal" data-id="'.$vvv['id'].'" data-toggle="tooltip" data-placement="top" title="Remove Team"><i class="zmdi zmdi-close"></i></button></center>';
                            $data[] = $row;
                        }
                    }
                }
            }
        }
        $result = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->team_model->count_all(),
                "recordsFiltered" => $this->team_model->count_filtered(),
                "data" => array_slice($data, $_POST['start'], $_POST['length']),
            );

        $this->json_result($result);
    }
    
    public function add_team(){
        $this->load->model('team_model');
        
        $update_data = array(
            'name' => $this->input->post('team-name'),
            'decription' => $this->input->post('description'),
            'slug' => $this->input->post('slug'),
            'parent' => $this->input->post('parent'),
            'flag' => $this->input->post('flag'),
        );
        $this->team_model->insert($update_data);
    }
    
    public function delete_team_modal(){
        $id = $this->input->post('id');
        $data = array(
            'id' => $id
        );
        echo $this->load->view('pages/home/widget_team_delete', $data, true);
    }
    
    public function delete_team(){
        $this->load->model('team_model');
        
        $id = $this->input->post('data_id');
        $this->team_model->delete($id);
    }
    
    public function edit_team_modal(){
        $this->load->model('team_model');
        $team_id = $this->input->post('id');
        $team = $this->team_model
                ->get($team_id);
        $team_lists = $this->team_model->get_all();
        
        $team_list = $this->hierarchical_array($team_lists);
        $data = array(
            'team' => $team,
            'team_id' => $team_id,
            'team_list' => $team_list
        );
        echo $this->load->view('pages/home/widget_edit_team', $data, true);
    }   
    
    public function edit_team(){
            $update_team = array(
                'name' => $this->input->post('teamname'),
                'decription' => $this->input->post('description_'),
                'slug' => $this->input->post('slug'),
                'parent' => $this->input->post('parent'),
                'flag' => $this->input->post('flag')
            );
            $teamid = $this->input->post('teamid');
            $this->load->model('team_model');
            $this->team_model->update($update_team, $teamid);
    }
    
    public function employee() {
        if(!$this->ion_auth->in_group(1)) {
            redirect('home');
        }
        $this->load->model('team_model');
        $team_list = $this->team_model->get_all();
        $team = $this->hierarchical_array($team_list);
        
        $this->load->model('job_role_model');
        $job_role_list = $this->job_role_model
                    ->get_all();
        $job_role = $this->hierarchical_array($job_role_list);
        
        $this->load->model('job_title_model');
        $job_title_list = $this->job_title_model
                        ->get_all();
        $job_title = $this->hierarchical_array($job_title_list);
        
        $data = array(
            'title' => 'Employee',
            'content' => 'home/employee',
            'team' => $team,
            '_css' => array(
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/plugins/simplepaginationjs/css/simplePagination.css',
                'assets/uplon/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/uplon/plugins/select2/css/select2.min.css',
                'assets/uplon/plugins/switchery/switchery.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/simplepaginationjs/js/jquery.simplePagination.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/uplon/plugins/datatables/jquery.dataTables.min.js',
                'assets/uplon/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/uplon/plugins/switchery/switchery.min.js',
                'assets/pages/home/employee.js?v='.time()
            ),
            'menu_list' => $this->get_menu(),
            'job_role' => $job_role,
            'job_title' => $job_title
        );
        $this->render_page($data);
    }
    
    public function employee_data() {
        $this->load->model('member_model');
        $list = $this->member_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $v) {
            $no++;
            $active = $this->ion_auth->user($v['users_id'])->row()->active;
            $row = array();
            $row[] = $v['nik'];
            $row[] = $v['member_name'];
            $row[] = $v['email'];
            $row[] = $v['team_name'];
            if($v['users_id']) {
                if($active == 1) {
                    $row[] = '<center><input class="employee-status" type="checkbox" checked data-plugin="switchery" data-size="small" data-color="#039cfd" data-id="'.$v['users_id'].'"/></center>';
                } else {
                    $row[] = '<center><input class="employee-status" type="checkbox" data-plugin="switchery" data-size="small" data-color="#039cfd" data-id="'.$v['users_id'].'"/></center>';
                }
            } else {
                $row[] = '';
            }
            $action = '<center>';
            if(!$v['users_id']) {
                $action .= ' <button class="btn btn-sm btn-primary waves-effect waves-light tooltips" data-nik="'.$v['nik'].'" id="confirm-modal-button" data-toggle="tooltip" data-placement="top" title="Create User"><i class="zmdi zmdi-plus"></i></button> ';
            }
            $action .= '<button class="btn btn-sm btn-success waves-effect waves-light tooltips" data-toggle="modal" id="edit-employee-modal" data-nik="'.$v['nik'].'" data-id="'.$v['users_id'].'" data-toggle="tooltip" data-placement="top" title="Edit Member"><i class="zmdi zmdi-edit"></i></button>
                      <button class="btn btn-sm btn-danger waves-effect waves-light tooltips" data-toggle="modal" id="delete-employee-modal" data-nik="'.$v['nik'].'" data-toggle="tooltip" data-placement="top" title="Remove Member"><i class="zmdi zmdi-close"></i></button>';
            $action .= '</center>';
            $row[] = $action;
            $data[] = $row;
        }
        $result = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->member_model->count_all(),
                "recordsFiltered" => $this->member_model->count_filtered(),
                "data" => $data,
            );
        
        $this->json_result($result);
    }
    
    public function add_employee(){
        $this->load->model('member_model');
        
        $avatar = $this->do_upload('add-avatar');
        $name = $this->input->post('first-name').' '.$this->input->post('last-name');
        $update_data = array(
            'email' => $this->input->post('email'), 
            'name' => $name,
            'nik' => $this->input->post('nik'),
            'team_id' => $this->input->post('team'),
            'job_role_id' => $this->input->post('jobrole'),
            'job_title_id' => $this->input->post('jobtitle')
            );
        if ($avatar) {
            $update_data['avatar'] = $avatar;
        }

        $check = $this->member_model->get($this->input->post('nik'));
        if(!$check) {
            $this->member_model->insert($update_data);

            $result = array(
                'status' => true,
                'message' => 'Success'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'NIK Already Registered'
            );
        }

        $this->json_result($result);
    }
    
    public function delete_employee_modal(){
        $nik = $this->input->post('nik');
        $data = array(
            'nik' => $nik
        );
        echo $this->load->view('pages/home/widget_delete_employee', $data, true);
    }
    
    public function delete_employee(){
        $this->load->model('member_model');
        
        $nik = $this->input->post('data_nik');
        $this->member_model->delete($nik);
    }
    
    public function update_employee_status() {
        $id = $this->input->post('id');

        $status = array(
            'active' => $this->input->post('status') ? 1 : 0
        );
        
        $update = $this->db->update('users', $status, array('id' => $id));
        
        if($update) {
            $result = array(
                'status' => true,
                'message' => 'Successfully to update member'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to update member'
            );
        }
        
        $this->json_result($result);
    }
    
    public function confirm_modal(){
        $this->load->model('member_model');
        $this->load->model('Groups_model');
        $nik = $this->input->post('nik');
        $group = $this->Groups_model
                ->get_all();
        $member = $this->member_model
                ->get($nik);
        $data = array(
            'nik' => $nik,
            'group' => $group,
            'member' => $member
        );
        echo $this->load->view('pages/home/widget_add_to_user', $data, true);
    }
    
    public function add_to_user(){
        if($this->input->post('password_') == $this->input->post('confirmpassword_')){
            $username = $this->input->post('username_');
            $password = $this->input->post('password_');
            $email = $this->input->post('email_');

                $update_data = array(
                    'first_name' => $this->input->post('firstname_'),
                    'last_name' => $this->input->post('lastname_')
                        );
            $group = array(
                $this->input->post('groups')
            );
        }
        $id = $this->ion_auth->register($username, $password, $email, $update_data, $group);
        $nik = $this->input->post('nikk');
        $this->load->model('member_model');
        $update = array(
            'users_id' => $id
        );
        
        $this->member_model->update($update, $nik);
    }
    
    public function edit_employee_modal_t(){
        $this->load->model('member_model');
        $this->load->model('team_model');
        $nik = $this->input->post('nik');
        $id = $this->input->post('users_id');
        
        $avatar = $this->do_upload('employee-avatar');
        $member = $this->member_model
                ->get($nik);
        $team = $this->team_model
                ->get_all();
        
        $this->load->model('job_title_model');
        $job_title_list = $this->job_title_model->get_all();
        $job_title = $this->hierarchical_array($job_title_list);
        
        $this->load->model('job_role_model');
        $job_role_list = $this->job_role_model->get_all();
        $job_role = $this->hierarchical_array($job_role_list);
        
        $data = array(
            'member' => $member,
            'nik' => $nik,
            'team' => $team,
            'job_title' => $job_title,
            'job_role' => $job_role
        );
        echo $this->load->view('pages/home/widget_edit_employee_modal_t', $data, true);
    }
    
    public function edit_employee_t(){
            $avatar = $this->do_upload('employee-avatar-t');
            $name = $this->input->post('firstnamet').' '.$this->input->post('lastnamet');
            $update_member = array(
                'nik' => $this->input->post('nikt'),
                'team_id' => $this->input->post('teamt'),
                'name' => $name,
                'email' => $this->input->post('emailt'),
                'job_title' => $this->input->post('job_title'),
                'job_role' => $this->input->post('job_role')
            );
            if($avatar){
                $update_member['avatar'] = $avatar;
            }
            $niknik = $this->input->post('niknikt');
            $this->load->model('member_model');
            $this->member_model->update($update_member, $niknik);
    }
    
    public function edit_employee_modal(){
        $this->load->model('users_model');
        $this->load->model('member_model');
        $this->load->model('team_model');
        $nik = $this->input->post('nik');
        $id = $this->input->post('users_id');

        $user = false;
        if($id) {
            $user = $this->users_model
                ->get($id);
        }

        $member = $this->member_model
                ->get($nik);
        $team_list = $this->team_model->get_all();
        $team = $this->hierarchical_array($team_list);
        
        $this->load->model('job_title_model');
        $job_title_list = $this->job_title_model->get_all();
        $job_title = $this->hierarchical_array($job_title_list);
        
        $this->load->model('job_role_model');
        $job_role_list = $this->job_role_model->get_all();
        $job_role = $this->hierarchical_array($job_role_list);
        
        $data = array(
            'user' => $user,
            'member' => $member,
            'nik' => $nik,
            'id' => $id,
            'team' => $team,
            'job_title' => $job_title,
            'job_role' => $job_role
        );
        echo $this->load->view('pages/home/widget_edit_employee_modal', $data, true);
    }
    
    public function edit_employee(){
        if($this->input->post('_password') == $this->input->post('_confirmpassword')){
            $avatar = $this->do_upload('employee-avatar');
            $update_users = array(
                'username' => $this->input->post('_username'),
                'first_name' => $this->input->post('_firstname')
            );
            if($this->input->post('_password')){
                $update_users['password'] = $this->input->post('_password');
            }
            if($avatar){
                $update_users['avatar'] = $avatar;
            }
            $userid = $this->input->post('userid');
            $this->load->model('users_model');
            if($this->input->post('_email') == $this->input->post('_email_old')) {

            } else {
                $update_users['email'] = $this->input->post('_email');
                if(!$this->ion_auth->email_check($this->input->post('_email'))) {
                    $this->ion_auth->update($userid, $update_users);
                } else {
                    $this->json_result(array(
                        'status' => false,
                        'message' => 'Email already exists'
                    ));
                }
            }

            $name = $this->input->post('_firstname');
            $update_member = array(
                'nik' => $this->input->post('_nik'),
                'team_id' => $this->input->post('_team'),
                'name' => $name,
                'email' => $this->input->post('_email'),
                'job_title_id' => $this->input->post('_job_title'),
                'job_role_id' => $this->input->post('_job_role')
            );
            if($avatar){
                $update_member['avatar'] = $avatar;
            }
            $niknik = $this->input->post('niknik');
            $this->load->model('member_model');
            $this->member_model->update($update_member, $niknik);
        }
    }
    
    public function managerial() {
        if(!$this->ion_auth->in_group(array(1,3))) {
            redirect('home');
        }
        $data = array(
            'title' => 'Managerial',
            'content' => 'home/managerial',
            '_css' => array(
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/plugins/simplepaginationjs/css/simplePagination.css',
                'assets/uplon/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/uplon/plugins/select2/css/select2.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/plugins/simplepaginationjs/js/jquery.simplePagination.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/uplon/plugins/datatables/jquery.dataTables.min.js',
                'assets/uplon/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/pages/home/managerial.js?v='.time()
            ),
            'menu_list' => $this->get_menu()
        );
        $this->render_page($data);
    }
    
    public function managerial_view($type = null) {
        switch($type) {
            case 'plan':
                $content = 'pages/home/managerial_plan';
                break;
            case 'task':
                $content = 'pages/home/managerial_task';
                break;
            default:
                $content = 'pages/home/managerial_plan';
                break;
        }
        $this->load->model('team_model');
        $team_list = $this->team_model
                    ->order_by('name', 'ASC')
                    ->get_all();
        $team = $this->hierarchical_array($team_list);
        $this->load->model('task_type_model');
        $task_type = $this->task_type_model
                    ->order_by('name', 'asc')
                    ->get_all();
        
        $typetask = array();
        foreach($task_type as $v) {
            $typetask[$v['parent']][] = $v;
        }
        
        $this->load->model('plan_model');
        $plan_list = $this->plan_model
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->order_by('name', 'ASC')
                    ->get_all();

        $this->load->model('plan_type_model');
        $plan_type_list = $this->plan_type_model->get_all();
        $plan_type = $this->hierarchical_array($plan_type_list);
        
        $data = array(
            'type' => $typetask,
            'current_team' => $this->get_member_info('team_id'),
            'team' => $team,
            'plan' => $plan_list,
            'plan_type' => $plan_type,
            'team_id' => $this->get_member_info('team_id')
        );
        $result = array(
            'view' => $this->load->view($content, $data, true)
        );
        $this->json_result($result);
    }
    
    public function managerial_list($type) {
        $default_plan = $this->input->get('default_plan');

        $data = array(
            'type' => $type,
            'default_plan' => $default_plan,
            'team_id' => $this->get_member_info('team_id')
        );
        $result = array(
            'view' => $this->load->view('pages/home/widget_managerial_list',$data,true)
        );
        $this->json_result($result);
    }
    
    public function managerial_data($type) {
        switch($type) {
            case 'plan':
                $this->load->model('plan_type_model');
                if($this->ion_auth->in_group(1)) {
                    $team_id = null;
                } else {
                    $team_id = $this->get_member_info('team_id');
                }
                $this->load->model('plan_model');
                $this->load->model('plan_exposure_view_model');

                $list = $this->plan_model->get_datatables($team_id);
                $data = array();
                $no = $_POST['start'];
                foreach ($list as $v) {
                    $plan_type = $this->plan_type_model->get($v['plan_type_id']);
                    $statistic = $this->plan_exposure_view_model->get($v['id']);
                    $no++;
                    $row = array();
                    $row[] = $no;
                    $row[] = '<a href="'.site_url().'/home/manage?default_plan='.$v['id'].'" target="_blank">'.$v['name'].' #'.$v['sequence'].'</a>';
                    $row[] = $v['description'];
                    $row[] = '<center>'.$plan_type['name'].'</center>';
                    if($statistic) {
                        $plan_stat = '<center>';
                        if($statistic['task']) {
                            $plan_stat .= '<label class="label label-pill label-default tooltips" data-toggle="tooltip" data-placement="top" title="Total Task"><i class="zmdi zmdi-format-list-bulleted m-r-5"></i> '.$statistic['task'].'</label>';
                        }
                        if($statistic['contribution']) {
                            $plan_stat .= ' <label class="label label-pill label-default tooltips" data-toggle="tooltip" data-placement="top" title="Total Contributor"><i class="zmdi zmdi-account m-r-5"></i>'.$statistic['contribution'].'</label>';
                        }
                        $plan_stat .= '</center>';
                        $row[] = $plan_stat;
                    } else {
                        $row[] = '';
                    }
                    $row[] = '<center>
                            <button style="margin-bottom:5px;" class="btn btn-info btn-sm waves-effect waves-light share-plan tooltips" data-plan_id="'.$v['id'].'" data-team_id="'.$v['team_id'].'" data-toggle="tooltip" data-placement="top" title="Share Plan"><i class="zmdi zmdi-share"></i></button>
                            &nbsp;
                            <button style="margin-bottom:5px;" class="btn btn-warning btn-sm waves-effect waves-light btn-edit tooltips" data-id="'.$v['id'].'" data-type="plan" data-toggle="tooltip" data-placement="top" title="Edit Plan"><i class="zmdi zmdi-edit"></i></button>
                            &nbsp;
                            <button style="margin-bottom:5px;" class="btn btn-danger btn-sm waves-effect waves-light btn-delete tooltips" data-id="'.$v['id'].'" data-type="plan" data-toggle="tooltip" data-placement="top" title="Remove Plan"><i class="zmdi zmdi-delete"></i></button>'
                            . '</center>';

                    $data[] = $row;
                    
                }

                $result = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->plan_model->count_all($team_id),
                        "recordsFiltered" => $this->plan_model->count_filtered($team_id),
                        "data" => $data,
                );
                break;
            
            case 'task':
                $this->load->model('task_model');
                $this->load->model('task_type_model');
                $this->load->model('task_exposure_view_model');
                $default_plan = $this->input->post('default_plan');
                $list = $this->task_model->get_datatables($default_plan);
                $data = array();
                $no = $_POST['start'];
                foreach ($list as $v) {
//                    $task_type = $this->task_type_model->get($v['task_type_id']);
                    $statistic = $this->task_exposure_view_model->get($v['task_id']);
                    $no++;
                    $row = array();
                    $row[] = $no;
                    $row[] = '<a href="#" class="task-name" data-id="'.$v['task_id'].'" data-task_type_id="'.$v['task_type_id'].'" data-status="'.$v['status'].'" style="word-wrap: break-word; word-break: break-all; white-space: normal;">'.$v['task_name'].'</a>';
                    $row[] = $v['description'];
                    $row[] = $v['task_type_name'];
                    if($statistic) {
                        $plan_stat = '<center>';
                        if($statistic['activity']) {
                            $plan_stat .= '<label class="label label-pill label-default tooltips" data-toggle="tooltip" data-placement="top" title="Total Activity"><i class="zmdi zmdi-format-list-bulleted m-r-5"></i> '.$statistic['activity'].'</label>';
                        }
                        if($statistic['contribution']) {
                            $plan_stat .= ' <label class="label label-pill label-default tooltips" data-toggle="tooltip" data-placement="top" title="Total Contributor"><i class="zmdi zmdi-account m-r-5"></i> '.$statistic['contribution'].'</label>';
                        }
                        $plan_stat .= '</center>';
                        $row[] = $plan_stat;
                    } else {
                        $row[] = '';
                    }
                    $row[] = '<center><button class="btn btn-warning btn-sm waves-effect waves-light btn-edit tooltips" data-id="'.$v['task_id'].'" data-type="task" style="margin-bottom:5px;" data-toggle="tooltip" data-placement="top" title="Edit Task"><i class="zmdi zmdi-edit"></i></button>
                              <button class="btn btn-danger btn-sm waves-effect waves-light btn-delete tooltips" data-id="'.$v['task_id'].'" data-type="task" style="margin-bottom:5px;" data-toggle="tooltip" data-placement="top" title="Remove Task"><i class="zmdi zmdi-delete"></i></button></center>';
                    $data[] = $row;
                }
                $result = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->task_model->count_all($default_plan),
                        "recordsFiltered" => $this->task_model->count_filtered($default_plan),
                        "data" => $data,
                );
                break;
        }
        echo json_encode($result);
    }
    
    public function managerial_edit($type) {
        $id = $this->input->get('id');
        $this->load->model('team_model');
        
        $typetask = array();
        switch($type) {
            case 'plan':
                $this->load->model('plan_model');
                $detail = $this->plan_model
                        ->get($id);
                $content = 'pages/home/widget_edit_plan';
                break;
            case 'task':
                $this->load->model('task_model');
                $detail = $this->task_model
                        ->get($id);
                $content = 'pages/home/widget_edit_task';
                $this->load->model('task_type_model');
                $task_type = $this->task_type_model
                            ->order_by('name', 'asc')
                            ->get_all();

                foreach($task_type as $v) {
                    $typetask[$v['parent']][] = $v;
                }
                break;
            default:
                $content = 'pages/home/widget_edit_plan';
                break;
        }
        $team_list = $this->team_model
                    ->where('parent', 0)
                    ->order_by('name', 'ASC')
                    ->get_all();
        $team = array();
        foreach($team_list as $v) {
            $child = $this->team_model
                    ->where('parent', $v['id'])
                    ->order_by('name', 'ASC')
                    ->get_all();
            $team[] = array(
                        'parent' => $v,
                        'child' => $child
                    );
        }
        $this->load->model('plan_model');
        $plan_list = $this->plan_model
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->get_all();
        
        $this->load->model('plan_shared_model');
        $plan_shared = $this->plan_shared_model
                    ->with_plan()
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->get_all();
                
        $shared_plan = array();
        if($plan_shared) {
            foreach($plan_shared as $v) {
                if (array_key_exists('plan', $v)){
                    $shared_plan[] = $v['plan'];
                }
            }
        }
        $this->load->model('plan_type_model');
        $plan_type_list = $this->plan_type_model->get_all();
        $plan_type = $this->hierarchical_array($plan_type_list);
        
        $data = array(
            'plan' => array_merge($plan_list, $shared_plan),
            'type' => $typetask,
            'team' => $team,
            'plan_type' => $plan_type,
            'detail' => $detail
        );
        $result = array(
            'view' => $this->load->view($content, $data, true)
        );
        $this->json_result($result);
    }
    
    public function managerial_delete($type) {
        $id = $this->input->get('id');
        
        switch($type) {
            case 'plan':
                $this->load->model('plan_model');
                $plan_list = (array) $this->plan_model
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->where('id !=', $id)
                    ->get_all();
                
                $this->load->model('plan_shared_model');
                $plan_shared = $this->plan_shared_model
                            ->with_plan()
                            ->where('team_id', $this->get_member_info('team_id'))
                            ->get_all();
                
                $shared_plan = array();
                if($plan_shared) {
                    foreach($plan_shared as $v) {
                        if (array_key_exists('plan', $v)){
                        $shared_plan[] = $v['plan'];
                        }
                    }
                }
                
                $data = array(
                    'id' => $id,
                    'type' => 'plan',
                    'plan' => array_merge($plan_list, $shared_plan)
                );
                break;
            case 'task':
                $data = array(
                    'id' => $id,
                    'type' => 'task'
                );
                break;
            default:
                break;
        }
        
        $result = array(
            'view' => $this->load->view('pages/home/widget_managerial_delete_confirm', $data, true)
        );
        $this->json_result($result);
    }
    
    public function update_plan() {
        $this->load->model('plan_model');
        
        $sequence = $this->plan_model
                    ->fields('id')
                    ->where('id !=', $this->input->post('plan_id'))
                    ->where('name', $this->input->post('name'))
                    ->where('sequence', $this->input->post('sequence'))
                    ->get();
        
        if($sequence) {
            $result = array(
                'status' => false,
                'message' => 'Cannot set sequence to '.$this->input->post('sequence')
            );
        } else {
            $data = array(
                'name' => $this->input->post('name'),
                'sequence' => $this->input->post('sequence'),
                'description' => $this->input->post('description'),
                'plan_type_id' => $this->input->post('plantype')
            );

            $this->plan_model
                 ->update($data, $this->input->post('plan_id'));
            
            $result = array(
                'status' => true,
                'message' => 'Successfully edit plan.'
            );
        }
        
        $this->json_result($result);
    }
    
    public function update_task() {
        $this->load->model('task_model');
        
        $due_date = $this->input->post('due_date') ? date('Y-m-d', strtotime($this->input->post('due_date'))) : null;
        $data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'task_type_id' => $this->input->post('task_type'),
            'due_at' => $due_date,
            'plan_id' => $this->input->post('plan_id'),
        );
        
        $this->task_model
             ->update($data, $this->input->post('task_id'));
    }
    
    public function delete_plan() {
        $this->load->model('plan_model');
        
        $plan_id = $this->input->post('plan_id');
        $type = $this->input->post('task_action');
        
        switch($type) {
            case '0':
                $delete_plan = $this->plan_model
                               ->delete($plan_id);
                if($delete_plan) {
                    $result = array(
                        'status' => true,
                        'message' => 'Successfully to delete plan'
                    );
                } else {
                    $result = array(
                        'status' => false,
                        'message' => 'Failed to delete plan'
                    );
                }
                break;
            case '1':
                $this->load->model('task_model');
                $task_list = $this->task_model
                        ->fields('id')
                        ->where('plan_id', $plan_id)
                        ->get_all();
                $task_id = array();
                if($task_list) {
                    foreach($task_list as $v) {
                        $task_id[] = $v['id'];
                    }
                }
                if($task_id) {
                    $this->db->set('plan_id', $this->input->post('plan'), FALSE);
                    $this->db->where_in('id', $task_id);
                    $this->db->update('task');
                }
                
                $delete_plan = $this->plan_model
                               ->delete($plan_id);
                if($delete_plan) {
                    $result = array(
                        'status' => true,
                        'message' => 'Successfully to delete plan'
                    );
                } else {
                    $result = array(
                        'status' => false,
                        'message' => 'Failed to delete plan'
                    );
                }
                break;
            default:
                break;
        }
        
        $this->json_result($result);
    }
    
    public function delete_task() {
        $this->load->model('task_model');
        $id = $this->input->post('task_id');
        
        $delete_task = $this->task_model
                       ->delete($id);
        if($delete_task) {
            $result = array(
                        'status' => true,
                        'message' => 'Successfully to delete task'
                    );
        } else {
            $result = array(
                    'status' => false,
                    'message' => 'Failed to delete task'
                );
        }
        
        $this->json_result($result);
    }
    
    public function review() {
        if(!$this->ion_auth->in_group(array(3,1))) {
            redirect('');
        }
        $this->load->model('team_model');
        $this->load->model('member_model');
        $this->load->model('job_role_model');
        $team = $this->team_model
                ->get_all();
        $teams = $this->hierarchical_array($team);

        $team_list = array();
        $my_team_id = $this->get_member_info('team_id');
        if($teams) {
            if($this->ion_auth->in_group(1)) {
                $team_list = $teams;
            } else {
                foreach($teams as $v) {
                    if($v['id'] == $my_team_id) {
                        $team_list[] = $v;
                    }
                    if(array_key_exists('child', $v)) {
                        foreach($v['child'] as $vv) {
                            if($vv['id'] == $my_team_id) {
                                $team_list[] = $vv;
                            }
                            if(array_key_exists('child', $vv)) {
                                foreach($vv['child'] as $vvv) {
                                    if($vvv['id'] == $my_team_id) {
                                        $team_list[] = $vvv;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if($this->ion_auth->in_group(1)) {
            $this->load->model('plan_model');
            $plan = $this->plan_model->get_all();
        } else {
            $plan = $this->get_plan();
        }
        $plan_list = array();
        if($plan) {
            foreach($plan as $v) {
                $team_name = $this->team_model
                            ->fields('name')
                            ->get($v['team_id']);
                $v['team_name'] = $team_name['name'];
                $plan_list[$v['team_id']][] = $v;
            }
        }

        if(!$this->ion_auth->in_group(1)) {
            $this->db->where('team_id', $this->get_member_info('team_id'));
        }
        $employee = $this->member_model
                    ->fields('nik,name')
                    ->get_all();
        $job_role_list = $this->job_role_model
                    ->get_all();
        $job_role = $this->hierarchical_array($job_role_list);
        
        $data = array(
            'title' => 'Review',
            'content' => 'home/review',
            '_css' => array(
                'assets/plugins/nprogress/css/nprogress.css',
                'assets/uplon/plugins/toastr/toastr.min.css',
                'assets/uplon/plugins/fullcalendar/dist/fullcalendar.css',
                'assets/uplon/plugins/select2/css/select2.min.css',
                'assets/uplon/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css'
            ),
            '_js' => array(
                'assets/plugins/ajaxform/ajaxform.min.js',
                'assets/plugins/momentjs/momentjs.min.js',
                'assets/plugins/nprogress/js/nprogress.js',
                'assets/uplon/plugins/toastr/toastr.min.js',
                'assets/uplon/plugins/fullcalendar/dist/fullcalendar.min.js',
                'assets/uplon/plugins/select2/js/select2.full.min.js',
                'assets/uplon/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/global.js',
                'assets/pages/home/review.js?v='.time()
            ),
            'menu_list' => $this->get_menu(),
            'team_list' => $team_list,
            'plan_list' => $plan_list,
            'employee' => $employee,
            'job_role' => $job_role
        );
        $this->render_page($data);
    }
    
    public function review_data() {
        $type = $this->input->get('type');
        $team_params = $this->input->get('team');
        $plan = $this->input->get('plan');
        $employee = $this->input->get('employee');
        $jobrole = $this->input->get('jobrole');
        $filter_by_team = $this->input->get('filter_by_team');
        $filter_by_plan = $this->input->get('filter_by_plan');
        $filter_by_employee = $this->input->get('filter_by_employee');
        $filter_by_jobrole = $this->input->get('filter_by_jobrole');
        $start = $this->input->get('start');
        $end = $this->input->get('end');
        
        $this->load->model('member_model');
        $this->load->model('activity_model');
        
        $team_id = array();
        $my_team_id = $this->get_member_info('team_id');
        switch($type) {
                case 'month':
                    if($team_params) {
                        $this->db->where('team_id', $filter_by_team);
                        if($filter_by_employee) {
                            $this->db->where('nik', $filter_by_employee);
                        }
                    } else if(!$this->ion_auth->in_group(1)){
                        $this->db->where('team_id', $my_team_id);
                    }
                    if($jobrole) {
                        $this->db->where('job_role_id', $filter_by_jobrole);
                    }
                    $member = $this->member_model
                            ->fields('nik')
                            ->get_all();
                        $member_nik = array();
                        if($member) {
                            foreach($member as $v) {
                                $member_nik[] = $v['nik'];
                            }
                        } else {
                            $member_nik = '0';
                        }
                    if($start || $end) {
                        $this->db->where('created_at BETWEEN "'.$start.'" AND "'.$end.'"');
                    }
                    $activity = $this->activity_model
                                ->where('nik', $member_nik)
                                ->fields('task_id,created_at')
                                ->order_by('created_at', 'ASC')
                                ->get_all();

                    $this->load->model('task_model');
                    $task_list = array();
                    if($activity) {
                        foreach($activity as $v) {
                            if($v['task_id']) {
                                if($plan) {
                                    $this->db->where('plan_id', $filter_by_plan);
                                }
                                $task = $this->task_model
                                        ->get($v['task_id']);
                                $task_list[date('Y-m-d', strtotime($v['created_at']))][$v['task_id']] = $task;
                            }
                        }
                    }
                    $event_list = array();
                    if($task_list) {
                        foreach($task_list as $k => $v) {
                            foreach($v as $kk => $vv) {
                                if($vv['id']) {
                                    $event_list[] = array(
                                        'task_id' => $vv['id'],
                                        'title' => $vv['name'],
                                        'start' => $k,
                                        'end' => $k,
                                        'color' => '#64B0F2',
                                        'url' => '#',
                                        'params_start' => $start,
                                        'params_end' => $end,
                                        'filter_by' => array(
                                            'team' => $team_params,
                                            'filter_by_team' => $filter_by_team,
                                            'plan' => $plan,
                                            'employee' => $employee,
                                            'jobrole' => $jobrole,
                                            'filter_by_plan' => $filter_by_plan,
                                            'filter_by_employee' => $filter_by_employee,
                                            'filter_by_jobrole' => $filter_by_jobrole
                                        ),
                                        'status' => true
                                    );
                                }
                            }
                        }
                    }

                    $result = $seq = $task_id = $last_date = array();
                    $interval = 0;
                    if($event_list) {
                        foreach ($event_list as $key => $value) {
                            if(array_key_exists($value['task_id'], $last_date)) {
                                $interval = floor((strtotime($value['start']) - strtotime($last_date[$value['task_id']])) / (60 * 60 * 24) );
                            }
                            if (in_array($value['task_id'], $task_id)) {
                                if ($interval == 1) {
                                    $interval_start_end = floor((strtotime($value['start']) - strtotime($value['end'])) / (60 * 60 * 24) );
    //                                if($interval_start_end <= 1) {
                                        $result[$value['task_id'].'-'.$seq[$value['task_id']]]['end'] = date('Y-m-d', strtotime($value['start'] . '+1 day'));
    //                                } else {
    //                                    $result[$value['task_id'].'-'.$seq[$value['task_id']]]['end'] = $value['start'];
    //                                }
                                } else {
                                    $seq[$value['task_id']] += 1;

                                    $result[$value['task_id'].'-'.$seq[$value['task_id']]] = $value;
                                } 
                            } else {
                                if (!array_key_exists($value['task_id'], $seq)) {
                                    $seq[$value['task_id']] = 1;
                                }
                                $result[$value['task_id'].'-'.$seq[$value['task_id']]] = $value;
                                $task_id[$value['task_id']] = $value['task_id'];
                            }
                            $last_date[$value['task_id']] = $value['start'];
                        }
                    } else {
                        $result = array();
                    }
                    break;

                case 'agenda':
                    if($team_params) {
                        $this->db->where('team_id', $filter_by_team);
                        if($filter_by_employee) {
                            $this->db->where('nik', $filter_by_employee);
                        }
                    } else if(!$this->ion_auth->in_group(1)){
                        $this->db->where('team_id', $my_team_id);
                    }
                    if($jobrole) {
                        $this->db->where('job_role_id', $filter_by_jobrole);
                    }
                    $member = $this->member_model
                            ->fields('nik')
                            ->get_all();
                        $member_nik = array();
                        if($member) {
                            foreach($member as $v) {
                                $member_nik[] = $v['nik'];
                            }
                        } else {
                            $member_nik = '0';
                        }

                    $this->db->where('created_at BETWEEN "'.$start.'" AND "'.$end.'"');
                    $activity = $this->activity_model
                                ->where('nik', $member_nik)
                                ->fields('task_id,created_at,description,nik')
                                ->with_member('fields:name')
                                ->order_by('created_at', 'ASC')
                                ->get_all();

                    $this->load->model('task_model');
                    $task_list = array();
                    if($activity) {
                        foreach($activity as $v) {
                            if($v['task_id']) {
                                if($plan) {
                                    $this->db->where('plan_id', $filter_by_plan);
                                }
                                $task = $this->task_model
                                        ->get($v['task_id']);
                                $activity_date[$v['task_id']] = $v['created_at'];
                                $task['activity'] = $v['description'];
                                $task['activity_by'] = $v['member']['name'];
                                $task['activity_date'] = $v['created_at'];
                                $task_list[] = $task;
                            }
                        }
                    }
                    $event_list = array();
                    if($task_list) {
                        foreach($task_list as $k => $v) {
                            if(array_key_exists('id', $v)) {
                                $event_list[] = array(
                                    'task_id' => $v['id'],
                                    'title' => $v['activity'].' by '.$v['activity_by'].' in '.$v['name'],
                                    'start' => $v['activity_date'],
                                    'end' => $v['activity_date'],
                                    'color' => '#64B0F2',
                                    'url' => '#',
                                    'allDay' => false,
                                    'params_start' => $start,
                                    'params_end' => $end,
                                    'filter_by' => array(
                                        'plan' => $plan,
                                        'employee' => $employee,
                                        'jobrole' => $jobrole,
                                        'filter_by_plan' => $filter_by_plan,
                                        'filter_by_employee' => $filter_by_employee,
                                        'filter_by_jobrole' => $filter_by_jobrole
                                    )
                                );
                            }
                        }
                    }
                    $result = $event_list;
                    break;
        }
        $this->json_result(array_values($result));
    }
    
    public function review_activity_modal() {
        $task_id = $this->input->get('task_id');
        
        $this->load->model('task_model');
        $status = $this->task_model
                ->where('id', $task_id)
                ->fields('status')
                ->with_status()
                ->get();

        $this->load->model('member_model');
        $employee = $this->member_model
                    ->where('team_id', $this->get_member_info('team_id'))
                    ->get_all();
        
        $data = array(
            'employee' => $employee,
            'task_id' => $task_id,
            'status' => $status['status']
        );
        
	echo $this->load->view('pages/home/widget_review_activity_modal',$data,true);
    }
    
    public function review_activity() {
        $this->load->model('activity_model');
        $this->load->model('task_model');
        
        $task_id = $this->input->post('task_id');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $employee = (array) json_decode($this->input->post('employee'));
        
        if($employee) {
            $this->db->where_not_in('nik', array_values($employee));
        }
        $activity = $this->activity_model
                    ->where('DATE(created_at) BETWEEN "'.date('Y-m-d', strtotime($start)).'" AND "'.date('Y-m-d', strtotime($end)).'"',NULL,NULL,FALSE,FALSE,TRUE)
                    ->where('task_id', $task_id)
                    ->with_member()
                    ->with_activity_file('fields:name,description,path,fullpath,extension,size')
                    ->order_by('created_at', 'DESC')
                    ->get_all();
        
        $task_detail = $this->task_model
                    ->with_team()
                    ->with_task_type()
                    ->with_plan()
                    ->with_exposure()
                    ->with_contribution(array(
                        'with' => array(
                            'relation' => 'member',
                            'fields' => 'name,avatar'
                        )
                    ))
                    ->get($task_id);
        
        $data = array(
            'activity' => $activity,
            'task_detail' => $task_detail,
            'task_id' => $task_id,
            'start' => $start,
            'end' => $end
        );
        
        $result = array(
            'view_activity' => $this->load->view('pages/home/widget_review_activity', $data, TRUE)
        );
        
        $this->json_result($result);
    }
    
    public function filter_task_get_member() {
        $team_id = $this->input->post('team_id');
        
        $this->load->model('member_model');
        $member = $this->member_model
                ->where('team_id', $team_id)
                ->get_all();
        
        $this->json_result($member);
    }

    public function report_modal() {
        $this->load->model('team_model');
        $team = $this->team_model
            ->get_all();
        $teams = $this->hierarchical_array($team);

        $team_list = array();
        $team_id = array();
        $my_team_id = $this->get_member_info('team_id');
        if($teams) {
            if($this->ion_auth->in_group(1)) {
                $team_list = $teams;
            } else {
                foreach($teams as $v) {
                    if($v['id'] == $my_team_id) {
                        $team_list[] = $v;
                        $team_id[] = $v['id'];
                    }
                    if(array_key_exists('child', $v)) {
                        foreach($v['child'] as $vv) {
                            $have_sub_child = false;
                            if($vv['id'] == $my_team_id || $vv['parent'] == $my_team_id) {
                                $team_list[] = $vv;
                                $team_id[] = $vv['id'];
                                $have_sub_child = true;
                            }
                            if(array_key_exists('child', $vv)) {
                                foreach($vv['child'] as $vvv) {
                                    if($vvv['id'] == $my_team_id || ($have_sub_child && $vvv['parent'] == $vv['id']) || $vvv['parent'] == $my_team_id) {
                                        $team_list[] = $vvv;
                                        $team_id[] = $vvv['id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->load->model('member_model');
        $member = $this->member_model
                ->where('team_id', $team_id)
                ->get_all();

        $this->load->model('task_type_model');
        $task_type = $this->task_type_model
                    ->where('parent', 0)
                    ->get_all();

        $data = array(
            'team' => $team_list,
            'member' => $member,
            'team_id' => $team_id,
            'task_type' => $task_type
        );

        $result = array(
            'view' => $this->load->view('pages/home/widget_report', $data, true)
        );
        $this->json_result($result);
    }

    public function report_action($type) {
        $filter = $this->input->post('filter');
        $range = $this->input->post('range');
        $task_type = $this->input->post('task_type');

        $this->load->model('member_model');
        if($filter == 'all') {
            $this->db->where_in('team_id', explode(';', $this->input->post('team_id_all')));
        } else if($filter == 'team') {
            $this->db->where_in('team_id', $this->input->post('filter_by_team'));
        } else if($filter == 'employee') {
            $this->db->where_in('nik', $this->input->post('filter_by_employee'));
        }
        $member = $this->member_model
            ->fields('nik,name,team_id')
            ->get_all();

        $nik = array();
        if($member) {
            foreach($member as $v) {
                $nik[] = $v['nik'];
            }
        }

        $this->load->model('activity_model');
        $task_type_id = array();
        if($task_type) {
            $this->load->model('task_type_model');
            $task_type_list = $this->task_type_model
                ->where('id', $this->input->post('task_type'))
                ->where('parent', $this->input->post('task_type'), null, true)
                ->fields('id, parent')
                ->get_all();
            if($task_type_list) {
                foreach($task_type_list as $v) {
                    $task_type_id[] = $v['id'];
                }
            }
        }

        $activity_list = $this->activity_model->report_activity($nik, $range, $task_type_id);
        $activity = array();
        foreach($nik as $v) {
            $member = $this->member_model->with('team')->get($v);
            $activity[$v]['info']['name'] = $member['name'];
            $activity[$v]['info']['team'] = $member['team']['name'];
            if($range == 'monthly') {
                $range_monthly = explode(' ', $this->input->post('range_monthly'));
                $year = $range_monthly[1];
                $month = date('m', strtotime($range_monthly[0]));
                $start_time = strtotime("01-".$month."-".$year);
                $end_time = strtotime("+1 month", $start_time);

                for($i=$start_time; $i<$end_time; $i+=86400) {
                    $activity[$v]['data'][date('Y-m-d', $i)] = array();
                }
            } else if($range == 'weekly') {
                $range_weekly = $this->input->post('range_weekly');
                $week_number = date('W', strtotime($range_weekly));
                $year = date('Y', strtotime($range_weekly));

                for($day=1; $day<=7; $day++) {
                    $activity[$v]['data'][date('Y-m-d', strtotime($year."W".$week_number.$day))] = null;
                }
            }
        }
        $sort_by_name = array();
        foreach ($activity as $k => $v) {
            $sort_by_name[$k]  = $v['info']['name'];
        }
        asort($sort_by_name);

        if($activity_list) {
            foreach($activity_list as $v) {
                $activity[$v['member_nik']]['info']['name'] = $v['member_name'];
                $activity[$v['member_nik']]['info']['team'] = $v['team_name'];
                $activity[$v['member_nik']]['data'][date('Y-m-d', strtotime($v['activity_created_at']))][$v['plan_id']]['plan_name'] = $v['plan_name'];
                unset($v['plan_name']);
                $activity[$v['member_nik']]['data'][date('Y-m-d', strtotime($v['activity_created_at']))][$v['plan_id']]['plan_sequence'] = $v['plan_sequence'];
                unset($v['plan_sequence']);
                $activity[$v['member_nik']]['data'][date('Y-m-d', strtotime($v['activity_created_at']))][$v['plan_id']][$v['task_id']]['task_name'] = $v['task_name'];
                unset($v['task_name']);
                $activity[$v['member_nik']]['data'][date('Y-m-d', strtotime($v['activity_created_at']))][$v['plan_id']][$v['task_id']][] = $v;
            }
        }

        if($range == 'monthly') {
            $file_name = 'Task Report ('.$this->input->post('range_monthly').')';
        } else if($range == 'weekly') {
            $range_weekly = $this->input->post('range_weekly');
            $monday = date('d M Y', strtotime('monday this week', strtotime($range_weekly)));
            $friday = date('d M Y', strtotime('sunday this week', strtotime($range_weekly)));
            $file_name = 'Task Report ('.$monday.' - '.$friday.')';
        }

        foreach($activity as $k => $v) {
            $sort_by_name[$k] = $v;
        }

        $data = array(
            'activity' => $sort_by_name,
            'file_name' => $file_name
        );

        switch($type) {
            case 'preview':
                $result = array(
                    'status' => true,
                    'message' => 'Preview loaded',
                    'view' => $this->load->view('pages/home/widget_report_preview', $data, true),
                    'nik' => $nik,
                    'data' => $data
                );
                $this->json_result($result);
                break;
            case 'download':
                if($filter == 'employee' && !$this->input->post('filter_by_employee')) {
                    echo 'Filter by employee must be select minimum 1 employee <a href="#" onclick="window.history.back();">Back</a>';
                    exit;
                }
                if($filter == 'team' && !$this->input->post('filter_by_team')) {
                    echo 'Filter by team must be select minimum 1 team';
                    exit;
                }
                $this->load->library("Excel/PHPExcel");

                $objPHPExcel = new PHPExcel();

//                $sheet = $objPHPExcel->getActiveSheet(0);
                $i = 0;

                if($sort_by_name) {
                    foreach($sort_by_name as $k => $v) {
                        $objWorkSheet = $objPHPExcel->createSheet($i);

                        $objWorkSheet->setCellValue('A1', 'Activity Report')
                            ->setCellValue('A2', 'Name')
                            ->setCellValue('B2', $v['info']['name'])
                            ->setCellValue('A3', 'Division')
                            ->setCellValue('B3', $v['info']['team'])
                            ->setCellValue('A5', 'Date / Time')
                            ->setCellValue('B5', 'Plan')
                            ->setCellValue('C5', 'Task Name')
                            ->setCellValue('D5', 'Activity & Result');

                        if(array_key_exists('data', $v)) {
                            $row = 6;
                            foreach($v['data'] as $kk => $vv) {
                                $objWorkSheet->setCellValue('A' . $row, date('d M Y', strtotime($kk)));
                                if(strtolower(date('D', strtotime($kk))) == 'sun' || strtolower(date('D', strtotime($kk))) == 'sat') {
                                    $objWorkSheet->getStyle('A'.$row)->applyFromArray(array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'color' => array('rgb' => 'DADFE1')
                                        )
                                        ));
                                }
                                $row++;
                                if($vv) {
                                    $plan = 0;
                                    $task = 0;
                                    foreach($vv as $kkk => $vvv) {
                                        if($vvv) {
                                            foreach ($vvv as $kkkk => $vvvv) {
                                                if (is_array($vvvv)) {
                                                    foreach ($vvvv as $kkkkk => $vvvvv) {
                                                        if (is_array($vvvvv)) {
                                                            $objWorkSheet->setCellValue('A' . $row, '')
                                                                ->setCellValue('B' . $row, $plan < 1 ? $vvv['plan_name'] . ' #' . $vvv['plan_sequence'] : '')
                                                                ->setCellValue('C' . $row, $task < 1 ? $vvvv['task_name'] : '')
                                                                ->setCellValue('D' . $row, $vvvvv['activity_description']);
                                                            $plan++;
                                                            $task++;
                                                            $row++;
                                                        }
                                                    }
                                                }
                                                $task = 0;
                                            }
                                        }
                                        $plan = 0;
                                    }
                                }
                            }
                        }

                        $objWorkSheet->getStyle('A1')->applyFromArray(array(
                            'font'  => array(
                                'bold'  => true,
                                'size'  => 12
                            )));
                        $objWorkSheet->mergeCells('A1:D1');

                        $objWorkSheet->getStyle('A2:A3')->applyFromArray(array(
                            'font'  => array(
                                'bold'  => true
                            )));
                        $objWorkSheet->mergeCells('B2:D2');
                        $objWorkSheet->mergeCells('B3:D3');

                        $objWorkSheet->getStyle('A5:D5')->applyFromArray(array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            ),
                            'font' => array(
                                'bold' => true
                            ),
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '8DB4E3')
                            )
                        ));

                        $objWorkSheet->getStyle('A6:A'.($row-1))->applyFromArray(array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            )
                        ));

                        $column_data = $objWorkSheet->getHighestDataColumn();
                        $objWorkSheet->getStyle('A5:'.$column_data.($row-1))->applyFromArray(array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        ));

                        $objWorkSheet->getColumnDimension('A')->setWidth(15);
                        $objWorkSheet->getColumnDimension('B')->setWidth(25);
                        $objWorkSheet->getColumnDimension('C')->setWidth(50);
                        $objWorkSheet->getColumnDimension('D')->setWidth(100);

                        $sheet_name = $v['info']['name'];
                        $objWorkSheet->setTitle("$sheet_name");

                        $i++;
                    }
                }

                $objPHPExcel->removeSheetByIndex(count($activity));

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$file_name.'.xlsx"');
                $objWriter->save("php://output");
                break;
        }
    }
}
